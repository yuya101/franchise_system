<?php
ob_start();
session_start();
session_cache_expire(20);  // TimeOut in scale of minutes.
$cache_expire = session_cache_expire();

header("Content-Type: text/plain; charset=UTF-8");

$return_arr = array();

//***** For Add Top Up  ****//
// for($i=0; $i<=5; $i++){
	$row_array['docTypeID'] = "1";
	$row_array['docCategoryID'] = "1";
	$row_array['docTitle'] = "Test Upload From Web Service";
	$row_array['docMonth'] = "08/2017";
	$row_array['docDescription'] = "Test Upload From Web Service With Desc";
	$row_array['docFileName'] = "d021201708.xlsx";
	$row_array['requestTransaction'] = "t52014587";

	array_push($return_arr,$row_array);
// }

//***** For Add User  ****//
// $row_array['serverid'] = "S0001";
// $row_array['keypass'] = "1111";
// $row_array['method'] = "add";
// $row_array['action'] = "user";
// $row_array['request_trans_id'] = "ts0000001";
// $row_array['user_detail']['username'] = "ทดสอบ 10";
// $row_array['user_detail']['email'] = "TESTBALL10@GMAIL.COM";
// $row_array['user_detail']['mobile'] = "0844111113";
// $row_array['user_detail']['name'] = "บอล 10";
// $row_array['user_detail']['lastname'] = "ทดสอบ 10";
// $row_array['user_detail']['room'] = "ทดสอบ";
// $row_array['user_detail']['building'] = "ทดสอบ";
// $row_array['user_detail']['village'] = "ทดสอบ";
// $row_array['user_detail']['address'] = "ทดสอบ";
// $row_array['user_detail']['soi'] = "ทดสอบ";
// $row_array['user_detail']['mou'] = "ทดสอบ";
// $row_array['user_detail']['street'] = "ทดสอบ";
// $row_array['user_detail']['district'] = "ทดสอบ";
// $row_array['user_detail']['amphor'] = "ทดสอบ";
// $row_array['user_detail']['province'] = "ทดสอบ";
// $row_array['user_detail']['postcode'] = "10210";
// $row_array['user_detail']['telephone'] = "ทดสอบ";


//***** For Withdraw Money  ****//
// $row_array['serverid'] = "s0001";
// $row_array['keypass'] = "1111";
// $row_array['method'] = "withdraw";
// $row_array['action'] = "top_up";
// $row_array['request_trans_id'] = "ts0000007";
// $row_array['user_detail']['username'] = "ทดสอบ 10";
// $row_array['user_detail']['email'] = "TESTBALL10@GMAIL.COM";
// $row_array['user_detail']['mobile'] = "0844111113";
// $row_array['user_detail']['value'] = "100";
// $row_array['user_detail']['closejob'] = "1";
// $row_array['user_detail']['money_portal'] = "3";


//***** For Check Balance  ****//
// $row_array['serverid'] = "s0001";
// $row_array['keypass'] = "1111";
// $row_array['method'] = "check_balance";
// $row_array['action'] = "top_up";
// $row_array['request_trans_id'] = "ts0000016";
// $row_array['user_detail']['username'] = "ทดสอบ 10";
// $row_array['user_detail']['email'] = "TESTBALL10@GMAIL.COM";
// $row_array['user_detail']['mobile'] = "0844111113";
// $row_array['user_detail']['final_value'] = "15000";
// $row_array['user_detail']['closejob'] = "1";



//***** For Add User And Money First Time  ****//
// $row_array['serverid'] = "S0001";
// $row_array['keypass'] = "1111";
// $row_array['method'] = "firsttime_add";
// $row_array['action'] = "user";
// $row_array['request_trans_id'] = "ts0000012";
// $row_array['user_detail']['username'] = "ทดสอบ 120";
// $row_array['user_detail']['email'] = "TESTBALL120@GMAIL.COM";
// $row_array['user_detail']['mobile'] = "0844111114";
// $row_array['user_detail']['name'] = "บอล 10";
// $row_array['user_detail']['lastname'] = "ทดสอบ 10";
// $row_array['user_detail']['room'] = "ทดสอบ";
// $row_array['user_detail']['building'] = "ทดสอบ";
// $row_array['user_detail']['village'] = "ทดสอบ";
// $row_array['user_detail']['address'] = "ทดสอบ";
// $row_array['user_detail']['soi'] = "ทดสอบ";
// $row_array['user_detail']['mou'] = "ทดสอบ";
// $row_array['user_detail']['street'] = "ทดสอบ";
// $row_array['user_detail']['district'] = "ทดสอบ";
// $row_array['user_detail']['amphor'] = "ทดสอบ";
// $row_array['user_detail']['province'] = "ทดสอบ";
// $row_array['user_detail']['postcode'] = "10210";
// $row_array['user_detail']['telephone'] = "ทดสอบ";
// $row_array['user_detail']['firsttime_money'] = "2500";

// array_push($return_arr,$row_array);

$ret = array("detail" => $return_arr);

$jsonText = urlencode(json_encode($ret, JSON_UNESCAPED_UNICODE));

$opts = array (
        'http' => array (
            'header'=> "Content-Type: text/plain; charset=UTF-8"
            )
        );

$context = stream_context_create($opts);

// $urlRequest = "http://128.199.127.193/includes/webservice/topUpWebService.php?json=".$jsonText;
// $urlRequest = "http://128.199.127.193/includes/webservice/withdrawWebService.php?json=".$jsonText;
// $urlRequest = "http://128.199.127.193/includes/webservice/checkBalanceWebService.php?json=".$jsonText;
// $urlRequest = "http://128.199.127.193/includes/webservice/addUserFirstTimeWebService.php?json=".$jsonText;
$urlRequest = "http://localhost:5050/franchise_system/includes/webservice/masterFileWebService.php?json=".$jsonText;
$responeJSON = file_get_contents($urlRequest, false, $context);

print_r($responeJSON);


$responeJSON = str_replace("[", "", $responeJSON);
$responeJSON = str_replace("]", "", $responeJSON);
$responeJSON = json_decode(stripcslashes($responeJSON), TRUE);



// echo $responeJSON['status']." - ";
// echo $responeJSON['requestTransaction']." - ";
// echo $responeJSON['fileExist']." - ";
?>