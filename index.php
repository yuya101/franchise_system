<?php
ob_start();
session_start();
session_cache_expire(20);  // TimeOut in scale of minutes.
// error_reporting(E_ALL);
// ini_set( 'display_errors','1');
$cache_expire = session_cache_expire();


if((!isset($_SESSION['mLoginID'])))
{
    	header("location:login.php");
}
else
{
    	include("includes/class/autoload.php");

    	$dFunc = new DateFunction();
	$dateNow = $dFunc->getDateChris();
	unset($dFunc);

	if($_SESSION['modDatePlus60'] < $dateNow){
		// $_REQUEST["f"] = "chgPass";
		// $_SESSION['forceChgPass'] = 1;
		// session_write_close();
	}  //-------  if($modDatePlus60 > $dateNow)

	include("includes/control/pageSetup_Ctl.php");
	include("includes/view/header.php");
	include("includes/view/center.php");
	include("includes/view/footer.php");
}  //-----  if((!isset($_SESSION['mLoginID'])))
?>