<?php
ob_start();
session_start();
session_cache_expire(20);  // TimeOut in scale of minutes.
$cache_expire = session_cache_expire();

header("Content-Type: text/plain; charset=UTF-8");

$return_arr = array();

$row_array['docTypeID'] = "1";
$row_array['docCategoryID'] = "1";
$row_array['docTitle'] = "Test Upload From Web Service";
$row_array['docMonth'] = "08/2017";
$row_array['docDescription'] = "Test Upload From Web Service With Desc";
$row_array['docFileName'] = "d021201708.xlsx";
$row_array['requestTransaction'] = "t52014587";

array_push($return_arr,$row_array);

$ret = array("detail" => $return_arr);

$jsonText = urlencode(json_encode($ret, JSON_UNESCAPED_UNICODE));

$opts = array (
        'http' => array (
            'header'=> "Content-Type: text/plain; charset=UTF-8"
            )
        );

$context = stream_context_create($opts);
$urlRequest = "http://localhost:5050/franchise_system/includes/webservice/masterFileWebService.php?json=".$jsonText;
$responeJSON = file_get_contents($urlRequest, false, $context);

print_r($responeJSON);

// $responeJSON = str_replace("[", "", $responeJSON);
// $responeJSON = str_replace("]", "", $responeJSON);
// $responeJSON = json_decode(stripcslashes($responeJSON), TRUE);
?>