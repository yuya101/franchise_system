<?php
require_once("dimText.php");
require_once("config.php");

class Connect_DB
{
	private $conn;
	
	public function __construct()
	{		
		$this->conn=mysqli_init(); 
		mysqli_options ($this->conn, MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, false);
		mysqli_ssl_set($this->conn, NULL, NULL, ($_SERVER['DOCUMENT_ROOT']."/MyServerCACert.pem"), NULL, NULL);
		$link = mysqli_real_connect($this->conn, DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, 3306, NULL, MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT);
		mysqli_set_charset( $this->conn, 'utf8');

		if (!$link)
		{
		    die ('Connect1 error (' . mysqli_connect_errno() . '): ' . mysqli_connect_error() . "\n");
		}

		// echo mysql_errno($this->conn) . ": " . mysql_error($this->conn). "\n";
	}
	
	public function getConn()
	{
		return $this->conn;
	}
	
	public function freeConn()
	{
		mysqli_close($this->conn);
	}
}
?>