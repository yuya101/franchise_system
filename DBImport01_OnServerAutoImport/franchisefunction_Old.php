<?php
require_once("dimText.php");
require_once("mainquery.php");
require_once("mainfunction.php");
require_once("datefunction.php");
require_once("calValue.php");

class FranchiseFunction
{
	private $fileName;
	private $dFunc, $dateNow, $timeStamp;
	private $strFileName, $extension;
	
	
	public function __construct()
	{
		// Construction Method
	}


	public function unzipDocumentFile($startPath, $destinationPath){
		// $mQuery = new MainQuery();
		// $mFunc = new MainFunction();
		// $dFunc = new DateFunction();
		
		// $dateNow = $dFunc->getDateChris();
		// $timeNow = $dFunc->getTimeNow();

		$fileCount = 0;

		$files = array();

		$dir = opendir($startPath);

		while (($file = readdir($dir)) !== false){
			if(substr($file, -4)==".zip"){
				$files[] = $file;
			}  //-----  if(substr($file, -4)==".zip")
		}  //----  while (($file = readdir($dir)) !== false)

		closedir($dir);


		foreach ($files as $file){
			$tempFile = $file; 
			// $fileDetail = pathinfo($file);
			// $extension = ".".$fileDetail['extension'];
			// $fileName = $fileDetail['filename'];

			$zip = new ZipArchive;
			if ($zip->open($startPath.$tempFile) === TRUE) {
			  $zip->extractTo($destinationPath);
			  $zip->close();
			}   //----  if ($zip->open($destinationPath.$tempFile) === TRUE)

			unset($zip);

			unlink($startPath.$tempFile);

			$fileCount++;
		}  //-------  foreach ($files as $file)


		// unset($mQuery, $dFunc, $mFunc, $files, $file);

		return $fileCount;
	}  //------  public function unzipDocumentFile($startPath, $destinationPath)

	
	public function insertDocFileToDB($startPath, $destinationPath, $responseEmail)
	{
		$mQuery = new MainQuery();
		$mFunc = new MainFunction();
		$dFunc = new DateFunction();
		
		$dateNow = $dFunc->getDateChris();
		$timeNow = $dFunc->getTimeNow();

		$dateNowShow = $dFunc->changeDateToDDMMYYYY3($dateNow);

		$fileUnzipCount = $this->unzipDocumentFile($startPath, $startPath);  //---- ต้องการให้ลงที่เดิม

		$files = array();
		$docMoveComplete = 0;
		$sender_email = "admin@mfgfranchise.com";

		$dir = opendir($startPath);

		while (($file = readdir($dir)) !== false){

		    if((substr($file, -4)==".pdf") or (substr($file, -4)==".PDF")){
		        $files[] = $file;
		    }  //-------  if(substr($file, -4)==".pdf")

		}  //-------  while (($file = readdir($dir)) !== false)
		
		closedir($dir);

		foreach ($files as $file){
			$fileDetail = pathinfo($file);
			$extension = ".".$fileDetail['extension'];
			$fileName = $fileDetail['filename'];


			if(strlen($fileName) == 12){
				$docBranchName = substr(strtoupper($fileName), 0, 4);
				$documentYear = substr($fileName, 4, 4);
				$documentMonth = substr($fileName, -4, 2);
				$documentSpecialType = (int)substr($fileName, -2, 2);

				$documentDate = "01";

				$sql = "select special_type_name from db_special_fianace_type where fid=".$documentSpecialType;
				$specialTypeName =$mQuery->getResultOneRecord($sql, "special_type_name");

				$docTypeID = 1;
				$docCategoryID = 1;
				$docTitle = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear;
				$docMonth = $documentMonth."/".$documentYear;
				$docDescription = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear." - เอกสารประเภท ".$specialTypeName;
				$docFileName = $fileName.$extension;
			    	$targetFile =  $docFileName;

			    	$newFolderPath = $destinationPath.$docBranchName."/".$documentYear."/".$documentMonth."/";


			    	if (!file_exists($newFolderPath)) {
					mkdir($newFolderPath, 0777, true);
				}  //------  if (!file_exists($newFolderPath)) 

			    	if (!file_exists($newFolderPath.$docFileName)) {
			    		$startBrandName = strtoupper(substr($fileName, 0, 1));

					$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
					}else{
						$brandid = 0;
					}  //-----  if($num > 0)

					$sql = "insert into db_document values(NULL";
					$sql = $sql.", ".$docTypeID."";
					$sql = $sql.", ".$docCategoryID."";
					$sql = $sql.", ".$brandid."";
					$sql = $sql.", '".$docBranchName."'";
					$sql = $sql.", '-'";
					$sql = $sql.", '".$docTitle."'";
					$sql = $sql.", '".$docDescription."'";
					$sql = $sql.", '".$docFileName."'";
					$sql = $sql.", '".$newFolderPath."'";
					$sql = $sql.", '".$documentDate."'";
					$sql = $sql.", '".$documentMonth."'";
					$sql = $sql.", '".$documentYear."'";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.")";
					$mQuery->querySQL($sql);


					$sql = "select did from db_document order by did desc limit 1";
					$did = $mQuery->getResultOneRecord($sql, "did");

					$sql = "select uaid, site_customer, email from db_user_auth where shop_code='".$docBranchName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$uaid = $mQuery->getResultOneRecord($sql, "uaid");
						$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
						$email_customer = $mQuery->getResultOneRecord($sql, "email");
					}else{
						$uaid = 0;
						$site_customer = "-";
						$email_customer = "-";
					}  //------  if($num > 0)

					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
					$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
					$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
					$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
					$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);


					$sqlDocBranch = "insert into db_document_map_special_type values(".$did.", ".$documentSpecialType.")";
					$mQuery->querySQL($sqlDocBranch);
					
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}else{
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}  //------  if (!file_exists($newFolderPath)) 

				$docMoveComplete = $docMoveComplete + 1;

			}elseif(strlen($fileName) == 15){
				$docBranchName = substr(strtoupper($fileName), 0, 4);
				$documentYear = substr($fileName, 4, 4);
				$documentMonth = substr($fileName, 8, 2);
				$documentSpecialType = (int)substr($fileName, 10, 2);
				$documentExtra3Digits = substr($fileName, 12, 3); //------ Extra Type From Outsource PDF Files

				$documentDate = "01";

				$sql = "select special_type_name from db_special_fianace_type where fid=".$documentSpecialType;
				$specialTypeName =$mQuery->getResultOneRecord($sql, "special_type_name");

				$docTypeID = 1;
				$docCategoryID = 1;
				$docTitle = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear;
				$docMonth = $documentMonth."/".$documentYear;
				$docDescription = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear." - เอกสารประเภท ".$specialTypeName;
				$docFileName = $fileName.$extension;
			    	$targetFile =  $docFileName;

			    	$newFolderPath = $destinationPath.$docBranchName."/".$documentYear."/".$documentMonth."/";


			    	if (!file_exists($newFolderPath)) {
					mkdir($newFolderPath, 0777, true);
				}  //------  if (!file_exists($newFolderPath)) 

			    	if (!file_exists($newFolderPath.$docFileName)) {
			    		$startBrandName = strtoupper(substr($fileName, 0, 1));

					$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
					}else{
						$brandid = 0;
					}  //-----  if($num > 0)

					$sql = "insert into db_document values(NULL";
					$sql = $sql.", ".$docTypeID."";
					$sql = $sql.", ".$docCategoryID."";
					$sql = $sql.", ".$brandid."";
					$sql = $sql.", '".$docBranchName."'";
					$sql = $sql.", '-'";
					$sql = $sql.", '".$docTitle."'";
					$sql = $sql.", '".$docDescription."'";
					$sql = $sql.", '".$docFileName."'";
					$sql = $sql.", '".$newFolderPath."'";
					$sql = $sql.", '".$documentDate."'";
					$sql = $sql.", '".$documentMonth."'";
					$sql = $sql.", '".$documentYear."'";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.")";
					$mQuery->querySQL($sql);


					$sql = "select did from db_document order by did desc limit 1";
					$did = $mQuery->getResultOneRecord($sql, "did");

					$sql = "select uaid, site_customer, email from db_user_auth where shop_code='".$docBranchName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$uaid = $mQuery->getResultOneRecord($sql, "uaid");
						$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
						$email_customer = $mQuery->getResultOneRecord($sql, "email");
					}else{
						$uaid = 0;
						$site_customer = "-";
						$email_customer = "-";
					}  //------  if($num > 0)

					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
					$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
					$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
					$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
					$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);


					$sqlDocBranch = "insert into db_document_map_special_type values(".$did.", ".$documentSpecialType.")";
					$mQuery->querySQL($sqlDocBranch);
					
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}else{
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}  //------  if (!file_exists($newFolderPath)) 

				$docMoveComplete = $docMoveComplete + 1;
			}  //-----  if(strlen($fileName) == 12)
		}  //-------  foreach ($files as $file)

		unset($files, $file);

		
		if($responseEmail != "-"){
			$docTitleEmail = "ผลการดึงไฟล์จาก Interface Server ประจำวันที่ ".$dateNowShow;
			$docEmailDescription = "ผลการดึงไฟล์จาก Interface Server ประจำวันที่ ".$dateNowShow." - เวลา ".$timeNow." สามารถนำเข้าไฟล์ได้ทั้งหมด ".$docMoveComplete." ไฟล์";
			// $mFunc->sendMailForNewDocument($responseEmail, $docTitleEmail, $docEmailDescription, $sender_email);

			$strHeader = "Content-type: text/html; charset=UTF-8\r\n"; // or UTF-8 //
			$strHeader .= "From: Miner Group<info@minor.com>";

			@mail($responseEmail,$docTitleEmail,$docEmailDescription,$strHeader);  // @ = No Show Error //
		}  //----  if($responseEmail != "-")

		unset($mQuery, $dFunc, $mFunc);
	}  //--------  public function insertDocFileDetailToDB()
	
	
	public function close_db()
	{
		// mysqli_close();
	}
}
?> 