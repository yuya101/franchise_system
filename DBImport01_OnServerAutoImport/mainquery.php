<?php
require_once("dimText.php");
require_once("connect_db.php");

class MainQuery
{
	private $connect, $getConnect;
	private $query, $result, $num, $sql;
	private $uid;
	
	
	public function __construct()
	{
		// Construction Method
		$this->connect = new Connect_DB();
		$this->getConnect = $this->connect->getConn();
	}
	
	public function checkNumRows($sqlTemp)  //  ------ For Check Count Row 
	{
		// $connect = new Connect_DB();
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		// $this->close_db($connect);
		
		// unset($connect);
		
		return $this->num;
	}
	
	
	public function getPrimaryID($sqlTemp, $wantID)  //  ---------  For Get target Primary id
	{
		// $connect = new Connect_DB();
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			//$this->result = mysqli_fetch_assoc($this->query, 1);
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result[$wantID];
		}
		else
		{
			$this->uid = 0;
		}
		
		// $this->close_db($connect);
		// unset($connect);
		
		return $this->uid;
	}
	
	
	public function getNewPrimaryID($sqlTemp, $wantID)  //  ---------  For Get next Primary id
	{
		// $connect = new Connect_DB();
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			//$this->result = mysqli_fetch_assoc($this->query, 1);
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result[$wantID] + 1;
		}
		else
		{
			$this->uid = 1;
		}
		
		// $this->close_db($connect);
		// unset($connect);
		
		return $this->uid;
	}
	
	
	public function querySQL($sqlTemp)  //-------- For Query SQL
	{
		// $connect = new Connect_DB();
		
		// mysqli_query($this->getConnect, $sqlTemp);

		if(!mysqli_query($this->getConnect, $sqlTemp))
		{
			die(mysqli_error($this->connect));
		}  //-----  if(!mysqli_query($this->getConnect, $sqlTemp))

		// $this->close_db($connect);
		
		// unset($connect);
	}
	
	
	public function getResultAll($sqlTemp)  //--------- For Fetch SQL Result Set 
	{
		// $connect = new Connect_DB();
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			while($this->result = mysqli_fetch_assoc($this->query))
			{
				$allResult[] = $this->result;
			}
		}
		
		// $this->close_db($connect);
		
		// unset($connect);
		
		return $allResult;
	}
	
	
	public function getResultOneRecord($sqlTemp, $wantRecord)  //--------- For Fetch SQL Result 1 Record
	{
		// $connect = new Connect_DB();
		
		$oneResult = "";
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			while($this->result = mysqli_fetch_assoc($this->query))
			{
				$oneResult = $this->result[$wantRecord];
			}
		}
		
		// $this->close_db($connect);
		
		// unset($connect);
		
		return $oneResult;
	}
	
	
	public function getRandomRecord($tmpSQL, $recChkID, $noOfRec)
	{		
		// $connect = new Connect_DB();
		$noOfRec = intval($noOfRec);  //--------------- จำนวน Record ที่ต้องการ Random
		
		$this->query = mysqli_query($this->getConnect, $tmpSQL);
		$this->num = mysqli_num_rows($this->query);
		$maxValue = $this->num;
		
		for($i=0; $i<$noOfRec; $i++)
		{
			$randomNo = rand(1, $maxValue);
			$this->sql = $tmpSQL.' and '.$recChkID.'='.$randomNo;
			$this->query = mysqli_query($this->getConnect, $this->sql);
			$this->num = mysqli_num_rows($this->query);
				
			if($this->num > 0)
			{
				if(isset($randomRec))
				{
					if(in_array($randomNo, $randomRec))
					{
						$i = $i - 1;
					}
					else
					{
						$randomRec[$i] = $randomNo;
					}
				}
				else
				{
					$randomRec[$i] = $randomNo;
				}
			}
			else
			{
				$i = $i - 1;
			} //-----------  if($this->num > 0)			
		}  //---------  for($i=0; $i<$noOfRec; $i++)
		
		// $this->close_db($connect);		
		// unset($connect);
				
		return $randomRec;
	}
	
	
	public function getTopicPoint($topicID)
	{
		// $connect = new Connect_DB();
		
		$sql = "select topicPoint from point_topic where topicID=".$topicID;
		$this->query = mysqli_query($this->getConnect, $sql);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result['topicPoint'];
		}
		else
		{
			$this->uid = 0;
		}
		
		// $this->close_db($connect);
		
		// unset($connect);
		
		return $this->uid;
	}
	
	
	
	public function close_db($connect)
	{
		// mysqli_close($connect);
	}
}
?> 