<?php
ob_start();
session_start();
session_cache_expire(20);  // TimeOut in scale of minutes.
// error_reporting(E_ALL);
// ini_set( 'display_errors','1');
$cache_expire = session_cache_expire();


if(isset($_SESSION['mLoginID']))
{
    header("location:index.php");
}
else
{
    include("includes/class/autoload.php");
    include("includes/view/loginFull_view.php");
}
?>