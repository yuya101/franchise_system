jQuery(document).ready(function() {

	$.getJSON('http://localhost:5050/franchise_system/includes/ajax/getLoginUserData.php?callback=?', function (data) {
		$('#loginCharts').highcharts({
			chart: {
				renderTo: 'container',
			            	type: 'spline'
			},
                                         legend: {
                                             enabled: false
                                         },
			series: [{
                                            type: 'area',
		                name: 'การเข้าใช้งานต่อวัน',
		                data: data
		              }],
		            	title: {
		                text: 'ภาพรวมการเข้าใช้งานต่อวัน',
                                            x: -20 //center
		           	},
                                        subtitle: {
                                            text: 'แสดงเป็นราย 15 วัน',
                                            x: -20 //center
                                        },
                                        xAxis: {
                                            title: {
                                                text: 'วัน'
                                            },
                                            categories: []
                                        },
                                        yAxis: {
                                            title: {
                                                text: 'ครั้ง'
                                            }
                                        },
                                        tooltip: {
                                            valueSuffix: ' ครั้ง'
                                        },
                                        plotOptions: {
                                            area: {
                                                fillColor: {
                                                    linearGradient: {
                                                        x1: 0,
                                                        y1: 0,
                                                        x2: 0,
                                                        y2: 1
                                                    },
                                                    stops: [
                                                        [0, Highcharts.getOptions().colors[0]],
                                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                    ]
                                                },
                                                marker: {
                                                    radius: 2
                                                },
                                                lineWidth: 1,
                                                states: {
                                                    hover: {
                                                        lineWidth: 1
                                                    }
                                                },
                                                threshold: null
                                            }
                                        }
		});
	});






            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getDownloadHistoryData.php?callback=?', function (data) {
                            $('#downloadCharts').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนครั้งในการ Download',
                                                                color: '#FF0000',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภาพรวมจำนวนการ Download ข้อมูล',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 15 วัน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'วัน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'ครั้ง'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' ครั้ง'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });






            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getDocumentAddHistoryData.php?callback=?', function (data) {
                            $('#addDocumentCharts').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนการเพิ่ม Document ในระบบ',
                                                                color: '#0066FF',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภาพรวมจำนวนการเพิ่ม Document ในระบบ',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 15 วัน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'วัน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'ครั้ง'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' ครั้ง'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });





            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartRegisterCustomData.php?callback=?', function (data) {
                            $('#registerCustomer').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนสมาชิกสมัครเข้าระบบ',
                                                                color: '#FF0000',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภายรวมจำนวนสมาชิกในระบบ',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 12 เดือน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'เดือน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'คน'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' คน'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartTopUpOverAllData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#topupOverAll').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวน Transaction',
                                                                color: '#0066FF',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'จำนวน Volumn',
                                                                color: '#123',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'รายได้จากการเติมเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartTopUpByPaymentSystemTransactionData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var dataline6 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                                    dataline6.push(json[a][6]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#topupByPaymentSystemTransaction').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'การเทียบข้อมูล (Check Balance)',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'ตู้ ATM',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'อินเตอร์เน็ต (Internet)',
                                                                data: dataline3,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'ตู้ฝากเงินสด (CDM)',
                                                                data: dataline4,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'เคาเตอร์ธนาคาร',
                                                                data: dataline5,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'โทรศัพท์มือถือ (Mobile Banking)',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'จำนวน TRANSACTION จากการเติมเงินแบ่งตามช่องทางการเติม',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'รายการ'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartTopUpByPaymentSystemVolumnData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var dataline6 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                                    dataline6.push(json[a][6]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#topupByPaymentSystemVolumn').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'การเทียบข้อมูล (Check Balance)',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'ตู้ ATM',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'อินเตอร์เน็ต (Internet)',
                                                                data: dataline3,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'ตู้ฝากเงินสด (CDM)',
                                                                data: dataline4,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'เคาเตอร์ธนาคาร',
                                                                data: dataline5,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'โทรศัพท์มือถือ (Mobile Banking)',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'รายได้จากการเติมเงินแบ่งตามช่องทางการเติม',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartTopUpByServerTransactionData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#topupByServerTransaction').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'SERVER 1',
                                                                data: dataline1
                                                              },{
                                                                name: 'SERVER 2',
                                                                data: dataline2
                                                              },{
                                                                name: 'GARENA SERVER',
                                                                data: dataline3
                                                              },{
                                                                name: 'SERVER 3',
                                                                data: dataline4
                                                              },{
                                                                name: 'SERVER 4',
                                                                data: dataline5
                                                              }],
                                                                title: {
                                                                text: 'จำนวน TRANSACTION ของ Server ส่งข้อมูลการเติมเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'รายการ'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartTopUpByServerVolumnData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#topupByServerVolumn').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'SERVER 1',
                                                                data: dataline1
                                                              },{
                                                                name: 'SERVER 2',
                                                                data: dataline2
                                                              },{
                                                                name: 'GARENA SERVER',
                                                                data: dataline3
                                                              },{
                                                                name: 'SERVER 3',
                                                                data: dataline4
                                                              },{
                                                                name: 'SERVER 4',
                                                                data: dataline5
                                                              }],
                                                                title: {
                                                                text: 'รายได้ผ่าน Server ส่งข้อมูลการเติมเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartWithdrawOverAllData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#withdrawOverAll').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวน Transaction',
                                                                color: '#0066FF',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'จำนวน Volumn',
                                                                color: '#123',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'รายได้จากการถอนเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartWithdrawByPaymentSystemTransactionData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var dataline6 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                                    dataline6.push(json[a][6]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#withdrawByPaymentSystemTransaction').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'การเทียบข้อมูล (Check Balance)',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'ตู้ ATM',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'อินเตอร์เน็ต (Internet)',
                                                                data: dataline3,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'ตู้ฝากเงินสด (CDM)',
                                                                data: dataline4,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'เคาเตอร์ธนาคาร',
                                                                data: dataline5,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              },{
                                                                name: 'โทรศัพท์มือถือ (Mobile Banking)',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' รายการ'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'จำนวน TRANSACTION จากการถอนเงินแบ่งตามช่องทางการถอน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'รายการ'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartWithdrawByPaymentSystemVolumnData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var dataline6 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                                    dataline6.push(json[a][6]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#withdrawByPaymentSystemVolumn').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'การเทียบข้อมูล (Check Balance)',
                                                                data: dataline1,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'ตู้ ATM',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'อินเตอร์เน็ต (Internet)',
                                                                data: dataline3,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'ตู้ฝากเงินสด (CDM)',
                                                                data: dataline4,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'เคาเตอร์ธนาคาร',
                                                                data: dataline5,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              },{
                                                                name: 'โทรศัพท์มือถือ (Mobile Banking)',
                                                                data: dataline2,
                                                                tooltip: {
                                                                    valueSuffix: ' บาท'
                                                                 }
                                                              }],
                                                                title: {
                                                                text: 'รายจ่ายจากการถอนเงินแบ่งตามช่องทางการถอน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartWithDrawByServerTransactionData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#withdrawByServerTransaction').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'SERVER 1',
                                                                data: dataline1
                                                              },{
                                                                name: 'SERVER 2',
                                                                data: dataline2
                                                              },{
                                                                name: 'GARENA SERVER',
                                                                data: dataline3
                                                              },{
                                                                name: 'SERVER 3',
                                                                data: dataline4
                                                              },{
                                                                name: 'SERVER 4',
                                                                data: dataline5
                                                              }],
                                                                title: {
                                                                text: 'จำนวน TRANSACTION ของ Server ส่งข้อมูลการถอนเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'รายการ'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartWithDrawByServerVolumnData.php?callback=?', function (json) {

                            var dataline1 = [];
                            var dataline2 = [];
                            var dataline3 = [];
                            var dataline4 = [];
                            var dataline5 = [];
                            var categories = [];

                            for(a=1; a<json.length; a++)
                            {
                                    categories.push(json[a][0]);
                                    dataline1.push(json[a][1]);
                                    dataline2.push(json[a][2]);
                                    dataline3.push(json[a][3]);
                                    dataline4.push(json[a][4]);
                                    dataline5.push(json[a][5]);
                            }  //-----  for(a=0; a<json.length; a++)

                            $('#withdrawByServerVolumn').highcharts({
                                                    chart: {
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'SERVER 1',
                                                                data: dataline1
                                                              },{
                                                                name: 'SERVER 2',
                                                                data: dataline2
                                                              },{
                                                                name: 'GARENA SERVER',
                                                                data: dataline3
                                                              },{
                                                                name: 'SERVER 3',
                                                                data: dataline4
                                                              },{
                                                                name: 'SERVER 4',
                                                                data: dataline5
                                                              }],
                                                                title: {
                                                                text: 'รายได้ผ่าน Server ส่งข้อมูลการถอนเงิน',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 30 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {},
                                                        categories: categories
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'บาท'
                                                        }
                                                    },
                                                    tooltip: {
                                                        shared: true
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });




            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getChartCustomProvinceData.php?callback=?', function (data) {
                    $('#customProvince').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'รายละเอียดจังหวัดของลูกค้า'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                colorByPoint: true,
                                data: data
                            }]
                        });
            });


            $.getJSON('http://localhost:5050/franchise_system/includes/ajax/getTopUpSevenDayData.php?callback=?', function (data) {
                            $('#topUpSevenDay').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'ภาพรวมการ DOWNLOAD ข้อมูล',
                                                                color: '#0066FF',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภาพรวมการ DOWNLOAD ข้อมูล',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 7 วันย้อนหลัง',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'วัน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'ครั้ง'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' ครั้ง'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });


	
});