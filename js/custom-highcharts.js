jQuery(document).ready(function() {

	$.getJSON('https://mfgfranchise-uat.azurewebsites.net/franchise_system/includes/ajax/getLoginUserData.php?callback=?', function (data) {
		$('#loginCharts').highcharts({
			chart: {
				renderTo: 'container',
			            	type: 'spline'
			},
                                         legend: {
                                             enabled: false
                                         },
			series: [{
                                            type: 'area',
		                name: 'การเข้าใช้งานต่อวัน',
		                data: data
		              }],
		            	title: {
		                text: 'ภาพรวมการเข้าใช้งานต่อวัน',
                                            x: -20 //center
		           	},
                                        subtitle: {
                                            text: 'แสดงเป็นราย 15 วัน',
                                            x: -20 //center
                                        },
                                        xAxis: {
                                            title: {
                                                text: 'วัน'
                                            },
                                            categories: []
                                        },
                                        yAxis: {
                                            title: {
                                                text: 'ครั้ง'
                                            }
                                        },
                                        tooltip: {
                                            valueSuffix: ' ครั้ง'
                                        },
                                        plotOptions: {
                                            area: {
                                                fillColor: {
                                                    linearGradient: {
                                                        x1: 0,
                                                        y1: 0,
                                                        x2: 0,
                                                        y2: 1
                                                    },
                                                    stops: [
                                                        [0, Highcharts.getOptions().colors[0]],
                                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                                    ]
                                                },
                                                marker: {
                                                    radius: 2
                                                },
                                                lineWidth: 1,
                                                states: {
                                                    hover: {
                                                        lineWidth: 1
                                                    }
                                                },
                                                threshold: null
                                            }
                                        }
		});
	});






            $.getJSON('https://mfgfranchise-uat.azurewebsites.net/franchise_system/includes/ajax/getDownloadHistoryData.php?callback=?', function (data) {
                            $('#downloadCharts').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนครั้งในการ Download',
                                                                color: '#FF0000',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภาพรวมจำนวนการ Download ข้อมูล',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 15 วัน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'วัน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'ครั้ง'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' ครั้ง'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });






            $.getJSON('https://mfgfranchise-uat.azurewebsites.net/franchise_system/includes/ajax/getDocumentAddHistoryData.php?callback=?', function (data) {
                            $('#addDocumentCharts').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนการเพิ่ม Document ในระบบ',
                                                                color: '#0066FF',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภาพรวมจำนวนการเพิ่ม Document ในระบบ',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 15 วัน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'วัน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'ครั้ง'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' ครั้ง'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });





            $.getJSON('https://mfgfranchise-uat.azurewebsites.net/franchise_system/includes/ajax/getChartRegisterCustomData.php?callback=?', function (data) {
                            $('#registerCustomer').highcharts({
                                                    chart: {
                                                        renderTo: 'container',
                                                                    type: 'spline'
                                                    },
                                                    series: [{
                                                                name: 'จำนวนสมาชิกสมัครเข้าระบบ',
                                                                color: '#FF0000',
                                                                data: data
                                                              }],
                                                                title: {
                                                                text: 'ภายรวมจำนวนสมาชิกในระบบ',
                                                                                    x: -20 //center
                                                            },
                                                    subtitle: {
                                                        text: 'แสดงเป็นราย 12 เดือน',
                                                        x: -20 //center
                                                    },
                                                    xAxis: {
                                                        title: {
                                                            text: 'เดือน'
                                                        },
                                                        categories: []
                                                    },
                                                    yAxis: {
                                                        title: {
                                                            text: 'คน'
                                                        }
                                                    },
                                                    tooltip: {
                                                        valueSuffix: ' คน'
                                                    },
                                                    plotLines: [{
                                                        value: 0,
                                                        width: 1,
                                                        color: '#f7a35c'
                                                    }]
                            });
            });

	
});