// JavaScript Document
var xmlHttp;
var dimDelay = 300;
var showTime = 700;
var tmpPageName;

function createXMLHttpRequest()
{	
	/*if(window.ActiveXObject)
	{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else if(window.XMLHttpRequest)
	{
		xmlHttp = new XMLHttpRequest();
	}
	
	return xmlHttp;*/
	
	// initially set the object to false 
	if (window.XMLHttpRequest)
	{
		  // check for Safari, Mozilla, Opera...
	  xmlHttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) 
	{
	  // check for Internet Explorer
	  xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (!xmlHttp)
	{
	  alert("Sorry your browser not support Ajax.");
	  // return false in case of failure
	  return false;
	}
	  // return the object in case of success
	//return XMLHttpRequestObject;
}


//Check numeric format ------------------------------------------------------------------------------
function CheckNumeric(str_item)
{	
	var flag_positive = true;
	for(var i = 0;i < str_item.length; ++i)
		if(!((parseInt(str_item.charAt(i)) >= 0)&&(parseInt(str_item.charAt(i)) <= 9)))		
	    	flag_positive = false;															
		
	return flag_positive;
}

function formatCurrency(val){
	if(val == "" || val == null || val == "NULL") return val;

	//Split Decimals
    var arrs = val.toString().split(".");
	//Split data and reverse
	var revs = arrs[0].split("").reverse().join("");
	var len = revs.length;
    var tmp = "";
    for(i = 0; i < len; i++){
        if(i >0 && (i%3) == 0){
            tmp+=","+revs.charAt(i);
        }else{
            tmp += revs.charAt(i);
        }
    }  

	//Split data and reverse back
	tmp = tmp.split("").reverse().join("");
	//Check Decimals
    if(arrs.length > 1 && arrs[1] != undefined){
        tmp += "."+ arrs[1];
    }
    return tmp;
}

Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 }; //----------  วิธีใช้ (123456789.12345).formatMoney(2, '.', ',');
 
//End function ------------------------------------------------------------------------------


//Check GreenCard ID Format ------------------------------------------------------------
function checkID(id)
{
	if(id.length != 13) return false;
	for(i=0, sum=0; i < 12; i++)
	sum += parseFloat(id.charAt(i))*(13-i); if((11-sum%11)%10!=parseFloat(id.charAt(12)))
	return false; return true;
}
//Check GreenCard ID Format ------------------------------------------------------------


//--------------------------- Start Function List Data In Drop Down Box ------------------------------------------
/*
rankID = ID ของอันดับ List
startObj = Object ที่เป็น List เริ่มต้นในการเลือก
targetObj = Oblect ที่เป็นเป้าหมายที่ต้องการให้เปลี่ยน
listQueryPageName = ชื่อ Page ของ PHP ที่ใช้ Query ข้อมูล Ajax
listObjID = ชื่อของ ID สำหรับไว้แสดงหรือซ่อน ต้องเอาไปรวมกับค่า rankID แล้วจะได้ ID จริง เช่น listProductLI + 1 = listProductLI1
numberOfAllList = จำนวนของ List ทั้งหมดทีต้องใช้เพราะเวลาเราจะ Clear List ที่มีทั้งหมดนับตั้งแต่ตัวที่เราเลือก
*/
var tmpRankID, tmpTargetObj, tmpListObjName, tmpNameOfDetailDiv, tmpNumberOfAllList, tmpSelectObj;

function refreshListBoxItem(rankID, startObj, targetObj, listQueryPageName, listObjName, numberOfAllList, nameOfDetailDiv)  
{
	hideListBox(rankID, listObjName, numberOfAllList, nameOfDetailDiv);
	tmpRankID = parseInt(rankID)+1;
	tmpTargetObj = targetObj;
	tmpListObjName = listObjName;
	tmpNumberOfAllList = numberOfAllList;
	tmpNameOfDetailDiv = nameOfDetailDiv;
	tmpSelectObj = startObj.value;

	
	if((startObj.value == "") || (startObj.value == "0") || (startObj.value == 99999))
	{
		return false;
	}
		
	clearListBox(document.getElementById(targetObj));
		
	var url = "includes/ajax/" + listQueryPageName + ".php?selectID="+startObj.value;
	createXMLHttpRequest();
	xmlHttp.onreadystatechange = handleListBoxStateChange;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function handleListBoxStateChange()
{
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{
			updateListBox();
		}
	}
}

function updateListBox()
{
	var objAmp = document.getElementById(tmpTargetObj);
	var result = xmlHttp.responseText;
	var option = null;
	var k, i;
	p = result.split(",");
	
	// option = document.createElement("option");
	// option.appendChild(document.createTextNode("---- Please Select Your Items ----"));
	// objAmp.appendChild(option);
	// objAmp.lastChild.value = "0";
	
	for(i=0; i<p.length-1; i++)
	{
		k = p[i].split("-");
		option = document.createElement("option");
		option.appendChild(document.createTextNode(k[1]));
		objAmp.appendChild(option);
		objAmp.lastChild.value = k[0];
	}
	
	
	// $(function(){				
	// 	$("#"+tmpListObjName+tmpRankID).fadeIn(showTime - 200);
		
	// 	if((parseInt(tmpNumberOfAllList) - parseInt(tmpRankID - 1)) == 1)
	// 	{
	// 		$("#"+tmpNameOfDetailDiv).fadeIn(showTime - 200);
	// 	}
	// })();

	(function($){				
		$("#"+tmpListObjName+tmpRankID).fadeIn(showTime - 200);
		
		if((parseInt(tmpNumberOfAllList) - parseInt(tmpRankID - 1)) == 1)
		{
			$("#"+tmpNameOfDetailDiv).fadeIn(showTime - 200);
		}
	})(jQuery);
}

function hideListBox(rankID, listObjName, numberOfAllList, nameOfDetailDiv)  //  Function สำหรับซ่อน List Box ที่อยู่ต่ำกว่าตัวที่ถูก Event onChange
{	
	for(var i=(parseInt(rankID)+1); i<=parseInt(numberOfAllList); i++)
	{
		document.getElementById(listObjName + i).style.display = 'none';
	}
	
	if(nameOfDetailDiv != "")
	{
		document.getElementById(nameOfDetailDiv).style.display = 'none';
	}
}

function clearListBox(obj)
{
	while(obj.length > 0)
	{
		obj.removeChild(obj.childNodes[0]);
	}
}
//--------------------------- End Function List Data In Drop Down Box ------------------------------------------



//--------------------------- Start Function List Data In Drop Down Box Version 2 ------------------------------------------
/*
rankID = ID ของอันดับ List
startObj = Object ที่เป็น List เริ่มต้นในการเลือก
targetObj = Oblect ที่เป็นเป้าหมายที่ต้องการให้เปลี่ยน
listQueryPageName = ชื่อ Page ของ PHP ที่ใช้ Query ข้อมูล Ajax
listObjID = ชื่อของ ID สำหรับไว้แสดงหรือซ่อน ต้องเอาไปรวมกับค่า rankID แล้วจะได้ ID จริง เช่น listProductLI + 1 = listProductLI1
numberOfAllList = จำนวนของ List ทั้งหมดทีต้องใช้เพราะเวลาเราจะ Clear List ที่มีทั้งหมดนับตั้งแต่ตัวที่เราเลือก
*/
var tmpNameOfShowAndHiddenDiv;

function refreshListBoxItemV2(rankID, startObj, targetObj, listQueryPageName, listObjName, numberOfAllList, nameOfDetailDiv, nameOfShowAndHiddenDiv) 
{
	hideListBoxV2(rankID, listObjName, numberOfAllList, nameOfDetailDiv, nameOfShowAndHiddenDiv);
	tmpRankID = parseInt(rankID)+1;
	tmpTargetObj = targetObj;
	tmpListObjName = listObjName;
	tmpNumberOfAllList = numberOfAllList;
	tmpNameOfDetailDiv = nameOfDetailDiv;
	tmpNameOfShowAndHiddenDiv = nameOfShowAndHiddenDiv;
	tmpSelectObj = startObj.value;
	
	if((startObj.value == "") || (startObj.value == "0") || (startObj.value == 99999))
	{
		return false;
	}
		
	clearListBox(document.getElementById(targetObj));
		
	var url = "includes/ajax/" + listQueryPageName + ".php?selectID="+startObj.value;
	createXMLHttpRequest();
	xmlHttp.onreadystatechange = handleListBoxStateChangeV2;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function handleListBoxStateChangeV2()
{
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{
			updateListBoxV2();
		}
	}
}

function updateListBoxV2()
{
	var objAmp = document.getElementById(tmpTargetObj);
	var result = xmlHttp.responseText;
	var option = null;
	var k, i;
	p = result.split(",");
	
	for(i=0; i<p.length-1; i++)
	{
		k = p[i].split("-");
		option = document.createElement("option");
		option.appendChild(document.createTextNode(k[1]));
		objAmp.appendChild(option);
		objAmp.lastChild.value = k[0];
	}

	(function($){				
		$("#"+tmpListObjName+tmpRankID).fadeIn(showTime - 200);
		
		if((parseInt(tmpNumberOfAllList) - parseInt(tmpRankID - 1)) == 1)
		{
			$("#"+tmpNameOfDetailDiv).fadeIn(showTime - 200);
		}
		
		if(parseInt(objAmp.value) == 1)
		{
			$("#"+tmpNameOfShowAndHiddenDiv).fadeIn(showTime - 200);
		}
	})(jQuery);
}

function hideListBoxV2(rankID, listObjName, numberOfAllList, nameOfDetailDiv, nameOfShowAndHiddenDiv)  //  Function สำหรับซ่อน List Box ที่อยู่ต่ำกว่าตัวที่ถูก Event onChange
{	
	for(var i=(parseInt(rankID)+1); i<=parseInt(numberOfAllList); i++)
	{
		document.getElementById(listObjName + i).style.display = 'none';
	}
	
	if(nameOfDetailDiv != "")
	{
		document.getElementById(nameOfShowAndHiddenDiv).style.display = 'none';
	}
	
	if(nameOfDetailDiv != "")
	{
		document.getElementById(nameOfDetailDiv).style.display = 'none';
	}
}

function clearListBox(obj)
{
	while(obj.length > 0)
	{
		obj.removeChild(obj.childNodes[0]);
	}
}
//--------------------------- End Function List Data In Drop Down Box Version 2 ------------------------------------------



//--------------------------- Function Show Manange Detail ------------------------------------------
function showManageDetail(obj, manageName, manageTopic)
{	
	tmpPageName = manageName;

	if(parseInt(obj.value) != 0)
	{
		var url = "includes/ajax/inq"+tmpPageName+".php?";
		url = url + "selectID=" + obj.value;
				
		createXMLHttpRequest();
		xmlHttp.onreadystatechange = handleShowManageDetailAjax;
		xmlHttp.open("GET", url, true);
		xmlHttp.send(null);
	}  //------------------  if(parseInt(obj) != 0)
}

function handleShowManageDetailAjax()
{	
	if(xmlHttp.readyState == 1)
	{
		document.getElementById(tmpPageName+'DivDetail').style.display = 'none';
		document.getElementById(tmpPageName+'DivDetail').innerHTML = '';
		document.getElementById(tmpPageName+'Loading').style.display = '';
	}
	
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{			
			//document.getElementById(tmpPageName+'DivDetail').innerHTML = xmlHttp.responseText;
			
			// $(function(){
			// 	$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
			// 	$("#"+tmpPageName+"Loading").fadeOut(showTime);
			// 	$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			// })();

			(function($){
				$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
				$("#"+tmpPageName+"Loading").fadeOut(showTime);
				$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			})(jQuery);
			
			return true;
		}
	}
}
//--------------------------- End Function Show  Manange Video Detail -------------------------------------------


//--------------------------- Function Show Manange Detail In Button ------------------------------------------
function showManageDetailInButton(obj, manageName, manageTopic)
{	
	tmpPageName = manageName;
	objectInText = document.getElementById(obj).value.trim();

	if(objectInText != "")
	{
		var url = "includes/ajax/inq"+tmpPageName+".php?";
		url = url + "selectText=" + objectInText;
				
		createXMLHttpRequest();
		xmlHttp.onreadystatechange = handleShowManageDetailInButtonAjax;
		xmlHttp.open("GET", url, true);
		xmlHttp.send(null);
	}  //------------------  if(parseInt(obj) != 0)
}

function handleShowManageDetailInButtonAjax()
{	
	if(xmlHttp.readyState == 1)
	{
		document.getElementById(tmpPageName+'DivDetail').style.display = 'none';
		document.getElementById(tmpPageName+'DivDetail').innerHTML = '';
		document.getElementById(tmpPageName+'Loading').style.display = '';
	}
	
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{						
			// $(function(){
			// 	$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
			// 	$("#"+tmpPageName+"Loading").fadeOut(showTime);
			// 	$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			// })();

			(function($){
				$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
				$("#"+tmpPageName+"Loading").fadeOut(showTime);
				$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			})(jQuery);
			
			return true;
		}
	}
}
//--------------------------- End Function Show  Manange In Button -------------------------------------------


//--------------------------- Function Show Manange Detail In Button Send By ID ------------------------------------------
function showManageDetailInButtonByID(objValue, manageName, manageTopic)
{	
	tmpPageName = manageName;
	objectInText = objValue;

	if(objectInText != "")
	{
		var url = "includes/ajax/inq"+tmpPageName+".php?";
		url = url + "selectText=" + objectInText;
				
		createXMLHttpRequest();
		xmlHttp.onreadystatechange = handleShowManageDetailInButtonByIDAjax;
		xmlHttp.open("GET", url, true);
		xmlHttp.send(null);
	}  //------------------  if(parseInt(obj) != 0)
}

function handleShowManageDetailInButtonByIDAjax()
{	
	if(xmlHttp.readyState == 1)
	{
		document.getElementById(tmpPageName+'DivDetail').style.display = 'none';
		document.getElementById(tmpPageName+'DivDetail').innerHTML = '';
		document.getElementById(tmpPageName+'Loading').style.display = '';
	}
	
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{						
			// $(function(){
			// 	$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
			// 	$("#"+tmpPageName+"Loading").fadeOut(showTime);
			// 	$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			// })();

			(function($){
				$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
				$("#"+tmpPageName+"Loading").fadeOut(showTime);
				$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			})(jQuery);
			
			return true;
		}
	}
}
//--------------------------- End Function Show  Manange In Button Send By ID -------------------------------------------


//--------------------------- Function Check Change Password -------------------------------------------
function chkChgPassword(obj)
{
	if(obj.confirmpassword.value != obj.newpassword.value)
	{
		alert("กรุณากรอกรหัสผ่านใหม่และยืนยันรหัสผ่านใหม่ให้ตรงกันด้วยค่ะ !");
		obj.newpassword.focus();
		return false;
	}  //-------  if(obj.oldpassword.value == "")

	if(!confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !'))
	{
		return false;
	}  //----  if(!confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !'))

	return true;
}  //----  function chkChgPassword(obj)
//--------------------------- End Function Check Change Password -------------------------------------------


//--------------------------- Function Count Download ------------------------------------------
function countDownload(aID, docID)
{	
	var url = "includes/ajax/countDownloadDocumentAjax.php?";
	url = url + "docID=" + docID;
	url = url + "&aID=" + aID;
				
	createXMLHttpRequest();
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}  //-----  function countDownload(docID)

//--------------------------- End Function Count Download ------------------------------------------



//--------------------------- Function Show Search Document In Button ------------------------------------------
function showSearchDocumentInButton(objListVal, objVal1, objVal2, manageName, manageTopic)
{	
	tmpPageName = manageName;

	var url = "includes/ajax/inq"+tmpPageName+".php?";
	url = url + "brandID=" + objListVal;
	url = url + "&docmonth=" + objVal1;
	url = url + "&docbranchcode=" + objVal2;
				
	createXMLHttpRequest();
	xmlHttp.onreadystatechange = handleShowSearchDocumentInButtonAjax;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function handleShowSearchDocumentInButtonAjax()
{	
	if(xmlHttp.readyState == 1)
	{
		document.getElementById(tmpPageName+'DivDetail').style.display = 'none';
		document.getElementById(tmpPageName+'DivDetail').innerHTML = '';
		document.getElementById(tmpPageName+'Loading').style.display = '';
	}
	
	if(xmlHttp.readyState == 4)
	{
		if(xmlHttp.status == 200)
		{				
			(function($){
				$("#"+tmpPageName+'DivDetail').html(xmlHttp.responseText);
				$("#"+tmpPageName+"Loading").fadeOut(showTime);
				$("#"+tmpPageName+"DivDetail").delay(dimDelay+300).slideDown(showTime - 200);
			})(jQuery);
			
			return true;
		}
	}
}
//--------------------------- End Function Show Search Document In Button -------------------------------------------