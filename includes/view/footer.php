                
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; <?php echo $programName; ?>
                    <a href="#"><?php echo $designBy; ?></a> 
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <?php // if(isset($f) and (($f == "manageUser") or ($f == "showUser") or ($f == "topupSystem") or ($f == "topupHistory") or ($f == "withdrawHistory") or ($f == "manageExternalServer"))){ ?>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
            <script src="assets/pages/scripts/table-datatables-scroller.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
        <?php // } ?>

        <?php // if(isset($pageName) and (($pageName== "Dashboard"))){ ?>
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
            <script src="assets/global/plugins/highcharts/js/highcharts-3d.js" type="text/javascript"></script>
            <script src="assets/global/plugins/highcharts/js/highcharts-more.js" type="text/javascript"></script>
            <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
            <script src="assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <!-- <script src="assets/pages/scripts/charts-highcharts.js" type="text/javascript"></script> -->
            <script src="js/custom-highcharts.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
        <?php // } ?>
        

        <!-- BEGIN CUSTOMER SCRIPTS -->
        <script src="js/mainScript.js" type="text/javascript" charset="utf-8" async defer></script>
        <!-- END CUSTOMER SCRIPTS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        
        <script src="assets/pages/scripts/form-dropzone.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/dropzone2/min/dropzone-amd-module.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->


        <script type="text/javascript">
            setTimeout(function() { window.location.href = "logout.php"; }, 20 * 60 * 1000);
        </script>

        
        <?php include("includes/view/modal_section.php"); ?>       
                                        
    </body>

</html>
<?php unset($mQuery, $mFunc, $dFunc); ?>