                                            <?php include("includes/control/queryDocumentType_Ctl.php"); ?>
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล Category : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errNameDup'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> ชื่อ Category : <?php echo base64_decode($_REQUEST['errNameDup']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_CATEGORY_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addCategory_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <h3 class="form-section">Category Detail.</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Document Type</label>
                                                                <div class="col-md-4">
                                                                    <select name="typeid" id="typeid" class="form-control input-circle font1emGray">
                                                                    <?php if($numDocType > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocType; $i++){ ?>
                                                                            <option value="<?php echo $docTypeID[$i]; ?>"><?php echo $docTypeName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Category Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="catname" id="catname" class="form-control input-circle font1emGray" placeholder="Please! Enter Category Name" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;ยืนยัน&nbsp;&nbsp;</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>