                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <!-- <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อผู้เข้าใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="กรุณากรอกชื่อผู้เข้าใช้งาน" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุลผู้เข้าใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="กรุณากรอกนามสกุลผู้เข้าใช้งาน" required>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Username</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="username" id="username" class="form-control input-circle font1emGray" placeholder="กรุณากรอก Username" required>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email Address</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอก Email Address ผู้เข้าใช้งาน" required> </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-5 col-md-9">
                                                                    <button type="button" class="btn btn-circle green font1emWhite" onclick="return showManageDetailInButton( 'username', 'showTopUpSearchByCustomDetail', 'ShowTopUpSearchByCustomDetail');">ค้นหาข้อมูลลูกค้า</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>


                                            <div id="showTopUpSearchByCustomDetailDivDetail" style="display:none;"></div>
                                            <div align="center" id="showTopUpSearchByCustomDetailLoading" style="display:none; margin-bottom: 50px;">
                                                <br /><br />
                                                <img src="img/loading.gif" border="0" />
                                                <br /><br />
                                                <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                            </div>