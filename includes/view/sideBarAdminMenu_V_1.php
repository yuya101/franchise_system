                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <!-- <form class="sidebar-search  sidebar-search-bordered" action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form> -->
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <li class="nav-item start ">
                                <a href="index.php" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="arrow"></span>
                                </a>
                            </li>
                            <li class="heading">
                                <h3 class="uppercase">Documents</h3>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-layers"></i>
                                    <span class="title">Financial</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                    <?php $arrBID = 0; ?>
                                    <?php foreach ($_SESSION['arrBrandName'] as $r) {?>
                                            <a href="javascript:;" class="nav-link nav-toggle">
                                                <span class="title"><?php echo $r; ?></span>
                                                <span class="arrow"></span>
                                            </a>
                                            <?php 
                                            $sqlBrand = "select uaid, shop_code from db_user_auth where aid=".$_SESSION['mLoginID']." and brand_id=".$_SESSION['arrBrandID'][$arrBID]." order by shop_code";
                                            $numBrand = $mQuery->checkNumRows($sqlBrand);

                                            if($numBrand > 0){
                                                $resultBrandMenu = $mQuery->getResultAll($sqlBrand);
                                                ?><ul class="sub-menu"><?php
                                                foreach ($resultBrandMenu as $rbm) {
                                                ?>
                                                    <li class="nav-item ">
                                                        <a href="javascript:;" class="nav-link nav-toggle"> 
                                                            <span class="title"><?php echo $rbm['shop_code']; ?></span>
                                                            <span class="arrow"></span>
                                                        </a>
                                                    <?php
                                                        $sqlSerachYear = "select did from db_document_authorize where uaid=".$rbm['uaid'];
                                                        $numSerachYear = $mQuery->checkNumRows($sqlSerachYear);

                                                        if($numSerachYear > 0){
                                                            $arrDocYear = array();

                                                            $docIDSearch = $mQuery->getResultOneRecord($sqlSerachYear, "did");

                                                            $sqlSerachYear = "select year from db_document where shop_code='".$rbm['shop_code']."' and type_id=1 and brand_id=".$_SESSION['arrBrandID'][$arrBID];
                                                            $numSerachYear = $mQuery->checkNumRows($sqlSerachYear);

                                                            if($numSerachYear > 0){
                                                                $resultSearchYear = $mQuery->getResultAll($sqlSerachYear);

                                                                foreach ($resultSearchYear as $rsyc) {
                                                                    $yearSearchText = $rsyc['year'];

                                                                    if(!in_array($yearSearchText, $arrDocYear)){
                                                                        array_push($arrDocYear, $yearSearchText);
                                                                    }  //------  if(in_array($yearSearchText, $arrDocYear))
                                                                }  //-----  foreach ($resultSearchYear as $rsyc)

                                                                unset($resultSearchYear, $rsyc);
                                                            }  //-------  if($numSerachYear > 0)

                                                            if(!(empty($arrDocYear))){
                                                                ?><ul class="sub-menu"><?php
                                                                foreach ($arrDocYear as $dy) {
                                                                ?>
                                                                        <li class="nav-item ">
                                                                            <a href="javascript:;" class="nav-link nav-toggle"> 
                                                                                <span class="title"><?php echo $dy; ?></span>
                                                                                <span class="arrow"></span>
                                                                            </a>
                                                                            <?php
                                                                            $sqlSerachMonth = "select distinct month from db_document where type_id=1 and brand_id=".$_SESSION['arrBrandID'][$arrBID]." and year='".$dy."' and shop_code='".$rbm['shop_code']."' order by month";
                                                                            $numSerachMonth = $mQuery->checkNumRows($sqlSerachMonth);

                                                                            if($numSerachMonth){
                                                                                $resultSerachMonth = $mQuery->getResultAll($sqlSerachMonth);
                                                                                ?><ul class="sub-menu"><?php
                                                                                foreach ($resultSerachMonth as $rsm) {
                                                                                ?>
                                                                                        <li class="nav-item ">
                                                                                            <a href="index.php?f=financial&bid=<?php echo base64_encode($_SESSION['arrBrandID'][$arrBID]) ?>&y=<?php echo base64_encode($dy); ?>&shopCode=<?php echo base64_encode($rbm['shop_code']) ?>&m=<?php echo base64_encode($rsm['month']) ?>" class="nav-link "> 
                                                                                                <?php echo $rsm['month']; ?>
                                                                                            </a>
                                                                                        </li>
                                                                                <?php
                                                                                }  //-----  foreach ($resultSerachMonth as $rsm)

                                                                                unset($resultSerachMonth, $rsm);
                                                                                ?></ul><!-- <ul class="sub-menu">  UL Menu For Doc Year --><?php
                                                                            }  //-----  if($numSerachMonth)
                                                                            ?>
                                                                        </li>
                                                                <?php
                                                                }  //-----  foreach ($arrDocYear as $dy)

                                                                ?></ul><!-- <ul class="sub-menu">  UL Menu For Doc Year --><?php
                                                                unset($arrDocYear, $dy);
                                                            }  //-----  if(!(empty($arrDocYear)))
                                                        }  //----  if($numSerachYear > 0)
                                                    ?> 
                                                    </li>  <!-- <li class="nav-item ">  LI Menu For Shop Code -->
                                                <?php
                                                }  //------  foreach ($resultBrandMenu as $rbm)
                                                ?></ul><!-- <ul class="sub-menu">  UL Menu For Shop Code --><?php
                                                unset($resultBrandMenu, $rbm);
                                            }  //-----  if($numBrand > 0)

                                        $arrBID++;
                                    }  //------  foreach ($_SESSION['arrBrandName'] as $r) 
                                    ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Document Type</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=franchiseManual" class="nav-link ">
                                            <span class="title">Franchise Manual</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=operateAndTraining" class="nav-link ">
                                            <span class="title">Operations and training</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=marketing" class="nav-link ">
                                            <span class="title">Marketing</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=consultant" class="nav-link ">
                                            <span class="title">Franchise (Business Consultant)</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=lastestNews" class="nav-link ">
                                            <span class="title">Latest News</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=letter" class="nav-link ">
                                            <span class="title">Letter</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=policy" class="nav-link ">
                                            <span class="title">Policy</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=others" class="nav-link ">
                                            <span class="title">Others</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-handbag"></i>
                                    <span class="title">Inquiry Data</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item ">
                                        <a href="javascript:;" class="nav-link nav-toggle"> 
                                            <span class="title">Inquiry Data</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item  ">
                                                <a href="index.php?f=tripexpenses-chill" class="nav-link ">
                                                    <span class="title">Chill</span>
                                                </a>
                                            </li>
                                            <li class="nav-item  ">
                                                <a href="index.php?f=tripexpenses-dry" class="nav-link ">
                                                    <span class="title">Dry</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Manage Category</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=addCategory" class="nav-link ">
                                            <span class="title">Add Category</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=manageCategory" class="nav-link ">
                                            <span class="title">Edit Category</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-paper-plane"></i>
                                    <span class="title">Manage Document</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=addFileData" class="nav-link ">
                                            <span class="title">Add Document File</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=manageFileData" class="nav-link ">
                                            <span class="title">Edit Document File</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=importFileData" class="nav-link ">
                                            <span class="title">Import Document File</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-pencil"></i>
                                    <span class="title">Manage Inquiry Data</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=importTripExpensesFileData" class="nav-link ">
                                            <span class="title">Import Inquiry Data File</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-user"></i>
                                    <span class="title">Manage Users</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=addUser" class="nav-link ">
                                            <span class="title">Add Users</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=manageUser" class="nav-link ">
                                            <span class="title">Manage Users</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item  ">
                                        <a href="index.php?f=showUser" class="nav-link ">
                                            <span class="title">Users History</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-feed"></i>
                                    <span class="title">Manage Branch</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=addBranch" class="nav-link ">
                                            <span class="title">Add Branch</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=manageBranch" class="nav-link ">
                                            <span class="title">Manage Branch</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="index.php?f=importBranchFromExcel" class="nav-link ">
                                            <span class="title">Import Branch From Excel</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">Setting</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="index.php?f=chgPass" class="nav-link ">
                                            <span class="title">Change Password</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item  ">
                                        <a href="index.php?f=chgUserProfile" class="nav-link ">
                                            <span class="title">Change Profile</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->