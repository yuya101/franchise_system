<?php  
$sql = "select * from db_document order by did";
$num = $mQuery->checkNumRows($sql);
?>


                                <div id="showDocumentFileDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showDocumentFileDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>

                                
                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูล Document File : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['errDupName'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ปรับปรุงข้อมูลไม่สำเร็จ!</strong> ข้อมูล Document File : <?php echo base64_decode($_REQUEST['errDupName']); ?> ซ้ำค่ะ กรุณาตรวจสอบอีกครั้ง.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูล Document File : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_MANAGE_FILE_DATA_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="typename"><label  class="font1emBlack">Type Name</label></th>
                                                    <th class="categoryname"><label  class="font1emBlack">Category Name</label></th>
                                                    <th class="doctitle"><label  class="font1emBlack">Document Title</label></th>
                                                    <th class="filename"><label  class="font1emBlack">File Name</label></th>
                                                    <th class="count"><label  class="font1emBlack">Download Count</label></th>
                                                    <th class="button"><label  class="font1emBlack">Edit</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $catID = $r['cat_id'];
                                                        $typeID = $r['type_id'];
                                                        $docID = $r['did'];
                                                        $title = $r['title'];
                                                        $fileName = $r['file_name'];
                                                        $filePath = $r['file_path'];
                                                        $forDate = $dFunc->showMonthYearInEnglish($r['year'].$r['month']);

                                                        $filePathNum = stripos($r['file_path'], "upload");
                                                        $filePath = substr($r['file_path'], $filePathNum);

                                                        $sql = "select type_name from db_document_type where type_id=".$typeID;
                                                        $typeName = $mQuery->getResultOneRecord($sql, "type_name");

                                                        $sql = "select cat_name from db_document_category where cat_id=".$catID;
                                                        $catName = $mQuery->getResultOneRecord($sql, "cat_name");

                                                        $sql = "select dhid from db_user_download_history where document_id=".$docID;
                                                        $downloadCount = number_format($mQuery->checkNumRows($sql), 0);
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $typeName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $catName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $title; ?></label></td>
                                                    <td><label  class="font1emGray"><a href="<?php echo $filePath.$fileName; ?>" target="_blank">fileName</a></label></td>
                                                    <td><label  class="font1emGray"><?php echo $downloadCount; ?></label></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $docID; ?>, 'showDocumentFileDetail', 'ShowDocumentFileDetail');">Edit Document</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->