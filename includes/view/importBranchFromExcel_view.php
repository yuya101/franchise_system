                                            <?php if(isset($_REQUEST['importComplete'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> นำเข้าข้อมูล Franchise และร้านสาขาจากไฟล์ : <?php echo base64_decode($_REQUEST['importComplete']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_IMPORT_BRANCH_FROM_EXCEL_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/importBranchFromExcel_Ctl.php" class="form-horizontal" enctype="multipart/form-data" method="post">
                                                        <div class="form-body">
                                                            <h3 class="form-section">Import Franchise And Branch from excel file.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 70px;">
                                                                    <label class="col-md-3 control-label font1emGray">Document File</label>
                                                                    <div class="col-md-4">
                                                                        <input type="file" name="textFile[]" id="textFile" class="form-control input-circle font1emGray" required>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <h3 class="form-section">Note.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 50px;">
                                                                    <div class="col-md-12">
                                                                        <ul class=" font1emGray" style="line-height: 30px;">
                                                                            <li>ไฟล์ที่ทำการ Upload จะต้องเป็นไฟล์ที่เป็นของ Microsoft Excel เท่านั้นค่ะ.</li>
                                                                            <li>ภายในไฟล์สามารถมีได้หลาย Sheet แต่โปรแกรมจะอ่านเฉพาะ Sheet ที่ 1 เท่านั้นค่ะ</li>
                                                                            <li>ตำแหน่งของ Column ภายใน Sheet จะต้องเรียงตามลำดับเหมือนในตัวอย่างเพื่อไม่ให้มีความผิดพลาดในการ Import ข้อมูลค่ะ.</li>
                                                                            <li>สามารถ Download ตัวอย่างของไฟล์ได้ที่นี่ค่ะ.&nbsp;&nbsp;&nbsp;<a href="upload/example.xlsx" class="input-circle" style="color:red;">ตัวอย่าง Import File</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;นำเข้าข้อมูล&nbsp;&nbsp;</button>
                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>