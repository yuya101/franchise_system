                                            
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite">ระบบเพิ่มข้อมูล Customer สำหรับทดสอบระบบ</label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addCustomerData_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <?php if(isset($_REQUEST['confirmAddCusOK'])){ ?>
                                                                <div class="alert alert-success font1emNoColor">
                                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล Customer : <?php echo base64_decode($_REQUEST['username']); ?> เรียบร้อยแล้วค่ะ.
                                                                </div>
                                                            <?php } ?>
                                                            <?php if(isset($_REQUEST['errUserDup'])){ ?>
                                                                <div class="alert alert-danger font1emNoColor">
                                                                    <strong>ผิดพลาด!</strong> Customer Name : <?php echo base64_decode($_REQUEST['userDup']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                                </div>
                                                            <?php } ?>
                                                            <?php if(isset($_REQUEST['errEmailDup'])){ ?>
                                                                <div class="alert alert-danger font1emNoColor">
                                                                    <strong>ผิดพลาด!</strong> Email : <?php echo base64_decode($_REQUEST['emailDup']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                                </div>
                                                            <?php } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">User Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="username" id="username" class="form-control input-circle font1emGray" placeholder="กรุณากรอก Username" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email Address</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอก Email Address" required> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์มือถือ</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="mobile" id="mobile" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเบอร์มือถือ" maxlength="20" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อจริง</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="กรุณากรอกชื่อจริง" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุลจริง</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="กรุณากรอกนามสกุลจริง" required>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หมายเลขห้อง</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="room" id="room" class="form-control input-circle font1emGray" placeholder="กรุณากรอกหมายเลขห้อง" maxlength="20" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อาคาร</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="building" id="building" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอาคาร" maxlength="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หมู่บ้าน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="village" id="village" class="form-control input-circle font1emGray" placeholder="กรุณากรอกหมู่บ้าน" maxlength="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="address" id="address" class="form-control input-circle font1emGray" placeholder="กรุณากรอกที่อยู่" maxlength="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ซอย</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="soi" id="soi" class="form-control input-circle font1emGray" placeholder="กรุณากรอกซอย" maxlength="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หมู่</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="mou" id="mou" class="form-control input-circle font1emGray" placeholder="กรุณากรอกหมู่" required maxlength="20">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ถนน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="street" id="street" class="form-control input-circle font1emGray" placeholder="กรุณากรอกถนน" required maxlength="200">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ตำบล</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="district" id="district" class="form-control input-circle font1emGray" placeholder="กรุณากรอกตำบล" maxlength="150" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อำเภอ</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="amphor" id="amphor" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอำเภอ" maxlength="150" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">จังหวัด</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="province" id="province" class="form-control input-circle font1emGray" placeholder="กรุณากรอกจังหวัด" maxlength="150" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสไปรษณีย์</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="postcode" id="postcode" class="form-control input-circle font1emGray" placeholder="กรุณากรอกรหัสไปรษณีย์" maxlength="5" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">โทรศัพท์บ้าน / ที่ทำงาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="telephone" id="telephone" class="form-control input-circle font1emGray" placeholder="กรุณากรอกโทรศัพท์บ้าน / ที่ทำงาน" maxlength="20" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">เพิ่มข้อมูล Customer</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>



                                            
                                            
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_TOPUP_DATA_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addTopupData_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <?php if(isset($_REQUEST['confirmAddTopUpDataOK'])){ ?>
                                                                <div class="alert alert-success font1emNoColor">
                                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล Top Up ของ Customer User Name : <?php echo base64_decode($_REQUEST['userNameTopUp']); ?> เรียบร้อยแล้วค่ะ.
                                                                </div>
                                                            <?php } ?>
                                                            <?php if(isset($_REQUEST['errUserNotMatch'])){ ?>
                                                                <div class="alert alert-danger font1emNoColor">
                                                                    <strong>ผิดพลาด!</strong> Customer Name : <?php echo base64_decode($_REQUEST['userNameNotMatch']); ?> ไม่มีในระบบ กรุณาตรวจสอบใหม่ค่ะ.
                                                                </div>
                                                            <?php } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer User Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="topup_username" id="topup_username" class="form-control input-circle font1emGray" placeholder="กรุณากรอก Customer User Name" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer Email Address</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="topup_email" id="topup_email" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอก Customer Email Address" required> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์มือถือของ Customer</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="topup_mobile" id="topup_mobile" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเบอร์มือถือของ Customer" maxlength="20" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">จำนวนเงินที่เติม (บาท)</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="topup_value" id="topup_value" class="form-control input-circle font1emGray" placeholder="กรุณากรอกจำนวนเงินที่เติม" min="1" max="100000" value="100" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">เพิ่มข้อมูล Topup</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>