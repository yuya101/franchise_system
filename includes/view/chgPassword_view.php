                                           <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงรหัสผ่านของท่านเรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errPassword'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> รหัสผ่านเดิมไม่ถูกต้อง กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_SESSION['forceChgPass'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> รหัสผ่านของท่านไม่ได้ถูกเปลี่ยนเกินกว่า 60 วัน. กรุณาเปลี่ยนรหัสผ่านใหม่ด้วยค่ะ !
                                                </div>
                                                <?php unset($_SESSION['forceChgPass']); ?>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_CHANGE_USER_PASSWORD_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/chgPass_Ctl.php" class="form-horizontal" method="post" onsubmit="return chkChgPassword(this);">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสผ่านเดิม</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="password" name="oldpassword" id="oldpassword" class="form-control input-circle-left font1emGray" placeholder="Old Password" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสผ่านใหม่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="password" name="newpassword" id="newpassword" class="form-control input-circle-left font1emGray" placeholder="New Password" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ยืนยันรหัสผ่านใหม่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="password" name="confirmpassword" id="confirmpassword" class="form-control input-circle-left font1emGray" placeholder="Confirm New Password" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite">ยืนยัน</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>