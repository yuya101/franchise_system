<?php  
$sql = "select * from db_withdraw_detail where withdraw_flag=2 order by adddate desc, addtime desc limit 2000";
$num = $mQuery->checkNumRows($sql);
?>
                                <div id="showTopUpSystemDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showTopUpSystemDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>
                                

                                
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_WITHDRAW_HISTORY_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="username"><label  class="font1emBlack">Username</label></th>
                                                    <th class="name"><label  class="font1emBlack">ชื่อ - นามสกุล</label></th>
                                                    <th class="none"><label  class="font1emBlack">Email Address</label></th>    
                                                    <th class="none"><label  class="font1emBlack">หมายเลขโทรศัพท์</label></th>
                                                    <th class="date"><label  class="font1emBlack">วัน - เวลาทำรายการ</label></th>
                                                    <th class="withdraw"><label  class="font1emBlack">เงินที่ถอนออกไป</label></th>
                                                    <th class="callcenter"><label  class="font1emBlack">พนักงานที่ดำเนินการเติมเงิน</label></th>
                                                    <th class="status"><label  class="font1emBlack">สถานะ</label></th>
                                                    <th class="none"><label  class="font1emBlack">วัน - เวลาที่ปิดงาน</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $withdrawID = $r['wid'];
                                                        $userID = $r['uid'];
                                                        $userName = $r['withdraw_user'];
                                                        $email = $r['withdraw_email'];
                                                        $mobile = $r['withdraw_mobile'];
                                                        $addDateAndTime = $dFunc->fullDateThai($r['adddate'])." - ".$r['addtime'];
                                                        $modDateAndTime = $dFunc->fullDateThai($r['moddate'])." - ".$r['modtime'];
                                                        $withdrawValue = number_format($r['withdraw_value'], 0);

                                                        $sql = "select user_real_name, user_real_lastname from db_user_detail where uid=".$userID;
                                                        $userFullName = "คุณ".$mQuery->getResultOneRecord($sql, "user_real_name")." ".$mQuery->getResultOneRecord($sql, "user_real_lastname");

                                                        $status = (int)$r['withdraw_flag'];

                                                        if($status == 0)
                                                        {
                                                            $statusText = "<label class='font1emRed'>รอการดำเนินการ</label>";
                                                        }
                                                        elseif($status == 1)
                                                        {
                                                            $statusText = "<label class='font1emOrange'>อยู่ระหว่างดำเนินการ</label>";
                                                        }
                                                        else
                                                        {
                                                            $statusText = "<label class='font1emBlue'>ปิดงาน</label>";
                                                        }  //----  if($status == 0)

                                                        $withdrawCallcenter = (int)$r['withdraw_aid'];

                                                        $sql = "select aname, alastname, ausername from db_admin where aid=".$withdrawCallcenter;
                                                        $num = $mQuery->checkNumRows($sql);

                                                        if($num > 0)
                                                        {
                                                                $withdrawCallCenter = $mQuery->getResultOneRecord($sql, "ausername")." ( คุณ".$mQuery->getResultOneRecord($sql, "aname")." ".$mQuery->getResultOneRecord($sql, "alastname")." )";
                                                        }
                                                        else
                                                        {
                                                                $serverCloseJobID = (int)substr($withdrawCallcenter, -3, 3);

                                                                $sql = "select sid, server_name from db_server where sid=".$serverCloseJobID;
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $withdrawCallCenter = $mQuery->getResultOneRecord($sql, "server_name");
                                                                }
                                                                else
                                                                {
                                                                    $withdrawCallCenter = "ไม่พบชื่อผู้ทำรายการเติมเงิน";
                                                                }  //------  if($num > 0)
                                                        }  //-----  if($num > 0)
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $userName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $userFullName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $email; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $mobile; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDateAndTime; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $withdrawValue; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $withdrawCallCenter; ?></label></td>
                                                    <td><?php echo $statusText; ?></td>
                                                    <td><label  class="font1emGray"><?php echo $modDateAndTime; ?></label></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->