<?php  
$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$dateMinusOne = $dFunc->datePlusDay($dateNow, -1, 0, 0);
$dateMinusSeven = $dFunc->datePlusDay($dateNow, -7, 0, 0);
$dateMinusThirdty = $dFunc->datePlusDay($dateNow, -30, 0, 0);

$yesterdayMinusOne = $dFunc->dateMinusDate($dateNow, 1);
$yesterdayMinusSeven = $dFunc->dateMinusDate($dateNow, 7);
$yesterdayMinusThirdty = $dFunc->dateMinusDate($dateNow, 30);

$dateShowNow = $dFunc->fullDateThai($dateNow);
$dateShowYesterday = $dFunc->fullDateThai($dateMinusOne);

$sql = "select mid, portal_name from db_money_portal order by mid";
$num = $mQuery->checkNumRows($sql);

if($num > 0)
{
    $result = $mQuery->getResultAll($sql);
    $i = 0;

    foreach($result as $r)
    {
        $mid[$i] = $r['mid'];
        $paymentName[$i] = $r['portal_name'];

        $sql = "select wid from db_withdraw_detail where withdraw_flag=2 and money_portal_id=".$mid[$i]." and adddate='".$dateNow."'";
        $withdrawTransactionToday[$i] = intval($mQuery->checkNumRows($sql));

        $sql = "select wid from db_withdraw_detail where withdraw_flag=2 and money_portal_id=".$mid[$i]." and adddate='".$dateMinusOne."'";
        $withdrawTransactionYesterday[$i] = intval($mQuery->checkNumRows($sql));

        $withdrawDiffTransaction[$i] = $withdrawTransactionToday[$i] - $withdrawTransactionYesterday[$i];


        $sql = "select SUM(withdraw_value) as withdraw_summary from db_withdraw_detail where withdraw_flag=2 and money_portal_id=".$mid[$i]." and adddate='".$dateNow."'";
        $withdrawVolumnToday[$i] = intval($mQuery->getResultOneRecord($sql, "withdraw_summary"));

        $sql = "select SUM(withdraw_value) as withdraw_summary from db_withdraw_detail where withdraw_flag=2 and money_portal_id=".$mid[$i]." and adddate='".$dateMinusOne."'";
        $withdrawVolumnYesterday[$i] = intval($mQuery->getResultOneRecord($sql, "withdraw_summary"));

        $withdrawDiffVolumn[$i] = $withdrawVolumnToday[$i] - $withdrawVolumnYesterday[$i];

        $i++;
    }  //-----  foreach($result as $r)

    unset($result, $r);
}  //--------  if($num > 0)
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_WITHDRAW_BY_PAYMENT_SYSTEM_TITLE; ?>&nbsp;(Transaction และ Volumn)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="withdrawByPaymentSystem_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupByPaymentSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">ช่องทางการถอนเงิน</label></th>
                                                    <th class="transaction" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Transaction (รายการ)</label></th>
                                                    <th class="volumn" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Volumn (บาท)</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeTrans" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeVolumn" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php for($i=0; $i< count($mid); $i++){ ?>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $paymentName[$i]; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTransactionToday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTransactionYesterday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffTransaction[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawVolumnToday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawVolumnYesterday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffVolumn[$i], 0); ?></label></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์จำนวน Transaction ผ่านช่องทางการถอนเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="withdrawByPaymentSystemTransaction" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์รายจ่ายผ่านช่องทางการถอนเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="withdrawByPaymentSystemVolumn" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->