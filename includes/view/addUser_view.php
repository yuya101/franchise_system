<?php  
$sql = "select * from db_user_group order by gid";
$num = $mQuery->checkNumRows($sql);
?> 
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล User : <?php echo base64_decode($_REQUEST['username']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errEmail'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> Email : <?php echo base64_decode($_REQUEST['email']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errName'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> ชื่อ : <?php echo base64_decode($_REQUEST['username']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_USER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addUser_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <!-- <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">First Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="firstname" class="form-control input-circle font1emGray">
                                                                        <option value="นาย" selected>นาย</option>
                                                                        <option value="นาง">นาง</option>
                                                                        <option value="นางสาว">นางสาว</option>
                                                                    </select>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="cusno" id="cusno" class="form-control input-circle font1emGray" placeholder="Please! Enter customer number." required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer Name.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="Please! Enter customer name." required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer LastName.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="Please! Enter customer lastname." required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email Address.</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="Please! Enter email address." required> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Password.</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="password" name="password" id="password" class="form-control input-circle-left font1emGray" placeholder="Please! Enter customer password." required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Authorization</label>
                                                                <div class="col-md-4">
                                                                    <select name="userauth" id="userauth" class="form-control input-circle font1emGray">
                                                                    <?php if($num > 0){ ?>
                                                                        <?php 
                                                                            $result = $mQuery->getResultAll($sql); 

                                                                            foreach($result as $r)
                                                                            {
                                                                        ?>
                                                                                <option value="<?php echo $r["gid"] ?>"><?php echo $r['group_name'] ?></option>
                                                                        <?php
                                                                            }  //------  foreach($result as $r)
                                                                        ?>
                                                                    <?php }  //-----  if($num > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ยืนยัน</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>