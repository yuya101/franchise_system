<?php  
$docTypeID = 1;

if(isset($_REQUEST['bid'])){
    $brandIDLink = base64_decode($_REQUEST['bid']);
    $yearLink = base64_decode($_REQUEST['y']);
}else{
    $brandIDLink = "";
}    //-------  if(isset($_REQUEST['bid']))

$chkArrBrandIDEmpty = array_filter($_SESSION['arrBrandID']);
$chkArrBranchIDEmpty = array_filter($_SESSION['arrBranchID']);

if($brandIDLink == ""){
    $sql = "select * from db_document where type_id=".$docTypeID." order by adddate desc";
}else{
    $sql = "select * from db_document where type_id=".$docTypeID." and brand_id=".$brandIDLink." and year='".$yearLink."' order by adddate desc";
}  //------  if($brandIDLink == "")

$num = $mQuery->checkNumRows($sql);
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $pageTitle; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="typename"><label  class="font1emBlack">Type Name</label></th>
                                                    <th class="categoryname"><label  class="font1emBlack">Category Name</label></th>
                                                    <th class="doctitle"><label  class="font1emBlack">Document Title</label></th>
                                                    <th class="docmonth"><label  class="font1emBlack">Document Month</label></th>
                                                    <th class="filename"><label  class="font1emBlack">File Name</label></th>
                                                    <th class="count"><label  class="font1emBlack">Download Count</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $docID = $r['did'];
                                                        $chkAuth = 0;

                                                        $sqlChkAuth = "select daid, brand_id from db_document_authorize where did=".$docID." and type_id=".$docTypeID." and uaid=0";
                                                        $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                        if($numChkAuth > 0){
                                                            if(!empty($chkArrBrandIDEmpty)){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID'])){
                                                                        $chkAuth = 1;
                                                                    }  //------  if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID']))
                                                                }  //-----  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------  if(!empty($chkArrEmpty))
                                                        }else{
                                                            $sqlChkAuth = "select uaid from db_document_authorize where did=".$docID." and type_id=".$docTypeID;
                                                            $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                            if($numChkAuth > 0){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(!empty($chkArrBranchIDEmpty)){
                                                                        if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID'])){
                                                                            $chkAuth = 1;
                                                                        }  //-----  if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID']))
                                                                    }  //------  if(!empty($chkArrEmpty))
                                                                }  //------  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------ if($numChkAuth > 0)
                                                        }  //---  if($numChkAuth > 0)

                                                        if($chkAuth == 1){
                                                            $catID = $r['cat_id'];
                                                            $typeID = $r['type_id'];
                                                            $title = $r['title'];
                                                            $fileName = $r['file_name'];
                                                            $filePath = $r['file_path'];
                                                            $docMonth = $r['month'];
                                                            $docYear = $r['year'];

                                                            $filePathNum = stripos($r['file_path'], "upload");
                                                            $filePath = substr($r['file_path'], $filePathNum);

                                                            $forDate = $dFunc->showMonthYearInEnglish($r['year'].$r['month']);

                                                            $sql = "select type_name from db_document_type where type_id=".$typeID;
                                                            $typeName = $mQuery->getResultOneRecord($sql, "type_name");

                                                            $sql = "select cat_name from db_document_category where cat_id=".$catID;
                                                            $catName = $mQuery->getResultOneRecord($sql, "cat_name");

                                                            $sql = "select dhid from db_user_download_history where document_id=".$docID;
                                                            $downloadCount = number_format($mQuery->checkNumRows($sql), 0);
                                            ?>
                                                        <tr>
                                                            <td><label  class="font1emGray"><?php echo $typeName; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $catName; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $title; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $docMonth."/".$docYear; ?></label></td>
                                                            <td><label  class="font1emGray"><a href="<?php echo $filePath.$fileName; ?>" target="_blank" onclick="countDownload(<?php echo $_SESSION['mLoginID']; ?>, <?php echo $docID; ?>);"><?php echo $fileName; ?></a></label></td>
                                                            <td><label  class="font1emGray"><?php echo number_format($downloadCount, 0); ?></label></td>
                                                        </tr>
                                                    <?php }  //------  if($chkAuth == 1) ?>
                                                <?php }  //-------  foreach($result as $r) ?>
                                                <?php unset($result, $r); ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->