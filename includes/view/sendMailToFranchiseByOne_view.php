                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> ส่ง E-mail ให้กับ E-mail ผู้รับ เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_SEND_MAIL_FRANCHISE_BY_ONE_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"></a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/sendMailToFranchiseByOne_Ctl.php" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        <div class="form-body" id="listProductLI1">
                                                            <h3 class="form-section">Send Mail Section.</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Receiver E-Mail</label>
                                                                <div class="col-md-4">
                                                                    <input type="email" name="receiveEmail" id="receiveEmail" class="form-control input-circle font1emGray" placeholder="Ex : admin@mfgfranchise.com" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="fileDataDetailDiv" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Subject</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="title" id="title" class="form-control input-circle font1emGray" placeholder="Please! Enter Document Title" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Detail</label>
                                                                    <div class="col-md-9">
                                                                        <!-- <textarea name="description" id="description" class="form-control input-circle font1emGray" rows="10" placeholder="Please! Enter Document Description" required></textarea> -->
                                                                        <textarea class="wysihtml5 form-control font1emGray" name="description" id="description" rows="6" cols="20" placeholder="Please! Enter E-Mail Description" required></textarea>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;Send E-mail&nbsp;&nbsp;</button>
                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>