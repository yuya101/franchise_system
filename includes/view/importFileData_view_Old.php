                                            <?php if(isset($_REQUEST['importComplete'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> นำเข้าข้อมูล Document Type จาก FTP เรียบร้อย นำเข้าทั้งหมด : <?php echo base64_decode($_REQUEST['importComplete']); ?> รายการค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_IMPORT_FILE_DATA_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/importFileData_Ctl.php" class="form-horizontal" enctype="multipart/form-data" method="post">
                                                        <div class="form-body">
                                                            <h3 class="form-section">Import Document File From Upload Folder.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 70px;">
                                                                    <label class="col-md-3 control-label font1emGray"></label>
                                                                    <div class="col-md-4">
                                                                        <input type="submit" name="importBtn" id="importBtn" class="form-control btn btn-circle green font1emWhite" value="Click! Import Data From Folder" onclick="return confirm('Confirm Import Document File !');">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <h3 class="form-section">Note.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 50px;">
                                                                    <div class="col-md-12">
                                                                        <ul class=" font1emGray" style="line-height: 30px;">
                                                                            <li>ท่านจะต้องทำการนำข้อมูลเข้าสู่ Folder ผ่านทางการ FTP ให้เรียบร้อยก่อนค่ะ.</li>
                                                                            <li>ท่านต้องนำไฟล์ Micosoft Excel ตามรูปแบบที่กำหนดใส่ไว้พร้อมกันใน Folder ที่ทำการ FTP ด้วยค่ะ.</li>
                                                                            <li>ชื่อไฟล์ของ Microsoft Excel จะต้องใช้ชื่อ importFile.xlsx เท่านั้นค่ะ.</li>
                                                                            <li>สามารถ Download ตัวอย่างของไฟล์ Excel ได้ที่นี่ค่ะ.&nbsp;&nbsp;&nbsp;<a href="upload/importFile.xlsx" class="input-circle" style="color:red;">ตัวอย่าง Import File</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>