<?php include("queryFatherMother.php"); ?>
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูลไก่ : <?php echo base64_decode($_REQUEST['name']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errName'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> ชื่อ : <?php echo base64_decode($_REQUEST['username']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite">
                                                                    <?php if($pageID == 1){ ?>
                                                                        <?php echo PAGE_REGISTER_FATHER_TITLE; ?>
                                                                    <?php }elseif($pageID == 2){ ?>
                                                                        <?php echo PAGE_REGISTER_MOTHER_TITLE; ?>
                                                                    <?php }else{ ?>
                                                                        <?php echo PAGE_REGISTER_TITLE; ?>
                                                                    <?php } ?>
                                                        </label> 
                                                    </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addRegisterChicken_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สายพันธุ์</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="strain" id="strain" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดสายพันธุ์" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-eyedropper"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ซื้อมาจาก</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="buyfrom" id="buyfrom" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดซื้อมาจาก" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ขายให้</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="saleto" id="saleto" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดขายให้">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-pencil-square-o"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อไก่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="chickenname" id="chickenname" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกชื่อไก่" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-language"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เพศไก่</label>
                                                                <div class="col-md-4">
                                                                    <select name="sex" class="form-control input-circle font1emGray">
                                                                    <?php if($pageID == 1){ ?>
                                                                        <option value="1" selected>เพศผู้</option>
                                                                    <?php }elseif($pageID == 2){ ?>
                                                                        <option value="2">เพศเมีย</option>
                                                                    <?php }else{ ?>
                                                                        <option value="1" selected>เพศผู้</option>
                                                                        <option value="2">เพศเมีย</option>
                                                                    <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกไก่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-cubes"></i>
                                                                        </span>
                                                                        <input type="text" name="chickenid" id="chickenid" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอกรหัสปีกไก่" value="" maxlength="10" required> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกพ่อ</label>
                                                                <div class="col-md-4">
                                                                    <select name="fatherid" class="form-control input-circle font1emGray">
                                                                        <option value="0" selected>ไม่มีพ่อพันธุ์</option>
                                                                        <?php if($numFather > 0){ ?>
                                                                            <?php for($i=0; $i<$numFather; $i++){ ?>
                                                                                <option value="<?php echo $fatherID[$i]; ?>"><?php echo $fatherRealID[$i]." (".$fatherName[$i].")"; ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกแม่</label>
                                                                <div class="col-md-4">
                                                                    <select name="motherid" class="form-control input-circle font1emGray">
                                                                        <option value="0" selected>ไม่มีแม่พันธุ์</option>
                                                                        <?php if($numMother > 0){ ?>
                                                                            <?php for($i=0; $i<$numMother; $i++){ ?>
                                                                                <option value="<?php echo $motherID[$i]; ?>"><?php echo $motherRealID[$i]." (".$motherName[$i].")"; ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รุ่นที่</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="generation" id="generation" min="0" max="20000" class="form-control input-circle font1emGray" value="1" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">จำนวนลูกในครอกเดียวกัน</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="familyno" id="familyno" min="0" max="2000" class="form-control input-circle font1emGray" value="1" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หมายเหตุ</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="comment" id="comment" class="form-control input-circle font1emGray" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">เพิ่มข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="pageid" value="<?php echo $pageID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>