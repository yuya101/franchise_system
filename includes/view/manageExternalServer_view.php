<?php  
$sql = "select * from db_server order by server_name";
$num = $mQuery->checkNumRows($sql);
?>


                                <div id="showServerDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showServerDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>

                                
                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูล Server : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูล Server : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_MANAGE_SERVER_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="servername"><label  class="font1emBlack">Server Name</label></th>
                                                    <th class="serverid"><label  class="font1emBlack">Server ID</label></th>
                                                    <th class="keypass"><label  class="font1emBlack">Key Pass</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่เพิ่มข้อมูล</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่แก้ไขล่าสุด</label></th>
                                                    <th class="none"><label  class="font1emBlack">URL</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $serverName = $r['server_name'];
                                                        $serverID = $r['serial_id'];
                                                        $keypass = $r['keypass'];
                                                        $addDate = $dFunc->fullDateThai($r['adddate']);
                                                        $modDate = $dFunc->fullDateThai($r['moddate']);
                                                        $serverURL = "http://".$_SERVER['HTTP_HOST']."/includes/webservice/topUpWebService.php";
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $serverName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $serverID; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $keypass; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDate; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $modDate; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $serverURL; ?></label></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $r['sid']; ?>, 'showServerDetail', 'ShowServerDetail');">แก้ไข</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->