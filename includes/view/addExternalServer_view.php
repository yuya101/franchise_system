<?php $autoKeyPass = base64_encode($dateNow.$timeNow);  ?>
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล Server : <?php echo base64_decode($_REQUEST['server']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errServer'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> Server ID : <?php echo base64_decode($_REQUEST['server']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_SERVER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addExternalServer_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Server Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="servername" id="servername" maxlength="200" class="form-control input-circle font1emGray" placeholder="Ex : Garena Server" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Server ID</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="serverid" id="serverid" maxlength="20" class="form-control input-circle font1emGray" placeholder="Ex : SV000001" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Key Pass</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="keypass" id="keypass" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="Ex : <?php echo $autoKeyPass; ?>"  required value="<?php echo $autoKeyPass; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-5 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ยืนยัน</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>