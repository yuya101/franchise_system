                                            <?php if(isset($_REQUEST['importComplete'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> นำเข้าข้อมูล Document Type จาก FTP เรียบร้อย นำเข้าทั้งหมด : <?php echo base64_decode($_REQUEST['importComplete']); ?> รายการค่ะ.
                                                </div>
                                            <?php } ?>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <form action="includes/control/importFileData_Ctl.php" class="dropzone dropzone-file-area" style="border: 2px dashed #028AF4; margin-top: 50px;" style=" margin-top: 50px;">
                                                                        <h3 class="font2emNoColor">กรุณาวางไฟล์หรือเลือกไฟล์ที่ท่านต้องการอัพโหลดค่ะ.</h3>
                                                                        <p class="font1emBlue"> เมื่อท่านกดเลือกไฟล์เรียบร้อยแล้ว ระบบจะนำเข้าให้อัตโนมัติ โดยไม่ต้องกดปุ่มใดๆทั้งสิ้นค่ะ.</p>
                                                                        <div class="fallback">
                                                                            <input name="file" type="file" multiple />
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>


                                                            <h3 class="form-section" style="margin-top: 100px;">Note.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 50px;">
                                                                    <div class="col-md-12">
                                                                        <ul class=" font1emGray" style="line-height: 30px;">
                                                                            <li>ท่านจะต้องเตรียม File ที่จะทำการ Upload ให้ตรงกับ Format ที่ต้องการค่ะ.</li>
                                                                            <li>Format ของ File ที่จะนำเข้า คือ <label class="font1emRed">[รหัสสาขา 4 หลัก][ปีค.ศ. 4 หลัก][เดือน 2 หลัก][รหัสรูปแบบของเอกสาร 2 หลัก]</label> เช่น D02120180501.PDF เป็นต้นค่ะ.</li>
                                                                            <li>เมื่อท่านเลือกไฟล์ที่ต้องการ Upload ระบบจะนำเข้าและสร้างรายละเอียดทั้งหมดให้อัตโนมัติค่ะ.</li>
                                                                            <li>หากรูปแบบชื่อ File ไม่เป็นไปตามที่กำหนด File จะไม่ถูกนำเข้าและสร้างในระบบค่ะ.</li>
                                                                            <li>ขนาดของ File ที่จะ Upload ต้อง <label class="font1emRed">ไม่เกิน 2 MB. ต่อ 1 ไฟล์</label> ค่ะ</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>