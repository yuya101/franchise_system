                                        <div id="responsive" class="modal fade" tabindex="-1" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                <form action="includes/control/sendMailHelp_Ctl.php" class="form-horizontal" method="post" enctype="multipart/form-data" target="_top">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title font1emNoColor">ติดต่อสอบถามข้อมูล</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="scroller" style="height:250px" data-always-visible="1" data-rail-visible1="1">
                                                                        <div class="form-group" style="padding-bottom: 10px !important;">
                                                                            <label class="col-md-3 control-label font1emGray">หัวข้อ</label>
                                                                            <div class="col-md-9">
                                                                                <input type="text" name="helpTopic" id="helpTopic" class="form-control input-circle font1emGray" placeholder="กรุณาใส่หัวข้อที่ต้องการสอบถามค่ะ !" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label font1emGray">รายละเอียด</label>
                                                                            <div class="col-md-9">
                                                                                <textarea name="helpDetail" id="helpDetail" class="form-control input-circle font1emGray" rows="8" placeholder="กรุณาใส่รายละเอียดที่ต้องการสอบถามค่ะ !" required="required"></textarea>
                                                                            </div>
                                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal" class="btn dark btn-outline font1emNoColor">ยกเลิก</button>
                                                        <button type="submit" class="btn green font1emNoColor">ส่งรายละเอียด</button>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        </div>