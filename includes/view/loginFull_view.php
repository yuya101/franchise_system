<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo $webTitle; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="<?php echo $webTitle; ?>" name="description" />
        <meta content="idaeDesign" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout/css/mainStyle.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset mt-login-5-bsfix">
                    <div class="login-bg" style="background-image:url(assets/pages/img/login/bg1.jpg)">
                        <!-- <img class="login-logo" src="assets/pages/img/login/logo.png" /> --> </div>
                </div>
                <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <h1 class="font1emNoColor"><?php echo $webTitle; ?></h1>
                        <p class="font1emNoColor"> กรุณา Log In เพื่อเข้าสู่ระบบด้วยค่ะ. </p>
                        <form action="includes/control/login_Ctl.php" class="login-form" method="post">
                            <?php if(isset($_REQUEST['errLogin'])){ ?>
                                <div class="alert alert-danger">
                                    <button class="close" data-close="alert"></button>
                                    <span class="font1emNoColor">Username หรือ Password ไม่ถูกต้อง กรุณาลองใหม่อีกครั้งค่ะ. </span>
                                </div>
                            <?php } ?>
                            <?php if(isset($_REQUEST['errEmail'])){ ?>
                                <div class="alert alert-danger" style="margin-top: 20px; margin-bottom: 20px;">
                                    <button class="close" data-close="alert"></button>
                                    <span class="font1emNoColor">ไม่พบ Email ของท่าน กรุณาตรวจสอบใหม่อีกครั้งค่ะ. </span>
                                </div>
                            <?php } ?>
                            <?php if(isset($_REQUEST['sendComplete'])){ ?>
                                <div class="alert alert-success" style="margin-top: 20px; margin-bottom: 20px;">
                                    <button class="close" data-close="alert"></button>
                                    <span class="font1emNoColor">ดำเนินการส่ง รหัสผ่าน ให้ท่านเรียบร้อย กรุณาตรวจสอบ Email ที่ลงทะเบียนไว้ค่ะ. </span>
                                </div>
                            <?php } ?>
                            <div class="alert alert-danger display-hide" style="margin-top:5px;">
                                <button class="close" data-close="alert"></button>
                                <span class="font1emNoColor">กรุณากรอก Username และ Password ด้วยค่ะ. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group font1emNoColor" type="text" autocomplete="off" placeholder="Username" name="username" required/> </div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group font1emNoColor" type="password" autocomplete="off" placeholder="Password" name="password" required/> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="rem-password">
                                        <label class="rememberme mt-checkbox mt-checkbox-outline font1emNoColor">
                                            <input type="checkbox" name="remember" value="1" /> Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password">
                                        <a href="javascript:;" id="forget-password" class="forget-password font1emNoColor">ลืมรหัสผ่าน?</a>
                                    </div>
                                    <button class="btn green font1emNoColor" type="submit">เข้าสู่ระบบ</button>
                                </div>
                            </div>
                        </form>
                        <!-- BEGIN FORGOT PASSWORD FORM -->
                        <form class="forget-form" action="includes/control/resetPassword_Ctl.php" method="post">
                            <h3 class="font-green font2emNoColor">ลืมรหัสผ่าน ?</h3>
                            <p class="font1emNoColor"> กรุณากรอก Email ของท่านเพื่อทำการ reset password ค่ะ. </p>
                            <?php if(isset($_REQUEST['errEmail'])){ ?>
                                <div class="alert alert-danger" style="margin-top: 20px; margin-bottom: 20px;">
                                    <button class="close" data-close="alert"></button>
                                    <span class="font1emNoColor">ไม่พบ Email ของท่าน กรุณาตรวจสอบใหม่อีกครั้งค่ะ. </span>
                                </div>
                            <?php } ?>
                            <?php if(isset($_REQUEST['sendComplete'])){ ?>
                                <div class="alert alert-success" style="margin-top: 20px; margin-bottom: 20px;">
                                    <button class="close" data-close="alert"></button>
                                    <span class="font1emNoColor">ดำเนินการส่ง รหัสผ่าน ให้ท่านเรียบร้อย กรุณาตรวจสอบ Email ที่ลงทะเบียนไว้ค่ะ. </span>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group font1emNoColor" type="email" autocomplete="off" placeholder="Email ของท่าน" name="email" required /> </div>
                            <div class="form-actions">
                                <button type="button" id="back-btn" class="btn green btn-outline font1emNoColor">กลับสู่ระบบ Login</button>
                                <button type="submit" class="btn btn-success uppercase pull-right font1emNoColor">ยืนยัน</button>
                            </div>
                        </form>
                        <!-- END FORGOT PASSWORD FORM -->
                        
                        <div style="margin-top: 50px; margin-bottom: 20px;">
                                <span class="font1emNoColor"><u>วิธีการใช้งานระบบ Accounting E-Doc</u></span>
                        </div>
                        <div style="margin-top: 20px;">
                                <span class="font1emNoColor">
                                    <a href="upload/Acct_Tax_Doc_Franchiseweb.pdf" target="_blank">
                                        1. เอกสารคู่มือการใช้งาน
                                    </a>
                                </span>
                        </div>
                        <div style="margin-top: 20px; margin-bottom: 20px;">
                                <span class="font1emNoColor">
                                    <a href="https://youtu.be/zcpf5PC2xD4" target="_blank">
                                        2. วีดีโอการใช้งาน
                                    </a>
                                </span>
                        </div>
                    </div>
                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div class="col-xs-5 bs-reset">
                                <!-- <ul class="login-social">
                                    <li>
                                        <a href="_upload/Franchise_System_Manual.mp4" target="_blank">
                                            <i class="icon-social-facebook">วีดีโอการใช้งาน</i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="_upload/Acct_Tax_Doc_Franchiseweb.pptx" target="_blank">
                                            <i class="icon-social-twitter">คู่มือการใช้งาน</i>
                                        </a>
                                    </li>
                                </ul> -->
                            </div>
                            <div class="col-xs-7 bs-reset">
                                <div class="login-copyright text-right">
                                    <p>Copyright &copy; <?php echo $programName; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<script src="assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>