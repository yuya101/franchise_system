<?php  
$showPathText = "";
$dateNow = $dFunc->getDateChris();

$inqType = $mFunc->chgSpecialCharInputText($_REQUEST['inqType']);
$inqType = (int)base64_decode($inqType);
$inqBranchID = $mFunc->chgSpecialCharInputText($_REQUEST['branchID']);
$inqBranchID = base64_decode($inqBranchID);
$inqYear = $mFunc->chgSpecialCharInputText($_REQUEST['year']);
$inqYear = base64_decode($inqYear);
$inqMonth = $mFunc->chgSpecialCharInputText($_REQUEST['month']);
$inqMonth = base64_decode($inqMonth);


if ($inqType == 2) {  //-----  Case Credit Card
    $rowCount = 0;
    $statementAmountSumAll = 0;
    $feeAmountSumAll = 0;
    $taxAmountSumAll = 0;
    $netAmountSumAll = 0;

    $tmpSQL = "db_inquiry_credit_card where year='".$inqYear."' and month='".$inqMonth."' and aid='".$inqBranchID."'";

    $sql = "select business_unit, site_name from ".$tmpSQL." group by business_unit";
    $num = $mQuery->checkNumRows($sql);

    if($num > 0){
        $businessUnit = $mQuery->getResultOneRecord($sql, "business_unit");
        $siteName = $mQuery->getResultOneRecord($sql, "site_name");

        $sql = "select channel_type from ".$tmpSQL." group by channel_type order by channel_type";
        $num = $mQuery->checkNumRows($sql);

        if ($num > 0) {
            $result = $mQuery->getResultAll($sql);
            $channelTypeArr = array();

            foreach ($result as $r) {
                array_push($channelTypeArr, $r['channel_type']);
            }  //---------  foreach ($result as $r) 

            unset($result, $r);

            if(sizeof($channelTypeArr) > 0){
                $m = 0;
                foreach ($channelTypeArr as $ct) {
                    $sql = "select net_amount from ".$tmpSQL." and channel_type='".$ct."' order by business_date";
                    $num = $mQuery->checkNumRows($sql);
                    $rowNoSumCount[$m] = $num;
                    $rowCount = $rowCount + $num + 1;  //----- +1 For Summary Row
                    $m++;
                }  //---------  foreach ($channelTypeArr as $ct)

                unset($ct);

                if($rowCount > 0){

                    $showPathText = $pageTitle." / Statement Commission / ".$businessUnit." / ".$inqYear." / ".$inqMonth;
                ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                        <div class="tools"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="creditTB">
                                                <thead>
                                                    <tr>
                                                        <th><label  class="font1emBlack">Business Unit</label></th>
                                                        <th><label  class="font1emBlack">Site Name</label></th>
                                                        <th><label  class="font1emBlack">Channel Type</label></th>
                                                        <th><label  class="font1emBlack">Business Date</label></th>
                                                        <th><label  class="font1emBlack">Statement Date</label></th>
                                                        <th><label  class="font1emBlack">Statement Amount</label></th>
                                                        <th><label  class="font1emBlack">Fee Amount</label></th>
                                                        <th><label  class="font1emBlack">Tax Amount</label></th>
                                                        <th><label  class="font1emBlack">Net Amount</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td rowspan="<?php echo $rowCount; ?>"><label  class="font1emGray"><?php echo $businessUnit; ?></label></td>
                                                        <td rowspan="<?php echo $rowCount; ?>"><label  class="font1emGray"><?php echo $siteName; ?></label></td>
                                                        <?php
                                                        $m = 0;
                                                        $rowSpanUseCount = 0;

                                                        foreach ($channelTypeArr as $ct) {
                                                            $statementAmountSumSection = 0;
                                                            $feeAmountSumSection = 0;
                                                            $taxAmountSumSection = 0;
                                                            $netAmountSumSection = 0;

                                                            if($m > 0){ 
                                                                echo "<tr>";
                                                                $rowSpanUseCount = 0;
                                                            }  //-----  if($m > 0)
                                                        ?>
                                                            <td rowspan="<?php echo $rowNoSumCount[$m]; ?>"><label  class="font1emGray"><?php echo $ct; ?></label></td>
                                                        <?php
                                                                $sql = "select business_date, statement_date, statement_amount, fee_amount, tax_amount, net_amount from ".$tmpSQL." and channel_type='".$ct."' order by business_date";
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0){
                                                                    $resultInq = $mQuery->getResultAll($sql);

                                                                    foreach ($resultInq as $rq) {
                                                                        if(($rowSpanUseCount < $rowNoSumCount[$m]) and ($rowSpanUseCount  != 0)){echo "<tr>";}
                                                                    ?>
                                                                        <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($rq['business_date']); ?></label></td>
                                                                        <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($rq['statement_date']); ?></label></td>
                                                                        <td><label  class="font1emGray"><?php echo number_format($rq['statement_amount'], 2); ?></label></td>
                                                                        <td><label  class="font1emGray"><?php echo number_format($rq['fee_amount'], 2); ?></label></td>
                                                                        <td><label  class="font1emGray"><?php echo number_format($rq['tax_amount'], 2); ?></label></td>
                                                                        <td><label  class="font1emGray"><?php echo number_format($rq['net_amount'], 2); ?></label></td>
                                                                    </tr>
                                                                    <?php
                                                                        $statementAmountSumAll = $statementAmountSumAll + $rq['statement_amount'];
                                                                        $feeAmountSumAll = $feeAmountSumAll + $rq['fee_amount'];
                                                                        $taxAmountSumAll = $taxAmountSumAll + $rq['tax_amount'];
                                                                        $netAmountSumAll = $netAmountSumAll + $rq['net_amount'];

                                                                        $statementAmountSumSection = $statementAmountSumSection + $rq['statement_amount'];
                                                                        $feeAmountSumSection = $feeAmountSumSection + $rq['fee_amount'];
                                                                        $taxAmountSumSection = $taxAmountSumSection + $rq['tax_amount'];
                                                                        $netAmountSumSection = $netAmountSumSection + $rq['net_amount'];

                                                                        $rowSpanUseCount++;
                                                                    }  //-----  foreach ($resultInq as $rq)

                                                                    ?>
                                                                    <tr style="background-color: #9E9E9E;">
                                                                        <td colspan="3"><label  class="font1emBlack"><?php echo $ct." Total"; ?></label></td>
                                                                        <td><label  class="font1emBlack"><?php echo number_format($statementAmountSumSection, 2); ?></label></td>
                                                                        <td><label  class="font1emBlack"><?php echo number_format($feeAmountSumSection, 2); ?></label></td>
                                                                        <td><label  class="font1emBlack"><?php echo number_format($taxAmountSumSection, 2); ?></label></td>
                                                                        <td><label  class="font1emBlack"><?php echo number_format($netAmountSumSection, 2); ?></label></td>
                                                                    </tr>
                                                                    <?php
                                                                    unset($resultInq, $rq);
                                                                }  //------  if($num > 0)

                                                            $m++;
                                                        }  //-------  foreach ($channelTypeArr as $ct)

                                                        unset($ct);
                                                        ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="background-color: #4CAF50;">
                                                        <td colspan="5"><label class="font1emBlack"><?php echo $businessUnit." Total"; ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($statementAmountSumAll, 2); ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($feeAmountSumAll, 2); ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($taxAmountSumAll, 2); ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($netAmountSumAll, 2); ?></label></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                <?php
                }  //------  if($rowCount > 0)
            }  //---------  if(sizeof($channelTypeArr) > 0)

            unset($channelTypeArr);
        }  //--------  if ($num > 0)
    }  //------  if($num > 0)
}  //-------  if ($inqType == 1)
?>