<?php
$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$sql = "select aid from db_user";
$allUserInSystem = number_format($mQuery->checkNumRows($sql), 0);

$sql = "select ahid from db_user_login_history where logindate='".$dateNow."'";
$userLoginPerDay = number_format($mQuery->checkNumRows($sql), 0);

$sql = "select dhid from db_user_download_history";
$allDownloadDocument = number_format($mQuery->checkNumRows($sql), 0);

$sql = "select did from db_document";
$allDocument = number_format($mQuery->checkNumRows($sql), 0);
?>	           <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $allUserInSystem; ?>">0</span> <label class="font1emWhite">ท่าน</label>
                                        </div>
                                        <div class="desc font1emWhite" style="margin-top: 10px;"> จำนวนสมาชิกในระบบ </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $userLoginPerDay; ?>">0</span> <label class="font1emWhite">ครั้ง</label>
                                        </div>
                                        <div class="desc font1emWhite" style="margin-top: 10px;"> จำนวนการเข้าใช้งานต่อวัน </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $allDownloadDocument; ?>">0</span> <label class="font1emWhite">ครั้ง</label>
                                        </div>
                                        <div class="desc font1emWhite" style="margin-top: 18px;"> จำนวนการ Download ข้อมูล </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="<?php echo $allDocument; ?>">0</span> <label class="font1emWhite">ไฟล์</label>
                                        </div>
                                        <div class="desc font1emWhite" style="margin-top: 18px;"> จำนวนข้อมูลทั้งหมดในระบบ </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->



                        <!-- BEGIN : HIGHCHARTS -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-green"></i>
                                            <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมการเข้าใช้งานต่อวัน 15 วันย้อนหลัง</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="loginCharts" style="height:500px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-green"></i>
                                            <span class="caption-subject font-green bold uppercase font1emNoColor">จำนวนการเพิ่มข้อมูลในระบบ 15 วันย้อนหลัง</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="addDocumentCharts" style="height:500px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-green"></i>
                                            <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมการ Download ข้อมูล 15 วันย้อนหลัง</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="downloadCharts" style="height:500px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
