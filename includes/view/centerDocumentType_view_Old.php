<?php  
$showPathText = "";
$dateNow = $dFunc->getDateChris();

switch($f){
    case "financial" :
        $docTypeID = 1;
        $docTypeNameText = "Financial";
        break;
    case "franchiseManual" :
        $docTypeID = 2;
        $docTypeNameText = "Franchise Manual";
        break;
    case "operateAndTraining" :
        $docTypeID = 3;
        $docTypeNameText = "Operations and training";
        break;
    case "marketing" :
        $docTypeID = 4;
        $docTypeNameText = "Marketing";
        break;
    case "consultant" :
        $docTypeID = 5;
        $docTypeNameText = "Franchise (Business Consultant)";
        break;
    case "lastestNews" :
        $docTypeID = 6;
        $docTypeNameText = "Latest News";
        break;
    case "letter" :
        $docTypeID = 7;
        $docTypeNameText = "Letter";
        break;
    case "policy" :
        $docTypeID = 8;
        $docTypeNameText = "Policy";
        break;
    case "others" :
        $docTypeID = 9;
        $docTypeNameText = "Others";
        break;
    default :
        $docTypeID = 1;
        $docTypeNameText = "Financial";
        break;
}  //---  switch($f)

if(isset($_REQUEST['bid'])){
    $brandIDLink = base64_decode($_REQUEST['bid']);
    $yearLink = base64_decode($_REQUEST['y']);
    $monthLink = base64_decode($_REQUEST['m']);
    $shopCode = base64_decode($_REQUEST['shopCode']);
}else{
    $brandIDLink = "";
    $yearLink = "";
    $monthLink = "";
}    //-------  if(isset($_REQUEST['bid']))


$chkArrBrandIDEmpty = array_filter($_SESSION['arrBrandID']);
$chkArrBranchIDEmpty = array_filter($_SESSION['arrBranchID']);
$retentionDate = $dFunc->datePlusDay($dateNow, 0, 0, -1);

if($brandIDLink == ""){
    if($docTypeID == 1){
        $sql = "select shop_code from db_user_auth where uaid=".$_SESSION['arrBranchID'][0];
        $shopTmpCode = $mQuery->getResultOneRecord($sql, "shop_code");

        $sql = "select brand_name from db_brand where bid=".$_SESSION['arrBranchID'][0];
        $brandNameShow = $mQuery->getResultOneRecord($sql, "brand_name");

        $sql = "select * from db_document where type_id=".$docTypeID." and brand_id=".$_SESSION['arrBrandID'][0]." and shop_code='".$shopTmpCode."' and adddate>='".$retentionDate."' order by adddate desc";

        $showPathText = $pageTitle."/".$brandNameShow."/".$shopTmpCode;

        if($yearLink != ""){
            $showPathText = $showPathText."/".$yearLink."/".$monthLink;
        }  //-----  if($yearLink != "")
    }else{
        $sql = "select * from db_document where type_id=".$docTypeID." and adddate>='".$retentionDate."' order by adddate desc";
        $showPathText = $pageTitle;
    }  //-----  if($docTypeID == 1)
}else{
    $sql = "select brand_name from db_brand where bid=".$brandIDLink;
    $brandNameShow = $mQuery->getResultOneRecord($sql, "brand_name");

    $sql = "select * from db_document where type_id=".$docTypeID." and brand_id=".$brandIDLink." and year='".$yearLink."' and shop_code='".$shopCode."' and month='".$monthLink."' and adddate>='".$retentionDate."' order by adddate desc";

    $showPathText = $pageTitle."/".$brandNameShow."/".$shopCode."/".$yearLink."/".$monthLink;
}  //------  if($brandIDLink == "")

$num = $mQuery->checkNumRows($sql);
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                        <div class="tools"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <!-- <th class="typename"><label  class="font1emBlack">Type Name</label></th> -->
                                                    <!-- <th class="categoryname"><label  class="font1emBlack">Category Name</label></th> -->
                                                    <th class="doctitle"><label  class="font1emBlack">Document Title</label></th>
                                                    <th class="docmonth"><label  class="font1emBlack">Document Month</label></th>
                                                    <th class="docremove"><label  class="font1emBlack">Document Retention Date</label></th>
                                                    <th class="filename"><label  class="font1emBlack">File Name</label></th>
                                                    <!-- <th class="count"><label  class="font1emBlack">Download Count</label></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $docID = $r['did'];
                                                        $chkAuth = 0;

                                                        $sqlChkAuth = "select daid, brand_id from db_document_authorize where did=".$docID." and type_id=".$docTypeID." and uaid=0";
                                                        $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                        if($numChkAuth > 0){
                                                            if(!empty($chkArrBrandIDEmpty)){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID'])){
                                                                        $chkAuth = 1;
                                                                    }  //------  if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID']))
                                                                }  //-----  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------  if(!empty($chkArrEmpty))
                                                        }else{
                                                            $sqlChkAuth = "select uaid from db_document_authorize where did=".$docID." and type_id=".$docTypeID;
                                                            $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                            if($numChkAuth > 0){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(!empty($chkArrBranchIDEmpty)){
                                                                        if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID'])){
                                                                            $chkAuth = 1;
                                                                        }  //-----  if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID']))
                                                                    }  //------  if(!empty($chkArrEmpty))
                                                                }  //------  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------ if($numChkAuth > 0)
                                                        }  //---  if($numChkAuth > 0)

                                                        if($chkAuth == 1){
                                                            $catID = $r['cat_id'];
                                                            $typeID = $r['type_id'];
                                                            $title = $r['description'];
                                                            $fileName = $r['file_name'];
                                                            $filePath = $r['file_path'];
                                                            $docMonth = $r['month'];
                                                            $docYear = $r['year'];
                                                            $docRetentionDate = $dFunc->datePlusDay($r['adddate'], 0, 0, 1);
                                                            $docRetentionDate = $dFunc->fullDateChris($docRetentionDate);

                                                            $filePathNum = stripos($r['file_path'], "upload");
                                                            $filePath = substr($r['file_path'], $filePathNum);

                                                            $forDate = $dFunc->showMonthYearInEnglish($r['year'].$r['month']);

                                                            $sql = "select type_name from db_document_type where type_id=".$typeID;
                                                            $typeName = $mQuery->getResultOneRecord($sql, "type_name");

                                                            $sql = "select cat_name from db_document_category where cat_id=".$catID;
                                                            $catName = $mQuery->getResultOneRecord($sql, "cat_name");

                                                            $sql = "select dhid from db_user_download_history where document_id=".$docID;
                                                            $downloadCount = number_format($mQuery->checkNumRows($sql), 0);
                                            ?>
                                                        <tr>
                                                            <!-- <td><label  class="font1emGray"><?php //echo $typeName; ?></label></td> -->
                                                            <!-- <td><label  class="font1emGray"><?php //echo $catName; ?></label></td> -->
                                                            <td><label  class="font1emGray"><?php echo $title; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $docMonth."/".$docYear; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $docRetentionDate; ?></label></td>
                                                            <td><label  class="font1emGray"><a href="<?php echo $filePath.$fileName; ?>" target="_blank" onclick="countDownload(<?php echo $_SESSION['mLoginID']; ?>, <?php echo $docID; ?>);"><?php echo $fileName; ?></a></label></td>
                                                            <!-- <td><label  class="font1emGray"><?php //echo number_format($downloadCount, 0); ?></label></td> -->
                                                        </tr>
                                                    <?php }  //------  if($chkAuth == 1) ?>
                                                <?php }  //-------  foreach($result as $r) ?>
                                                <?php unset($result, $r); ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->