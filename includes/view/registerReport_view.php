<?php  
$sql = "select * from db_register_form order by adddate desc, addtime desc limit 3000";
$num = $mQuery->checkNumRows($sql);
?>
                                <div id="showTopUpSystemDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showTopUpSystemDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>
                                

                                
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_REGISTER_REPORT_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="username"><label  class="font1emBlack">รหัสประจำตัวสมาชิก</label></th>
                                                    <th class="name"><label  class="font1emBlack">ชื่อ - นามสกุล</label></th>
                                                    <th class="none"><label  class="font1emBlack">รหัสประจำตัวประชาชน</label></th>    
                                                    <th class="none"><label  class="font1emBlack">หมายเลขโทรศัพท์</label></th>
                                                    <th class="payment"><label  class="font1emBlack">ค่าสมัคร(บาท)</label></th>
                                                    <th class="position"><label  class="font1emBlack">ตำแหน่งที่สมัคร</label></th>
                                                    <th class="callcenter"><label  class="font1emBlack">ผู้ที่รับสัมคร</label></th>
                                                    <th class="date"><label  class="font1emBlack">วัน - เวลาทำรายการ</label></th>
                                                    <th class="none"><label  class="font1emBlack">อาชีพ</label></th>
                                                    <th class="none"><label  class="font1emBlack">ผู้แนะนำ</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $memberID = $r['member_id'];
                                                        $name = $r['first_name'].$r['name']." ".$r['lastname'];
                                                        $gcid = $r['gcid'];
                                                        $tel = $r['tel'];
                                                        $registerType = (int)$r['register_position'];
                                                        $suggestionID = $r['suggestion_id'];

                                                        if($registerType == 1)
                                                        {
                                                                $registerPrice = "3,500";
                                                                $registerPosition = "กรรมการ";
                                                        }
                                                        else
                                                        {
                                                                $registerPrice = "750";
                                                                $registerPosition = "สมาชิก";
                                                        }  //-------  if($registerType == 1)

                                                        $occupation = $r['occupation'];
                                                        $addDateAndTime = $dFunc->fullDateThai($r['adddate'])." - ".$r['addtime'];

                                                        $aid = $r['addaid'];

                                                        $sql = "select aname, alastname from db_admin where aid=".$aid;
                                                        $aname = $mQuery->getResultOneRecord($sql, "aname");
                                                        $alastname = $mQuery->getResultOneRecord($sql, "alastname");

                                                        $aname = "คุณ".$aname." ".$alastname;
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $memberID; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $name; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $gcid; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $tel; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $registerPrice; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $registerPosition; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $aname; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDateAndTime; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $occupation; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $suggestionID; ?></label></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->