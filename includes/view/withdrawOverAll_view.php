<?php  
$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$dateMinusOne = $dFunc->datePlusDay($dateNow, -1, 0, 0);
$dateMinusSeven = $dFunc->datePlusDay($dateNow, -7, 0, 0);
$dateMinusThirdty = $dFunc->datePlusDay($dateNow, -30, 0, 0);

$yesterdayMinusOne = $dFunc->dateMinusDate($dateNow, 1);
$yesterdayMinusSeven = $dFunc->dateMinusDate($dateNow, 7);
$yesterdayMinusThirdty = $dFunc->dateMinusDate($dateNow, 30);

$dateShowNow = $dFunc->fullDateThai($dateNow);
$dateShowYesterday = $dFunc->fullDateThai($dateMinusOne);

$sql = "select wid from db_withdraw_detail where withdraw_flag=2 and adddate='".$dateNow."'";
$withdrawTransactionToday = intval($mQuery->checkNumRows($sql));

$sql = "select wid from db_withdraw_detail where withdraw_flag=2 and adddate='".$dateMinusOne."'";
$withdrawTransactionYesterday = intval($mQuery->checkNumRows($sql));

$withdrawDiffTransaction = $withdrawTransactionToday - $withdrawTransactionYesterday;


$sql = "select withdraw_summary from db_withdraw_summary where date='".$dateNow."'";
$withdrawVolumnToday = intval($mQuery->getResultOneRecord($sql, "withdraw_summary"));

$sql = "select withdraw_summary from db_withdraw_summary where date='".$dateMinusOne."'";
$withdrawVolumnYesterday = intval($mQuery->getResultOneRecord($sql, "withdraw_summary"));

$withdrawDiffVolumn = $withdrawVolumnToday - $withdrawVolumnYesterday;




//----------- A1  -----------------//
$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusOne."'";
$withdrawTodayA1 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusSeven."'";
$withdrawTodayA7 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusThirdty."'";
$withdrawTodayA30 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusOne."'";
$withdrawYesterdayA1 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusSeven."'";
$withdrawYesterdayA7 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusThirdty."'";
$withdrawYesterdayA30 = $mQuery->checkNumRows($sql);

$withdrawDiffA1 = $withdrawTodayA1 - $withdrawYesterdayA1;
$withdrawDiffA7 = $withdrawTodayA7 - $withdrawYesterdayA7;
$withdrawDiffA30 = $withdrawTodayA30 - $withdrawYesterdayA30;
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_WITHDRAW_OVERALL_TITLE; ?>&nbsp;(Transaction และ Volumn)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="withdrawOverAll_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="withdrawOverAll_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">วันที่ปัจจุบัน</label></th>
                                                    <th class="transaction" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Transaction (รายการ)</label></th>
                                                    <th class="volumn" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Volumn (บาท)</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeTrans" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeVolumn" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $dateShowNow; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTransactionToday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTransactionYesterday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffTransaction, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawVolumnToday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawVolumnYesterday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffVolumn, 0); ?></label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_WITHDRAW_OVERALL_TITLE; ?>&nbsp;(A1 - A7 - A30)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="withdrawOverAllA1ToA30_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="withdrawOverAllA1ToA30_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">วันที่ปัจจุบัน</label></th>
                                                    <th class="a1" colspan="3" style="text-align: center;"><label  class="font1emBlack">A1</label></th>
                                                    <th class="a7" colspan="3" style="text-align: center;"><label  class="font1emBlack">A7</label></th>
                                                    <th class="a30" colspan="3" style="text-align: center;"><label  class="font1emBlack">A30</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayA1" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA1" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA1" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayA7" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA7" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA7" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayA30" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA30" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA30" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $dateShowNow; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTodayA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawYesterdayA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTodayA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawYesterdayA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawTodayA30, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawYesterdayA30, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($withdrawDiffA30, 0); ?></label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์การถอนเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="withdrawOverAll" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->