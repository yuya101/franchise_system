<?php  
$sql = "select * from db_document_category order by type_id";
$num = $mQuery->checkNumRows($sql);
?>


                                <div id="showCategoryDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showCategoryDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>

                                
                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูล Category : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['errDupName'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ปรับปรุงข้อมูลไม่สำเร็จ!</strong> ข้อมูล Category : <?php echo base64_decode($_REQUEST['errDupName']); ?> ซ้ำค่ะ กรุณาตรวจสอบอีกครั้ง.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูล Category : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_MANAGE_CATEGORY_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="typename"><label  class="font1emBlack">Type Name</label></th>
                                                    <th class="categoryname"><label  class="font1emBlack">Category Name</label></th>
                                                    <th class="adddate"><label  class="font1emBlack">วันที่เพิ่มข้อมูล</label></th>
                                                    <th class="addtime"><label  class="font1emBlack">วันที่แก้ไขล่าสุด</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $catID = $r['cat_id'];
                                                        $typeID = $r['type_id'];
                                                        $catName = $r['cat_name'];
                                                        $addDate = $dFunc->fullDateThai($r['adddate']);
                                                        $modDate = $dFunc->fullDateThai($r['moddate']);

                                                        $sql = "select type_name from db_document_type where type_id=".$typeID;
                                                        $typeName = $mQuery->getResultOneRecord($sql, "type_name");
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $typeName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $catName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDate; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $modDate; ?></label></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $catID; ?>, 'showCategoryDetail', 'ShowCategoryDetail');">Edit Category</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->