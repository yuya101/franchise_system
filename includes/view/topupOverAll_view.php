<?php  
$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$dateMinusOne = $dFunc->datePlusDay($dateNow, -1, 0, 0);
$dateMinusSeven = $dFunc->datePlusDay($dateNow, -7, 0, 0);
$dateMinusThirdty = $dFunc->datePlusDay($dateNow, -30, 0, 0);

$yesterdayMinusOne = $dFunc->dateMinusDate($dateNow, 1);
$yesterdayMinusSeven = $dFunc->dateMinusDate($dateNow, 7);
$yesterdayMinusThirdty = $dFunc->dateMinusDate($dateNow, 30);

$dateShowNow = $dFunc->fullDateThai($dateNow);
$dateShowYesterday = $dFunc->fullDateThai($dateMinusOne);

$sql = "select topid from db_topup_detail where topup_flag=2 and adddate='".$dateNow."'";
$topupTransactionToday = intval($mQuery->checkNumRows($sql));

$sql = "select topid from db_topup_detail where topup_flag=2 and adddate='".$dateMinusOne."'";
$topupTransactionYesterday = intval($mQuery->checkNumRows($sql));

$topupDiffTransaction = $topupTransactionToday - $topupTransactionYesterday;


$sql = "select topup_summary from db_topup_summary where date='".$dateNow."'";
$topupVolumnToday = intval($mQuery->getResultOneRecord($sql, "topup_summary"));

$sql = "select topup_summary from db_topup_summary where date='".$dateMinusOne."'";
$topupVolumnYesterday = intval($mQuery->getResultOneRecord($sql, "topup_summary"));

$topupDiffVolumn = $topupVolumnToday - $topupVolumnYesterday;




//----------- A1  -----------------//
$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusOne."'";
$topupTodayA1 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusSeven."'";
$topupTodayA7 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$dateMinusThirdty."'";
$topupTodayA30 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusOne."'";
$topupYesterdayA1 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusSeven."'";
$topupYesterdayA7 = $mQuery->checkNumRows($sql);

$sql = "select distinct(uid) from db_active_data_history where active_date>='".$yesterdayMinusThirdty."'";
$topupYesterdayA30 = $mQuery->checkNumRows($sql);

$topupDiffA1 = $topupTodayA1 - $topupYesterdayA1;
$topupDiffA7 = $topupTodayA7 - $topupYesterdayA7;
$topupDiffA30 = $topupTodayA30 - $topupYesterdayA30;
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_TOPUP_OVERALL_TITLE; ?>&nbsp;(Transaction และ Volumn)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="topupOverAll_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupOverAll_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">วันที่ปัจจุบัน</label></th>
                                                    <th class="transaction" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Transaction (รายการ)</label></th>
                                                    <th class="volumn" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Volumn (บาท)</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeTrans" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeVolumn" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $dateShowNow; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTransactionToday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTransactionYesterday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffTransaction, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupVolumnToday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupVolumnYesterday, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffVolumn, 0); ?></label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_TOPUP_OVERALL_TITLE; ?>&nbsp;(A1 - A7 - A30)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="topupOverAllA1ToA30_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupOverAllA1ToA30_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">วันที่ปัจจุบัน</label></th>
                                                    <th class="a1" colspan="3" style="text-align: center;"><label  class="font1emBlack">A1</label></th>
                                                    <th class="a7" colspan="3" style="text-align: center;"><label  class="font1emBlack">A7</label></th>
                                                    <th class="a30" colspan="3" style="text-align: center;"><label  class="font1emBlack">A30</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayA1" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA1" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA1" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayA7" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA7" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA7" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayA30" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayA30" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="diffA30" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $dateShowNow; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTodayA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupYesterdayA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffA1, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTodayA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupYesterdayA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffA7, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTodayA30, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupYesterdayA30, 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffA30, 0); ?></label></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์การเติมเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="topupOverAll" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->