<?php  
$sql = "select * from db_topup_detail where topup_flag!=2 order by adddate, addtime limit 1500";
$num = $mQuery->checkNumRows($sql);
?>
                                <div id="showTopUpSystemDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showTopUpSystemDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>
                                

                                <?php if(isset($_REQUEST['closeJob'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ปิดงานเรียบร้อย!</strong> เติมเงิน User : <?php echo base64_decode($_REQUEST['closeJob']); ?> จำนวน <?php echo number_format(base64_decode($_REQUEST['value']), 0); ?> บาท เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['openJob'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>เปิดงานใหม่!</strong> เปิดงานของ User : <?php echo base64_decode($_REQUEST['openJob']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_TOPUP_SYSTEM_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="topupSystem_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="username"><label  class="font1emBlack">Username</label></th>
                                                    <th class="none"><label  class="font1emBlack">ชื่อ - นามสกุล</label></th>
                                                    <th class="none"><label  class="font1emBlack">Email Address</label></th>
                                                    <th class="none"><label  class="font1emBlack">หมายเลขโทรศัพท์</label></th>
                                                    <th class="none"><label  class="font1emBlack">วัน - เวลาทำรายการ</label></th>
                                                    <th class="topup"><label  class="font1emBlack">เงิน Top Up ที่เข้ามา</label></th>
                                                    <th class="status"><label  class="font1emBlack">สถานะ</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $topupID = $r['topid'];
                                                        $userID = $r['uid'];
                                                        $userName = $r['topup_user'];
                                                        $email = $r['topup_email'];
                                                        $mobile = $r['topup_mobile'];
                                                        $addDateAndTime = $dFunc->fullDateThai($r['adddate'])." - ".$r['addtime'];
                                                        $topupValue = number_format($r['topup_value'], 0);

                                                        $sql = "select user_real_name, user_real_lastname from db_user_detail where uid=".$userID;
                                                        $userFullName = "คุณ".$mQuery->getResultOneRecord($sql, "user_real_name")." ".$mQuery->getResultOneRecord($sql, "user_real_lastname");

                                                        $status = (int)$r['topup_flag'];

                                                        if($status == 0)
                                                        {
                                                            $statusText = "<label class='font1emRed'>รอการดำเนินการ</label>";
                                                        }
                                                        elseif($status == 1)
                                                        {
                                                            $statusText = "<label class='font1emOrange'>อยู่ระหว่างดำเนินการ</label>";
                                                        }
                                                        else
                                                        {
                                                            $statusText = "<label class='font1emBlue'>ปิดงาน</label>";
                                                        }  //----  if($status == 0)

                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $userName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $userFullName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $email; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $mobile; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDateAndTime; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $topupValue; ?></label></td>
                                                    <td><?php echo $statusText; ?></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $topupID; ?>, 'showTopUpSystemDetail', 'ShowTopUpSystemDetail');">ทำการเติมเงิน</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->