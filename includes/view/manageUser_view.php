<?php  
$sql = "select * from db_user where aid!='1' order by ausername";
$num = $mQuery->checkNumRows($sql);
?>


                                <div id="showManageUserDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showManageUserDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>

                                
                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูล User : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูล User : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_MANAGE_USER_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="name"><label  class="font1emBlack">ชื่อ</label></th>
                                                    <th class="lastname"><label  class="font1emBlack">นามสกุล</label></th>
                                                    <th class="email"><label  class="font1emBlack">Email Address</label></th>
                                                    <th class="none"><label  class="font1emBlack">Password</label></th>
                                                    <th class="auth"><label  class="font1emBlack">สิทธ์การใช้งาน</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่เพิ่มข้อมูล</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่แก้ไขล่าสุด</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $password = base64_decode($r['apass']);

                                                        $sql = "select group_name from db_user_group where gid=".$r['groupid'];
                                                        $groupName = $mQuery->getResultOneRecord($sql, "group_name");

                                                        $addDate = $dFunc->fullDateThai($r['adddate']);
                                                        $modDate = $dFunc->fullDateThai($r['moddate']);
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $r['aname']; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $r['alastname']; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $r['email']; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $password; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $groupName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDate; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $modDate; ?></label></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $r['aid']; ?>, 'showManageUserDetail', 'ShowManageUserDetail');">แก้ไข</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->