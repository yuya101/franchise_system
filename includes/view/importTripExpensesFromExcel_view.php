                                            <?php include("includes/control/queryInquiryData_Ctl.php"); ?>
                                            <?php if(isset($_REQUEST['completeImport'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> นำเข้าข้อมูล Inquiry Data จากไฟล์ : <?php echo base64_decode($_REQUEST['completeImport']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errImport'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> <?php echo base64_decode($_REQUEST['errImport']); ?>ค่ะ กรุณาตรวจสอบข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_IMPORT_TRIP_EXPENSE_FROM_EXCEL_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/importTripExpenseFromExcel_Ctl.php" class="form-horizontal" enctype="multipart/form-data" method="post">
                                                        <div class="form-body">
                                                            <h3 class="form-section">Import Inquiry Data from text file.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Inquiry Data Type</label>
                                                                    <div class="col-md-4">
                                                                        <select name="inq_type" id="inq_type" class="form-control input-circle font1emGray">
                                                                            <?php if($numDocType > 0){ ?>
                                                                                <?php for($i=0; $i<$numDocType; $i++){ ?>
                                                                                    <option value="<?php echo $inqTypeIDArr[$i]; ?>"><?php echo $inqTypeNameArr[$i]; ?></option>
                                                                                <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                            <?php }  //-----  if($numDocType > 0) ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 70px;">
                                                                    <label class="col-md-3 control-label font1emGray">Inquiry Data File</label>
                                                                    <div class="col-md-4">
                                                                        <input type="file" name="textFile[]" id="textFile" class="form-control input-circle font1emGray" required>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <h3 class="form-section">Note.</h3>
                                                            <div class="form-body">
                                                                <div class="form-group" style="margin-bottom: 50px;">
                                                                    <div class="col-md-12">
                                                                        <ul class=" font1emGray" style="line-height: 30px;">
                                                                            <li>ไฟล์ที่ทำการ Upload จะต้องเป็น Text File เท่านั้นค่ะ.</li>
                                                                            <li>ภายในไฟล์จะต้องมี Format(รูปแบบ) เรียงตามลำดับเหมือนในตัวอย่างเพื่อไม่ให้มีความผิดพลาดในการ Import ข้อมูลค่ะ.</li>
                                                                            <li>สามารถ Download ตัวอย่าง Credit Card ไฟล์ได้ที่นี่ค่ะ.&nbsp;&nbsp;&nbsp;<a href="upload/CreditCard_text.txt" class="input-circle" style="color:red;">ตัวอย่าง Import File</a></li>
                                                                            <li>สามารถ Download ตัวอย่าง Docket ไฟล์ได้ที่นี่ค่ะ.&nbsp;&nbsp;&nbsp;<a href="upload/Docket_text.txt" class="input-circle" style="color:red;">ตัวอย่าง Import File</a></li>
                                                                            <li>สามารถ Download ตัวอย่าง Trip Expenses ไฟล์ได้ที่นี่ค่ะ.&nbsp;&nbsp;&nbsp;<a href="upload/TripExpense.txt" class="input-circle" style="color:red;">ตัวอย่าง Import File</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;นำเข้าข้อมูล&nbsp;&nbsp;</button>
                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>