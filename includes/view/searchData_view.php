<?php  
$sql = "select * from db_chicken_main order by adddate desc, addtime desc";
$num = $mQuery->checkNumRows($sql);
?>
                                <div id="showSearchChickenDataDivDetail" style="display:none;"></div>
                                <div align="center" id="showSearchChickenDataLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>
                                

                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูลไก่ : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูลไก่ : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_REGISTER_SEARCH_DATA_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="chickenid"><label  class="font1emBlack">รหัสปีกไก่</label></th>
                                                    <th class="name"><label  class="font1emBlack">ชื่อไก่</label></th>
                                                    <th class="none"><label  class="font1emBlack">สายพันธุ์</label></th>    
                                                    <th class="none"><label  class="font1emBlack">ซื้อมาจาก</label></th>
                                                    <th class="none"><label  class="font1emBlack">ขายให้</label></th>
                                                    <th class="sex"><label  class="font1emBlack">เพศไก่</label></th>
                                                    <th class="father"><label  class="font1emBlack">รหัสปีกพ่อ</label></th>
                                                    <th class="mother"><label  class="font1emBlack">รหัสปีกแม่</label></th>
                                                    <th class="generation"><label  class="font1emBlack">รุ่นที่</label></th>
                                                    <th class="none"><label  class="font1emBlack">จำนวนลูกในครอกเดียวกัน(ตัว)</label></th>
                                                    <th class="none"><label  class="font1emBlack">ชื่อ - รหัสปู่</label></th>
                                                    <th class="none"><label  class="font1emBlack">ชื่อ - รหัสย่า</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);
                                                    $grandFatherID = 0;
                                                    $grandMotherID = 0;

                                                     foreach($result as $r)
                                                    {
                                                        $id = $r['id'];
                                                        $chickenid = $r['chickenid'];
                                                        $strain = $r['strain'];
                                                        $buyfrom = $r['buyfrom'];
                                                        $saleto = $r['saleto'];
                                                        $chickenname = $r['chickenname'];
                                                        $sex = (int)$r['sex'];
                                                        $fatherid = (int)$r['fatherid'];
                                                        $motherid = (int)$r['motherid'];
                                                        $generation = (int)$r['generation'];
                                                        $familyno = (int)$r['familyno'];

                                                        $sexText = ($sex == 1 ? "เพศผู้" : "เพศเมีย");

                                                        if($fatherid > 0)
                                                        {
                                                                $sql = "select chickenid, chickenname from db_chicken_main where id=".$fatherid;
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $fatherRealID = $mQuery->getResultOneRecord($sql, "chickenid");
                                                                    $fatherName = $mQuery->getResultOneRecord($sql, "chickenname")." (".$fatherRealID.")";
                                                                }
                                                                else
                                                                {
                                                                    $fatherName = "ไม่มีข้อมูลพ่อพันธุ์";
                                                                }  //----  if($num > 0)
                                                        }
                                                        else
                                                        {
                                                                $fatherName = "ไม่มีข้อมูลพ่อพันธุ์";
                                                        }  //------  if($fatherid > 0)
                                                        

                                                        if($fatherid > 0)
                                                        {
                                                                $sql = "select fatherid from db_chicken_main where id=".$fatherid." and fatherid>0";
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $grandFatherID = $mQuery->getResultOneRecord($sql, "fatherid");

                                                                    $sql = "select chickenid, chickenname from db_chicken_main where id=".$grandFatherID;
                                                                    $num = $mQuery->checkNumRows($sql);

                                                                    if($num > 0)
                                                                    {
                                                                        $grandFatherRealID = $mQuery->getResultOneRecord($sql, "chickenid");
                                                                        $grandFatherName = $mQuery->getResultOneRecord($sql, "chickenname")." (".$grandFatherRealID.")";
                                                                    }
                                                                    else
                                                                    {
                                                                        $grandFatherName = "ไม่มีข้อมูลรุ่นปู่";
                                                                    }  //----  if($num > 0)
                                                                }
                                                                else
                                                                {
                                                                    $grandFatherName = "ไม่มีข้อมูลรุ่นปู่";
                                                                }  //-----  if($num > 0)
                                                        }
                                                        else
                                                        {
                                                                $grandFatherName = "ไม่มีข้อมูลรุ่นปู่";
                                                        }  //------  if($fatherid > 0)



                                                        if($motherid > 0)
                                                        {
                                                                $sql = "select chickenid, chickenname from db_chicken_main where id=".$motherid;
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $motherRealID = $mQuery->getResultOneRecord($sql, "chickenid");
                                                                    $motherName = $mQuery->getResultOneRecord($sql, "chickenname")." (".$motherRealID.")";
                                                                }
                                                                else
                                                                {
                                                                    $motherName = "ไม่มีข้อมูลแม่พันธุ์";
                                                                }  //----  if($num > 0)
                                                        }
                                                        else
                                                        {
                                                                $motherName = "ไม่มีข้อมูลแม่พันธุ์";
                                                        }  //------  if($fatherid > 0)
                                                        

                                                        if($motherid > 0)
                                                        {
                                                                $sql = "select motherid from db_chicken_main where id=".$motherid." and motherid>0";
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $grandMotherID = $mQuery->getResultOneRecord($sql, "motherid");

                                                                    $sql = "select chickenid, chickenname from db_chicken_main where id=".$grandMotherID;
                                                                    $num = $mQuery->checkNumRows($sql);

                                                                    if($num > 0)
                                                                    {
                                                                        $grandMotherRealID = $mQuery->getResultOneRecord($sql, "chickenid");
                                                                        $grandMotherName = $mQuery->getResultOneRecord($sql, "chickenname")." (".$grandMotherRealID.")";
                                                                    }
                                                                    else
                                                                    {
                                                                        $grandMotherName = "ไม่มีข้อมูลรุ่นย่า";
                                                                    }  //----  if($num > 0)
                                                                }
                                                                else
                                                                {
                                                                    $grandMotherName = "ไม่มีข้อมูลรุ่นย่า";
                                                                }  //-----  if($num > 0)
                                                        }
                                                        else
                                                        {
                                                                $grandMotherName = "ไม่มีข้อมูลรุ่นย่า";
                                                        }  //------  if($fatherid > 0)
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $chickenid; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $chickenname; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $strain; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $buyfrom; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $saleto; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $sexText; ?></label></td>
                                                    <?php if($fatherid > 0){ ?>
                                                        <td><label  class="font1emGray"><a href="javascript:;" onclick="return showManageDetailInButtonByID(<?php echo $fatherid; ?>, 'showSearchChickenData', 'ShowSearchChickenData');"><?php echo $fatherName; ?></label></td>
                                                    <?php }else{ ?>
                                                        <td><label  class="font1emGray"><?php echo $fatherName; ?></label></td>
                                                    <?php } ?>
                                                    <?php if($motherid > 0){ ?>
                                                        <td><label  class="font1emGray"><a href="javascript:;" onclick="return showManageDetailInButtonByID(<?php echo $motherid; ?>, 'showSearchChickenData', 'ShowSearchChickenData');"><?php echo $motherName; ?></label></td>
                                                    <?php }else{ ?>
                                                        <td><label  class="font1emGray"><?php echo $motherName; ?></label></td>
                                                    <?php } ?>
                                                    <td><label  class="font1emGray"><?php echo $generation; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $familyno; ?></label></td>
                                                    <?php if($grandFatherID > 0){ ?>
                                                        <td><label  class="font1emGray"><a href="javascript:;" onclick="return showManageDetailInButtonByID(<?php echo $grandFatherID; ?>, 'showSearchChickenData', 'ShowSearchChickenData');"><?php echo $grandFatherName; ?></label></td>
                                                    <?php }else{ ?>
                                                        <td><label  class="font1emGray"><?php echo $grandFatherName; ?></label></td>
                                                    <?php } ?>
                                                    <?php if($grandMotherID > 0){ ?>
                                                        <td><label  class="font1emGray"><a href="javascript:;" onclick="return showManageDetailInButtonByID(<?php echo $grandMotherID; ?>, 'showSearchChickenData', 'ShowSearchChickenData');"><?php echo $grandMotherName; ?></label></td>
                                                    <?php }else{ ?>
                                                        <td><label  class="font1emGray"><?php echo $grandMotherName; ?></label></td>
                                                    <?php } ?>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                                <?php unset($result, $r); ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->