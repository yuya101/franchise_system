                                            <?php include("includes/control/queryDocumentType_Ctl.php"); ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_SEARCH_FINANCIAL_FORM_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="javascript:;" class="form-horizontal" method="post">
                                                        <div class="form-body" id="listProductLI1">
                                                            <h3 class="form-section">Search Document</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Brand</label>
                                                                <div class="col-md-4">
                                                                    <select name="brandid" id="brandid" class="form-control input-circle font1emGray">
                                                                    <?php if($numDocBrand > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocBrand; $i++){ ?>
                                                                            <option value="<?php echo $docBrandID[$i]; ?>"><?php echo $docBrandName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocBrand; $i++) ?>
                                                                    <?php }  //-----  if($numDocBrand > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="fileDataDetailDiv" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Month</label>
                                                                    <div class="col-md-4 input-group date date-picker" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                        <input type="text" name="docmonth" id="docmonth" class="form-control input-circle" style="margin-left: 15px;" readonly>
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Branch Code</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="docbranchcode" id="docbranchcode" class="form-control input-circle font1emGray" placeholder="Please! Enter Branch Code">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="button" class="btn btn-circle green font1emWhite" onclick="return showSearchDocumentInButton(document.getElementById('brandid').options[document.getElementById('brandid').selectedIndex].value, document.getElementById('docmonth').value, document.getElementById('docbranchcode').value,'showSearchDocument', 'ShowSearchDocument');">&nbsp;&nbsp;ค้นหาเอกสาร&nbsp;&nbsp;</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>


                                            <div id="showSearchDocumentDivDetail" style="display:none;"></div>
                                            <div align="center" id="showSearchDocumentLoading" style="display:none; margin-bottom: 50px;">
                                                <br /><br />
                                                <img src="img/loading.gif" border="0" />
                                                <br /><br />
                                                <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                            </div>