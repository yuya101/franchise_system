                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูลลูกค้า : <?php echo base64_decode($_REQUEST['name']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errEmail'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> Email : <?php echo base64_decode($_REQUEST['email']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errName'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong> ชื่อ : <?php echo base64_decode($_REQUEST['username']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_REGISTER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addRegisterForm_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ตำแหน่งการสมัคร/Register Position</label>
                                                                <div class="col-md-4">
                                                                    <select name="registerPosition" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>กรรมการ : 3,500 บาท</option>
                                                                        <option value="2">สมาชิก : 750 บาท</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสผู้แนะนำ/Suggest No.</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="text" name="suggestid" id="suggestid" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอกรหัสผู้แนะนำ เช่น MT1610AA321" value="MT" maxlength="13"> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">คำนำหน้า</label>
                                                                <div class="col-md-4">
                                                                    <select name="firstname" class="form-control input-circle font1emGray">
                                                                        <option value="นาย" selected>นาย</option>
                                                                        <option value="นาง">นาง</option>
                                                                        <option value="นางสาว">นางสาว</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อ/First name</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="name" id="name" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกชื่อ" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุล/Surname</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="lastname" id="lastname" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกนามสกุล" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สัญชาติ/Nationality</label>
                                                                <div class="col-md-4">
                                                                    <select name="nationality" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>ไทย</option>
                                                                        <option value="2">ต่างสัญชาติ</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วัน/เดือน/ปี เกิด /Date of Birth</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="dateofbirth" id="dateofbirth" class="form-control input-circle font1emGray" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อายุ/Age</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="age" id="age" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอายุเป็นตัวเลขเท่านั้น" min="1" max="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เลขปะจำตัวประชาชน/ID No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="gcid" id="gcid" maxlength="13" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเลขบัตรประจำตัวประชาชน 13 หลัก" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์โทรศัพท์/Telephone No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="tel" id="tel" maxlength="50" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเบอร์โทรศัพท์ค่ะ" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่ปัจจุบัน/Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="address" id="address" class="form-control input-circle font1emGray" required="required" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่ส่งเอกสาร/Mailing Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="mailingaddress" id="mailingaddress" class="form-control input-circle font1emGray" required="required" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อาชีพ/Occupation</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="occupation" id="occupation" maxlength="100" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอาชีพ" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สถานที่ทำงาน/Work Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="workaddress" id="workaddress" class="form-control input-circle font1emGray" required="required" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สถานะภาพสมรส/Marital Status</label>
                                                                <div class="col-md-4">
                                                                    <select name="maritalstatus" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>โสด/Single</option>
                                                                        <option value="2">สมรส/Married</option>
                                                                        <option value="3">หย่า/Divorce</option>
                                                                        <option value="4">อื่นๆ/Others</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label font1emGray"></label>
                                                                <div class="col-md-5">
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อผู้รับผลประโยชน์ (1)/First name</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="benefitname1" id="benefitname1" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกชื่อ" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุลผู้รับผลประโยชน์ (1)/Surname</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="benefitlastname1" id="benefitlastname1" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกนามสกุล" required>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สัญชาติผู้รับผลประโยชน์ (1)/Nationality</label>
                                                                <div class="col-md-4">
                                                                    <select name="benefitnationality1" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>ไทย</option>
                                                                        <option value="2">ต่างสัญชาติ</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วัน/เดือน/ปี เกิดผู้รับผลประโยชน์ (1) /Date of Birth</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="benefitdateofbirth1" id="benefitdateofbirth1" class="form-control input-circle font1emGray" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อายุผู้รับผลประโยชน์ (1)/Age</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="benefitage1" id="benefitage1" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอายุเป็นตัวเลขเท่านั้น" min="1" max="200" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เลขปะจำตัวประชาชนผู้รับผลประโยชน์ (1)/ID No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="benefitgcid1" id="benefitgcid1" maxlength="13" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเลขบัตรประจำตัวประชาชน 13 หลัก" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์โทรศัพท์ผู้รับผลประโยชน์ (1)/Telephone No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="benefittel1" id="benefittel1" maxlength="50" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเบอร์โทรศัพท์ค่ะ" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่ปัจจุบันผู้รับผลประโยชน์ (1)/Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="benefitaddress1" id="benefitaddress1" class="form-control input-circle font1emGray" required="required" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label font1emGray"></label>
                                                                <div class="col-md-5">
                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            

                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อผู้รับผลประโยชน์ (2)/First name</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="benefitname2" id="benefitname2" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกชื่อ">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุลผู้รับผลประโยชน์ (2)/Surname</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="benefitlastname2" id="benefitlastname2" maxlength="100" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกนามสกุล">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สัญชาติผู้รับผลประโยชน์ (2)/Nationality</label>
                                                                <div class="col-md-4">
                                                                    <select name="benefitnationality2" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>ไทย</option>
                                                                        <option value="2">ต่างสัญชาติ</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วัน/เดือน/ปี เกิดผู้รับผลประโยชน์ (2) /Date of Birth</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="benefitdateofbirth2" id="benefitdateofbirth2" class="form-control input-circle font1emGray">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">อายุผู้รับผลประโยชน์ (2)/Age</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="benefitage2" id="benefitage2" class="form-control input-circle font1emGray" placeholder="กรุณากรอกอายุเป็นตัวเลขเท่านั้น" min="1" max="200">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เลขปะจำตัวประชาชนผู้รับผลประโยชน์ (2)/ID No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="benefitgcid2" id="benefitgcid2" maxlength="13" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเลขบัตรประจำตัวประชาชน 13 หลัก">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์โทรศัพท์ผู้รับผลประโยชน์ (2)/Telephone No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="benefittel2" id="benefittel2" maxlength="50" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเบอร์โทรศัพท์ค่ะ">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่ปัจจุบันผู้รับผลประโยชน์ (2)/Address</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="benefitaddress2" id="benefitaddress2" class="form-control input-circle font1emGray" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label font1emGray"></label>
                                                                <div class="col-md-5">
                                                                    <hr>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หนังสือเดินทางหรือเอกสารเดินทางเลขที่/ Passport or C.I. No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="passportno" id="passportno" maxlength="50" class="form-control input-circle font1emGray" placeholder="กรุณากรอกเลขที่หนังสือเดินทางหรือเอกสารเดินทาง" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ออกให้ที่/Issue at</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="passport_issue_at" id="passport_issue_at" class="form-control input-circle font1emGray">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วันที่ออก/Date of Issue</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="passport_date_of_issue" id="passport_date_of_issue" class="form-control input-circle font1emGray">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วันหมดอายุ/Date of Expire</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="passport_date_of_expire" id="passport_date_of_expire" class="form-control input-circle font1emGray">
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label font1emGray"></label>
                                                                <div class="col-md-5">
                                                                    <hr>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วันที่เดินทางเข้ามา/Arrived Date</label>
                                                                <div class="col-md-4">
                                                                    <input type="date" name="arrived_date" id="arrived_date" class="form-control input-circle font1emGray">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เดินทางด้วย/Arrived By</label>
                                                                <div class="col-md-4">
                                                                    <select name="arrived_by" class="form-control input-circle font1emGray">
                                                                        <option value="1" selected>เดินเท้า/Walk</option>
                                                                        <option value="2">รถยนต์/Car</option>
                                                                        <option value="3">อื่นๆ/Other</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-2 control-label font1emGray"></label>
                                                                <div class="col-md-5">
                                                                    <hr>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อนายจ้าง/ Employer's name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="employername" id="employername" maxlength="200" class="form-control input-circle font1emGray" placeholder="กรุณากรอกชื่อนายจ้าง">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ที่อยู่สถานที่ทำงาน/Location of work</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="locationofwork" id="locationofwork" class="form-control input-circle font1emGray" cols="8" rows="5"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ยืนยัน</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>