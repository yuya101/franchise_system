                                            <?php include("includes/control/queryDocumentType_Ctl.php"); ?>
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูล Document File : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_FILE_DATA_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addFileData_Ctl.php" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        <div class="form-body" id="listProductLI1">
                                                            <h3 class="form-section">Document Detail.</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Document Type</label>
                                                                <div class="col-md-4">
                                                                    <select name="typeid" id="typeid" class="form-control input-circle font1emGray" onchange="refreshListBoxItem(1, this, 'catid', 'listCategory', 'listProductLI', 2, 'fileDataDetailDiv');">
                                                                    <?php if($numDocType > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocType; $i++){ ?>
                                                                            <option value="<?php echo $docTypeID[$i]; ?>"><?php echo $docTypeName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="listProductLI2" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Category</label>
                                                                    <div class="col-md-4">
                                                                        <select name="catid" id="catid" class="form-control input-circle font1emGray">
                                                                        <?php if($numDocCat > 0){ ?>
                                                                            <?php for($i=0; $i<$numDocCat; $i++){ ?>
                                                                                <option value="<?php echo $docCatID[$i]; ?>"><?php echo $docCatName[$i]; ?></option>
                                                                            <?php }  //-----  for($i=0; $i<$numDocCat; $i++) ?>
                                                                        <?php }  //-----  if($numDocCat > 0) ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="fileDataDetailDiv" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Month</label>
                                                                    <div class="col-md-4 input-group date date-picker" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                        <input type="text" name="docmonth" id="docmonth" class="form-control input-circle" style="margin-left: 15px;" readonly>
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Title</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="title" id="title" class="form-control input-circle font1emGray" placeholder="Please! Enter Document Title" required>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Description</label>
                                                                    <div class="col-md-4">
                                                                        <textarea name="description" id="description" class="form-control input-circle font1emGray" rows="10" placeholder="Please! Enter Document Description"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 70px;">
                                                                    <label class="col-md-3 control-label font1emGray">Document File</label>
                                                                    <div class="col-md-4">
                                                                        <input type="file" name="docfile[]" id="docfile" class="form-control input-circle font1emGray" required>
                                                                    </div>
                                                                </div>


                                                                <?php if($numDocBrand > 0){?>
                                                                    <?php for($i=0; $i<$numDocBrand; $i++){?>
                                                                        <h3 class="form-section">Branch Authorization (&nbsp;<img src="<?php echo $docBrandPicture[$i]; ?>">&nbsp;&nbsp;<?php echo $docBrandName[$i]; ?>)</h3>
                                                                        <!--/row-->
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                <?php if($numDocBranch[$i] > 0){ ?>
                                                                                        <div class="col-md-12">
                                                                                            <div class="mt-checkbox-list">
                                                                                                <label class="mt-checkbox">
                                                                                                    <input type="checkbox" value="1" name="useallbrand<?php echo $docBrandID[$i]; ?>" /><?php echo "Add authorize to all branch in &nbsp;<img src='".$docBrandPicture[$i]."'>&nbsp;&nbsp;".$docBrandName[$i]." brand."; ?>
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php for($j=0; $j<$numDocBranch[$i]; $j++){ ?>
                                                                                        <div class="col-md-2">
                                                                                            <div class="mt-checkbox-list">
                                                                                                <label class="mt-checkbox">
                                                                                                    <input type="checkbox" value="1" name="<?php echo $docBranchID[$i][$j]; ?>" /> <?php echo $docBranchSiteName[$i][$j]; ?>
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php }else{ ?>
                                                                                    <div class="col-md-12">
                                                                                        <div class="mt-checkbox-list">
                                                                                            <label class="mt-checkbox">ไม่มีข้อมูลร้านสาขาของแบรนด์ <?php echo $docBrandName[$i]; ?>&nbsp;&nbsp;<img src="<?php echo $docBrandPicture[$i]; ?>"> ค่ะ. </label>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } //-------  if($numDocBranch[$i] > 0) ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;ยืนยัน&nbsp;&nbsp;</button>
                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>