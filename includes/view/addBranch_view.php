                                            <?php include("includes/control/queryUsers_Ctl.php"); ?>
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> เพิ่มข้อมูลร้านสาขา : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <?php if(isset($_REQUEST['errNameDup'])){ ?>
                                                <div class="alert alert-danger font1emNoColor">
                                                    <strong>ผิดพลาด!</strong>Shop Code : <?php echo base64_decode($_REQUEST['errNameDup']); ?> ซ้ำ กรุณากรอกข้อมูลใหม่ค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_ADD_BRANCH_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/addBranch_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Franchise Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="userid" id="userid" class="form-control input-circle font1emGray">
                                                                    <?php if($numUser > 0){ ?>
                                                                        <?php for($i=0; $i<$numUser; $i++){ ?>
                                                                            <option value="<?php echo $userID[$i]; ?>"><?php echo $userNo[$i]." - ".$userName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Brand Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="brandid" id="brandid" class="form-control input-circle font1emGray">
                                                                    <?php if($numDocBrand > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocBrand; $i++){ ?>
                                                                            <option value="<?php echo $docBrandID[$i]; ?>"><?php echo $docBrandName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Shop Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="shopcode" id="shopcode" class="form-control input-circle font1emGray" placeholder="Please! Enter Shop Code." required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">BU Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="bucode" id="bucode" class="form-control input-circle font1emGray" placeholder="Please! Enter BU Code" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Store Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="storename" id="storename" class="form-control input-circle font1emGray" placeholder="Please! Enter Store Name" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Active Store Date</label>
                                                                <div class="col-md-4 input-group date date-picker" data-date-format="dd/mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                    <input type="text" name="activedate" id="activedate" class="form-control input-circle" style="margin-left: 15px;" readonly required="required">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Suspend Store Date</label>
                                                                <div class="col-md-4 input-group date date-picker" data-date-format="dd/mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                    <input type="text" name="suspenddate" id="suspenddate" class="form-control input-circle" style="margin-left: 15px;">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;&nbsp;ยืนยัน&nbsp;&nbsp;</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>