<?php  
$sql = "select * from db_user_auth order by brand_id, shop_code";
$num = $mQuery->checkNumRows($sql);
?>


                                <div id="showManageBranchDetailDivDetail" style="display:none;"></div>
                                <div align="center" id="showManageBranchDetailLoading" style="display:none; margin-bottom: 50px;">
                                    <br /><br />
                                    <img src="img/loading.gif" border="0" />
                                    <br /><br />
                                    <font class="font1emGray">...กรุณารอสักครู่ค่ะ...</font>
                                </div>

                                
                                <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                <div class="alert alert-success font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูล ร้าน : <?php echo base64_decode($_REQUEST['confirmOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['deleteOK'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>ดำเนินการสำเร็จ!</strong> ลบข้อมูล ร้าน : <?php echo base64_decode($_REQUEST['deleteOK']); ?> เรียบร้อยแล้วค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['errDupName'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>เกิดข้อผิดพลาด!</strong> ข้อมูล Shop Code : <?php echo base64_decode($_REQUEST['errDupName']); ?> ซ้ำ. กรุณาตรวจสอบอีกครั้งค่ะ.
                                </div>
                                <?php } ?>
                                <?php if(isset($_REQUEST['errNo'])){ ?>
                                <div class="alert alert-warning font1emNoColor">
                                    <strong>เกิดข้อผิดพลาด!</strong> ไม่พบข้อมูลร้านสาขาที่ต้องการปรับปรุงค่ะ.
                                </div>
                                <?php } ?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_MANAGE_BRANCH_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="brand"><label  class="font1emBlack">Brand</label></th>
                                                    <th class="shopcode"><label  class="font1emBlack">Shop Code</label></th>
                                                    <th class="bucode"><label  class="font1emBlack">BU Code</label></th>
                                                    <th class="site"><label  class="font1emBlack">Site Customer</label></th>
                                                    <th class="store"><label  class="font1emBlack">Store ID - Store Name</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่เพิ่มข้อมูล</label></th>
                                                    <th class="none"><label  class="font1emBlack">วันที่แก้ไขล่าสุด</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $uaid = $r['uaid'];
                                                        $brandID = $r['brand_id'];
                                                        $shopCode = $r['shop_code'];
                                                        $buCode = $r['bu_code'];
                                                        $siteCustomer = $r['site_customer'];
                                                        $storeIDName = $r['store_id_name'];

                                                        $addDate = $dFunc->fullDateThai($r['adddate']);
                                                        $modDate = $dFunc->fullDateThai($r['moddate']);

                                                        $sql = "select brand_name from db_brand where bid=".$brandID;
                                                        $brandName = $mQuery->getResultOneRecord($sql, "brand_name");
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $brandName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $shopCode; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $buCode; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $siteCustomer; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $storeIDName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDate; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $modDate; ?></label></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $uaid; ?>, 'showManageBranchDetail', 'ShowManageBranchDetail');">แก้ไข</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->