<?php  
$sql = "select * from db_admin where aid=".$_SESSION['mLoginID'];

$firstname = $mQuery->getResultOneRecord($sql, "afirstname");
$name = $mQuery->getResultOneRecord($sql, "aname");
$lastname = $mQuery->getResultOneRecord($sql, "alastname");
$username = $mQuery->getResultOneRecord($sql, "ausername");
$password = base64_decode($mQuery->getResultOneRecord($sql, "apass"));
$email = $mQuery->getResultOneRecord($sql, "email");
$groupID = (int)$mQuery->getResultOneRecord($sql, "groupid");
?> 
                                            <?php if(isset($_REQUEST['confirmOK'])){ ?>
                                                <div class="alert alert-success font1emNoColor">
                                                    <strong>ดำเนินการสำเร็จ!</strong> ปรับปรุงข้อมูลของท่านเรียบร้อยแล้วค่ะ.
                                                </div>
                                            <?php } ?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_CHANGE_USER_PROFILE_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/chgUserProfile_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">คำนำหน้า</label>
                                                                <div class="col-md-4">
                                                                    <select name="firstname" class="form-control input-circle font1emGray">
                                                                        <option value="นาย" <?php if($firstname == "นาย"){echo "selected";} ?>>นาย</option>
                                                                        <option value="นาง" <?php if($firstname == "นาง"){echo "selected";} ?>>นาง</option>
                                                                        <option value="นางสาว" <?php if($firstname == "นางสาว"){echo "selected";} ?>>นางสาว</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อผู้เข้าใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="กรุณากรอกชื่อผู้เข้าใช้งาน" value="<?php echo $name; ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">นามสกุลผู้เข้าใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="กรุณากรอกนามสกุลผู้เข้าใช้งาน" value="<?php echo $lastname; ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Username</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="username" id="username" class="form-control input-circle font1emGray" placeholder="กรุณากรอก Username" value="<?php echo $username; ?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email Address</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอก Email Address ผู้เข้าใช้งาน" value="<?php echo $email; ?>" disabled required> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สิทธ์การใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <select name="userauth" id="userauth" class="form-control input-circle font1emGray" disabled="disabled">
                                                                    <?php 
                                                                        $sql = "select * from db_user_group order by gid";
                                                                        $num = $mQuery->checkNumRows($sql);

                                                                        if($num > 0){ ?>
                                                                        <?php 
                                                                            $result = $mQuery->getResultAll($sql); 

                                                                            foreach($result as $r)
                                                                            {
                                                                        ?>
                                                                                <option value="<?php echo $r["gid"] ?>" <?php if($groupID == (int)$r["gid"]){echo "selected";} ?>><?php echo $r['group_name'] ?></option>
                                                                        <?php
                                                                            }  //------  foreach($result as $r)
                                                                        ?>
                                                                    <?php }  //-----  if($num > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="aid" value="<?php echo $_SESSION['mLoginID']; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>