                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!-- <img alt="" class="img-circle" src="assets/layouts/layout/img/avatar3_small.jpg" /> -->
                                    <span class="username username-hide-on-mobile font1emNoColor"> <?php echo $_SESSION['userName']; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!-- <li>
                                        <a href="index.php?f=chgUserProfile" class="font1emNoColor">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="index.php?f=chgPass" class="font1emNoColor">
                                            <i class="icon-calendar"></i> Change Password </a>
                                    </li>
                                    <li class="divider"> </li> -->
                                    <li>
                                        <a  data-toggle="modal" href="#responsive" class="font1emNoColor">
                                            <i class="icon-call-out"></i> ติดต่อสอบถาม </a>
                                    </li>
                                    <li>
                                        <a href="logout.php" class="font1emNoColor">
                                            <i class="icon-key"></i> ออกจากระบบ </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="logout.php" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li>
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->