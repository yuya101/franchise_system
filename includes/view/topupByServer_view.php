<?php  
$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$dateMinusOne = $dFunc->datePlusDay($dateNow, -1, 0, 0);
$dateMinusSeven = $dFunc->datePlusDay($dateNow, -7, 0, 0);
$dateMinusThirdty = $dFunc->datePlusDay($dateNow, -30, 0, 0);

$yesterdayMinusOne = $dFunc->dateMinusDate($dateNow, 1);
$yesterdayMinusSeven = $dFunc->dateMinusDate($dateNow, 7);
$yesterdayMinusThirdty = $dFunc->dateMinusDate($dateNow, 30);

$dateShowNow = $dFunc->fullDateThai($dateNow);
$dateShowYesterday = $dFunc->fullDateThai($dateMinusOne);

$sql = "select sid, server_name from db_server order by sid";
$num = $mQuery->checkNumRows($sql);

if($num > 0)
{
    $result = $mQuery->getResultAll($sql);
    $i = 0;

    foreach($result as $r)
    {
        $sid[$i] = intval("9".(substr("000".$r['sid'], -3, 3)));
        $sid2[$i] = intval("8".(substr("000".$r['sid'], -3, 3)));
        $serverName[$i] = $r['server_name'];

        $sql = "select topid from db_topup_detail where topup_flag=2 and (topup_aid=".$sid[$i]." or topup_aid=".$sid2[$i].") and adddate='".$dateNow."'";
        $topupTransactionToday[$i] = intval($mQuery->checkNumRows($sql));

        $sql = "select topid from db_topup_detail where topup_flag=2 and (topup_aid=".$sid[$i]." or topup_aid=".$sid2[$i].") and adddate='".$dateMinusOne."'";
        $topupTransactionYesterday[$i] = intval($mQuery->checkNumRows($sql));

        $topupDiffTransaction[$i] = $topupTransactionToday[$i] - $topupTransactionYesterday[$i];


        $sql = "select SUM(topup_value) as topup_summary from db_topup_detail where topup_flag=2 and (topup_aid=".$sid[$i]." or topup_aid=".$sid2[$i].") and adddate='".$dateNow."'";
        $topupVolumnToday[$i] = intval($mQuery->getResultOneRecord($sql, "topup_summary"));

        $sql = "select SUM(topup_value) as topup_summary from db_topup_detail where topup_flag=2 and (topup_aid=".$sid[$i]." or topup_aid=".$sid2[$i].") and adddate='".$dateMinusOne."'";
        $topupVolumnYesterday[$i] = intval($mQuery->getResultOneRecord($sql, "topup_summary"));

        $topupDiffVolumn[$i] = $topupVolumnToday[$i] - $topupVolumnYesterday[$i];

        $i++;
    }  //-----  foreach($result as $r)

    unset($result, $r);
}  //--------  if($num > 0)
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_TOPUP_BY_SERVER_TITLE; ?>&nbsp;(Transaction และ Volumn)</label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body" id="topupByServer_div">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupByServer_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="datenow" rowspan="2" style="text-align: center;"><label  class="font1emBlack">ชื่อ Server</label></th>
                                                    <th class="transaction" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Transaction (รายการ)</label></th>
                                                    <th class="volumn" colspan="3" style="text-align: center;"><label  class="font1emBlack">จำนวน Volumn (บาท)</label></th>
                                                </tr>
                                                <tr>
                                                    <th class="todayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayTrans" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeTrans" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                    <th class="todayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowNow; ?></label></th>
                                                    <th class="yesterdayVolumn" style="text-align: center;"><label  class="font1emBlack"><?php echo $dateShowYesterday; ?></label></th>
                                                    <th class="changeVolumn" style="text-align: center;"><label  class="font1emBlack">เปลี่ยนแปลง</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php for($i=0; $i< count($sid); $i++){ ?>
                                                <tr>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo $serverName[$i]; ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTransactionToday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupTransactionYesterday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffTransaction[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupVolumnToday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupVolumnYesterday[$i], 0); ?></label></td>
                                                    <td style="text-align: center;"><label  class="font1emGray"><?php echo number_format($topupDiffVolumn[$i], 0); ?></label></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์จำนวน Transaction ของ Server ส่งข้อมูลการเติมเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="topupByServerTransaction" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->


                                <!-- BEGIN : HIGHCHARTS -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase font1emNoColor">ภาพรวมวิเคราะห์รายได้ผ่าน Server ส่งข้อมูลการเติมเงิน 30 วันย้อนหลัง</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="topupByServerVolumn" style="height:500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END : HIGHCHARTS -->