<?php
$dir = $_SERVER['DOCUMENT_ROOT'];
$appDir = "/franchise_system";
$fullURL = "http://".$_SERVER['HTTP_HOST'];

$textFilePath = $dir.$appDir."/upload/";
$configPath = $dir.$appDir."/includes/config/";
$classPath = $dir.$appDir."/includes/class/";
$imagePath = $dir.$appDir."/img/";
$jsonPath = $dir.$appDir."/includes/json/";
$ajaxPath = $dir.$appDir."/includes/ajax/";
$importClassPath = $dir.$appDir."/includes/class/importClass/";
$uploadPath = "upload/";
$uploadFolderPath = $fullURL.$appDir."/upload/";
$uploadBeforeSplitFilePath = $dir.$appDir."/upload/_uploadBefSplit/";
$uploadAfterSplitFilePath = $dir.$appDir."/upload/_uploadAfterSplit/";
$uploadFromWebServiceFilePath = $dir.$appDir."/upload/_uploadForWebService/";
$uploadFromWebServiceAfterSplitFilePath = $dir.$appDir."/upload/_uploadForWebServiceAfterSplit/";


$uploadTestFilePath = $dir.$appDir."/test/";



require_once($classPath."dimText.php");
require_once($classPath."mainfunction.php");
require_once($classPath."mainquery.php");
require_once($classPath."datefunction.php");
require_once($configPath."calValue.php");
require_once($configPath."language.php");
require_once($classPath."franchisefunction.php");
?>
