<?php
require_once("dimText.php");
require_once($configPath."config.php");

class Connect_DB
{
	private $conn;
	
	public function __construct()
	{		
		$this->conn=mysqli_init(); 
		mysqli_ssl_set($this->conn, NULL, NULL, ($_SERVER['DOCUMENT_ROOT']."/MyServerCACert.pem"), NULL, NULL);
		mysqli_real_connect($this->conn, DB_HOST, DB_USERNAME, DB_PASS, DB_NAME, 3306) or die("Could not connect to MySQL Database");
		mysqli_set_charset( $this->conn, 'utf8');
	}
	
	public function getConn()
	{
		return $this->conn;
	}
	
	public function freeConn()
	{
		mysqli_close($this->conn);
	}
}
?>