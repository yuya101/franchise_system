<?php
require_once("dimText.php");
require_once($classPath."mainquery.php");
require_once($classPath."mainfunction.php");
require_once($classPath."datefunction.php");
require_once($configPath."calValue.php");

class FranchiseFunction
{
	private $fileName;
	private $dFunc, $dateNow, $timeStamp;
	private $strFileName, $extension;
	
	
	public function __construct()
	{
		// Construction Method
	}


	public function unzipDocumentFile($startPath, $destinationPath){
		$mQuery = new MainQuery();
		$mFunc = new MainFunction();
		$dFunc = new DateFunction();
		
		$dateNow = $dFunc->getDateChris();
		$timeNow = $dFunc->getTimeNow();

		$fileCount = 0;

		$files = array();

		$dir = opendir($startPath);

		while (($file = readdir($dir)) !== false){
			if((substr($file, -4)==".zip") or (substr($file, -4)==".ZIP")){
				$files[] = $file;
			}  //-----  if(substr($file, -4)==".zip")
		}  //----  while (($file = readdir($dir)) !== false)

		closedir($dir);


		foreach ($files as $file){
			$tempFile = $file; 
			// $fileDetail = pathinfo($file);
			// $extension = ".".$fileDetail['extension'];
			// $fileName = $fileDetail['filename'];

			$zip = new ZipArchive;
			if ($zip->open($startPath.$tempFile) === TRUE) {
			  $zip->extractTo($destinationPath);
			  $zip->close();
			}   //----  if ($zip->open($destinationPath.$tempFile) === TRUE)

			unset($zip);

			unlink($startPath.$tempFile);

			$fileCount++;
		}  //-------  foreach ($files as $file)


		unset($mQuery, $dFunc, $mFunc, $files, $file);

		return $fileCount;
	}  //------  public function unzipDocumentFile($startPath, $destinationPath)

	
	public function insertDocFileToDB($startPath, $destinationPath, $responseEmail)
	{
		$mQuery = new MainQuery();
		$mFunc = new MainFunction();
		$dFunc = new DateFunction();
		
		$dateNow = $dFunc->getDateChris();
		$timeNow = $dFunc->getTimeNow();

		$monthChk = substr($dateNow, 0, 6);
		$monthShowInThai = $dFunc->monthInThai2($monthChk);

		$dateNowShow = $dFunc->changeDateToDDMMYYYY3($dateNow);
		$loopReadLimit = 1000;
		$loopChk = 0;

		$fileUnzipCount = $this->unzipDocumentFile($startPath, $startPath);  //---- ต้องการให้ลงที่เดิม

		$files = array();
		$docMoveComplete = 0;
		$sender_email = "admin@mfgfranchise.com";

		$dir = opendir($startPath);

		while (($file = readdir($dir)) !== false){

		    if((substr($file, -4)==".pdf") or (substr($file, -4)==".PDF")){
		        $files[] = $file;
		    }  //-------  if(substr($file, -4)==".pdf")

		}  //-------  while (($file = readdir($dir)) !== false)
		
		closedir($dir);

		foreach ($files as $file){
			$fileDetail = pathinfo($file);
			$extension = ".".$fileDetail['extension'];
			$fileName = $fileDetail['filename'];
			$email_customer = "-";


			if(strlen($fileName) == 12){
				$docBranchName = substr(strtoupper($fileName), 0, 4);
				$documentYear = substr($fileName, 4, 4);
				$documentMonth = substr($fileName, -4, 2);
				$documentSpecialType = (int)substr($fileName, -2, 2);

				$documentDate = "01";

				$sql = "select special_type_name from db_special_fianace_type where fid=".$documentSpecialType;
				$specialTypeName =$mQuery->getResultOneRecord($sql, "special_type_name");

				$docTypeID = 1;
				$docCategoryID = 1;
				$docTitle = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear;
				$docMonth = $documentMonth."/".$documentYear;
				//$docDescription = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear." - เอกสารประเภท ".$specialTypeName;
				$docDescription = "เอกสารประเภท ".$specialTypeName;
				$docFileName = $fileName.$extension;
			    	$targetFile =  $docFileName;

			    	$newFolderPath = $destinationPath.$docBranchName."/".$documentYear."/".$documentMonth."/";


			    	if (!file_exists($newFolderPath)) {
					mkdir($newFolderPath, 0777, true);
				}  //------  if (!file_exists($newFolderPath)) 

			    	if (!file_exists($newFolderPath.$docFileName)) {
			    		$startBrandName = strtoupper(substr($fileName, 0, 1));

					$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
					}else{
						$brandid = 0;
					}  //-----  if($num > 0)

					$sql = "insert into db_document values(NULL";
					$sql = $sql.", ".$docTypeID."";
					$sql = $sql.", ".$docCategoryID."";
					$sql = $sql.", ".$brandid."";
					$sql = $sql.", '".$docBranchName."'";
					$sql = $sql.", '-'";
					$sql = $sql.", '".$docTitle."'";
					$sql = $sql.", '".$docDescription."'";
					$sql = $sql.", '".$docFileName."'";
					$sql = $sql.", '".$newFolderPath."'";
					$sql = $sql.", '".$documentDate."'";
					$sql = $sql.", '".$documentMonth."'";
					$sql = $sql.", '".$documentYear."'";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.")";
					$mQuery->querySQL($sql);


					$sql = "select did from db_document order by did desc limit 1";
					$did = $mQuery->getResultOneRecord($sql, "did");

					$sql = "select uaid, site_customer, email from db_user_auth where shop_code='".$docBranchName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$uaid = $mQuery->getResultOneRecord($sql, "uaid");
						$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
						$email_customer = $mQuery->getResultOneRecord($sql, "email");
					}else{
						$uaid = 0;
						$site_customer = "-";
						$email_customer = "-";
					}  //------  if($num > 0)

					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
					$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
					$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
					$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
					$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);


					$sqlDocBranch = "insert into db_document_map_special_type values(".$did.", ".$documentSpecialType.")";
					$mQuery->querySQL($sqlDocBranch);
					
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}else{
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}  //------  if (!file_exists($newFolderPath)) 

				$docMoveComplete = $docMoveComplete + 1;

			}elseif(strlen($fileName) == 15){
				$docBranchName = substr(strtoupper($fileName), 0, 4);
				$documentYear = substr($fileName, 4, 4);
				$documentMonth = substr($fileName, 8, 2);
				$documentSpecialType = (int)substr($fileName, 10, 2);
				$documentExtra3Digits = substr($fileName, -3, 3); //------ Extra Type From Outsource PDF Files

				$documentDate = "01";

				$sql = "select special_type_name from db_special_fianace_type where fid=".$documentSpecialType;
				$specialTypeName =$mQuery->getResultOneRecord($sql, "special_type_name");

				$docTypeID = 1;
				$docCategoryID = 1;
				$docTitle = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear;
				$docMonth = $documentMonth."/".$documentYear;
				//$docDescription = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear." - เอกสารประเภท ".$specialTypeName;
				$docDescription = "เอกสารประเภท ".$specialTypeName." ฉบับที่ ".$documentExtra3Digits;
				$docFileName = $fileName.$extension;
			    	$targetFile =  $docFileName;

			    	$newFolderPath = $destinationPath.$docBranchName."/".$documentYear."/".$documentMonth."/";


			    	if (!file_exists($newFolderPath)) {
					mkdir($newFolderPath, 0777, true);
				}  //------  if (!file_exists($newFolderPath)) 

			    	if (!file_exists($newFolderPath.$docFileName)) {
			    		$startBrandName = strtoupper(substr($fileName, 0, 1));

					$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
					}else{
						$brandid = 0;
					}  //-----  if($num > 0)

					$sql = "insert into db_document values(NULL";
					$sql = $sql.", ".$docTypeID."";
					$sql = $sql.", ".$docCategoryID."";
					$sql = $sql.", ".$brandid."";
					$sql = $sql.", '".$docBranchName."'";
					$sql = $sql.", '-'";
					$sql = $sql.", '".$docTitle."'";
					$sql = $sql.", '".$docDescription."'";
					$sql = $sql.", '".$docFileName."'";
					$sql = $sql.", '".$newFolderPath."'";
					$sql = $sql.", '".$documentDate."'";
					$sql = $sql.", '".$documentMonth."'";
					$sql = $sql.", '".$documentYear."'";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.", '".$dateNow."'";
					$sql = $sql.", '".$timeNow."'";
					$sql = $sql.", 0";
					$sql = $sql.")";
					$mQuery->querySQL($sql);


					$sql = "select did from db_document order by did desc limit 1";
					$did = $mQuery->getResultOneRecord($sql, "did");

					$sql = "select uaid, site_customer, email from db_user_auth where shop_code='".$docBranchName."'";
					$num = $mQuery->checkNumRows($sql);

					if($num > 0){
						$uaid = $mQuery->getResultOneRecord($sql, "uaid");
						$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
						$email_customer = $mQuery->getResultOneRecord($sql, "email");
					}else{
						$uaid = 0;
						$site_customer = "-";
						$email_customer = "-";
					}  //------  if($num > 0)

					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
					$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
					$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
					$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
					$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);


					$sqlDocBranch = "insert into db_document_map_special_type values(".$did.", ".$documentSpecialType.")";
					$mQuery->querySQL($sqlDocBranch);
					
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}else{
					rename($startPath.$targetFile, $newFolderPath.$targetFile);
				}  //------  if (!file_exists($newFolderPath)) 

				$docMoveComplete = $docMoveComplete + 1;
			}  //-----  if(strlen($fileName) == 12)


			if($email_customer != "-"){
				$sql = "select id from db_send_mail_history where month_check='".$monthChk."' limit 1";
				$num = $mQuery->checkNumRows($sql);

				if($num == 0){
					$sql = "delete from db_send_mail_history";
					$mQuery->querySQL($sql);
				}  //------  if($num == 0)


				$sql = "select aid from db_user where ausername='".$email_customer."'";
				$num = $mQuery->checkNumRows($sql);

				if($num > 0){
					$userID = 0;
					$userID = $mQuery->getResultOneRecord($sql, "aid");

					$sql = "select id from db_send_mail_history where aid=".$userID;
					$num = $mQuery->checkNumRows($sql);

					if($num == 0){
						$sql = "insert into db_send_mail_history values(NULL, ".$userID.", '".$monthChk."', '".$dateNow."', '".$timeNow."')";
						$mQuery->querySQL($sql);

						$toEmail = '{"email": "'.$email_customer.'"}';
						$fromEmail = '{"email": "support@mfgfranchise.com"}';
						$mailSubject = 'เอกสาร Finance ประจำเดือน '.$monthShowInThai.' พร้อมตรวจสอบแล้วค่ะ';
						$mailContent = 'เรียน ท่านลูกค้า<br><br>ระบบได้พร้อมนำส่งข้อมูล Finance ประจำเดือน '.$monthShowInThai.' เข้าสู่ระบบเรียบร้อยแล้ว ท่านสามารถตรวจสอบเอกสารได้ทันทีค่ะ.<br>';
						$mailContent = $mailContent."<a href='https://mfgfranchise-uat.azurewebsites.net/franchise_system' target='_blank'>กดที่นี่เพื่อเข้าสู่ระบบ</a><br><br>';
						$mailContent = $mailContent.'ขอแสดงความนับถือ<br>mfgfranchise.com";
						// $this->sendMailWithcURL($toEmail, $fromEmail, $mailSubject, $mailContent);
					}  //--------  if($num == 0)
				}  //------------  if($num > 0)
			}  //---------------  if($email_customer != "-")

			$loopChk++;

			if($loopChk >= $loopReadLimit){
				break;
			}  //-------  if($loopChk >= $loopReadLimit)
		}  //-------  foreach ($files as $file)

		unset($files, $file);

		
		if($responseEmail != "-"){
			$toEmail = $responseEmail;
			$fromEmail = '{"email": "admin@mfgfranchise.com"}';
			$mailSubject = "ผลการดึงไฟล์จาก Interface Server ประจำวันที่ ".$dateNowShow;
			$mailContent = "เรียน ท่านผู้เกี่ยวข้อง<br><br>ผลการดึงไฟล์จาก Interface Server ประจำวันที่ ".$dateNowShow." - เวลา ".$timeNow." สามารถนำเข้าไฟล์ได้ทั้งหมด ".$loopChk." ไฟล์ค่ะ.<br><br>";
			$mailContent = $mailContent."ขอแสดงความนับถือ<br>mfgfranchise.com";
			
			$this->sendMailWithcURL($toEmail, $fromEmail, $mailSubject, $mailContent);
		}  //----  if($responseEmail != "-")

		unset($mQuery, $dFunc, $mFunc);
	}  //--------  public function insertDocFileDetailToDB()


	public function sendMail($classPath, $fromName, $fromEmail, $toName, $toEmail, $contentType, $contentDetail){
		require $classPath.'../../sendgrid/vendor/autoload.php';

		$from = new SendGrid\Email($fromName, $fromEmail);
		$subject = "Sending with SendGrid is Fun3";
		$to = new SendGrid\Email($toName, $toEmail);
		$content = new SendGrid\Content($contentType, $contentDetail);
		$mail = new SendGrid\Mail($from, $subject, $to, $content);

		$apiKey = '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';
		$sg = new \SendGrid($apiKey);

		$sg->client->mail()->send()->post($mail);
		// echo $response->statusCode();
		// print_r($response->headers());
		// echo $response->body();
	}  //------  public function sendMail($responseEmail)


	public function sendMailWithcURL($toEmail, $fromEmail, $subject, $content){
		//------ For Test Project
	}  //------  public function sendMail($responseEmail)


	public function sendMailWithcURLReal($toEmail, $fromEmail, $subject, $content){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => '{"personalizations": [{"to": ['.$toEmail.']}], "from": '.$fromEmail.',"subject":"'.$subject.'", "content": [{"type": "text/html", "value": "'.$content.'"}]}',
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Bearer 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",
		    "content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		 // '{"personalizations": [{"to": ['.$toEmail.'],"cc": [{"email":"yuya101@hotmail.com"}]}], "from": {"email": "yuya101@hotmail.com"},"subject":"Test New Mail Function!", "content": [{"type": "text/html", "value": "Heya!"}]}',
	}  //------  public function sendMail($responseEmail)
	
	
	public function close_db()
	{
		// mysqli_close();
	}
}
?> 