<?php
require_once("dimText.php");
require_once($configPath."config.php");

class Connect_DB
{
	private $conn;
	
	public function __construct()
	{		
		$this->conn=mysqli_connect(DB_HOST, DB_USERNAME, DB_PASS, DB_NAME) or die("Could not connect to MySQL Database");
		mysqli_set_charset( $this->conn, 'utf8');
		// mysqli_select_db(DB_NAME) or die("Could not select to database");
		// mysqli_query("SET NAMES UTF8");
	}
	
	public function getConn()
	{
		return $this->conn;
	}
	
	public function freeConn()
	{
		mysqli_close($this->conn);
	}
}
?>