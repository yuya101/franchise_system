<?php
require_once("dimText.php");
require_once($classPath."connect_db.php");

class MainQuery
{
	private $connect, $getConnect;
	private $query, $result, $num, $sql;
	private $uid;
	
	
	public function __construct()
	{
		// Construction Method
		$this->connect = new Connect_DB();
		$this->getConnect = $this->connect->getConn();
	}
	
	public function checkNumRows($sqlTemp)  //  ------ For Check Count Row 
	{
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		return $this->num;
	}
	
	
	public function getPrimaryID($sqlTemp, $wantID)  //  ---------  For Get target Primary id
	{
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result[$wantID];
		}
		else
		{
			$this->uid = 0;
		}
		
		return $this->uid;
	}
	
	
	public function getNewPrimaryID($sqlTemp, $wantID)  //  ---------  For Get next Primary id
	{
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result[$wantID] + 1;
		}
		else
		{
			$this->uid = 1;
		}
		
		return $this->uid;
	}
	
	
	public function querySQL($sqlTemp)  //-------- For Query SQL
	{
		if(!mysqli_query($this->getConnect, $sqlTemp))
		{
			die(mysqli_error($this->connect));
		}  //-----  if(!mysqli_query($this->getConnect, $sqlTemp))
	}
	
	
	public function getResultAll($sqlTemp)  //--------- For Fetch SQL Result Set 
	{
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			while($this->result = mysqli_fetch_assoc($this->query))
			{
				$allResult[] = $this->result;
			}
		}

		return $allResult;
	}
	
	
	public function getResultOneRecord($sqlTemp, $wantRecord)  //--------- For Fetch SQL Result 1 Record
	{
		$oneResult = "";
		
		$this->query = mysqli_query($this->getConnect, $sqlTemp);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			while($this->result = mysqli_fetch_assoc($this->query))
			{
				$oneResult = $this->result[$wantRecord];
			}
		}
		
		return $oneResult;
	}
	
	
	public function getRandomRecord($tmpSQL, $recChkID, $noOfRec)
	{		
		$noOfRec = intval($noOfRec);  //--------------- จำนวน Record ที่ต้องการ Random
		
		$this->query = mysqli_query($this->getConnect, $tmpSQL);
		$this->num = mysqli_num_rows($this->query);
		$maxValue = $this->num;
		
		for($i=0; $i<$noOfRec; $i++)
		{
			$randomNo = rand(1, $maxValue);
			$this->sql = $tmpSQL.' and '.$recChkID.'='.$randomNo;
			$this->query = mysqli_query($this->getConnect, $this->sql);
			$this->num = mysqli_num_rows($this->query);
				
			if($this->num > 0)
			{
				if(isset($randomRec))
				{
					if(in_array($randomNo, $randomRec))
					{
						$i = $i - 1;
					}
					else
					{
						$randomRec[$i] = $randomNo;
					}
				}
				else
				{
					$randomRec[$i] = $randomNo;
				}
			}
			else
			{
				$i = $i - 1;
			} //-----------  if($this->num > 0)			
		}  //---------  for($i=0; $i<$noOfRec; $i++)
				
		return $randomRec;
	}
	
	
	public function getTopicPoint($topicID)
	{
		$sql = "select topicPoint from point_topic where topicID=".$topicID;
		$this->query = mysqli_query($this->getConnect, $sql);
		$this->num = mysqli_num_rows($this->query);
		
		if($this->num > 0)
		{
			$this->result = mysqli_fetch_assoc($this->query);
			$this->uid = (int)$this->result['topicPoint'];
		}
		else
		{
			$this->uid = 0;
		}
		
		return $this->uid;
	}
	
	
	
	public function close_db($connect)
	{
		// mysqli_close($connect);
	}
}
?> 