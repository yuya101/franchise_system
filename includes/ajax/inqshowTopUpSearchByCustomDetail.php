<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $username = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_topup_detail where topup_user like '".$username."' order by adddate desc, addtime desc limit 1000";
    $num = $mQuery->checkNumRows($sql);
?>
                                <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
                                
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-globe"></i><label class="font1emWhite"> <?php echo PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="username"><label  class="font1emBlack">Username</label></th>
                                                    <th class="name"><label  class="font1emBlack">ชื่อ - นามสกุล</label></th>
                                                    <th class="none"><label  class="font1emBlack">Email Address</label></th>
                                                    <th class="none"><label  class="font1emBlack">หมายเลขโทรศัพท์</label></th>
                                                    <th class="date"><label  class="font1emBlack">วัน - เวลาทำรายการ</label></th>
                                                    <th class="topup"><label  class="font1emBlack">เงิน Top Up ที่เข้ามา</label></th>
                                                    <th class="callcenter"><label  class="font1emBlack">พนักงานที่ดำเนินการเติมเงิน</label></th>
                                                    <th class="status"><label  class="font1emBlack">สถานะ</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $topupID = $r['topid'];
                                                        $userID = $r['uid'];
                                                        $userName = $r['topup_user'];
                                                        $email = $r['topup_email'];
                                                        $mobile = $r['topup_mobile'];
                                                        $addDateAndTime = $dFunc->fullDateThai($r['adddate'])." - ".$r['addtime'];
                                                        $topupValue = number_format($r['topup_value'], 0);

                                                        $sql = "select user_real_name, user_real_lastname from db_user_detail where uid=".$userID;
                                                        $userFullName = "คุณ".$mQuery->getResultOneRecord($sql, "user_real_name")." ".$mQuery->getResultOneRecord($sql, "user_real_lastname");

                                                        $status = (int)$r['topup_flag'];

                                                        if($status == 0)
                                                        {
                                                            $statusText = "<label class='font1emRed'>รอการดำเนินการ</label>";
                                                        }
                                                        elseif($status == 1)
                                                        {
                                                            $statusText = "<label class='font1emOrange'>อยู่ระหว่างดำเนินการ</label>";
                                                        }
                                                        else
                                                        {
                                                            $statusText = "<label class='font1emBlue'>ปิดงาน</label>";
                                                        }  //----  if($status == 0)


                                                        $topUpCallcenter = (int)$r['topup_aid'];

                                                        $sql = "select aname, alastname, ausername from db_admin where aid=".$topUpCallcenter;
                                                        $num = $mQuery->checkNumRows($sql);

                                                        if($num > 0)
                                                        {
                                                                $topUpCallCenter = $mQuery->getResultOneRecord($sql, "ausername")." ( คุณ".$mQuery->getResultOneRecord($sql, "aname")." ".$mQuery->getResultOneRecord($sql, "alastname")." )";
                                                        }
                                                        else
                                                        {
                                                                $serverCloseJobID = (int)substr($topUpCallcenter, -3, 3);

                                                                $sql = "select sid, server_name from db_server where sid=".$serverCloseJobID;
                                                                $num = $mQuery->checkNumRows($sql);

                                                                if($num > 0)
                                                                {
                                                                    $topUpCallCenter = $mQuery->getResultOneRecord($sql, "server_name");
                                                                }
                                                                else
                                                                {
                                                                    $topUpCallCenter = "ไม่พบชื่อผู้ทำรายการเติมเงิน";
                                                                }  //------  if($num > 0)
                                                        }  //-----  if($num > 0)
                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $userName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $userFullName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $email; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $mobile; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDateAndTime; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $topupValue; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $topUpCallCenter; ?></label></td>
                                                    <td><?php echo $statusText; ?></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

                                <!-- BEGIN PAGE LEVEL PLUGINS -->
                                <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                                <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                                <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                                <!-- END PAGE LEVEL PLUGINS -->
                                <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                <script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
                                <!-- END PAGE LEVEL SCRIPTS -->

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>