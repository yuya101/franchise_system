<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_admin_login_history where aid=".$selectID." order by logindate desc, logintime desc limit 500";
    $num = $mQuery->checkNumRows($sql);
?>
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-laptop"></i><label class="font1emWhite"> <?php echo PAGE_SHOW_USER_TITLE; ?></label> </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="showUserHistory_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="number">No.</th>
                                                    <th class="date">วันที่</th>
                                                    <th class="time">เวลา</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);
                                                    $i = 1;

                                                     foreach($result as $r)
                                                    {
                                                        $logindate = $dFunc->fullDateThai($r['logindate']);
                                                        $logintime = $r['logintime'];
                                            ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $logindate; ?></td>
                                                    <td><?php echo $logintime; ?></td>
                                                </tr>
                                                <?php $i++; ?>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>