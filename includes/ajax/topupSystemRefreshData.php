<?php  
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$sql = "select * from db_topup_detail where topup_flag!=2 order by adddate, addtime limit 1500";
$num = $mQuery->checkNumRows($sql);
?>
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="topupSystem_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="username"><label  class="font1emBlack">Username</label></th>
                                                    <th class="none"><label  class="font1emBlack">ชื่อ - นามสกุล</label></th>
                                                    <th class="none"><label  class="font1emBlack">Email Address</label></th>
                                                    <th class="none"><label  class="font1emBlack">หมายเลขโทรศัพท์</label></th>
                                                    <th class="none"><label  class="font1emBlack">วัน - เวลาทำรายการ</label></th>
                                                    <th class="topup"><label  class="font1emBlack">เงิน Top Up ที่เข้ามา</label></th>
                                                    <th class="status"><label  class="font1emBlack">สถานะ</label></th>
                                                    <th class="button"><label  class="font1emBlack">จัดการรายละเอียด</label></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);

                                                     foreach($result as $r)
                                                    {
                                                        $topupID = $r['topid'];
                                                        $userID = $r['uid'];
                                                        $userName = $r['topup_user'];
                                                        $email = $r['topup_email'];
                                                        $mobile = $r['topup_mobile'];
                                                        $addDateAndTime = $dFunc->fullDateThai($r['adddate'])." - ".$r['addtime'];
                                                        $topupValue = number_format($r['topup_value'], 0);

                                                        $sql = "select user_real_name, user_real_lastname from db_user_detail where uid=".$userID;
                                                        $userFullName = "คุณ".$mQuery->getResultOneRecord($sql, "user_real_name")." ".$mQuery->getResultOneRecord($sql, "user_real_lastname");

                                                        $status = (int)$r['topup_flag'];

                                                        if($status == 0)
                                                        {
                                                            $statusText = "<label class='font1emRed'>รอการดำเนินการ</label>";
                                                        }
                                                        elseif($status == 1)
                                                        {
                                                            $statusText = "<label class='font1emOrange'>อยู่ระหว่างดำเนินการ</label>";
                                                        }
                                                        else
                                                        {
                                                            $statusText = "<label class='font1emBlue'>ปิดงาน</label>";
                                                        }  //----  if($status == 0)

                                            ?>
                                                <tr>
                                                    <td><label  class="font1emGray"><?php echo $userName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $userFullName; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $email; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $mobile; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $addDateAndTime; ?></label></td>
                                                    <td><label  class="font1emGray"><?php echo $topupValue; ?></label></td>
                                                    <td><?php echo $statusText; ?></td>
                                                    <td><button type="button" class="btn green-meadow font1emWhite" onclick="return showManageDetailInButtonByID(<?php echo $topupID; ?>, 'showTopUpSystemDetail', 'ShowTopUpSystemDetail');">ทำการเติมเงิน</button></td>
                                                </tr>
                                                <?php }  //-------  foreach($result as $r) ?>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                        </table>

                                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                                        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
                                        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
                                        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
                                        <!-- END PAGE LEVEL PLUGINS -->
                                        <!-- BEGIN PAGE LEVEL SCRIPTS -->
                                        <script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
                                        <!-- END PAGE LEVEL SCRIPTS -->
<?php unset($mQuery, $mFunc, $dFunc); ?>