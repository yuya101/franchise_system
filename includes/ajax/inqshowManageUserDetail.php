<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_user where aid=".$selectID;
    $customerNo = $mQuery->getResultOneRecord($sql, "customer_no");
    $customerName = $mQuery->getResultOneRecord($sql, "customer_name");
    $customerLastname = $mQuery->getResultOneRecord($sql, "alastname");
    $password = base64_decode($mQuery->getResultOneRecord($sql, "apass"));
    $email = $mQuery->getResultOneRecord($sql, "email");
    $groupID = (int)$mQuery->getResultOneRecord($sql, "groupid");
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_MANAGE_USER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/manageUser_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer No.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="cusno" id="cusno" class="form-control input-circle font1emGray" placeholder="Please! Enter customer number." required value="<?php echo $customerNo; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer Name.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="Please! Enter customer name." required value="<?php echo $customerName; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Customer LastName.</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="Please! Enter customer lastname." required value="<?php echo $customerLastname; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email Address.</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="Please! Enter email address." readonly value="<?php echo $email; ?>"> </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Password.</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="password" name="password" id="password" class="form-control input-circle-left font1emGray" placeholder="Please! Enter customer password." required value="<?php echo $password; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สิทธ์การใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <select name="userauth" id="userauth" class="form-control input-circle font1emGray">
                                                                    <?php 
                                                                        $sql = "select * from db_user_group order by gid";
                                                                        $num = $mQuery->checkNumRows($sql);

                                                                        if($num > 0){ ?>
                                                                        <?php 
                                                                            $result = $mQuery->getResultAll($sql); 

                                                                            foreach($result as $r)
                                                                            {
                                                                        ?>
                                                                                <option value="<?php echo $r["gid"] ?>" <?php if($groupID == (int)$r["gid"]){echo "selected";} ?>><?php echo $r['group_name'] ?></option>
                                                                        <?php
                                                                            }  //------  foreach($result as $r)
                                                                        ?>
                                                                    <?php }  //-----  if($num > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                    <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="aid" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>