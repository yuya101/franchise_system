<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_document_category where cat_id=".$selectID;
    $typeID = $mQuery->getResultOneRecord($sql, "type_id");
    $catName = $mQuery->getResultOneRecord($sql, "cat_name");


    $sqlDocType = "select * from db_document_type order by type_name";
    $numDocType = $mQuery->checkNumRows($sqlDocType);

    if($numDocType > 0){
        $resultDocType = $mQuery->getResultAll($sqlDocType);
        $i = 0;

        foreach ($resultDocType as $rd) {
            $docTypeID[$i] = $rd['type_id'];
            $docTypeName[$i] = $rd['type_name'];

            $i++;
        }  //-----  foreach ($resultDocType as $rd)


        $sqlDocCat = "select * from db_document_category where type_id=".$docTypeID[0];
        $numDocCat = $mQuery->checkNumRows($sqlDocCat);

        if($numDocCat > 0){
            $resultDocCat = $mQuery->getResultAll($sqlDocCat);
            $j = 0;

            foreach ($resultDocCat as $rc) {
                $docCatID[$j] = $rc['cat_id'];
                $docCatName[$j] = $rc['cat_name'];

                $j++;
            }  //-----  foreach ($resultDocCat as $rc)

            unset($resultDocCat, $rc);
        } //-----  if($num > 0)

        unset($resultDocType, $rd);
    }  //----  if($numDocType > 0)
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_MANAGE_CATEGORY_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/manageCategory_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <h3 class="form-section">Category Detail.</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Document Type</label>
                                                                <div class="col-md-4">
                                                                    <select name="typeid" id="typeid" class="form-control input-circle font1emGray">
                                                                    <?php if($numDocType > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocType; $i++){ ?>
                                                                            <option value="<?php echo $docTypeID[$i]; ?>" <?php if((int)$typeID == (int)$docTypeID[$i]){echo "selected";} ?>><?php echo $docTypeName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Category Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="catname" id="catname" class="form-control input-circle font1emGray" placeholder="Please! Enter Category Name" value="<?php echo $catName; ?>" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                    <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type="hidden" name="catID" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>