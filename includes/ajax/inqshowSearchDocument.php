<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['brandID']) and $_REQUEST['brandID'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $retentionDate = $dFunc->datePlusDay($dateNow, 0, 0, -1);
    $docTypeID = 1;
    $docTypeNameText = "Financial";

    $downloadAllBtn = 1;

    $brandID = $mFunc->chgSpecialCharInputNumber($_REQUEST['brandID']);
    $docmonth = $mFunc->chgSpecialCharInputText($_REQUEST['docmonth']);
    $docbranchcode = $mFunc->chgSpecialCharInputText($_REQUEST['docbranchcode']);
    

    $sql = "select brand_name from db_brand where bid=".$brandID;
    $brandNameShow = $mQuery->getResultOneRecord($sql, "brand_name");

    $showPathText = "ระบบการค้นหาเอกสาร / ".$brandNameShow;

    $sql = "select * from db_document where brand_id=".$brandID;

    if ($docbranchcode != "-") {
        $sql = $sql." and shop_code like '".$docbranchcode."%'";

        $showPathText = $showPathText." / ".$docbranchcode;
    }  //-----  if ($docbranchcode != "-")

    if ($docmonth != "-") {
        $year = substr($docmonth,-4, 4);
        $month = substr($docmonth, 0, 2);

        $showPathText = $showPathText." / ".$year." / ".$month;

        $sql = $sql." and month='".$month."' and year='".$year."'";
    }  //-----  if ($docmonth != "-")

    $sql = $sql." and adddate>='".$retentionDate."' order by shop_code, adddate desc";
    $num = $mQuery->checkNumRows($sql);
?>
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                        <div class="tools"></div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="manageUser_tb" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="typename"><label  class="font1emBlack">Branch Code</label></th>
                                                    <!-- <th class="categoryname"><label  class="font1emBlack">Category Name</label></th> -->
                                                    <th class="doctitle"><label  class="font1emBlack">Document Title</label></th>
                                                    <th class="docmonth"><label  class="font1emBlack">Document Month</label></th>
                                                    <?php if($docTypeID == 1){ ?>
                                                        <th class="docremove"><label  class="font1emBlack">Document Retention Date</label></th>
                                                    <?php }  //-----  if($docTypeID == 1) ?>
                                                    <th class="filename"><label  class="font1emBlack">File Name</label></th>
                                                    <!-- <th class="count"><label  class="font1emBlack">Download Count</label></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                if($num > 0)
                                                {
                                                    $result = $mQuery->getResultAll($sql);
                                                    $fileZipArray = array();

                                                     foreach($result as $r)
                                                    {
                                                        $docID = $r['did'];
                                                        $chkAuth = 1;

                                                        $sqlChkAuth = "select daid, brand_id from db_document_authorize where did=".$docID." and type_id=".$docTypeID." and uaid=0";
                                                        $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                        if($numChkAuth > 0){
                                                            if(!empty($chkArrBrandIDEmpty)){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID'])){
                                                                        $chkAuth = 1;
                                                                    }  //------  if(in_array((int)$ra['brand_id'], $_SESSION['arrBrandID']))
                                                                }  //-----  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------  if(!empty($chkArrEmpty))
                                                        }else{
                                                            $sqlChkAuth = "select uaid from db_document_authorize where did=".$docID." and type_id=".$docTypeID;
                                                            $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                                                            if($numChkAuth > 0){
                                                                $resultChkAuth = $mQuery->getResultAll($sqlChkAuth);

                                                                foreach ($resultChkAuth as $ra) {
                                                                    if(!empty($chkArrBranchIDEmpty)){
                                                                        if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID'])){
                                                                            $chkAuth = 1;
                                                                        }  //-----  if(in_array((int)$ra['uaid'], $_SESSION['arrBranchID']))
                                                                    }  //------  if(!empty($chkArrEmpty))
                                                                }  //------  foreach ($resultChkAuth as $ra)

                                                                unset($resultChkAuth, $ra);
                                                            }  //------ if($numChkAuth > 0)
                                                        }  //---  if($numChkAuth > 0)

                                                        if($chkAuth == 1){
                                                            $catID = $r['cat_id'];
                                                            $typeID = $r['type_id'];
                                                            $title = $r['description'];
                                                            $fileName = $r['file_name'];
                                                            $filePath = $r['file_path'];
                                                            $docMonth = $r['month'];
                                                            $docYear = $r['year'];
                                                            $docRetentionDate = $dFunc->datePlusDay($r['adddate'], 0, 0, 1);
                                                            $docRetentionDate = $dFunc->fullDateChris($docRetentionDate);

                                                            $filePathNum = stripos($r['file_path'], "upload");
                                                            $filePath = substr($r['file_path'], $filePathNum);

                                                            $forDate = $dFunc->showMonthYearInEnglish($r['year'].$r['month']);

                                                            $sql = "select type_name from db_document_type where type_id=".$typeID;
                                                            $typeName = $mQuery->getResultOneRecord($sql, "type_name");

                                                            $sql = "select cat_name from db_document_category where cat_id=".$catID;
                                                            $catName = $mQuery->getResultOneRecord($sql, "cat_name");

                                                            $sql = "select dhid from db_user_download_history where document_id=".$docID;
                                                            $downloadCount = number_format($mQuery->checkNumRows($sql), 0);

                                                            array_push($fileZipArray, $filePath.$fileName);
                                            ?>
                                                        <tr>
                                                            <td><label  class="font1emGray"><?php echo $r['shop_code']; ?></label></td>
                                                            <!-- <td><label  class="font1emGray"><?php //echo $catName; ?></label></td> -->
                                                            <td><label  class="font1emGray"><?php echo $title; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $docMonth."/".$docYear; ?></label></td>
                                                            <?php if($docTypeID == 1){ ?>
                                                                <td><label  class="font1emGray"><?php echo $docRetentionDate; ?></label></td>
                                                            <?php }  //-----  if($docTypeID == 1) ?>
                                                            <td><label  class="font1emGray"><a href="<?php echo $filePath.$fileName; ?>" target="_blank" onclick="countDownload(<?php echo $_SESSION['mLoginID']; ?>, <?php echo $docID; ?>);"><?php echo $fileName; ?></a></label></td>
                                                            <!-- <td><label  class="font1emGray"><?php //echo number_format($downloadCount, 0); ?></label></td> -->
                                                        </tr>
                                                    <?php }  //------  if($chkAuth == 1) ?>
                                                <?php }  //-------  foreach($result as $r) ?>
                                                <?php unset($result, $r); ?>
                                                <?php
                                                    $zipname = 'financeAcheive.zip';
                                                    $zip = new ZipArchive;
                                                    $zip->open($filePath.$zipname, ZipArchive::CREATE);
                                                    foreach ($fileZipArray as $file) {
                                                      $zip->addFile($file);
                                                    }  //-------  foreach ($fileZipArray as $file)
                                                    $zip->close();
                                                ?>
                                            <?php 
                                                }else{ 
                                                    $downloadAllBtn = 0;
                                            ?>
                                                    <tr>
                                                        <td colspan="5" style="text-align: center;">
                                                            <label class="font1emRed">
                                                                    ไม่พบข้อมูลที่ท่านต้องการค้นหาค่ะ
                                                            </label>
                                                        </td>
                                                    </tr>
                                            <?php }  //-----  if($num > 0) ?>
                                            </tbody>
                                            <?php if($downloadAllBtn > 0){ ?>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="5" style="text-align: right;"><a href="includes/control/downloadAllZip_Ctl.php?lo=<?php echo base64_encode($filePath); ?>&file=<?php echo base64_encode($zipname); ?>" class="btn btn-circle blue font1emWhite">Download เอกสารทั้งหมด</a></td>
                                                    </tr>
                                                </tfoot>
                                            <?php }  //-----  if($downloadAllBtn > 0) ?>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-scroller.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->