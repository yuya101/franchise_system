<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

$foundData = 0;

if(isset($_REQUEST['brandID']) and $_REQUEST['brandID'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $retentionDate = $dFunc->datePlusDay($dateNow, 0, 0, -1);
    $docTypeID = 1;

    $downloadAllBtn = 1;

    $inqType = $mFunc->chgSpecialCharInputNumber($_REQUEST['brandID']);
    $docmonth = $mFunc->chgSpecialCharInputText($_REQUEST['docmonth']);
    $inqBranchID = $mFunc->chgSpecialCharInputText($_REQUEST['docbranchcode']);
    $inqBranchID = strtoupper($inqBranchID);

    $inqYear = substr($docmonth,-4, 4);
    $inqMonth = substr($docmonth, 0, 2);

    $pageTitle = "ระบบการค้นหาเอกสาร ";

    $showPathText = "";

    if ($inqType == 1) {  //-----  Case Credit Card
        $rowCount = 0;
        $statementAmountSumAll = 0;
        $feeAmountSumAll = 0;
        $taxAmountSumAll = 0;
        $netAmountSumAll = 0;

        $tmpSQL = "db_inquiry_credit_card where year='".$inqYear."' and month='".$inqMonth."' and aid='".$inqBranchID."'";

        $sql = "select business_unit, site_name from ".$tmpSQL." group by business_unit";
        $num = $mQuery->checkNumRows($sql);

        if($num > 0){
            $businessUnit = $mQuery->getResultOneRecord($sql, "business_unit");
            $siteName = $mQuery->getResultOneRecord($sql, "site_name");

            $sql = "select channel_type from ".$tmpSQL." group by channel_type order by channel_type";
            $num = $mQuery->checkNumRows($sql);

            if ($num > 0) {
                $result = $mQuery->getResultAll($sql);
                $channelTypeArr = array();

                foreach ($result as $r) {
                    array_push($channelTypeArr, $r['channel_type']);
                }  //---------  foreach ($result as $r) 

                unset($result, $r);

                if(sizeof($channelTypeArr) > 0){
                    $m = 0;
                    foreach ($channelTypeArr as $ct) {
                        $sql = "select net_amount from ".$tmpSQL." and channel_type='".$ct."' order by business_date";
                        $num = $mQuery->checkNumRows($sql);
                        $rowNoSumCount[$m] = $num;
                        $rowCount = $rowCount + $num + 1;  //----- +1 For Summary Row
                        $m++;
                    }  //---------  foreach ($channelTypeArr as $ct)

                    unset($ct);

                    if($rowCount > 0){

                        $showPathText = $pageTitle." / Statement Commission / ".$businessUnit." / ".$inqYear." / ".$inqMonth;
                    ?>
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                            <div class="tools"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <!-- <div class="table-responsive"> -->
                                                <table class="table table-striped table-bordered table-hover order-column" id="tableScroller">
                                                    <thead>
                                                        <tr>
                                                            <th><label  class="font1emBlack">Business Unit</label></th>
                                                            <th><label  class="font1emBlack">Site Name</label></th>
                                                            <th><label  class="font1emBlack">Channel Type</label></th>
                                                            <th><label  class="font1emBlack">Business Date</label></th>
                                                            <th><label  class="font1emBlack">Statement Date</label></th>
                                                            <th><label  class="font1emBlack">Statement Amount</label></th>
                                                            <th><label  class="font1emBlack">Fee Amount</label></th>
                                                            <th><label  class="font1emBlack">Tax Amount</label></th>
                                                            <th><label  class="font1emBlack">Net Amount</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label  class="font1emGray"><?php echo $businessUnit; ?></label></td>
                                                            <td><label  class="font1emGray"><?php echo $siteName; ?></label></td>
                                                            <?php
                                                            $m = 0;
                                                            $rowSpanUseCount = 0;
                                                            $foundData = 1;

                                                            foreach ($channelTypeArr as $ct) {
                                                                $statementAmountSumSection = 0;
                                                                $feeAmountSumSection = 0;
                                                                $taxAmountSumSection = 0;
                                                                $netAmountSumSection = 0;
                                                                $chkCT = 0;

                                                                if($m > 0){ 
                                                                    echo '<tr>';
                                                                    $rowSpanUseCount = 0;
                                                                }  //-----  if($m > 0)

                                                                    $sql = "select business_date, statement_date, statement_amount, fee_amount, tax_amount, net_amount from ".$tmpSQL." and channel_type='".$ct."' order by business_date";
                                                                    $num = $mQuery->checkNumRows($sql);

                                                                    if($num > 0){
                                                                        $resultInq = $mQuery->getResultAll($sql);

                                                                        foreach ($resultInq as $rq) {
                                                                            if(($rowSpanUseCount < $rowNoSumCount[$m]) and ($rowSpanUseCount  != 0)){echo "<tr>";}

                                                                            if($chkCT == 0){
                                                                                if($m==0){
                                                                                    echo '<td><label class="font1emGray">'.$ct.'</label></td>';
                                                                                    $chkCT = 1;
                                                                                }else{
                                                                                    echo '<td style="border-right-style: hidden !important;"></td><td></td><td><label class="font1emGray">'.$ct.'</label></td>';
                                                                                    $chkCT = 1;
                                                                                }  //---- if($m==0)
                                                                            }else{
                                                                                echo '<td style="border-right-style: hidden !important;"></td><td style="border-right-style: hidden !important;"></td><td></td>';
                                                                            }  //-------  if($chkCT == 0)
                                                                        ?>
                                                                            <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($rq['business_date']); ?></label></td>
                                                                            <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($rq['statement_date']); ?></label></td>
                                                                            <td><label  class="font1emGray"><?php echo number_format($rq['statement_amount'], 2); ?></label></td>
                                                                            <td><label  class="font1emGray"><?php echo number_format($rq['fee_amount'], 2); ?></label></td>
                                                                            <td><label  class="font1emGray"><?php echo number_format($rq['tax_amount'], 2); ?></label></td>
                                                                            <td><label  class="font1emGray"><?php echo number_format($rq['net_amount'], 2); ?></label></td>
                                                                        </tr>
                                                                        <?php
                                                                            $statementAmountSumAll = $statementAmountSumAll + $rq['statement_amount'];
                                                                            $feeAmountSumAll = $feeAmountSumAll + $rq['fee_amount'];
                                                                            $taxAmountSumAll = $taxAmountSumAll + $rq['tax_amount'];
                                                                            $netAmountSumAll = $netAmountSumAll + $rq['net_amount'];

                                                                            $statementAmountSumSection = $statementAmountSumSection + $rq['statement_amount'];
                                                                            $feeAmountSumSection = $feeAmountSumSection + $rq['fee_amount'];
                                                                            $taxAmountSumSection = $taxAmountSumSection + $rq['tax_amount'];
                                                                            $netAmountSumSection = $netAmountSumSection + $rq['net_amount'];

                                                                            $rowSpanUseCount++;
                                                                        }  //-----  foreach ($resultInq as $rq)

                                                                        ?>
                                                                        <tr style="background-color: #9E9E9E;">
                                                                            <td style="border-right-style: hidden !important;"></td>
                                                                            <td></td>
                                                                            <td style="border-right-style: hidden !important;"><label  class="font1emBlack"><?php echo $ct." Total"; ?></label></td>
                                                                            <td style="border-right-style: hidden !important;"></td>
                                                                            <td></td>
                                                                            <td><label  class="font1emBlack"><?php echo number_format($statementAmountSumSection, 2); ?></label></td>
                                                                            <td><label  class="font1emBlack"><?php echo number_format($feeAmountSumSection, 2); ?></label></td>
                                                                            <td><label  class="font1emBlack"><?php echo number_format($taxAmountSumSection, 2); ?></label></td>
                                                                            <td><label  class="font1emBlack"><?php echo number_format($netAmountSumSection, 2); ?></label></td>
                                                                        </tr>
                                                                        <?php
                                                                        unset($resultInq, $rq);
                                                                    }  //------  if($num > 0)

                                                                $m++;
                                                            }  //-------  foreach ($channelTypeArr as $ct)

                                                            unset($ct);
                                                            ?>
                                                        <tr style="background-color: #4CAF50;">
                                                            <td style="border-right-style: hidden !important;"><label class="font1emBlack"><?php echo $businessUnit." Total"; ?></label></td>
                                                            <td style="border-right-style: hidden !important;"></td>
                                                            <td style="border-right-style: hidden !important;"></td>
                                                            <td style="border-right-style: hidden !important;"></td>
                                                            <td></td>
                                                            <td><label  class="font1emBlack"><?php echo number_format($statementAmountSumAll, 2); ?></label></td>
                                                            <td><label  class="font1emBlack"><?php echo number_format($feeAmountSumAll, 2); ?></label></td>
                                                            <td><label  class="font1emBlack"><?php echo number_format($taxAmountSumAll, 2); ?></label></td>
                                                            <td><label  class="font1emBlack"><?php echo number_format($netAmountSumAll, 2); ?></label></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <!-- </div> -->
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                    <?php
                    }  //------  if($rowCount > 0)
                }  //---------  if(sizeof($channelTypeArr) > 0)

                unset($channelTypeArr);
            }  //--------  if ($num > 0)
        }  //------  if($num > 0)
    }  //-------  if ($inqType == 1)



    if ($inqType == 2) {  //-----  Case Docket
        $callInSumAll = 0;
        $deliverySumAll = 0;
        $grandTotalSumAll = 0;

        $tmpSQL = "db_inquiry_docket where year='".$inqYear."' and month='".$inqMonth."' and branch='".$inqBranchID."'";
        
        $sql = "select branch, bu, store_name from ".$tmpSQL." group by branch";
        $num = $mQuery->checkNumRows($sql);

        if($num > 0){
            $branchID = $mQuery->getResultOneRecord($sql, "branch");
            $oracleBU = $mQuery->getResultOneRecord($sql, "bu");
            $storeName = $mQuery->getResultOneRecord($sql, "store_name");

            $sql = "select dob from ".$tmpSQL." group by dob order by dob";
            $num = $mQuery->checkNumRows($sql);

            if($num > 0){
                $result = $mQuery->getResultAll($sql);

                $showPathText = $pageTitle." / Docket / ".$branchID." / ".$inqYear." / ".$inqMonth;
            ?>
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                            <div class="tools"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover order-column" id="tableScroller">
                                                <thead>
                                                    <tr>
                                                        <th><label  class="font1emBlack">STORE NAME</label></th>
                                                        <th colspan="5" style="text-align: left;"><label  class="font1emBlack"><?php echo $storeName; ?></label></th>
                                                    </tr>
                                                    <tr>
                                                        <th><label  class="font1emBlack">Oracle BU</label></th>
                                                        <th><label  class="font1emBlack">Branch</label></th>
                                                        <th><label  class="font1emBlack">DOB</label></th>
                                                        <th><label  class="font1emBlack">Call in</label></th>
                                                        <th><label  class="font1emBlack">Delivery</label></th>
                                                        <th><label  class="font1emBlack">Grand Total</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
            <?php
                $m = 0;
                $foundData = 1;

                foreach ($result as $r) {
                    $sql = "select count from ".$tmpSQL." and order_mode='Call in' and dob='".$r['dob']."'";
                    $callinNum = $mQuery->checkNumRows($sql);

                    if ($callinNum > 0) {
                        $callinCount = (int)$mQuery->getResultOneRecord($sql, "count");
                        $callInSumAll = $callInSumAll + $callinCount;
                    }else{
                        $callinCount = 0;
                    }  //-----  if ($callinNum > 0)


                    $sql = "select count from ".$tmpSQL." and order_mode='Delivery' and dob='".$r['dob']."'";
                    $deliveryNum = $mQuery->checkNumRows($sql);

                    if ($deliveryNum > 0) {
                        $deliveryCount = (int)$mQuery->getResultOneRecord($sql, "count");
                        $deliverySumAll = $deliverySumAll + $deliveryCount;
                    }else{
                        $deliveryCount = 0;
                    }  //-----  if ($deliveryNum > 0)

                    echo "<tr>";

                    if($m == 0){
                        echo '<td><label  class="font1emGray">'.$oracleBU.'</label></td>';
                        echo '<td><label  class="font1emGray">'.$branchID.'</label></td>';
                        $m = 1;
                    }else{
                        echo '<td style="border-right-style: hidden !important;"></td>';
                        echo '<td></td>';
                    }  //------  if($m == 0)
            ?>
                                                        <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($r['dob']); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($callinCount, 0); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($deliveryCount, 0); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($callinCount + $deliveryCount, 0); ?></label></td>
            <?php
                    echo "</tr>";
                }  //--------  foreach ($result as $r)

                unset($result, $r);
            ?>
                                                    <tr style="background-color: #4CAF50;">
                                                        <td style="border-right-style: hidden !important;">Grand Total</label></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($callInSumAll, 0); ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($deliverySumAll, 0); ?></label></td>
                                                        <td><label  class="font1emBlack"><?php echo number_format($callInSumAll + $deliverySumAll, 0); ?></label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
            <?php
            }  //-------  if($num > 0)
        }  //----- if($num > 0)
    }  //-------  if ($inqType == 2)







    if ($inqType == 3) {  //-----  Case Trip Expense
        $finalsSumAll = 0;
        $endDatePeriod = $inqYear.$inqMonth."25";  //-------  ใช้เดือนของตัว End Period เป็นตัวหลักการแสดงเดือน
        $startDatePeriod = $dFunc->datePlusDay($endDatePeriod, 1, -1, 0);

        //$tmpSQL = "db_inquiry_trip_expense where year='".$inqYear."' and month='".$inqMonth."' and branch='".$inqBranchID."'";
        $tmpSQL = "db_inquiry_trip_expense where date between '".$startDatePeriod."' and '".$endDatePeriod."' and branch='".$inqBranchID."'";
        
        $sql = "select code, stores from ".$tmpSQL." group by code";
        $num = $mQuery->checkNumRows($sql);

        if($num > 0){
            $storesName = $mQuery->getResultOneRecord($sql, "stores");
            $codeID = $mQuery->getResultOneRecord($sql, "code");

            $sql = "select f_d from ".$tmpSQL." group by f_d order by f_d";
            $num = $mQuery->checkNumRows($sql);

            if($num > 0){
                $result = $mQuery->getResultAll($sql);

                $showPathText = $pageTitle." / Trip Expenses / ".$inqBranchID." / ".$inqYear." / ".$inqMonth;
            ?>
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cubes"></i><label class="font1emWhite"> <?php echo $showPathText; ?></label> </div>
                                            <div class="tools"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover order-column" id="tableScroller">
                                                <thead>
                                                    <tr>
                                                        <th><label  class="font1emBlack">Code</label></th>
                                                        <th><label  class="font1emBlack">F/D</label></th>
                                                        <th><label  class="font1emBlack">Date</label></th>
                                                        <th><label  class="font1emBlack">Stores</label></th>
                                                        <th><label  class="font1emBlack">Comment</label></th>
                                                        <th><label  class="font1emBlack">Reason</label></th>
                                                        <th><label  class="font1emBlack">Num.</label></th>
                                                        <th><label  class="font1emBlack">Cubics</label></th>
                                                        <th><label  class="font1emBlack">Weights</label></th>
                                                        <th><label  class="font1emBlack">Total</label></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
            <?php
                $n = 0;
                $foundData = 1;

                foreach ($result as $r) {
                    $m = 0;
                    $countLoop = 0;
                    $sectionSumAll = 0;

                    $sql = "select date, comment, reason, number, cubics, weights, final from ".$tmpSQL." and f_d='".$r['f_d']."' order by date";
                    $num = $mQuery->checkNumRows($sql);
                    $chkNumber = $num;

                    if($num > 0){
                        $resultDetail = $mQuery->getResultAll($sql);

                        foreach ($resultDetail as $rd) {
                            $sectionSumAll = $sectionSumAll + $rd['final'];
                            $finalsSumAll = $finalsSumAll + $rd['final'];

                            echo "<tr>";

                            if($n == 0){
                                echo '<td><label  class="font1emGray">'.$codeID.'</label></td>';
                                $n = 1;
                            }else{
                                echo '<td></td>';
                            }  //------  if($m == 0)

                            if($m == 0){
                                echo '<td><label  class="font1emGray">'.$r['f_d'].'</label></td>';
                                $m = 1;
                            }else{
                                echo '<td></td>';
                            }  //------  if($m == 0)
            ?>
                                                        <td><label  class="font1emGray"><?php echo $dFunc->changeDateToDDMMYYYY3($rd['date']); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo $storesName; ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo $rd['comment']; ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo $rd['reason']; ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo $rd['number']; ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($rd['cubics'], 2); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($rd['weights'], 2); ?></label></td>
                                                        <td><label  class="font1emGray"><?php echo number_format($rd['final'], 2); ?></label></td>
            <?php
                            echo "</tr>";

                            $countLoop++;

                            if($chkNumber == $countLoop){
            ?>
                                <tr style="background-color: #9E9E9E;">
                                    <td></td>
                                    <td style="border-right-style: hidden !important;"><label  class="font1emBlack"><?php echo $r['f_d']." Total"; ?></label></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td style="border-right-style: hidden !important;"></td>
                                    <td></td>
                                    <td><label  class="font1emBlack"><?php echo number_format($sectionSumAll, 2); ?></label></td>
                                </tr>
            <?php
                            }  //--------  if($chkNumber == $countLoop)
                        }  //-----  foreach ($resultDetail as $rd) 

                        unset($resultDetail, $rd);
                    }  //-----  if($num > 0)
                }  //-----  foreach ($result as $r)

                unset($result, $r);
            ?>
                                                    <tr style="background-color: #4CAF50;">
                                                        <td style="border-right-style: hidden !important;">Grand Total</label></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td style="border-right-style: hidden !important;"></td>
                                                        <td></td>
                                                        <td><label  class="font1emBlack"><?php echo "THB ".number_format($finalsSumAll, 2); ?></label></td></label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
            <?php
            }  //-------  if($num > 0){
        }  //-------- if($num > 0)
    }  //-------  if ($inqType == 3)
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}  //-----  if(isset($_REQUEST['brandID']) and $_REQUEST['brandID'] != "")

if($foundData == 0){
?>
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-cubes"></i><label class="font1emWhite">ไม่พบเอกสารที่ต้องการ</label> </div>
                                            <div class="tools"></div>
                                        </div>
                                        <div class="portlet-body">
                                            <!-- <div class="table-responsive"> -->
                                                <table class="table table-striped table-bordered table-hover order-column" id="tableScroller">
                                                    <thead>
                                                        <tr>
                                                            <th><label  class="font1emBlack">Business Unit</label></th>
                                                            <th><label  class="font1emBlack">Site Name</label></th>
                                                            <th><label  class="font1emBlack">Channel Type</label></th>
                                                            <th><label  class="font1emBlack">Business Date</label></th>
                                                            <th><label  class="font1emBlack">Statement Date</label></th>
                                                            <th><label  class="font1emBlack">Statement Amount</label></th>
                                                            <th><label  class="font1emBlack">Fee Amount</label></th>
                                                            <th><label  class="font1emBlack">Tax Amount</label></th>
                                                            <th><label  class="font1emBlack">Net Amount</label></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="9" style="text-align: center;">
                                                                <label class="font1emRed">
                                                                        ไม่พบข้อมูลที่ท่านต้องการค้นหาค่ะ
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <!-- </div> -->
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
<?php
}
?>
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-scroller.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->