<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $topupID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_topup_detail where topid=".$topupID;
    $topupFlag = (int)$mQuery->getResultOneRecord($sql, "topup_flag");
    $userID = (int)$mQuery->getResultOneRecord($sql, "uid");
    $userName = $mQuery->getResultOneRecord($sql, "topup_user");
    $email = $mQuery->getResultOneRecord($sql, "topup_email");
    $mobile = $mQuery->getResultOneRecord($sql, "topup_mobile");
    $addDateAndTime = $dFunc->fullDateThai($mQuery->getResultOneRecord($sql, "adddate"))." - ".($mQuery->getResultOneRecord($sql, "addtime"));
    $topupValue = $mQuery->getResultOneRecord($sql, "topup_value");

    $sql = "select user_real_name, user_real_lastname from db_user_detail where uid=".$userID;
    $userFullName = "คุณ".$mQuery->getResultOneRecord($sql, "user_real_name")." ".$mQuery->getResultOneRecord($sql, "user_real_lastname");

    $sql = "update db_topup_detail set topup_flag=1 where topid=".$topupID;
    $mQuery->querySQL($sql);
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_TOPUP_SYSTEM_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">


                                                <?php if(($topupFlag == 0) or ($topupFlag == 1)){ ?>
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/topupSystem_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อผู้เข้าใช้งาน</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="name" id="name" class="form-control input-circle font1emGray" placeholder="กรุณากรอกชื่อผู้เข้าใช้งาน" value="<?php echo $userName; ?>" required disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อ - นามสกุล</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="lastname" id="lastname" class="form-control input-circle font1emGray" placeholder="กรุณากรอกนามสกุลผู้เข้าใช้งาน" value="<?php echo $userFullName; ?>" required disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Email</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-envelope"></i>
                                                                        </span>
                                                                        <input type="email" name="email" id="email" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอก Email Address ผู้เข้าใช้งาน" value="<?php echo $email; ?>" disabled required> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เบอร์มือถือ</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="mobile" id="mobile" class="form-control input-circle-left font1emGray" placeholder="mobile" value="<?php echo $mobile; ?>" required disabled>
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-mobile"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">จำนวนเงินที่เติม</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-money"></i>
                                                                        </span>
                                                                        <input type="text" name="topupValue" id="topupValue" class="form-control input-circle-right font1emGray" placeholder="topupValue" value="<?php echo number_format($topupValue, 0); ?>" disabled required> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">วันที่ - เวลาที่เติมเข้าระบบ</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </span>
                                                                        <input type="text" name="dateAndTime" id="dateAndTime" class="form-control input-circle-right font1emGray" placeholder="วันที่ - เวลาที่เติมเข้าระบบ" value="<?php echo $addDateAndTime; ?>" disabled required> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <?php if($topupFlag == 0){ ?>
                                                                    <div class="col-md-offset-5 col-md-9">
                                                                        <button type="submit" name="close" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">&nbsp;ปิดงาน&nbsp;</button>
                                                                    </div>
                                                                <?php } ?>
                                                                <?php if($topupFlag == 1){ ?>
                                                                    <div class="col-md-offset-5 col-md-9">
                                                                        <button type="submit" name="cancel" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ยกเลิกการปิดงาน</button>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="topupID" value="<?php echo $topupID; ?>">
                                                        <input type="hidden" name="topupValue" value="<?php echo $topupValue; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                <?php }elseif($topupFlag == 2){ ?>
                                                    <div class="form-body">
                                                        <div class="alert alert-success font1emNoColor">
                                                            <strong>การเติมเงินได้ถูกดำเนินการแล้วค่ะ กรุณาเลือกลูกค้าท่านใหม่ค่ะ !</strong>
                                                        </div>
                                                    </div>
                                                <?php } ?>

                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>