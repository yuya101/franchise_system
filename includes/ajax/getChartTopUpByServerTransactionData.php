<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();



$array = array();

for($i=30; $i>=0; $i--)
{	
	$dateShow = $dFunc->datePlusDay($dateNow, -($i), 0, 0);
	$showDayText = $dFunc->fullDateThai($dateShow);
	$dayOfWeek = date('w', strtotime($dateShow));

	switch((int)$dayOfWeek)
	{
		case 0 :
			$dayOfWeekText = "วันอาทิตย์ที่ ".$showDayText;
			break;
		case 1 :
			$dayOfWeekText = "วันจันทร์ที่ ".$showDayText;
			break;
		case 2 :
			$dayOfWeekText = "วันอังคารที่ ".$showDayText;
			break;
		case 3 :
			$dayOfWeekText = "วันพุธที่ ".$showDayText;
			break;
		case 4 :
			$dayOfWeekText = "วันพฤหัสบดีที่ ".$showDayText;
			break;
		case 5 :
			$dayOfWeekText = "วันศุกร์ที่ ".$showDayText;
			break;
		case 6 :
			$dayOfWeekText = "วันเสาร์ที่ ".$showDayText;
			break;
	}  //-------  switch($i)


	$sql = "select sid, server_name from db_server order by sid";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
	    $result = $mQuery->getResultAll($sql);
	    $j = 0;

	    foreach($result as $r)
	    {
	        	$sid[$j] = intval("9".(substr("000".$r['sid'], -3, 3)));
	        	$sid2[$j] = intval("8".(substr("000".$r['sid'], -3, 3)));
	        
	        	$sql = "select topid from db_topup_detail where adddate='".$dateShow."' and (topup_aid=".$sid[$j]." or topup_aid=".$sid2[$j].")";
		$topupTransaction[$j] = intval($mQuery->checkNumRows($sql));

	        	$j++;
	    }  //-----  foreach($result as $r)

	    unset($result, $r);
	}  //--------  if($num > 0)


	$row_arr = array($dayOfWeekText, $topupTransaction[0], $topupTransaction[1], $topupTransaction[2], $topupTransaction[3], $topupTransaction[4]);

	array_push($array, $row_arr);
}  //------  for($i=6; $i>=0; $i--)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>