<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    include("../view/queryFatherMother.php");

    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_chicken_main where id=".$selectID;
    $chickenid = $mQuery->getResultOneRecord($sql, "chickenid");
    $strain = $mQuery->getResultOneRecord($sql, "strain");
    $buyfrom = $mQuery->getResultOneRecord($sql, "buyfrom");
    $saleto = $mQuery->getResultOneRecord($sql, "saleto");
    $chickenname = $mQuery->getResultOneRecord($sql, "chickenname");
    $sex = $mQuery->getResultOneRecord($sql, "sex");
    $fatherEditID = $mQuery->getResultOneRecord($sql, "fatherid");
    $motherEditID = $mQuery->getResultOneRecord($sql, "motherid");
    $generation = $mQuery->getResultOneRecord($sql, "generation");
    $familyno = $mQuery->getResultOneRecord($sql, "familyno");
    $comment = $mQuery->getResultOneRecord($sql, "comment");
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_REGISTER_MANAGE_DATA_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/manageRegisterChicken_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">สายพันธุ์</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="strain" id="strain" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดสายพันธุ์" required value="<?php echo $strain; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-eyedropper"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ซื้อมาจาก</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="buyfrom" id="buyfrom" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดซื้อมาจาก" required value="<?php echo $buyfrom; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ขายให้</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="saleto" id="saleto" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกรายละเอียดขายให้" value="<?php echo $saleto; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-pencil-square-o"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">ชื่อไก่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="chickenname" id="chickenname" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="กรุณากรอกชื่อไก่" required value="<?php echo $chickenname; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-language"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">เพศไก่</label>
                                                                <div class="col-md-4">
                                                                    <select name="sex" class="form-control input-circle font1emGray">
                                                                    <?php if($pageID == 1){ ?>
                                                                        <option value="1" selected>เพศผู้</option>
                                                                    <?php }elseif($pageID == 2){ ?>
                                                                        <option value="2">เพศเมีย</option>
                                                                    <?php }else{ ?>
                                                                        <option value="1" <?php echo ((int)$sex == 1 ? "selected" : ""); ?>>เพศผู้</option>
                                                                        <option value="2" <?php echo ((int)$sex == 2 ? "selected" : ""); ?>>เพศเมีย</option>
                                                                    <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกไก่</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon input-circle-left">
                                                                            <i class="fa fa-cubes"></i>
                                                                        </span>
                                                                        <input type="text" name="chickenid" id="chickenid" class="form-control input-circle-right font1emGray" placeholder="กรุณากรอกรหัสปีกไก่" value="<?php echo $chickenid; ?>" maxlength="10" required> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกพ่อ</label>
                                                                <div class="col-md-4">
                                                                    <select name="fatherid" class="form-control input-circle font1emGray">
                                                                        <option value="0" selected>ไม่มีพ่อพันธุ์</option>
                                                                        <?php if($numFather > 0){ ?>
                                                                            <?php for($i=0; $i<$numFather; $i++){ ?>
                                                                                <option value="<?php echo $fatherID[$i]; ?>" <?php echo ((int)$fatherEditID == (int)$fatherID[$i] ? "selected" : ""); ?>><?php echo $fatherRealID[$i]." (".$fatherName[$i].")"; ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รหัสปีกแม่</label>
                                                                <div class="col-md-4">
                                                                    <select name="motherid" class="form-control input-circle font1emGray">
                                                                        <option value="0" selected>ไม่มีแม่พันธุ์</option>
                                                                        <?php if($numMother > 0){ ?>
                                                                            <?php for($i=0; $i<$numMother; $i++){ ?>
                                                                                <option value="<?php echo $motherID[$i]; ?>" <?php echo ((int)$motherEditID == (int)$motherID[$i] ? "selected" : ""); ?>><?php echo $motherRealID[$i]." (".$motherName[$i].")"; ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">รุ่นที่</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="generation" id="generation" min="0" max="20000" class="form-control input-circle font1emGray" value="<?php echo $generation; ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">จำนวนลูกในครอกเดียวกัน</label>
                                                                <div class="col-md-4">
                                                                    <input type="number" name="familyno" id="familyno" min="0" max="2000" class="form-control input-circle font1emGray" value="<?php echo $familyno; ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">หมายเหตุ</label>
                                                                <div class="col-md-4">
                                                                    <textarea name="comment" id="comment" class="form-control input-circle font1emGray" cols="8" rows="5"><?php echo $comment; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="edit" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                    <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="id" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>