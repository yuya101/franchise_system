<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();



$array = array();

for($i=30; $i>=0; $i--)
{	
	$dateShow = $dFunc->datePlusDay($dateNow, -($i), 0, 0);
	$showDayText = $dFunc->fullDateThai($dateShow);
	$dayOfWeek = date('w', strtotime($dateShow));

	switch((int)$dayOfWeek)
	{
		case 0 :
			$dayOfWeekText = "วันอาทิตย์ที่ ".$showDayText;
			break;
		case 1 :
			$dayOfWeekText = "วันจันทร์ที่ ".$showDayText;
			break;
		case 2 :
			$dayOfWeekText = "วันอังคารที่ ".$showDayText;
			break;
		case 3 :
			$dayOfWeekText = "วันพุธที่ ".$showDayText;
			break;
		case 4 :
			$dayOfWeekText = "วันพฤหัสบดีที่ ".$showDayText;
			break;
		case 5 :
			$dayOfWeekText = "วันศุกร์ที่ ".$showDayText;
			break;
		case 6 :
			$dayOfWeekText = "วันเสาร์ที่ ".$showDayText;
			break;
	}  //-------  switch($i)


	$sql = "select mid, portal_name from db_money_portal order by mid";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
	    $result = $mQuery->getResultAll($sql);
	    $j = 0;

	    foreach($result as $r)
	    {
	        	$mid[$j] = $r['mid'];
	        
	        	$sql = "select SUM(topup_value) as topupSummary from db_topup_detail where adddate='".$dateShow."' and money_portal_id=".$mid[$j]."";
	        	$topupTransaction[$j] = intval($mQuery->getResultOneRecord($sql, "topupSummary"));

	        	if(is_null($topupTransaction[$j]))
	        	{
			$topupTransaction[$j] = 0;
	        	}  //-----  if($num > 0)

	        	$j++;
	    }  //-----  foreach($result as $r)

	    unset($result, $r);
	}  //--------  if($num > 0)


	$row_arr = array($dayOfWeekText, $topupTransaction[0], $topupTransaction[1], $topupTransaction[2], $topupTransaction[3], $topupTransaction[4], $topupTransaction[5]);

	array_push($array, $row_arr);
}  //------  for($i=6; $i>=0; $i--)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>