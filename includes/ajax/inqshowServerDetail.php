<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_server where sid=".$selectID;
    $serverID = $mQuery->getResultOneRecord($sql, "serial_id");
    $keypass = $mQuery->getResultOneRecord($sql, "keypass");
    $serverName = $mQuery->getResultOneRecord($sql, "server_name");
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_MANAGE_SERVER_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/manageExternalServer_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Server Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="servername" id="servername" maxlength="200" class="form-control input-circle font1emGray" placeholder="Ex : Garena Server" required value="<?php echo $serverName; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Server ID</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="serverid" id="serverid" maxlength="20" class="form-control input-circle font1emGray" placeholder="Ex : SV000001" required value="<?php echo $serverID; ?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Key Pass</label>
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <input type="text" name="keypass" id="keypass" maxlength="200" class="form-control input-circle-left font1emGray" placeholder="Ex : <?php echo $keypass; ?>"  required value="<?php echo $keypass; ?>">
                                                                        <span class="input-group-addon input-circle-right">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                    <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="sid" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>