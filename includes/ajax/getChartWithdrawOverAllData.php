<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();



$array = array();

for($i=30; $i>=0; $i--)
{	
	$dateShow = $dFunc->datePlusDay($dateNow, -($i), 0, 0);
	$showDayText = $dFunc->fullDateThai($dateShow);
	$dayOfWeek = date('w', strtotime($dateShow));

	switch((int)$dayOfWeek)
	{
		case 0 :
			$dayOfWeekText = "วันอาทิตย์ที่ ".$showDayText;
			break;
		case 1 :
			$dayOfWeekText = "วันจันทร์ที่ ".$showDayText;
			break;
		case 2 :
			$dayOfWeekText = "วันอังคารที่ ".$showDayText;
			break;
		case 3 :
			$dayOfWeekText = "วันพุธที่ ".$showDayText;
			break;
		case 4 :
			$dayOfWeekText = "วันพฤหัสบดีที่ ".$showDayText;
			break;
		case 5 :
			$dayOfWeekText = "วันศุกร์ที่ ".$showDayText;
			break;
		case 6 :
			$dayOfWeekText = "วันเสาร์ที่ ".$showDayText;
			break;
	}  //-------  switch($i)


	$sql = "select wid from db_withdraw_detail where adddate='".$dateShow."'";
	$withdrawTransaction = $mQuery->checkNumRows($sql);

	$sql = "select SUM(withdraw_summary) as summary from db_withdraw_summary where date='".$dateShow."'";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$summary = intval($mQuery->getResultOneRecord($sql, "summary"));
		if($summary == 0)
		{
			$summary = 0;
		}  //----  if($summary == 0)

		$row_arr = array($dayOfWeekText, $withdrawTransaction, $summary);
	}
	else
	{
		$row_arr = array($dayOfWeekText, $withdrawTransaction, 0);
	}  //-------  if($num > 0)

	array_push($array, $row_arr);
}  //------  for($i=6; $i>=0; $i--)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>