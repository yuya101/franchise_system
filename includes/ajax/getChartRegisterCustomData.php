<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$monthNow = substr($dateNow, 0, 6);
$monthNow = (int)substr($monthNow, -2, 2);

$startDateShow = $dFunc->datePlusDay($dateNow, 0, -11, 0);

$startMonthShow = substr($startDateShow, 0, 6);
$startMonthShow = (int)substr($startMonthShow, -2, 2);

$startYearShow = (int)substr($startDateShow, 0, 4);

$array = array();
$plusYear = 0;

for($i=1; $i<=12; $i++)
{
	switch($startMonthShow)
	{
		case 1 :
			$monthText = "มกราคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."01";
			break;
		case 2 :
			$monthText = "กุมภาพันธ์ ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."02";
			break;
		case 3 :
			$monthText = "มีนาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."03";
			break;
		case 4 :
			$monthText = "เมษายน ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."04";
			break;
		case 5 :
			$monthText = "พฤษภาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."05";
			break;
		case 6 :
			$monthText = "มิถุนายน ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."06";
			break;
		case 7 :
			$monthText = "กรกฎาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."07";
			break;
		case 8 :
			$monthText = "สิงหาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."08";
			break;
		case 9 :
			$monthText = "กันยายน ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."09";
			break;
		case 10 :
			$monthText = "ตุลาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."10";
			break;
		case 11 :
			$monthText = "พฤศจิกายน ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."11";
			break;
		case 12 :
			$monthText = "ธันวาคม ".($startYearShow + 543 + $plusYear);
			$monthSelectCustom = ($startYearShow + $plusYear)."12";
			break;
	}  //-------  switch($i)


	$sql = "select uid from db_user_detail where adddate like '".$monthSelectCustom."%'";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$summary = $num;

		$row_arr = array($monthText, $summary);
	}
	else
	{
		$row_arr = array($monthText, 0);
	}  //-------  if($num > 0)

	array_push($array, $row_arr);


	if($startMonthShow == 12)
	{
		$startMonthShow = 1;
		$plusYear = 1;
	}
	else
	{
		$startMonthShow = $startMonthShow + 1;
	}  //------  if($monthNow == 12)
}  //------  for($i=1; $i<=12; $i++)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>