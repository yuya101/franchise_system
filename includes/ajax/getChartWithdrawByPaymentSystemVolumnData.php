<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();



$array = array();

for($i=30; $i>=0; $i--)
{	
	$dateShow = $dFunc->datePlusDay($dateNow, -($i), 0, 0);
	$showDayText = $dFunc->fullDateThai($dateShow);
	$dayOfWeek = date('w', strtotime($dateShow));

	switch((int)$dayOfWeek)
	{
		case 0 :
			$dayOfWeekText = "วันอาทิตย์ที่ ".$showDayText;
			break;
		case 1 :
			$dayOfWeekText = "วันจันทร์ที่ ".$showDayText;
			break;
		case 2 :
			$dayOfWeekText = "วันอังคารที่ ".$showDayText;
			break;
		case 3 :
			$dayOfWeekText = "วันพุธที่ ".$showDayText;
			break;
		case 4 :
			$dayOfWeekText = "วันพฤหัสบดีที่ ".$showDayText;
			break;
		case 5 :
			$dayOfWeekText = "วันศุกร์ที่ ".$showDayText;
			break;
		case 6 :
			$dayOfWeekText = "วันเสาร์ที่ ".$showDayText;
			break;
	}  //-------  switch($i)


	$sql = "select mid, portal_name from db_money_portal order by mid";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
	    $result = $mQuery->getResultAll($sql);
	    $j = 0;

	    foreach($result as $r)
	    {
	        	$mid[$j] = $r['mid'];
	        
	        	$sql = "select SUM(withdraw_value) as withdrawSummary from db_withdraw_detail where adddate='".$dateShow."' and money_portal_id=".$mid[$j]."";
	        	$withdrawTransaction[$j] = intval($mQuery->getResultOneRecord($sql, "withdrawSummary"));

	        	if(is_null($withdrawTransaction[$j]))
	        	{
			$withdrawTransaction[$j] = 0;
	        	}  //-----  if($num > 0)

	        	$j++;
	    }  //-----  foreach($result as $r)

	    unset($result, $r);
	}  //--------  if($num > 0)


	$row_arr = array($dayOfWeekText, $withdrawTransaction[0], $withdrawTransaction[1], $withdrawTransaction[2], $withdrawTransaction[3], $withdrawTransaction[4], $withdrawTransaction[5]);

	array_push($array, $row_arr);
}  //------  for($i=6; $i>=0; $i--)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>