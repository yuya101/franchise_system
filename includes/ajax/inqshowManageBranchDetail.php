<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_user_auth where uaid=".$selectID;
    $aid = $mQuery->getResultOneRecord($sql, "aid");
    $brandID = $mQuery->getResultOneRecord($sql, "brand_id");
    $shopCode = $mQuery->getResultOneRecord($sql, "shop_code");
    $buCode = $mQuery->getResultOneRecord($sql, "bu_code");
    $storeName = $mQuery->getResultOneRecord($sql, "store_name");
    $activedate = $mQuery->getResultOneRecord($sql, "active_date");
    $suspenddate = $mQuery->getResultOneRecord($sql, "suspend_date");
    $disablestatus = $mQuery->getResultOneRecord($sql, "disable_status");

    if($activedate != "-"){
        $activedate = $dFunc->changeDateToDDMMYYYY3($activedate);
    }  //-----  if($activedate != "-")

    if($suspenddate != "-"){
        $suspenddate = $dFunc->changeDateToDDMMYYYY3($suspenddate);
    }else{
        $suspenddate = "";
    }  //-----  if($activedate != "-")


    $sqlUser = "select aid, customer_no, customer_name, email from db_user where groupid=4 order by customer_no, customer_name";
    $numUser = $mQuery->checkNumRows($sqlUser);

    if($numUser > 0){
        $resultUser = $mQuery->getResultAll($sqlUser);
        $i = 0;

        foreach ($resultUser as $rd) {
            $userID[$i] = $rd['aid'];
            $userNo[$i] = $rd['customer_no'];
            $userName[$i] = $rd['customer_name'];
            $userEmail[$i] = $rd['email'];

            $i++;
        }  //-----  foreach ($resultUser as $rd)

        unset($resultUser, $rd);
    }  //----  if($numUser > 0)



    $sqlDocBrand = "select * from db_brand order by brand_name";
    $numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

    if($numDocBrand > 0){
        $resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
        $i = 0;

        foreach ($resultDocBrand as $rb) {
            $docBrandID[$i] = $rb['bid'];
            $docBrandName[$i] = $rb['brand_name'];

            $i++;
        }  //-----  foreach ($resultDocType as $rd)

        unset($resultDocBrand, $rb);
    }  //----  if($numDocType > 0)
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_MANAGE_BRANCH_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/manageBranch_Ctl.php" class="form-horizontal" method="post">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Franchise Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="userid" id="userid" class="form-control input-circle font1emGray">
                                                                    <?php if($numUser > 0){ ?>
                                                                        <?php for($i=0; $i<$numUser; $i++){ ?>
                                                                            <option value="<?php echo $userID[$i]; ?>" <?php if((int)$aid == (int)$userID[$i]){echo "selected";} ?>><?php echo $userNo[$i]." - ".$userName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Brand Name</label>
                                                                <div class="col-md-4">
                                                                    <select name="brandid" id="brandid" class="form-control input-circle font1emGray">
                                                                    <?php if($numDocBrand > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocBrand; $i++){ ?>
                                                                            <option value="<?php echo $docBrandID[$i]; ?>" <?php if((int)$brandID == (int)$docBrandID[$i]){echo "selected";} ?>><?php echo $docBrandName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Shop Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="shopcode" id="shopcode" class="form-control input-circle font1emGray" placeholder="Please! Enter Shop Code." required value="<?php echo $shopCode; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">BU Code</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="bucode" id="bucode" class="form-control input-circle font1emGray" placeholder="Please! Enter BU Code" required value="<?php echo $buCode; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Store Name</label>
                                                                <div class="col-md-4">
                                                                    <input type="text" name="storename" id="storename" class="form-control input-circle font1emGray" placeholder="Please! Enter Store Name" required value="<?php echo $storeName; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Active Store Date</label>
                                                                <div class="col-md-4 input-group date date-picker" data-date-format="dd/mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                    <input type="text" name="activedate" id="activedate" class="form-control input-circle" style="margin-left: 15px;" readonly required="required" value="<?php echo $activedate; ?>">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Suspend Store Date</label>
                                                                <div class="col-md-4 input-group date date-picker" data-date-format="dd/mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                    <input type="text" name="suspenddate" id="suspenddate" class="form-control input-circle" style="margin-left: 15px;" value="<?php echo $suspenddate; ?>">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Store Status</label>
                                                                <div class="col-md-4">
                                                                    <select name="disablestatus" id="disablestatus" class="form-control input-circle font1emGray">
                                                                        <option value="0" <?php if((int)$disablestatus == 0){echo "selected";} ?>>Open</option>
                                                                        <option value="1" <?php if((int)$disablestatus == 1){echo "selected";} ?>>Close</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                    <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="uaid" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->