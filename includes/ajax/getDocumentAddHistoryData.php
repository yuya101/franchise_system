<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();



$array = array();

for($i=14; $i>=0; $i--)
{	
	$dateShow = $dFunc->datePlusDay($dateNow, -($i), 0, 0);
	$showDayText = $dFunc->fullDateThai($dateShow);
	$dayOfWeek = date('w', strtotime($dateShow));

	switch((int)$dayOfWeek)
	{
		case 0 :
			$dayOfWeekText = "วันอาทิตย์ที่ ".$showDayText;
			break;
		case 1 :
			$dayOfWeekText = "วันจันทร์ที่ ".$showDayText;
			break;
		case 2 :
			$dayOfWeekText = "วันอังคารที่ ".$showDayText;
			break;
		case 3 :
			$dayOfWeekText = "วันพุธที่ ".$showDayText;
			break;
		case 4 :
			$dayOfWeekText = "วันพฤหัสบดีที่ ".$showDayText;
			break;
		case 5 :
			$dayOfWeekText = "วันศุกร์ที่ ".$showDayText;
			break;
		case 6 :
			$dayOfWeekText = "วันเสาร์ที่ ".$showDayText;
			break;
	}  //-------  switch($i)


	$sql = "select did from db_document where adddate='".$dateShow."'";
	$summary = intval($mQuery->checkNumRows($sql));

	$row_arr = array($dayOfWeekText, $summary);
	

	array_push($array, $row_arr);
}  //------  for($i=14; $i>=0; $i--)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>