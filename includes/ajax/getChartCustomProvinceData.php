<?php
ob_start();
session_start();

header("content-type: application/json");
require("../class/autoload.php");

$mQuery = new MainQuery();    
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$array = array();

$sql = "select PROVINCE_ID, PROVINCE_NAME from db_province order by PROVINCE_NAME";
$num = $mQuery->checkNumRows($sql);

if($num > 0)
{
	$result = $mQuery->getResultAll($sql);

	foreach($result as $r)
	{
		$provinceName = $r['PROVINCE_NAME'];
		$provinceID = $r['PROVINCE_ID'];

		$sql = "select uid from db_user_detail where user_province=".$provinceID;
		$numProvince = $mQuery->checkNumRows($sql);

		$row_arr = array($provinceName, $numProvince);

		array_push($array, $row_arr);
	}  //-------  foreach($result as $r)
}  //-------  if($num > 0)

echo $_GET['callback']. '('. json_encode($array) . ')';

unset($mQuery, $mFunc, $dFunc);
?>