<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");
require("../class/autoload.php");

if(isset($_REQUEST['selectText']) and $_REQUEST['selectText'] != "")
{
    $mQuery = new MainQuery();    
    $mFunc = new MainFunction();
    $dFunc = new DateFunction();
        
    $dateNow = $dFunc->getDateChris();
    $timeNow = $dFunc->getTimeNow();

    $selectID = $mFunc->chgSpecialCharInputText($_REQUEST['selectText']);

    $sql = "select * from db_document where did=".$selectID;
    $typeID = $mQuery->getResultOneRecord($sql, "type_id");
    $catID = $mQuery->getResultOneRecord($sql, "cat_id");
    $brandID = $mQuery->getResultOneRecord($sql, "brand_id");
    $shopCode = $mQuery->getResultOneRecord($sql, "shop_code");
    $buCode = $mQuery->getResultOneRecord($sql, "bu_code");
    $title = $mQuery->getResultOneRecord($sql, "title");
    $description = $mQuery->getResultOneRecord($sql, "description");
    $fileName = $mQuery->getResultOneRecord($sql, "file_name");
    $filePath = $mQuery->getResultOneRecord($sql, "file_path");
    $month = $mQuery->getResultOneRecord($sql, "month");
    $year = $mQuery->getResultOneRecord($sql, "year");

    $showMonth = $month."/".$year;


    $sqlDocType = "select * from db_document_type order by type_name";
    $numDocType = $mQuery->checkNumRows($sqlDocType);

    if($numDocType > 0){
        $resultDocType = $mQuery->getResultAll($sqlDocType);
        $i = 0;

        foreach ($resultDocType as $rd) {
            $docTypeID[$i] = $rd['type_id'];
            $docTypeName[$i] = $rd['type_name'];

            $i++;
        }  //-----  foreach ($resultDocType as $rd)

        unset($resultDocType, $rd);
    }  //----  if($numDocType > 0)




    $sqlDocCat = "select * from db_document_category where type_id=".$typeID;
    $numDocCat = $mQuery->checkNumRows($sqlDocCat);

    if($numDocCat > 0){
        $resultDocCat = $mQuery->getResultAll($sqlDocCat);
        $j = 0;

        foreach ($resultDocCat as $rc) {
            $docCatID[$j] = $rc['cat_id'];
            $docCatName[$j] = $rc['cat_name'];

            $j++;
        }  //-----  foreach ($resultDocCat as $rc)

        unset($resultDocCat, $rc);
    } //-----  if($num > 0)




    $sqlDocBrand = "select * from db_brand order by brand_name";
    $numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

    if($numDocBrand > 0){
        $resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
        $i = 0;

        foreach ($resultDocBrand as $rb) {
            $docBrandID[$i] = $rb['bid'];
            $docBrandName[$i] = $rb['brand_name'];
            $docBrandPicture[$i] = "img/".$rb['brand_picture'];

            $j = 0;

            $sqlDocBranch = "select * from db_user_auth where brand_id=".$docBrandID[$i]." group by shop_code order by site_customer";
            $numDocBranch[$i] = $mQuery->checkNumRows($sqlDocBranch);

            if($numDocBranch[$i] > 0){
                $docBranchAllChkAuth[$i] = 0;
                $resultDocBranch = $mQuery->getResultAll($sqlDocBranch);

                foreach ($resultDocBranch as $rdb) {
                    $docBranchID[$i][$j] = $rdb['uaid'];
                    $docBranchSiteName[$i][$j] = $rdb['site_customer'];
                    $docBranchStoreIDName[$i][$j] = $rdb['store_id_name'];

                    $sqlChkAuth = "select daid from db_document_authorize where did=".$selectID." and brand_id=".$docBrandID[$i]." and uaid=0";
                    $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                    if($numChkAuth > 0){
                        $docBranchAllChkAuth[$i] = 1;
                        $docBranchChkAuth[$i][$j] = 0;
                    }else{
                        $sqlChkAuth = "select daid from db_document_authorize where did=".$selectID." and brand_id=".$docBrandID[$i]." and uaid=".$docBranchID[$i][$j];
                        $numChkAuth = $mQuery->checkNumRows($sqlChkAuth);

                        if($numChkAuth > 0){
                            $docBranchChkAuth[$i][$j] = 1;
                        }else{
                            $docBranchChkAuth[$i][$j] = 0;
                        }  //-----  if($numChkAuth > 0)
                    }  //----  if($numChkAuth > 0)
                    
                    $j++;
                }  //-----  foreach ($resultDocBranch as $rdb)

                unset($resultDocBranch, $rdb);
            }  //------  if($numDocBranch[$i] > 0)

            $i++;
        }  //-----  foreach ($resultDocType as $rd)

        unset($resultDocBrand, $rb);
    }  //----  if($numDocType > 0)


    if((int)$typeID != 1){
        $showAuthSectionStyle = 'style="display: none;"';
    }else{
        $showAuthSectionStyle = "";
    }  //-----  if((int)$typeID != 1)
?>
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i><label class="font1emWhite"><?php echo PAGE_MANAGE_FILE_DATA_TITLE; ?></label> </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="includes/control/mamageFileData_Ctl.php" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        <div class="form-body" id="listProductLI1">
                                                            <h3 class="form-section">Document Detail.</h3>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label font1emGray">Document Type</label>
                                                                <div class="col-md-4">
                                                                    <select name="typeid" id="typeid" class="form-control input-circle font1emGray" onchange="refreshListBoxItemV2(1, this, 'catid', 'listCategory', 'listProductLI', 2, 'fileDataDetailDiv', 'branchAuthorizationFull');">
                                                                    <?php if($numDocType > 0){ ?>
                                                                        <?php for($i=0; $i<$numDocType; $i++){ ?>
                                                                            <option value="<?php echo $docTypeID[$i]; ?>" <?php if((int)$typeID == (int)$docTypeID[$i]){echo "selected";} ?>><?php echo $docTypeName[$i]; ?></option>
                                                                        <?php }  //-----  for($i=0; $i<$numDocType; $i++) ?>
                                                                    <?php }  //-----  if($numDocType > 0) ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="listProductLI2" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Category</label>
                                                                    <div class="col-md-4">
                                                                        <select name="catid" id="catid" class="form-control input-circle font1emGray">
                                                                        <?php if($numDocCat > 0){ ?>
                                                                            <?php for($i=0; $i<$numDocCat; $i++){ ?>
                                                                                <option value="<?php echo $docCatID[$i]; ?>" <?php if((int)$catID == (int)$docCatID[$i]){echo "selected";} ?>><?php echo $docCatName[$i]; ?></option>
                                                                            <?php }  //-----  for($i=0; $i<$numDocCat; $i++) ?>
                                                                        <?php }  //-----  if($numDocCat > 0) ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="fileDataDetailDiv" style="margin-top: -38px;">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Month</label>
                                                                    <div class="col-md-4 input-group date date-picker" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
                                                                        <input type="text" name="docmonth" id="docmonth" class="form-control input-circle" style="margin-left: 15px;" readonly value="<?php echo $showMonth; ?>">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Title</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" name="title" id="title" class="form-control input-circle font1emGray" placeholder="Please! Enter Document Title" required value="<?php echo $title; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Description</label>
                                                                    <div class="col-md-4">
                                                                        <textarea name="description" id="description" class="form-control input-circle font1emGray" rows="10" placeholder="Please! Enter Document Description"><?php echo $description; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-3 control-label font1emGray">Document Use File Name</label>
                                                                    <div class="col-md-4">
                                                                        <input type="text" class="form-control input-circle font1emGray" placeholder="Please! Enter Document Title" readonly value="<?php echo $fileName; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" style="margin-bottom: 70px;">
                                                                    <label class="col-md-3 control-label font1emGray">Upload New Document File</label>
                                                                    <div class="col-md-4">
                                                                        <input type="file" name="docfile[]" id="docfile" class="form-control input-circle font1emGray">
                                                                    </div>
                                                                </div>


                                                                <div id="branchAuthorizationFull" <?php echo $showAuthSectionStyle; ?>>
                                                                    <?php if($numDocBrand > 0){?>
                                                                        <?php for($i=0; $i<$numDocBrand; $i++){?>
                                                                            <h3 class="form-section">Branch Authorization (&nbsp;<img src="<?php echo $docBrandPicture[$i]; ?>">&nbsp;&nbsp;<?php echo $docBrandName[$i]; ?>)</h3>
                                                                            <!--/row-->
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                    <?php if($numDocBranch[$i] > 0){ ?>
                                                                                            <div class="col-md-12">
                                                                                                <div class="mt-checkbox-list">
                                                                                                    <label class="mt-checkbox">
                                                                                                        <input type="checkbox" value="1" <?php if($docBranchAllChkAuth[$i] == 1){echo "checked";} ?> name="useallbrand<?php echo $docBrandID[$i]; ?>" /><?php echo "Add authorize to all branch in &nbsp;<img src='".$docBrandPicture[$i]."'>&nbsp;&nbsp;".$docBrandName[$i]." brand."; ?>
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        <?php for($j=0; $j<$numDocBranch[$i]; $j++){ ?>
                                                                                            <div class="col-md-2">
                                                                                                <div class="mt-checkbox-list">
                                                                                                    <label class="mt-checkbox">
                                                                                                        <input type="checkbox" value="1" <?php if($docBranchChkAuth[$i][$j] == 1){echo "checked";} ?> name="<?php echo $docBranchID[$i][$j]; ?>" /> <?php echo $docBranchSiteName[$i][$j]; ?>
                                                                                                        <span></span>
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php }else{ ?>
                                                                                        <div class="col-md-12">
                                                                                            <div class="mt-checkbox-list">
                                                                                                <label class="mt-checkbox">ไม่มีข้อมูลร้านสาขาของแบรนด์ <?php echo $docBrandName[$i]; ?>&nbsp;&nbsp;<img src="<?php echo $docBrandPicture[$i]; ?>"> ค่ะ. </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } //-------  if($numDocBranch[$i] > 0) ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>

                                                            </div>

                                                            <div class="form-actions">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" name="manage" class="btn btn-circle green font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ปรับปรุงข้อมูล</button>
                                                                        <button type="button" class="btn btn-circle grey-salsa btn-outline font1emWhite">ยกเลิก</button>
                                                                        <button type="submit" name="delete" class="btn btn-circle red font1emWhite" onclick="return confirm('ยืนยันการทำรายการใช่หรือไม่ค่ะ !');">ลบข้อมูล</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="docID" value="<?php echo $selectID; ?>">
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>

<?php
    unset($dFunc, $mQuery, $mFunc, $dateNow, $timeNow);
}
?>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->