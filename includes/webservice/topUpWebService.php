<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$dFunc = new DateFunction();
$mFunc = new MainFunction();
$mQuery = new MainQuery();

$return_arr = array();


function addUserTopUp($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");
	
	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);
	$requestUserRealName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['name']);
	$requestUserRealLastName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['lastname']);
	$requestUserRoom = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['room']);
	$requestUserBuilding = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['building']);
	$requestUserVillage = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['village']);
	$requestUserAddress = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['address']);
	$requestUserSoi = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['soi']);
	$requestUserMou = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mou']);
	$requestUserStreet = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['street']);
	$requestUserDistrict = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['district']);
	$requestUserAmphor = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['amphor']);
	$requestUserProvince = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['province']);
	$requestUserPostCode = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['postcode']);
	$requestUserTelephone = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['telephone']);


	if(($requestUserName != "-") or ($requestUserEmail != "-") or ($requestUserMobile != "-"))
	{
		$sql = "select uid from db_user_detail where user_name='".$requestUserName."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			$sql = "select uid from db_user_detail where user_email='".$requestUserEmail."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0)
			{
				$responseTransID = $requestTransID;
				$jsonInText = serialize($jsonData);

				$sql = "insert into db_user_detail values(NULL";
				$sql = $sql.", '".$requestUserName."'";
				$sql = $sql.", '".$requestUserEmail."'";
				$sql = $sql.", '".$requestUserMobile."'";
				$sql = $sql.", '".$requestUserRealName."'";
				$sql = $sql.", '".$requestUserRealLastName."'";
				$sql = $sql.", '".$requestUserRoom."'";
				$sql = $sql.", '".$requestUserBuilding."'";
				$sql = $sql.", '".$requestUserVillage."'";
				$sql = $sql.", '".$requestUserAddress."'";
				$sql = $sql.", '".$requestUserSoi."'";
				$sql = $sql.", '".$requestUserMou."'";
				$sql = $sql.", '".$requestUserStreet."'";
				$sql = $sql.", '".$requestUserDistrict."'";
				$sql = $sql.", '".$requestUserAmphor."'";
				$sql = $sql.", '".$requestUserProvince."'";
				$sql = $sql.", '".$requestUserPostCode."'";
				$sql = $sql.", '".$requestUserTelephone."'";
				$sql = $sql.", ".$serverID."";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '-'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '-'";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_email='".$requestUserEmail."' and user_mobile='".$requestUserMobile."'";
				$uid = $mQuery->getResultOneRecord($sql, "uid");

				$sql = "insert into db_request_history values(NULL";
				$sql = $sql.", ".$serverID."";
				$sql = $sql.", '".$requestTransID."'";
				$sql = $sql.", '".$responseTransID."'";
				$sql = $sql.", '".$requestMethod."'";
				$sql = $sql.", '".$requestAction."'";
				$sql = $sql.", '".$jsonInText."'";
				$sql = $sql.", ".$uid."";
				$sql = $sql.", '".$requestUserMobile."'";
				$sql = $sql.", '".$requestUserEmail."'";
				$sql = $sql.", 0";
				$sql = $sql.", 1";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "insert into db_user_money values(NULL, ".$uid.", 0, 0)";
				$mQuery->querySQL($sql);


				$row_array['status'] = "success";
				$row_array['statuscode'] = "complete";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "Add New User Complete.";

				array_push($return_arr,$row_array);
					
				return json_encode($return_arr);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err203";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "Sorry! Email Already Exist.";

				array_push($return_arr,$row_array);
					
				return json_encode($return_arr);
			}  //-----  if($num == 0)
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err202";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Sorry! User Name Already Exist.";

			array_push($return_arr,$row_array);
				
			return json_encode($return_arr);
		}  //-------  if($num == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err201";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name Or Email Or Mobile Must Have Data.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if(($requestUserName != "-") and ($requestUserEmail != "-"))

}  //------  function addUserTopUp($jsonData)



function editUserTopUp($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");
	
	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);
	$requestUserRealName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['name']);
	$requestUserRealLastName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['lastname']);
	$requestUserRoom = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['room']);
	$requestUserBuilding = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['building']);
	$requestUserVillage = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['village']);
	$requestUserAddress = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['address']);
	$requestUserSoi = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['soi']);
	$requestUserMou = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mou']);
	$requestUserStreet = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['street']);
	$requestUserDistrict = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['district']);
	$requestUserAmphor = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['amphor']);
	$requestUserProvince = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['province']);
	$requestUserPostCode = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['postcode']);
	$requestUserTelephone = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['telephone']);


	$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_email='".$requestUserEmail."' and user_mobile='".$requestUserMobile."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$responseTransID = $requestTransID;
		$jsonInText = serialize($jsonData);

		$uid = $mQuery->getResultOneRecord($sql, "uid");

		$sql = "update db_user_detail set user_mobile='".$requestUserMobile."'";
		$sql = $sql.", user_real_name='".$requestUserRealName."'";
		$sql = $sql.", user_real_lastname='".$requestUserRealLastName."'";
		$sql = $sql.", user_room='".$requestUserRoom."'";
		$sql = $sql.", user_building='".$requestUserBuilding."'";
		$sql = $sql.", user_village='".$requestUserVillage."'";
		$sql = $sql.", user_address='".$requestUserAddress."'";
		$sql = $sql.", user_soi='".$requestUserSoi."'";
		$sql = $sql.", user_mou='".$requestUserMou."'";
		$sql = $sql.", user_street='".$requestUserStreet."'";
		$sql = $sql.", user_district='".$requestUserDistrict."'";
		$sql = $sql.", user_amphur='".$requestUserAmphor."'";
		$sql = $sql.", user_province='".$requestUserProvince."'";
		$sql = $sql.", user_postcode='".$requestUserPostCode."'";
		$sql = $sql.", user_telephone='".$requestUserTelephone."'";
		$sql = $sql.", sid=".$serverID."";
		$sql = $sql.", moddate='".$dateNow."'";
		$sql = $sql.", modtime='".$timeNow."'";
		$sql = $sql." where uid=".$uid;
		$mQuery->querySQL($sql);


		$sql = "insert into db_request_history values(NULL";
		$sql = $sql.", ".$serverID."";
		$sql = $sql.", '".$requestTransID."'";
		$sql = $sql.", '".$responseTransID."'";
		$sql = $sql.", '".$requestMethod."'";
		$sql = $sql.", '".$requestAction."'";
		$sql = $sql.", '".$jsonInText."'";
		$sql = $sql.", ".$uid."";
		$sql = $sql.", '".$requestUserMobile."'";
		$sql = $sql.", '".$requestUserEmail."'";
		$sql = $sql.", 0";
		$sql = $sql.", 1";
		$sql = $sql.", '".$dateNow."'";
		$sql = $sql.", '".$timeNow."'";
		$sql = $sql.", '".$dateNow."'";
		$sql = $sql.", '".$timeNow."'";
		$sql = $sql.")";
		$mQuery->querySQL($sql);

		$row_array['status'] = "success";
		$row_array['statuscode'] = "complete";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Edit User Complete.";

		array_push($return_arr,$row_array);
					
		return json_encode($return_arr);
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err301";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name, Mobile Or Email Not Match In System.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if($num > 0)
}  //--------  function editUserTopUp($jsonData)




function topUpMoney($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");

	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);
	$requestUserTopUpValue = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['value']);
	$requestUserTopUpCloseJob = (int)$mFunc->chgSpecialCharInputText($jsonData['user_detail']['closejob']);
	$requestUserTopUpPortal = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['money_portal']);


	$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_email='".$requestUserEmail."' and user_mobile='".$requestUserMobile."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$responseTransID = $requestTransID;
		$jsonInText = serialize($jsonData);

		$uid = $mQuery->getResultOneRecord($sql, "uid");

		if($requestUserTopUpCloseJob == 0)
		{
			$sql = "insert into db_topup_detail values(NULL, ".$uid.", '".$requestUserName."', '".$requestUserEmail."', '".$requestUserMobile."', ".$requestUserTopUpValue.", 0, 0, ".$requestUserTopUpPortal.", '".$dateNow."', '".$timeNow."', 0, '".$dateNow."', '".$timeNow."', 0)";
			$mQuery->querySQL($sql);


			$sql = "select umid from db_user_money where uid=".$uid;
			$num = $mQuery->checkNumRows($sql);

			if($num > 0)
			{
				$umid = $mQuery->getResultOneRecord($sql, "umid");

				$sql = "update db_user_money set money_wait_for_process=money_wait_for_process+".$requestUserTopUpValue." where umid=".$umid;
				$mQuery->querySQL($sql);
			}
			else
			{
				$sql = "insert into db_user_money values(NULL, ".$uid.", 0, ".requestUserTopUpValue.")";
				$mQuery->querySQL($sql);
			}  //------  if($num > 0)


			$sql = "insert into db_request_history values(NULL";
			$sql = $sql.", ".$serverID."";
			$sql = $sql.", '".$requestTransID."'";
			$sql = $sql.", '".$responseTransID."'";
			$sql = $sql.", '".$requestMethod."'";
			$sql = $sql.", '".$requestAction."'";
			$sql = $sql.", '".$jsonInText."'";
			$sql = $sql.", ".$uid."";
			$sql = $sql.", '".$requestUserMobile."'";
			$sql = $sql.", '".$requestUserEmail."'";
			$sql = $sql.", ".$requestUserTopUpValue."";
			$sql = $sql.", 1";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.")";
			$mQuery->querySQL($sql);
				

			$sql = "select adid from db_active_data_history where uid=".$uid." and active_date='".$dateNow."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0)
			{
				$sql = "insert into db_active_data_history values(NULL, ".$uid.", '".$dateNow."', '".$requestTransID."')";
				$mQuery->querySQL($sql);
			}  //-----  if($num == 0)


			$row_array['status'] = "success";
			$row_array['statuscode'] = "complete";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Add Top Up Into Top Up Process Complete.";

			array_push($return_arr,$row_array);
						
			return json_encode($return_arr);
		}
		elseif($requestUserTopUpCloseJob == 1)
		{
			$closeJobID = intval("9".(substr("000".$serverID, -3, 3)));
			$sql = "insert into db_topup_detail values(NULL, ".$uid.", '".$requestUserName."', '".$requestUserEmail."', '".$requestUserMobile."', ".$requestUserTopUpValue.", 2, ".$closeJobID.", ".$requestUserTopUpPortal.", '".$dateNow."', '".$timeNow."', 0, '".$dateNow."', '".$timeNow."', 0)";
			$mQuery->querySQL($sql);

			$sql = "select umid from db_user_money where uid=".$uid;
			$num = $mQuery->checkNumRows($sql);

			if($num > 0)
			{
				$umid = $mQuery->getResultOneRecord($sql, "umid");

				$sql = "update db_user_money set money_can_use=money_can_use+".$requestUserTopUpValue." where umid=".$umid;
				$mQuery->querySQL($sql);
			}
			else
			{
				$sql = "insert into db_user_money values(NULL, ".$uid.", ".requestUserTopUpValue.", 0)";
				$mQuery->querySQL($sql);
			}  //------  if($num > 0)


			$sql = "insert into db_request_history values(NULL";
			$sql = $sql.", ".$serverID."";
			$sql = $sql.", '".$requestTransID."'";
			$sql = $sql.", '".$responseTransID."'";
			$sql = $sql.", '".$requestMethod."'";
			$sql = $sql.", '".$requestAction."'";
			$sql = $sql.", '".$jsonInText."'";
			$sql = $sql.", ".$uid."";
			$sql = $sql.", '".$requestUserMobile."'";
			$sql = $sql.", '".$requestUserEmail."'";
			$sql = $sql.", ".$requestUserTopUpValue."";
			$sql = $sql.", 1";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.")";
			$mQuery->querySQL($sql);


			$sql = "select tsid from db_topup_summary where date='".$dateNow."'";
			$num = $mQuery->checkNumRows($sql);

			if($num > 0)
			{
				$tsid = intval($mQuery->getResultOneRecord($sql, "tsid"));

				$sql = "update db_topup_summary set topup_summary=topup_summary+".$requestUserTopUpValue." where tsid=".$tsid;
				$mQuery->querySQL($sql);
			}
			else
			{
				$day = intval(substr($dateNow, -2, 2));
				$month = intval(substr($dateNow, 4, 2));
				$year = intval(substr($dateNow, 0, 4));

				$sql = "insert into db_topup_summary values(NULL, ".$day.", ".$month.", ".$year.", '".$dateNow."', ".$requestUserTopUpValue.")";
				$mQuery->querySQL($sql);
			}  //------  if($num > 0)
				

			$sql = "select adid from db_active_data_history where uid=".$uid." and active_date='".$dateNow."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0)
			{
				$sql = "insert into db_active_data_history values(NULL, ".$uid.", '".$dateNow."', '".$requestTransID."')";
				$mQuery->querySQL($sql);
			}  //-----  if($num == 0)
			

			$row_array['status'] = "success";
			$row_array['statuscode'] = "complete";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Add Top Up Into Top Up Process Complete.";

			array_push($return_arr,$row_array);
						
			return json_encode($return_arr);
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err402";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Sorry! Your Close Job ID Not Match In System.";

			array_push($return_arr,$row_array);
					
			return json_encode($return_arr);
		}  //------  if($requestUserTopUpCloseJob == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err401";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name, Mobile Or Email Not Match In System.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if($num > 0)
}  //---------  function topUpMoney($jsonData)



function showUserTopUpMoney($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");

	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);


	$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_email='".$requestUserEmail."' and user_mobile='".$requestUserMobile."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$responseTransID = $requestTransID;
		$jsonInText = serialize($jsonData);

		$uid = $mQuery->getResultOneRecord($sql, "uid");

		$sql = "insert into db_request_history values(NULL";
		$sql = $sql.", ".$serverID."";
		$sql = $sql.", '".$requestTransID."'";
		$sql = $sql.", '".$responseTransID."'";
		$sql = $sql.", '".$requestMethod."'";
		$sql = $sql.", '".$requestAction."'";
		$sql = $sql.", '".$jsonInText."'";
		$sql = $sql.", ".$uid."";
		$sql = $sql.", '".$requestUserMobile."'";
		$sql = $sql.", '".$requestUserEmail."'";
		$sql = $sql.", 0";
		$sql = $sql.", 1";
		$sql = $sql.", '".$dateNow."'";
		$sql = $sql.", '".$timeNow."'";
		$sql = $sql.", '".$dateNow."'";
		$sql = $sql.", '".$timeNow."'";
		$sql = $sql.")";
		$mQuery->querySQL($sql);

		$sql = "select money_can_use, money_wait_for_process from db_user_money where uid=".$uid;
		$num = $mQuery->checkNumRows($sql);

		if($num > 0)
		{
			$userMoney = number_format($mQuery->getResultOneRecord($sql, "money_can_use"), 2);
			$userMoneyInProcess = number_format($mQuery->getResultOneRecord($sql, "money_wait_for_process"), 2);
		}
		else
		{
			$userMoney = number_format(0, 2);
			$userMoneyInProcess = number_format(0, 2);
		}  //------  if($num > 0)

		$row_array['status'] = "success";
		$row_array['statuscode'] = "complete";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['usermoney'] = $userMoney;
		$row_array['usermoneyinprocess'] = $userMoneyInProcess;
		$row_array['message'] = "This is User All Money.";

		array_push($return_arr,$row_array);
					
		return json_encode($return_arr);
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err501";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name, Mobile Or Email Not Match In System.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if($num > 0)
}  //--------  function showUserTopUpMoney($jsonData)



if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))
{
	$decodeJSON = ($_REQUEST['json']);
	$decodeJSON = str_replace("[", "", $decodeJSON);
	$decodeJSON = str_replace("]", "", $decodeJSON);
	$decodeJSON = json_decode(stripcslashes($decodeJSON), TRUE);

	$serverID = strtoupper($decodeJSON['serverid']);
	$serverKeyPass = $decodeJSON['keypass'];
	$requestTransID = strtoupper($decodeJSON['request_trans_id']);

	$sql = "select sid from db_server where serial_id='".$serverID."' and keypass='".$serverKeyPass."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$requestMethod = $decodeJSON['method'];
		$requestAction = $decodeJSON['action'];
		$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($decodeJSON['request_trans_id']));

		$sql = "select rid from db_request_history where request_trans_id='".$requestTransID."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			if(($requestAction == "user") and ($requestMethod == "add"))
			{
				echo addUserTopUp($decodeJSON);
			}
			elseif(($requestAction == "user") and ($requestMethod == "edit"))
			{
				echo editUserTopUp($decodeJSON);
			}
			elseif(($requestAction == "top_up") and ($requestMethod == "add"))
			{
				echo topUpMoney($decodeJSON);
			}
			elseif(($requestAction == "top_up") and ($requestMethod == "select"))
			{
				echo showUserTopUpMoney($decodeJSON);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err103";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "No Action Request In Web Service Process.";

				array_push($return_arr,$row_array);
					
				echo json_encode($return_arr);
			}  //-------  if($requestAction == "Add User")
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err104";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Request Transaction ID Duplication. Please Generation New Transaction ID.";

			array_push($return_arr,$row_array);
					
			echo json_encode($return_arr);
		}  //------  if($num == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err102";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! Your Server ID Or KeyPass Not Match.";

		array_push($return_arr,$row_array);
			
		echo json_encode($return_arr);
	}  //-------  if($num > 0)
}
else
{
	$row_array['status'] = "fail";
	$row_array['statuscode'] = "err101";
	$row_array['message'] = "Please! Send Correct JSON To Request Data.";

	array_push($return_arr,$row_array);
		
	echo json_encode($return_arr);
}  //------  if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))

unset($mFunc, $mQuery, $dFunc);



/*  
JSON REQUEST EXAMPLE

******Add User JSON
{"serverid":"s0001", "keypass":"1111", "method":"add", "action":"user", "request_trans_id":"ts0000001", "user_detail":{"username":"test005", "email":"yuya1015@gmail.com", "mobile":"0844259652", "name":"ball", "lastname":"maxnum", "room":"room", "building":"building", "village":"village", "address":"address", "soi":"soi", "mou":"mou", "street":"street", "district":"district", "amphor":"amphor", "province":"province", "postcode":"postcode", "telephone":"telephone"}}


******Edit User JSON
{"serverid":"s0001", "keypass":"1111", "method":"edit", "action":"user", "request_trans_id":"ts0000002", "user_detail":{"username":"test006", "email":"yuya1016@gmail.com", "mobile":"0844259651", "name":"ball1", "lastname":"maxnum1", "room":"room1", "building":"building1", "village":"village1", "address":"address1", "soi":"soi1", "mou":"mou1", "street":"street1", "district":"district1", "amphor":"amphor1", "province":"province1", "postcode":"postcode1", "telephone":"telephone1"}}


******Add Top Up JSON
{"serverid":"s0001", "keypass":"1111", "method":"add", "action":"top_up", "request_trans_id":"ts0000003", "user_detail":{"username":"test006", "email":"yuya1016@gmail.com", "mobile":"0844259651", "value":"57820", "closejob":"0"}}


******Select User Money JSON
{"serverid":"s0001", "keypass":"1111", "method":"select", "action":"top_up", "request_trans_id":"ts0000008", "user_detail":{"username":"test006", "email":"yuya1016@gmail.com", "mobile":"0844259651"}}

*/
?>