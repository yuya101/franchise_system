<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$dFunc = new DateFunction();
$mFunc = new MainFunction();
$mQuery = new MainQuery();

$return_arr = array();


function addUserFirstTime($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");

	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);
	$requestUserRealName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['name']);
	$requestUserRealLastName = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['lastname']);
	$requestUserRoom = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['room']);
	$requestUserBuilding = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['building']);
	$requestUserVillage = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['village']);
	$requestUserAddress = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['address']);
	$requestUserSoi = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['soi']);
	$requestUserMou = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mou']);
	$requestUserStreet = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['street']);
	$requestUserDistrict = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['district']);
	$requestUserAmphor = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['amphor']);
	$requestUserProvince = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['province']);
	$requestUserPostCode = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['postcode']);
	$requestUserTelephone = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['telephone']);
	$requestUserStartMoney = intval($mFunc->chgSpecialCharInputText($jsonData['user_detail']['firsttime_money']));

	if(($requestUserName != "-") or ($requestUserMobile != "-"))
	{
		$sql = "select uid from db_user_detail where user_name='".$requestUserName."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			$sql = "select uid from db_user_detail where user_mobile='".$requestUserMobile."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0)
			{
				$responseTransID = $requestTransID;
				$jsonInText = serialize($jsonData);

				$sql = "insert into db_user_detail values(NULL";
				$sql = $sql.", '".$requestUserName."'";
				$sql = $sql.", '".$requestUserEmail."'";
				$sql = $sql.", '".$requestUserMobile."'";
				$sql = $sql.", '".$requestUserRealName."'";
				$sql = $sql.", '".$requestUserRealLastName."'";
				$sql = $sql.", '".$requestUserRoom."'";
				$sql = $sql.", '".$requestUserBuilding."'";
				$sql = $sql.", '".$requestUserVillage."'";
				$sql = $sql.", '".$requestUserAddress."'";
				$sql = $sql.", '".$requestUserSoi."'";
				$sql = $sql.", '".$requestUserMou."'";
				$sql = $sql.", '".$requestUserStreet."'";
				$sql = $sql.", '".$requestUserDistrict."'";
				$sql = $sql.", '".$requestUserAmphor."'";
				$sql = $sql.", '".$requestUserProvince."'";
				$sql = $sql.", '".$requestUserPostCode."'";
				$sql = $sql.", '".$requestUserTelephone."'";
				$sql = $sql.", ".$serverID."";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '-'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '-'";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_mobile='".$requestUserMobile."'";
				$uid = $mQuery->getResultOneRecord($sql, "uid");

				$sql = "insert into db_request_history values(NULL";
				$sql = $sql.", ".$serverID."";
				$sql = $sql.", '".$requestTransID."'";
				$sql = $sql.", '".$responseTransID."'";
				$sql = $sql.", '".$requestMethod."'";
				$sql = $sql.", '".$requestAction."'";
				$sql = $sql.", '".$jsonInText."'";
				$sql = $sql.", ".$uid."";
				$sql = $sql.", '".$requestUserMobile."'";
				$sql = $sql.", '".$requestUserEmail."'";
				$sql = $sql.", 0";
				$sql = $sql.", 1";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "insert into db_user_money values(NULL, ".$uid.", ".$requestUserStartMoney.", 0)";
				$mQuery->querySQL($sql);


				$row_array['status'] = "success";
				$row_array['statuscode'] = "complete";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "Add New User And Start Money Complete.";

				array_push($return_arr,$row_array);
					
				return json_encode($return_arr);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err803";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "Sorry! Mobile Number Already Exist.";

				array_push($return_arr,$row_array);
					
				return json_encode($return_arr);
			}  //-----  if($num == 0)
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err802";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Sorry! User Name Already Exist.";

			array_push($return_arr,$row_array);
				
			return json_encode($return_arr);
		}  //-------  if($num == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err801";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name And Mobile Must Have Data.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if(($requestUserName != "-") and ($requestUserEmail != "-"))
}  //---------  function topUpMoney($jsonData)



if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))
{
	$decodeJSON = ($_REQUEST['json']);
	$decodeJSON = str_replace("[", "", $decodeJSON);
	$decodeJSON = str_replace("]", "", $decodeJSON);
	$decodeJSON = json_decode(stripcslashes($decodeJSON), TRUE);

	$serverID = strtoupper($decodeJSON['serverid']);
	$serverKeyPass = $decodeJSON['keypass'];
	$requestTransID = strtoupper($decodeJSON['request_trans_id']);

	$sql = "select sid from db_server where serial_id='".$serverID."' and keypass='".$serverKeyPass."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$requestMethod = $decodeJSON['method'];
		$requestAction = $decodeJSON['action'];
		$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($decodeJSON['request_trans_id']));

		$sql = "select rid from db_request_history where request_trans_id='".$requestTransID."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			if(($requestAction == "user") and ($requestMethod == "firsttime_add"))
			{
				echo addUserFirstTime($decodeJSON);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err103";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "No Action Request In Web Service Process.";

				array_push($return_arr,$row_array);
					
				echo json_encode($return_arr);
			}  //-------  if($requestAction == "Add User")
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err104";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Request Transaction ID Duplication. Please Generation New Transaction ID.";

			array_push($return_arr,$row_array);
					
			echo json_encode($return_arr);
		}  //------  if($num == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err102";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! Your Server ID Or KeyPass Not Match.";

		array_push($return_arr,$row_array);
			
		echo json_encode($return_arr);
	}  //-------  if($num > 0)
}
else
{
	$row_array['status'] = "fail";
	$row_array['statuscode'] = "err101";
	$row_array['message'] = "Please! Send Correct JSON To Request Data.";

	array_push($return_arr,$row_array);
		
	echo json_encode($return_arr);
}  //------  if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))

unset($mFunc, $mQuery, $dFunc);
?>