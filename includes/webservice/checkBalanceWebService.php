<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$dFunc = new DateFunction();
$mFunc = new MainFunction();
$mQuery = new MainQuery();

$return_arr = array();


function checkBalanceMoney($jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$serverID = $mFunc->chgSpecialCharInputText($jsonData['serverid']);
	$sql = "select sid from db_server where serial_id='".$serverID."'";
	$serverID = $mQuery->getResultOneRecord($sql, "sid");

	$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($jsonData['request_trans_id']));
	$requestMethod = $mFunc->chgSpecialCharInputText($jsonData['method']);
	$requestAction = $mFunc->chgSpecialCharInputText($jsonData['action']);
	$requestUserName = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['username']));
	$requestUserEmail = strtoupper($mFunc->chgSpecialCharInputText($jsonData['user_detail']['email']));
	$requestUserMobile = $mFunc->chgSpecialCharInputText($jsonData['user_detail']['mobile']);
	$requestUserBalanceValue = intval($mFunc->chgSpecialCharInputText($jsonData['user_detail']['final_value']));
	$requestUserBalanceCloseJob = (int)$mFunc->chgSpecialCharInputText($jsonData['user_detail']['closejob']);


	$sql = "select uid from db_user_detail where user_name='".$requestUserName."' and user_email='".$requestUserEmail."' and user_mobile='".$requestUserMobile."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$responseTransID = $requestTransID;
		$jsonInText = serialize($jsonData);

		$uid = $mQuery->getResultOneRecord($sql, "uid");

		if($requestUserBalanceCloseJob == 0)
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err702";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Sorry! Your Close Job ID Not Match In System.";

			array_push($return_arr,$row_array);
					
			return json_encode($return_arr);
		}
		elseif($requestUserBalanceCloseJob == 1)
		{
			$sql = "select umid from db_user_money where uid=".$uid;
			$num = $mQuery->checkNumRows($sql);

			if($num > 0)
			{
				$umid = $mQuery->getResultOneRecord($sql, "umid");
				$closeJobID = intval("9".(substr("000".$serverID, -3, 3)));
				
				$sql = "select money_can_use from db_user_money where uid=".$uid;
				$userMoney = intval($mQuery->getResultOneRecord($sql, "money_can_use"));

				if($userMoney == $requestUserBalanceValue)
				{
					$row_array['status'] = "success";
					$row_array['statuscode'] = "complete";
					$row_array['request_tx_id'] = $requestTransID;
					$row_array['message'] = "User Balance Complete. Balance Not Difference.";
				}
				else
				{	
					$diffValue = $requestUserBalanceValue - $userMoney;
					$diffValueNoSign = abs($diffValue);

					$sql = "update db_user_money set money_can_use=".$requestUserBalanceValue." where umid=".$umid;
					$mQuery->querySQL($sql);

					$sql = "insert into db_check_balance_detail values(NULL, ".$uid.", '".$requestUserName."', '".$requestUserEmail."', '".$requestUserMobile."', ".$diffValue.", ".$userMoney.", ".$requestUserBalanceValue.", 2, ".$closeJobID.", 0, '".$dateNow."', '".$timeNow."', 0, '".$dateNow."', '".$timeNow."', 0)";
					$mQuery->querySQL($sql);


					if($userMoney > $requestUserBalanceValue)
					{
						$closeJobID = intval("8".(substr("000".$serverID, -3, 3)));  //------ 8 = Check Balance

						$sql = "insert into db_withdraw_detail values(NULL, ".$uid.", '".$requestUserName."', '".$requestUserEmail."', '".$requestUserMobile."', ".$diffValueNoSign.", 2, ".$closeJobID.", 0, '".$dateNow."', '".$timeNow."', 0, '".$dateNow."', '".$timeNow."', 0)";
						$mQuery->querySQL($sql);


						$sql = "select wsid from db_withdraw_summary where date='".$dateNow."'";
						$num = $mQuery->checkNumRows($sql);

						if($num > 0)
						{
							$wsid = intval($mQuery->getResultOneRecord($sql, "wsid"));

							$sql = "update db_withdraw_summary set withdraw_summary=withdraw_summary+".$diffValueNoSign." where wsid=".$wsid;
							$mQuery->querySQL($sql);
						}
						else
						{
							$day = intval(substr($dateNow, -2, 2));
							$month = intval(substr($dateNow, 4, 2));
							$year = intval(substr($dateNow, 0, 4));

							$sql = "insert into db_withdraw_summary values(NULL, ".$day.", ".$month.", ".$year.", '".$dateNow."', ".$diffValueNoSign.")";
							$mQuery->querySQL($sql);
						}  //------  if($num > 0)

						$row_array['status'] = "success";
						$row_array['statuscode'] = "complete";
						$row_array['request_tx_id'] = $requestTransID;
						$row_array['message'] = "User Balance Complete. Balance Difference ".$diffValue." Baht. User Money From Server > User Money From Transaction.";
					}
					else
					{
						$closeJobID = intval("8".(substr("000".$serverID, -3, 3)));  //------ 8 = Check Balance

						$sql = "insert into db_topup_detail values(NULL, ".$uid.", '".$requestUserName."', '".$requestUserEmail."', '".$requestUserMobile."', ".$diffValueNoSign.", 2, ".$closeJobID.", 0, '".$dateNow."', '".$timeNow."', 0, '".$dateNow."', '".$timeNow."', 0)";
						$mQuery->querySQL($sql);

						$sql = "select tsid from db_topup_summary where date='".$dateNow."'";
						$num = $mQuery->checkNumRows($sql);

						if($num > 0)
						{
							$tsid = intval($mQuery->getResultOneRecord($sql, "tsid"));

							$sql = "update db_topup_summary set topup_summary=topup_summary+".$diffValueNoSign." where tsid=".$tsid;
							$mQuery->querySQL($sql);
						}
						else
						{
							$day = intval(substr($dateNow, -2, 2));
							$month = intval(substr($dateNow, 4, 2));
							$year = intval(substr($dateNow, 0, 4));

							$sql = "insert into db_topup_summary values(NULL, ".$day.", ".$month.", ".$year.", '".$dateNow."', ".$diffValueNoSign.")";
							$mQuery->querySQL($sql);
						}  //------  if($num > 0)

						$row_array['status'] = "success";
						$row_array['statuscode'] = "complete";
						$row_array['request_tx_id'] = $requestTransID;
						$row_array['message'] = "User Balance Complete. Balance Difference ".$diffValue." Baht. User Money From Server < User Money From Transaction.";
					}  //------  if($userMoney > $requestUserBalanceValue)


					$sql = "select adid from db_active_data_history where uid=".$uid." and active_date='".$dateNow."'";
					$num = $mQuery->checkNumRows($sql);

					if($num == 0)
					{
						$sql = "insert into db_active_data_history values(NULL, ".$uid.", '".$dateNow."', '".$requestTransID."')";
						$mQuery->querySQL($sql);
					}  //-----  if($num == 0)
				}  //-----  if($userMoney == $requestUserBalanceValue)


				$sql = "insert into db_request_history values(NULL";
				$sql = $sql.", ".$serverID."";
				$sql = $sql.", '".$requestTransID."'";
				$sql = $sql.", '".$responseTransID."'";
				$sql = $sql.", '".$requestMethod."'";
				$sql = $sql.", '".$requestAction."'";
				$sql = $sql.", '".$jsonInText."'";
				$sql = $sql.", ".$uid."";
				$sql = $sql.", '".$requestUserMobile."'";
				$sql = $sql.", '".$requestUserEmail."'";
				$sql = $sql.", ".$requestUserBalanceValue."";
				$sql = $sql.", 1";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.")";
				$mQuery->querySQL($sql);
				

				array_push($return_arr,$row_array);
							
				return json_encode($return_arr);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err703";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "Sorry! User Money Not Found In System.";

				array_push($return_arr,$row_array);
						
				return json_encode($return_arr);
			}  //------  if($num > 0)
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err702";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Sorry! Your Close Job ID Not Match In System.";

			array_push($return_arr,$row_array);
					
			return json_encode($return_arr);
		}  //------  if($requestUserBalanceCloseJob == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err701";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! User Name, Email Or Mobile Not Match In System.";

		array_push($return_arr,$row_array);
				
		return json_encode($return_arr);
	}  //------  if($num > 0)
}  //---------  function topUpMoney($jsonData)



if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))
{
	$decodeJSON = ($_REQUEST['json']);
	$decodeJSON = str_replace("[", "", $decodeJSON);
	$decodeJSON = str_replace("]", "", $decodeJSON);
	$decodeJSON = json_decode(stripcslashes($decodeJSON), TRUE);

	$serverID = strtoupper($decodeJSON['serverid']);
	$serverKeyPass = $decodeJSON['keypass'];
	$requestTransID = strtoupper($decodeJSON['request_trans_id']);

	$sql = "select sid from db_server where serial_id='".$serverID."' and keypass='".$serverKeyPass."'";
	$num = $mQuery->checkNumRows($sql);


	if($num > 0)
	{
		$requestMethod = $decodeJSON['method'];
		$requestAction = $decodeJSON['action'];
		$requestTransID = strtoupper($mFunc->chgSpecialCharInputText($decodeJSON['request_trans_id']));

		$sql = "select rid from db_request_history where request_trans_id='".$requestTransID."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			if(($requestAction == "top_up") and ($requestMethod == "check_balance"))
			{
				echo checkBalanceMoney($decodeJSON);
			}
			else
			{
				$row_array['status'] = "fail";
				$row_array['statuscode'] = "err103";
				$row_array['request_tx_id'] = $requestTransID;
				$row_array['message'] = "No Action Request In Web Service Process.";

				array_push($return_arr,$row_array);
					
				echo json_encode($return_arr);
			}  //-------  if($requestAction == "Add User")
		}
		else
		{
			$row_array['status'] = "fail";
			$row_array['statuscode'] = "err104";
			$row_array['request_tx_id'] = $requestTransID;
			$row_array['message'] = "Request Transaction ID Duplication. Please Generation New Transaction ID.";

			array_push($return_arr,$row_array);
					
			echo json_encode($return_arr);
		}  //------  if($num == 0)
	}
	else
	{
		$row_array['status'] = "fail";
		$row_array['statuscode'] = "err102";
		$row_array['request_tx_id'] = $requestTransID;
		$row_array['message'] = "Sorry! Your Server ID Or KeyPass Not Match.";

		array_push($return_arr,$row_array);
			
		echo json_encode($return_arr);
	}  //-------  if($num > 0)
}
else
{
	$row_array['status'] = "fail";
	$row_array['statuscode'] = "err101";
	$row_array['message'] = "Please! Send Correct JSON To Request Data.";

	array_push($return_arr,$row_array);
		
	echo json_encode($return_arr);
}  //------  if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))

unset($mFunc, $mQuery, $dFunc);
?>