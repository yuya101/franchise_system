<?php
ob_start();
session_start();
set_time_limit(0);

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$dFunc = new DateFunction();
$mFunc = new MainFunction();
$mQuery = new MainQuery();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

$loadFileStatus = "";

unset($mFunc, $mQuery, $dFunc);
?>