<?php
ob_start();
session_start();
set_time_limit(1800);  // ประมาณ 30 นาที

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$dFunc = new DateFunction();
$mFunc = new MainFunction();
$mQuery = new MainQuery();

$return_arr = array();


function readFileFromFolder($uploadFromWebServiceFilePath, $uploadFromWebServiceAfterSplitFilePath, $jsonData)
{
	global $mFunc, $mQuery, $dFunc, $return_arr;

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$countFileNo = 0;

	$jsonCount = count($jsonData['detail']);

	$objScan = scandir($uploadFromWebServiceFilePath);
	$numFile = count($objScan);

	for($i=0; $i<$jsonCount; $i++){
		$docTypeID = $jsonData['detail'][$i]['docTypeID'];
		$docCategoryID = $jsonData['detail'][$i]['docCategoryID'];
		$docTitle = $jsonData['detail'][$i]['docTitle'];
		$docMonth = $jsonData['detail'][$i]['docMonth'];
		$docDescription = $jsonData['detail'][$i]['docDescription'];
		$docFileName = $jsonData['detail'][$i]['docFileName'];
		$requestTransaction = $jsonData['detail'][$i]['requestTransaction'];

		$row_array['requestTransaction'] = $requestTransaction;
		$row_array['docFileName'] = $docFileName;

		if(in_array($docFileName, $objScan)){
			$row_array['fileExist'] = "Exist";

			$docdate = "01";
			list($docmonth, $docyear) = split('[/.-]', $docMonth);

			$docBranchName = substr(strtoupper($docFileName), 0, 4);
			$newFolderPath = $uploadFromWebServiceAfterSplitFilePath.$docBranchName."/".$docyear."/".$docmonth."/";

			if (!file_exists($newFolderPath)) {
			    mkdir($newFolderPath, 0777, true);
			}  //------  if (!file_exists($newFolderPath)) 

			rename($uploadFromWebServiceFilePath.$docFileName, $newFolderPath.$docFileName);

			$startBrandName = strtoupper(substr($docFileName, 0, 1));
			$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
			$num = $mQuery->checkNumRows($sql);

			if($num > 0){
				$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
			}else{
				$brandid = 0;
			}  //-----  if($num > 0)
			

			$sql = "insert into db_document values(NULL";
			$sql = $sql.", ".$docTypeID."";
			$sql = $sql.", ".$docCategoryID."";
			$sql = $sql.", ".$brandid."";
			$sql = $sql.", '-'";
			$sql = $sql.", '-'";
			$sql = $sql.", '".$docTitle."'";
			$sql = $sql.", '".$docDescription."'";
			$sql = $sql.", '".$docFileName."'";
			$sql = $sql.", '".$newFolderPath."'";
			$sql = $sql.", '".$docdate."'";
			$sql = $sql.", '".$docmonth."'";
			$sql = $sql.", '".$docyear."'";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.", 0";
			$sql = $sql.", '".$dateNow."'";
			$sql = $sql.", '".$timeNow."'";
			$sql = $sql.", 0";
			$sql = $sql.")";
			$mQuery->querySQL($sql);

			$sql = "select did from db_document order by did desc limit 1";
			$did = $mQuery->getResultOneRecord($sql, "did");

			$sql = "select uaid, site_customer from db_user_auth where shop_code='".$docBranchName."'";
			$num = $mQuery->checkNumRows($sql);

			if($num > 0){
				$uaid = $mQuery->getResultOneRecord($sql, "uaid");
				$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");

				$row_array['shop_id'] = $uaid;
				$row_array['site_customer'] = $site_customer;
				$row_array['shop_map_status'] = "Map With Shop ID";
			}else{
				$uaid = 0;
				$site_customer = "-";

				$row_array['shop_id'] = 0;
				$row_array['site_customer'] = "-";
				$row_array['shop_map_status'] = "Can't Map Shop ID";
			}  //------  if($num > 0)

			$sqlDocBranch = "insert into db_document_authorize values(NULL";
			$sqlDocBranch = $sqlDocBranch.", ".$did."";
			$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
			$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
			$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
			$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
			$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
			$sqlDocBranch = $sqlDocBranch.", 0";
			$sqlDocBranch = $sqlDocBranch.")";
			$mQuery->querySQL($sqlDocBranch);
		}else{
			$row_array['fileExist'] = "File Not Exist";
		}  //-------  if(in_array($docFileName, $objScan))

		array_push($return_arr,$row_array);
	}  //-----  for($i=0; $i<$jsonCount; $i++)

	$ret = array( "status" => "success", "file_detail" => $return_arr);
	echo json_encode($ret);
}  //--------  function readFileFromFolder($jsonData)



if(!is_null($_REQUEST['json']) and isset($_REQUEST['json'])){
	$decodeJSON = ($_REQUEST['json']);
	$decodeJSON = json_decode(stripcslashes($decodeJSON), TRUE);

	readFileFromFolder($uploadFromWebServiceFilePath, $uploadFromWebServiceAfterSplitFilePath, $decodeJSON);
}else{
	$row_array['statuscode'] = "err101";
	$row_array['message'] = "Please! Send Correct JSON To Request Data.";

	array_push($return_arr,$row_array);

	$ret = array( "Status" => "fail", "File Add Detail" => $return_arr);
	echo json_encode($ret);
}  //------  if(!is_null($_REQUEST['json']) and isset($_REQUEST['json']))

unset($mFunc, $mQuery, $dFunc);



/*  
JSON REQUEST EXAMPLE

******Add User JSON
{"serverid":"s0001", "keypass":"1111", "method":"add", "action":"user", "request_trans_id":"ts0000001", "user_detail":{"username":"test005", "email":"yuya1015@gmail.com", "mobile":"0844259652", "name":"ball", "lastname":"maxnum", "room":"room", "building":"building", "village":"village", "address":"address", "soi":"soi", "mou":"mou", "street":"street", "district":"district", "amphor":"amphor", "province":"province", "postcode":"postcode", "telephone":"telephone"}}

*/
?>