<?php
$mQuery = new MainQuery();
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();


$sqlDocType = "select * from db_inquiry_type order by inquiry_type_name";
$numDocType = $mQuery->checkNumRows($sqlDocType);

if($numDocType > 0){
	$resultDocType = $mQuery->getResultAll($sqlDocType);
	$i = 0;

	foreach ($resultDocType as $rd) {
		$inqTypeIDArr[$i] = $rd['id'];
		$inqTypeNameArr[$i] = $rd['inquiry_type_name'];

		$i++;
	}  //-----  foreach ($resultDocType as $rd)

	unset($resultDocType, $rd);
}  //----  if($numDocType > 0)

unset($mQuery, $mFunc, $dFunc);
?>