<?php
ob_start();
session_start();
set_time_limit(1800);  // ประมาณ 30 นาที

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(isset($_SESSION['mLoginID']))
{
	/** PHPExcel */
	require_once($importClassPath."PHPExcel.php");
	/** PHPExcel_IOFactory - Reader */
	include($importClassPath."PHPExcel/IOFactory.php");
	ini_set("precision", "15");


	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$strFileName = "-";
	$userauth = 4;
	$countImport = 0;

	if (file_exists($uploadBeforeSplitFilePath."importFile.xlsx")) {
		$strFileName = "importFile.xlsx";
	}else{
		$strFileName = "";
	}  //------  if (!file_exists($newFolderPath))

	$objScan = scandir($uploadBeforeSplitFilePath);

	if($strFileName != ""){
		$j = 0;

		$inputFileName = $uploadBeforeSplitFilePath.$strFileName; 
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);


		// for No header----------------------------------------
			
		$objWorksheet = $objPHPExcel->setActiveSheetIndex($j);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
			
		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				$namedDataArray[$r] = $dataRow[$row];
			}
		}  //----------  for ($row = 2 $row <= $highestRow; ++$row) 
			
		// End for No header----------------------------------------


		foreach ($namedDataArray as $result){
			$docTypeID = (int)$mFunc->changeToUTFBlankCheck($result['A']);
			$docCategoryID = (int)$mFunc->changeToUTFBlankCheck($result['B']);
			$docBranchName = strtoupper($mFunc->changeToUTFBlankCheck($result['C']));
			$docTitle = $mFunc->changeToUTFBlankCheck($result['D']);
			$docDescription = $mFunc->changeToUTFBlankCheck($result['E']);
			$docFileName = $mFunc->changeToUTFBlankCheck($result['F']);
			$docMonth = $mFunc->changeToUTFBlankCheck($result['G']);

			$docdate = "01";
			list($docmonth, $docyear) = split('[/.-]', $docMonth);

			$newFolderPath = $uploadFromWebServiceAfterSplitFilePath.$docBranchName."/".$docyear."/".$docmonth."/";

			if(in_array($docFileName, $objScan)){
				if (!file_exists($newFolderPath)) {
				    mkdir($newFolderPath, 0777, true);
				}  //------  if (!file_exists($newFolderPath)) 

				rename($uploadBeforeSplitFilePath.$docFileName, $newFolderPath.$docFileName);

				$startBrandName = strtoupper(substr($docFileName, 0, 1));
				$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
				$num = $mQuery->checkNumRows($sql);

				if($num > 0){
					$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
				}else{
					$brandid = 0;
				}  //-----  if($num > 0)

				$sql = "insert into db_document values(NULL";
				$sql = $sql.", ".$docTypeID."";
				$sql = $sql.", ".$docCategoryID."";
				$sql = $sql.", ".$brandid."";
				$sql = $sql.", '-'";
				$sql = $sql.", '-'";
				$sql = $sql.", '".$docTitle."'";
				$sql = $sql.", '".$docDescription."'";
				$sql = $sql.", '".$docFileName."'";
				$sql = $sql.", '".$newFolderPath."'";
				$sql = $sql.", '".$docdate."'";
				$sql = $sql.", '".$docmonth."'";
				$sql = $sql.", '".$docyear."'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", 0";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", 0";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "select did from db_document order by did desc limit 1";
				$did = $mQuery->getResultOneRecord($sql, "did");

				$sql = "select uaid, site_customer from db_user_auth where shop_code='".$docBranchName."'";
				$num = $mQuery->checkNumRows($sql);

				if($num > 0){
					$uaid = $mQuery->getResultOneRecord($sql, "uaid");
					$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
				}else{
					$uaid = 0;
					$site_customer = "-";
				}  //------  if($num > 0)

				$sqlDocBranch = "insert into db_document_authorize values(NULL";
				$sqlDocBranch = $sqlDocBranch.", ".$did."";
				$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
				$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
				$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
				$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
				$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
				$sqlDocBranch = $sqlDocBranch.", 0";
				$sqlDocBranch = $sqlDocBranch.")";
				$mQuery->querySQL($sqlDocBranch);

				$countImport++;
			}  //----------  if(in_array($docFileName, $objScan))
		}  //------  foreach ($namedDataArray as $result)

		unset($inputFileType, $objReader, $objPHPExcel, $objWorksheet, $highestRow, $highestColumn, $namedDataArray);
	}  //------  if($strFileName != "")

	header("location:../../index.php?f=importFileData&importComplete=".base64_encode($countImport));
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>