<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$_SESSION['mLoginID'] = 1;
session_write_close();

if(($_REQUEST['topupID'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$topupID = $mFunc->chgSpecialCharInputText($_REQUEST['topupID']);
	$topupValue = $mFunc->chgSpecialCharInputText($_REQUEST['topupValue']);


	$sql = "select * from db_topup_detail where topid=".$topupID;
	$uid = $mQuery->getResultOneRecord($sql, "uid");
	$userName = $mQuery->getResultOneRecord($sql, "topup_user");
	$email = $mQuery->getResultOneRecord($sql, "topup_email");

	if(isset($_REQUEST['close']))
	{
		$sql = "update db_topup_detail set topup_flag=2, topup_aid=".$_SESSION['mLoginID'].", moddate='".$dateNow."', modtime='".$timeNow."', modid=".$_SESSION['mLoginID']." where topid=".$topupID;
		$mQuery->querySQL($sql);

		$sql = "select umid from db_user_money where uid=".$uid;
		$num = $mQuery->checkNumRows($sql);

		if($num > 0)
		{
			$umid = $mQuery->getResultOneRecord($sql, "umid");

			$sql = "update db_user_money set money_can_use=money_can_use+".$topupValue.", money_wait_for_process=money_wait_for_process-".$topupValue." where umid=".$umid;
			$mQuery->querySQL($sql);
		}
		else
		{
			$sql = "insert into db_user_money values(NULL, ".$uid.", ".$topupValue.", 0)";
			$mQuery->querySQL($sql);
		}  //------  if($num > 0)

		$sql = "select tsid from db_topup_summary where date='".$dateNow."'";
		$num = $mQuery->checkNumRows($sql);

		if($num > 0)
		{
			$tsid = intval($mQuery->getResultOneRecord($sql, "tsid"));

			$sql = "update db_topup_summary set topup_summary=topup_summary+".$topupValue." where tsid=".$tsid;
			$mQuery->querySQL($sql);
		}
		else
		{
			$day = intval(substr($dateNow, -2, 2));
			$month = intval(substr($dateNow, 4, 2));
			$year = intval(substr($dateNow, 0, 4));

			$sql = "insert into db_topup_summary values(NULL, ".$day.", ".$month.", ".$year.", '".$dateNow."', ".$topupValue.")";
			$mQuery->querySQL($sql);
		}  //------  if($num > 0)

		header("location:../../index.php?f=topupSystem&closeJob=".base64_encode($userName)."&value=".base64_encode($topupValue));
	}  //--------  if(isset($_REQUEST['close']))


	if(isset($_REQUEST['cancel']))
	{
		$sql = "update db_topup_detail set topup_flag=0, moddate='".$dateNow."', modtime='".$timeNow."', modid=".$_SESSION['mLoginID']." where topid=".$topupID;
		$mQuery->querySQL($sql);

		header("location:../../index.php?f=topupSystem&openJob=".base64_encode($userName));
	}  //--------  if(isset($_REQUEST['close']))
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>