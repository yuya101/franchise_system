<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['buyfrom'] != "") and ($_REQUEST['chickenname'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	if(isset($_REQUEST['edit']))
	{
		$id = $mFunc->chgSpecialCharInputNumber($_REQUEST['id']);
		$chickenid = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['chickenid']));
		$strain = $mFunc->chgSpecialCharInputText($_REQUEST['strain']);
		$buyfrom = $mFunc->chgSpecialCharInputText($_REQUEST['buyfrom']);
		$saleto = $mFunc->chgSpecialCharInputText($_REQUEST['saleto']);
		$chickenname = $mFunc->chgSpecialCharInputText($_REQUEST['chickenname']);
		$sex = $mFunc->chgSpecialCharInputNumber($_REQUEST['sex']);
		$fatherid = $mFunc->chgSpecialCharInputNumber($_REQUEST['fatherid']);
		$motherid = $mFunc->chgSpecialCharInputNumber($_REQUEST['motherid']);
		$generation = $mFunc->chgSpecialCharInputNumber($_REQUEST['generation']);
		$familyno = $mFunc->chgSpecialCharInputNumber($_REQUEST['familyno']);
		$comment = $mFunc->chgSpecialCharInputText($_REQUEST['comment']);


		$sql = "update db_chicken_main set chickenid='".$chickenid."'";
		$sql = $sql.", strain='".$strain."'";
		$sql = $sql.", buyfrom='".$buyfrom."'";
		$sql = $sql.", saleto='".$saleto."'";
		$sql = $sql.", chickenname='".$chickenname."'";
		$sql = $sql.", sex=".$sex."";
		$sql = $sql.", fatherid=".$fatherid."";
		$sql = $sql.", motherid=".$motherid."";
		$sql = $sql.", generation=".$generation."";
		$sql = $sql.", familyno=".$familyno."";
		$sql = $sql.", comment='".$comment."'";
		$sql = $sql." where id=".$id;
		$mQuery->querySQL($sql);

		header("location:../../index.php?f=manageData&confirmOK=".base64_encode($chickenname));
	}  //-----  if(isset($_REQUEST['edit']))


	
	if(isset($_REQUEST['delete']))
	{
		$id = $mFunc->chgSpecialCharInputNumber($_REQUEST['id']);
		
		$sql = "delete from db_chicken_main where id=".$id;
		$mQuery->querySQL($sql);

		header("location:../../index.php?f=manageData&deleteOK=".base64_encode($chickenname));
	}  //-----  if(isset($_REQUEST['edit']))

	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>