<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$_SESSION['mLoginID'] = 1;
session_write_close();

if(($_REQUEST['aid'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$aid = $mFunc->chgSpecialCharInputText($_REQUEST['aid']);
	$cusNo = $mFunc->chgSpecialCharInputText($_REQUEST['cusno']);
	$cusName = $mFunc->chgSpecialCharInputText($_REQUEST['name']);
	$cusLastname = $mFunc->chgSpecialCharInputText($_REQUEST['lastname']);
	$password = base64_encode($mFunc->chgSpecialCharInputText($_REQUEST['password']));
	$userauth = $mFunc->chgSpecialCharInputNumber($_REQUEST['userauth']);

	$sql = "select aid, ausername from db_user where aid=".$aid;
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$username = $mQuery->getResultOneRecord($sql, "ausername");

		if(isset($_REQUEST['manage']))
		{
			$sql = "update db_user set apass='".$password."', customer_no='".$cusNo."', customer_name='".$cusName."', aname='".$cusName."', alastname='".$cusLastname."', groupid=".$userauth.", moddate='".$dateNow."', modtime='".$timeNow."', modip='".$_SERVER['REMOTE_ADDR']."', modaid=".$_SESSION['mLoginID']." where aid=".$aid;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageUser&confirmOK=".base64_encode($username));
		}  //------  if(isset($_REQUEST['manage']))


		if(isset($_REQUEST['delete']))
		{
			$sql = "delete from db_user where aid=".$aid;
			$mQuery->querySQL($sql);
			
			$sql = "delete from db_user_auth where aid=".$aid;
			$mQuery->querySQL($sql);
			
			$sql = "delete from db_user_download_history where aid=".$aid;
			$mQuery->querySQL($sql);
			
			$sql = "delete from db_user_login_history where aid=".$aid;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageUser&deleteOK=".base64_encode($username));
		}  //------  if(isset($_REQUEST['manage']))
	}
	else
	{
		header("location:../../index.php?f=manageUser&errNo=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>