<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");


if(($_REQUEST['email'] != "") and (!isset($_SESSION['mLoginID'])))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	$fcQuery = new FranchiseFunction();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$email = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['email']));

	$sql = "select ausername, apass from db_user where email='".$email."'";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$ausername = $mQuery->getResultOneRecord($sql, "ausername");
		$apass = $mQuery->getResultOneRecord($sql, "apass");
		$apass = base64_decode($apass);

		$strSubject = "Reset Password From Franchise System";
		
		$message = "Dear ".$ausername.",<br><br>";
		$message = $message."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$message = $message."Your Password is : ".$apass."<br>";
		$message = $message."Thank you,<br>";
		$message = $message."Auto Response System";
					
					
		$toEmail = '{"email": "'.$email.'"}';
		$fromEmail = '{"email": "support@mfgfranchise.com"}';
		$fcQuery->sendMailWithcURLReal($toEmail, $fromEmail, $strSubject, $message);

		header("location:../../login.php?sendComplete=".base64_encode("Complete"));
	}
	else
	{
		header("location:../../login.php?errEmail=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc, $fcQuery);
}
else
{
	header("location:../../login.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>