<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$_SESSION['mLoginID'] = 1;
session_write_close();

if(($_REQUEST['uaid'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$uaid = $mFunc->chgSpecialCharInputText($_REQUEST['uaid']);
	$userid = $mFunc->chgSpecialCharInputText($_REQUEST['userid']);
	$brandid = $mFunc->chgSpecialCharInputText($_REQUEST['brandid']);
	$shopcode = $mFunc->chgSpecialCharInputText($_REQUEST['shopcode']);
	$bucode = $mFunc->chgSpecialCharInputText($_REQUEST['bucode']);
	$storename = $mFunc->chgSpecialCharInputNumber($_REQUEST['storename']);
	$activedate = $mFunc->chgSpecialCharInputText($_REQUEST['activedate']);
	$suspenddate = $mFunc->chgSpecialCharInputText($_REQUEST['suspenddate']);
	$disablestatus = $mFunc->chgSpecialCharInputText($_REQUEST['disablestatus']);

	if($activedate != "-"){
		$activedate = $dFunc->chgDateChrisStyle($activedate);
	}  //-----  if($activedate != "-")

	if($suspenddate != "-"){
		$suspenddate = $dFunc->chgDateChrisStyle($suspenddate);
	}  //-----  if($suspenddate != "-")

	$siteCustomer = $bucode."-".$storename;
	$storeIDName = $shopcode."-".$storename;

	$sql = "select uaid from db_user_auth where uaid=".$uaid;
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		if(isset($_REQUEST['manage']))
		{
			$sql = "select uaid from db_user_auth where uaid!=".$uaid." and shop_code='".$shopcode."' and brand_id=".$brandid;
			$num = $mQuery->checkNumRows($sql);

			if ($num == 0) {

				$sql = "update db_user_auth set aid=".$userid.", brand_id=".$brandid.", shop_code='".$shopcode."', bu_code='".$bucode."', store_name='".$storename."', site_customer='".$siteCustomer."', store_id_name='".$storeIDName."', active_date='".$activedate."', suspend_date='".$suspenddate."', disable_status=".$disablestatus.", moddate='".$dateNow."', modtime='".$timeNow."', modid=".$_SESSION['mLoginID']." where uaid=".$uaid;
				$mQuery->querySQL($sql);

				header("location:../../index.php?f=manageBranch&confirmOK=".base64_encode($storeIDName));
			}else{
				header("location:../../index.php?f=manageBranch&errDupName=".base64_encode($shopcode));
			}  //------  if ($num == 0)
		}  //------  if(isset($_REQUEST['manage']))


		if(isset($_REQUEST['delete']))
		{
			$sql = "delete from db_user_auth where uaid=".$uaid;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageBranch&deleteOK=".base64_encode($storeIDName));
		}  //------  if(isset($_REQUEST['manage']))
	}
	else
	{
		header("location:../../index.php?f=manageBranch&errNo=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>