<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");


if(($_REQUEST['username'] != "") and (!isset($_SESSION['mLoginID'])))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$username = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['username']));
	$password = base64_encode($mFunc->chgSpecialCharInputText($_REQUEST['password']));

	$sql = "select aid, groupid, ausername, aname, afirstname, alastname, email, moddate from db_user where email='".$username."' and apass='".$password."'";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$arrBrandID = array();
		$arrBrandName = array();
		$arrBranchID = array();
		$arrBranchCode = array();

		$_SESSION['mLoginID'] = (int)$mQuery->getResultOneRecord($sql, "aid");
		$_SESSION['userAuth'] = (int)$mQuery->getResultOneRecord($sql, "groupid");
		$_SESSION['mFullName'] = "คุณ".$mQuery->getResultOneRecord($sql, "aname")." ".$mQuery->getResultOneRecord($sql, "alastname");
		$_SESSION['userName'] = $mQuery->getResultOneRecord($sql, "email");

		$modDate = $mQuery->getResultOneRecord($sql, "moddate");
		$_SESSION['modDatePlus60'] = $dFunc->datePlusDay($modDate, 60, 0, 0);


		$sql = "select DISTINCT brand_id, suspend_date from db_user_auth where aid=".$_SESSION['mLoginID']." and active_date<='".$dateNow."' and disable_status=0";
		$num = $mQuery->checkNumRows($sql);

		if($num > 0){
			$result = $mQuery->getResultAll($sql);

			foreach ($result as $r) {
				if($r['suspend_date'] == "-"){
					array_push($arrBrandID, (int)$r['brand_id']);

					$sql = "select brand_name from db_brand where bid=".$r['brand_id'];
					array_push($arrBrandName, $mQuery->getResultOneRecord($sql, "brand_name"));
				}else{
					if($r['suspend_date'] > $dateNow){
						array_push($arrBrandID, (int)$r['brand_id']);

						$sql = "select brand_name from db_brand where bid=".$r['brand_id'];
						array_push($arrBrandName, $mQuery->getResultOneRecord($sql, "brand_name"));
					}  //------  if($r['suspend_date'] > $dateNow)
				}  //----  if($r['suspend_date'] == "-")
			}  //------  foreach ($result as $r)

			unset($result, $r);
		}  //-----  if($num > 0)

		$_SESSION['arrBrandID'] = $arrBrandID;
		$_SESSION['arrBrandName'] = $arrBrandName;



		$sql = "select uaid, shop_code, suspend_date from db_user_auth where aid=".$_SESSION['mLoginID']." and active_date<='".$dateNow."' and disable_status=0 order by shop_code";
		$num = $mQuery->checkNumRows($sql);

		if($num > 0){
			$result = $mQuery->getResultAll($sql);

			foreach ($result as $r) {
				if($r['suspend_date'] == "-"){
					array_push($arrBranchID, (int)$r['uaid']);
					array_push($arrBranchCode, $r['shop_code']);
				}else{
					if($r['suspend_date'] > $dateNow){
						array_push($arrBranchID, (int)$r['uaid']);
						array_push($arrBranchCode, $r['shop_code']);
					}  //------  if($r['suspend_date'] > $dateNow)
				}  //----  if($r['suspend_date'] == "-")
			}  //------  foreach ($result as $r)

			unset($result, $r);
		}  //-----  if($num > 0)

		$_SESSION['arrBranchID'] = $arrBranchID;
		$_SESSION['arrBranchCode'] = $arrBranchCode;
		// $_SESSION['arrBranchCode'] = array('5001', '5002', 'D811', 'D801', 'D021', 'D023');
		session_write_close();



		$sql = "insert into db_user_login_history values(NULL, ".$_SESSION['mLoginID'].", '".$dateNow."', '".$timeNow."')";
		$mQuery->querySQL($sql);

		header("location:../../index.php");
	}
	else
	{
		header("location:../../login.php?errLogin=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../login.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>