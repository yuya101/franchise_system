<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$_SESSION['mLoginID'] = 1;
session_write_close();

if(($_REQUEST['sid'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$sid = $mFunc->chgSpecialCharInputText($_REQUEST['sid']);
	$servername = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['servername']));
	$keypass = $mFunc->chgSpecialCharInputText($_REQUEST['keypass']);

	$sql = "select serial_id from db_server where sid=".$sid;
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		if(isset($_REQUEST['manage']))
		{
			$sql = "update db_server set server_name='".$servername."', keypass='".$keypass."', moddate='".$dateNow."', modtime='".$timeNow."', modid=".$_SESSION['mLoginID']." where sid=".$sid;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageExternalServer&confirmOK=".base64_encode($servername));
		}  //------  if(isset($_REQUEST['manage']))


		if(isset($_REQUEST['delete']))
		{
			$sql = "delete from db_server where sid=".$sid;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageExternalServer&deleteOK=".base64_encode($servername));
		}  //------  if(isset($_REQUEST['manage']))
	}
	else
	{
		header("location:../../index.php?f=manageExternalServer&errNo=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>