<?php
$mQuery = new MainQuery();
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();


$sqlUser = "select aid, customer_no, customer_name, email from db_user where groupid=4 order by customer_no, customer_name";
$numUser = $mQuery->checkNumRows($sqlUser);

if($numUser > 0){
	$resultUser = $mQuery->getResultAll($sqlUser);
	$i = 0;

	foreach ($resultUser as $rd) {
		$userID[$i] = $rd['aid'];
		$userNo[$i] = $rd['customer_no'];
		$userName[$i] = $rd['customer_name'];
		$userEmail[$i] = $rd['email'];

		$i++;
	}  //-----  foreach ($resultUser as $rd)

	unset($resultUser, $rd);
}  //----  if($numUser > 0)



$sqlDocBrand = "select * from db_brand order by brand_name";
$numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

if($numDocBrand > 0){
	$resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
	$i = 0;

	foreach ($resultDocBrand as $rb) {
		$docBrandID[$i] = $rb['bid'];
		$docBrandName[$i] = $rb['brand_name'];

		$i++;
	}  //-----  foreach ($resultDocType as $rd)

	unset($resultDocBrand, $rb);
}  //----  if($numDocType > 0)

unset($mQuery, $mFunc, $dFunc);
?>