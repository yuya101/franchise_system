<?php
$mQuery = new MainQuery();
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();


$sqlDocType = "select * from db_document_type order by type_name";
$numDocType = $mQuery->checkNumRows($sqlDocType);

if($numDocType > 0){
	$resultDocType = $mQuery->getResultAll($sqlDocType);
	$i = 0;

	foreach ($resultDocType as $rd) {
		$docTypeID[$i] = $rd['type_id'];
		$docTypeName[$i] = $rd['type_name'];

		$i++;
	}  //-----  foreach ($resultDocType as $rd)

	unset($resultDocType, $rd);
}  //----  if($numDocType > 0)




$sqlDocCat = "select * from db_document_category where type_id=".$docTypeID[0];
$numDocCat = $mQuery->checkNumRows($sqlDocCat);

if($numDocCat > 0){
	$resultDocCat = $mQuery->getResultAll($sqlDocCat);
	$j = 0;

	foreach ($resultDocCat as $rc) {
		$docCatID[$j] = $rc['cat_id'];
		$docCatName[$j] = $rc['cat_name'];

		$j++;
	}  //-----  foreach ($resultDocCat as $rc)

	unset($resultDocCat, $rc);
} //-----  if($num > 0)




$sqlDocBrand = "select * from db_brand order by brand_name";
$numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

if($numDocBrand > 0){
	$resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
	$i = 0;

	foreach ($resultDocBrand as $rb) {
		$docBrandID[$i] = $rb['bid'];
		$docBrandName[$i] = $rb['brand_name'];
		$docBrandPicture[$i] = "img/".$rb['brand_picture'];

		$j = 0;

		$sqlDocBranch = "select * from db_user_auth where brand_id=".$docBrandID[$i]." group by shop_code order by site_customer";
		$numDocBranch[$i] = $mQuery->checkNumRows($sqlDocBranch);

		if($numDocBranch[$i] > 0){
			$resultDocBranch = $mQuery->getResultAll($sqlDocBranch);

			foreach ($resultDocBranch as $rdb) {
				$docBranchID[$i][$j] = $rdb['uaid'];
				$docBranchSiteName[$i][$j] = $rdb['site_customer'];
				$docBranchStoreIDName[$i][$j] = $rdb['store_id_name'];
				
				$j++;
			}  //-----  foreach ($resultDocBranch as $rdb)

			unset($resultDocBranch, $rdb);
		}
		// else{
		// 	$numDocBranch[$i] = 20;

		// 	for($j=0; $j<=20; $j++){
		// 		$docBranchID[$i][$j] = (int)($i.$j);
		// 		$docBranchSiteName[$i][$j] = "25006-Big C Hadyai#2";
		// 		$docBranchStoreIDName[$i][$j] = "6006-Big C Hadyai#2";
		// 	}  //------  for($j=0; $j<=20; $j++)
		// }  //------  if($numDocBranch[$i] > 0)

		$i++;
	}  //-----  foreach ($resultDocType as $rd)

	unset($resultDocBrand, $rb);
}  //----  if($numDocType > 0)


$sqlInquiryType = "select * from db_inquiry_type order by inquiry_type_name";
$numInquiryType = $mQuery->checkNumRows($sqlInquiryType);

if($numInquiryType > 0){
	$resultInquiryType = $mQuery->getResultAll($sqlInquiryType);
	$i = 0;

	foreach ($resultInquiryType as $rd) {
		$docInquiryTypeID[$i] = $rd['id'];
		$docInquiryTypeName[$i] = $rd['inquiry_type_name'];

		$i++;
	}  //-----  foreach ($resultInquiryType as $rd)

	unset($resultInquiryType, $rd);
}  //----  if($numInquiryType > 0)


unset($mQuery, $mFunc, $dFunc);
?>