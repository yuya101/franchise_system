<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['userid'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$userID = $mFunc->chgSpecialCharInputNumber($_REQUEST['userid']);
	$brandID = $mFunc->chgSpecialCharInputNumber($_REQUEST['brandid']);
	$shopCode = $mFunc->chgSpecialCharInputText($_REQUEST['shopcode']);
	$buCode = $mFunc->chgSpecialCharInputText($_REQUEST['bucode']);
	$storeName = $mFunc->chgSpecialCharInputText($_REQUEST['storename']);
	$activedate = $mFunc->chgSpecialCharInputText($_REQUEST['activedate']);
	$suspenddate = $mFunc->chgSpecialCharInputText($_REQUEST['suspenddate']);

	if($activedate != "-"){
		$activedate = $dFunc->chgDateChrisStyle($activedate);
	}  //-----  if($activedate != "-")

	if($suspenddate != "-"){
		$suspenddate = $dFunc->chgDateChrisStyle($suspenddate);
	}  //-----  if($suspenddate != "-")


	$sql = "select uaid from db_user_auth where shop_code='".$shopCode."'";
	$num = $mQuery->checkNumRows($sql);

	if($num == 0){
		$sql = "select email, customer_no, customer_name from db_user where aid=".$userID;
		$cusNo = $mQuery->getResultOneRecord($sql, "customer_no");
		$cusName = $mQuery->getResultOneRecord($sql, "customer_name");
		$cusEmail = $mQuery->getResultOneRecord($sql, "email");

		$siteCustomer = $buCode."-".$storeName;
		$storeIDName = $shopCode."-".$storeName;

		$sql = "insert into db_user_auth values(NULL, ".$userID.", ".$brandID.", '".$shopCode."', '".$buCode."', '".$cusNo."', '".$cusName."', '".$storeName."', '".$siteCustomer."', '".$storeIDName."', '".$cusEmail."', '".$activedate."', '".$suspenddate."', 0, '".$dateNow."', '".$timeNow."', ".$_SESSION['mLoginID'].", '".$dateNow."', '".$timeNow."', ".$_SESSION['mLoginID'].")";
		$mQuery->querySQL($sql);

		header("location:../../index.php?f=addBranch&confirmOK=".base64_encode($shopCode));
	}else{
		header("location:../../index.php?f=addBranch&errNameDup=".base64_encode($shopCode));
	}  //------  if($num == 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>