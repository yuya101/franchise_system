<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

$_SESSION['mLoginID'] = 1;
session_write_close();

if(($_REQUEST['catID'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$catID = $mFunc->chgSpecialCharInputText($_REQUEST['catID']);
	$catname = $mFunc->chgSpecialCharInputText($_REQUEST['catname']);
	$typeid = $mFunc->chgSpecialCharInputNumber($_REQUEST['typeid']);

	$sql = "select cat_id from db_document_category where cat_id=".$catID;
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		if(isset($_REQUEST['manage']))
		{
			$sql = "select cat_id from db_document_category where cat_id!=".$catID." and type_id=".$typeid." and cat_name='".$catname."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0){
				$sql = "update db_document_category set cat_name='".$catname."', moddate='".$dateNow."', modtime='".$timeNow."', modid=".$_SESSION['mLoginID']." where cat_id=".$catID;
				$mQuery->querySQL($sql);

				header("location:../../index.php?f=manageCategory&confirmOK=".base64_encode($catname));
			}else{
				header("location:../../index.php?f=manageCategory&errDupName=".base64_encode($catname));
			}  //-----  if($num == 0)
		}  //------  if(isset($_REQUEST['manage']))


		if(isset($_REQUEST['delete']))
		{
			$sql = "delete from db_document_category where cat_id=".$catID;
			$mQuery->querySQL($sql);


			header("location:../../index.php?f=manageCategory&deleteOK=".base64_encode($catname));
		}  //------  if(isset($_REQUEST['manage']))
	}
	else
	{
		header("location:../../index.php?f=manageCategory&errNo=".base64_encode("Error"));
	}  //------  if($num > 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>