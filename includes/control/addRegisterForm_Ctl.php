<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['registerPosition'] != "") and ($_REQUEST['name'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$registerPosition = ($mFunc->chgSpecialCharInputNumber($_REQUEST['registerPosition']));
	$suggestid = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['suggestid']));
	$firstname = ($mFunc->chgSpecialCharInputText($_REQUEST['firstname']));
	$name = $mFunc->chgSpecialCharInputText($_REQUEST['name']);
	$lastname = $mFunc->chgSpecialCharInputText($_REQUEST['lastname']);
	$nationality = $mFunc->chgSpecialCharInputNumber($_REQUEST['nationality']);

	$dateofbirth = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['dateofbirth']));

	$age = $mFunc->chgSpecialCharInputNumber($_REQUEST['age']);
	$gcid = $mFunc->chgSpecialCharInputText($_REQUEST['gcid']);
	$tel = $mFunc->chgSpecialCharInputText($_REQUEST['tel']);
	$address = $mFunc->chgSpecialCharInputText($_REQUEST['address']);
	$mailingaddress = $mFunc->chgSpecialCharInputText($_REQUEST['mailingaddress']);
	$occupation = $mFunc->chgSpecialCharInputText($_REQUEST['occupation']);
	$workaddress = $mFunc->chgSpecialCharInputText($_REQUEST['workaddress']);
	$maritalstatus = $mFunc->chgSpecialCharInputNumber($_REQUEST['maritalstatus']);


	$benefitname1 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitname1']);
	$benefitlastname1 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitlastname1']);
	$benefitnationality1 = $mFunc->chgSpecialCharInputNumber($_REQUEST['benefitnationality1']);
	$benefitdateofbirth1 = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['benefitdateofbirth1']));
	$benefitage1 = $mFunc->chgSpecialCharInputNumber($_REQUEST['benefitage1']);
	$benefitgcid1 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitgcid1']);
	$benefittel1 = $mFunc->chgSpecialCharInputText($_REQUEST['benefittel1']);
	$benefitaddress1 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitaddress1']);


	$benefitname2 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitname2']);
	$benefitlastname2 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitlastname2']);
	$benefitnationality2 = $mFunc->chgSpecialCharInputNumber($_REQUEST['benefitnationality2']);
	$benefitdateofbirth2 = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['benefitdateofbirth2']));
	$benefitage2 = $mFunc->chgSpecialCharInputNumber($_REQUEST['benefitage2']);
	$benefitgcid2 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitgcid2']);
	$benefittel2 = $mFunc->chgSpecialCharInputText($_REQUEST['benefittel2']);
	$benefitaddress2 = $mFunc->chgSpecialCharInputText($_REQUEST['benefitaddress2']);


	$passportno = $mFunc->chgSpecialCharInputText($_REQUEST['passportno']);
	$passport_issue_at = $mFunc->chgSpecialCharInputText($_REQUEST['passport_issue_at']);
	$passport_date_of_issue = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['passport_date_of_issue']));
	$passport_date_of_expire = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['passport_date_of_expire']));



	$arrived_date = $dFunc->chgDateChrisStyleNewStyle($mFunc->chgSpecialCharInputText($_REQUEST['arrived_date']));
	$arrived_by = $mFunc->chgSpecialCharInputNumber($_REQUEST['arrived_by']);



	$employername = $mFunc->chgSpecialCharInputText($_REQUEST['employername']);
	$locationofwork = $mFunc->chgSpecialCharInputNumber($_REQUEST['locationofwork']);

	$memberID = "-";

	$sql = "insert into db_register_form values(NULL";
	$sql = $sql.", '".$memberID."'";
	$sql = $sql.", '".$firstname."'";
	$sql = $sql.", '".$name."'";
	$sql = $sql.", '".$lastname."'";
	$sql = $sql.", ".$nationality."";
	$sql = $sql.", '".$dateofbirth."'";
	$sql = $sql.", ".$age."";
	$sql = $sql.", '".$gcid."'";
	$sql = $sql.", '".$tel."'";
	$sql = $sql.", '".$address."'";
	$sql = $sql.", '".$mailingaddress."'";
	$sql = $sql.", '".$occupation."'";
	$sql = $sql.", '".$workaddress."'";
	$sql = $sql.", ".$maritalstatus."";
	$sql = $sql.", '".$benefitname1."'";
	$sql = $sql.", '".$benefitlastname1."'";
	$sql = $sql.", ".$benefitnationality1."";
	$sql = $sql.", '".$benefitdateofbirth1."'";
	$sql = $sql.", ".$benefitage1."";
	$sql = $sql.", '".$benefitgcid1."'";
	$sql = $sql.", '".$benefittel1."'";
	$sql = $sql.", '".$benefitaddress1."'";
	$sql = $sql.", '".$benefitname2."'";
	$sql = $sql.", '".$benefitlastname2."'";
	$sql = $sql.", ".$benefitnationality2."";
	$sql = $sql.", '".$benefitdateofbirth2."'";
	$sql = $sql.", ".$benefitage2."";
	$sql = $sql.", '".$benefitgcid2."'";
	$sql = $sql.", '".$benefittel2."'";
	$sql = $sql.", '".$benefitaddress2."'";
	$sql = $sql.", '".$passportno."'";
	$sql = $sql.", '".$passport_issue_at."'";
	$sql = $sql.", '".$passport_date_of_issue."'";
	$sql = $sql.", '".$passport_date_of_expire."'";
	$sql = $sql.", '".$arrived_date."'";
	$sql = $sql.", ".$arrived_by."";
	$sql = $sql.", '".$employername."'";
	$sql = $sql.", '".$locationofwork."'";
	$sql = $sql.", ".$registerPosition."";
	$sql = $sql.", '".$suggestid."'";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.")";
	$mQuery->querySQL($sql);


	$sql = "select id from db_register_form where gcid='".$gcid."' limit 1";
	$mid = $mQuery->getResultOneRecord($sql, "id");

	$memberID = "MT".substr($dateNow, 2, 2).substr($dateNow, 4, 2).substr($dateNow, -2, 2).substr("000".$mid, -5, 5);

	$sql = "update db_register_form set member_id='".$memberID."' where id=".$mid;
	$mQuery->querySQL($sql);

	header("location:../../index.php?f=registerForm&confirmOK=".base64_encode("OK")."&name=".base64_encode($name." ".$lastname));
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>