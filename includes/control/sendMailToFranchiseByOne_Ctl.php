<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['description'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	$fcQuery = new FranchiseFunction();
	
	$receiveEmail = $mFunc->chgSpecialCharInputText($_REQUEST['receiveEmail']);
	$receiveEmail = strtolower($receiveEmail);
	$title = $mFunc->chgSpecialCharInputText($_REQUEST['title']);
	//$description = $mFunc->chgSpecialCharInputText($_REQUEST['description']);

	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$toEmail = '{"email": "'.$receiveEmail.'"}';
	$fromEmail = '{"email": "'.$_SESSION['userName'].'"}';


	if(isset($_REQUEST['description'])){
		$description = trim($_REQUEST['description']);
		//$description = str_replace('"', '\'' , $description);
		$description = addslashes($description);
	}else{
		$description = "";
	}  //---  if(isset($_REQUEST['description']))
	
							
	$fcQuery->sendMailWithcURLReal($toEmail, $fromEmail, $title, $description);

	header("location:../../index.php?f=sendMailToFranchiseByOne&confirmOK=".(base64_encode($title)));
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>