<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['buyfrom'] != "") and ($_REQUEST['chickenname'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$chickenid = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['chickenid']));
	$strain = $mFunc->chgSpecialCharInputText($_REQUEST['strain']);
	$buyfrom = $mFunc->chgSpecialCharInputText($_REQUEST['buyfrom']);
	$saleto = $mFunc->chgSpecialCharInputText($_REQUEST['saleto']);
	$chickenname = $mFunc->chgSpecialCharInputText($_REQUEST['chickenname']);
	$sex = $mFunc->chgSpecialCharInputNumber($_REQUEST['sex']);
	$fatherid = $mFunc->chgSpecialCharInputNumber($_REQUEST['fatherid']);
	$motherid = $mFunc->chgSpecialCharInputNumber($_REQUEST['motherid']);
	$generation = $mFunc->chgSpecialCharInputNumber($_REQUEST['generation']);
	$familyno = $mFunc->chgSpecialCharInputNumber($_REQUEST['familyno']);
	$comment = $mFunc->chgSpecialCharInputText($_REQUEST['comment']);
	$pageid = $mFunc->chgSpecialCharInputNumber($_REQUEST['pageid']);

	$sql = "insert into db_chicken_main values(NULL";
	$sql = $sql.", '".$chickenid."'";
	$sql = $sql.", '".$strain."'";
	$sql = $sql.", '".$buyfrom."'";
	$sql = $sql.", '".$saleto."'";
	$sql = $sql.", '".$chickenname."'";
	$sql = $sql.", ".$sex."";
	$sql = $sql.", ".$fatherid."";
	$sql = $sql.", ".$motherid."";
	$sql = $sql.", ".$generation."";
	$sql = $sql.", ".$familyno."";
	$sql = $sql.", '".$comment."'";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.")";
	$mQuery->querySQL($sql);


	switch($pageid)
	{
		case 1:
			header("location:../../index.php?f=registerFather&confirmOK=".base64_encode("OK")."&name=".base64_encode($chickenname));
			break;
		case 2:
			header("location:../../index.php?f=registerMother&confirmOK=".base64_encode("OK")."&name=".base64_encode($chickenname));
			break;
		case 3:
			header("location:../../index.php?f=registerChicken&confirmOK=".base64_encode("OK")."&name=".base64_encode($chickenname));
			break;
		default:
			header("location:../../index.php");
			break;
	}  //-----  switch($pageid)
	
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>