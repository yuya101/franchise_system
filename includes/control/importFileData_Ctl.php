<?php
ob_start();
session_start();
set_time_limit(1800);  // ประมาณ 30 นาที

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	 
	if (!empty($_FILES['file'])) {
	    	$tempFile = $_FILES['file']['tmp_name']; 

		$fileDetail = pathinfo($_FILES['file']['name']);
		$extension = ".".$fileDetail['extension'];
		$fileName = $fileDetail['filename'];
		$sender_email = "admin@mfgfranchise.com";

		if(strlen($fileName) == 12){
			$docBranchName = substr(strtoupper($fileName), 0, 4);
			$documentYear = substr($fileName, 4, 4);
			$documentMonth = substr($fileName, -4, 2);
			$documentSpecialType = (int)substr($fileName, -2, 2);

			$documentDate = "01";

			$sql = "select special_type_name from db_special_fianace_type where fid=".$documentSpecialType;
			$specialTypeName =$mQuery->getResultOneRecord($sql, "special_type_name");

			$docTypeID = 1;
			$docCategoryID = 1;
			$docTitle = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear;
			$docMonth = $documentMonth."/".$documentYear;
			// $docDescription = "เอกสาร Finance ประจำเดือน ".$documentMonth."/".$documentYear." - เอกสารประเภท ".$specialTypeName;
			$docDescription = "เอกสารประเภท ".$specialTypeName;
			$docFileName = $fileName.$extension;
		    	$targetFile =  $docFileName;

		    	$newFolderPath = $uploadFromWebServiceAfterSplitFilePath.$docBranchName."/".$documentYear."/".$documentMonth."/";


		    	if (!file_exists($newFolderPath)) {
				mkdir($newFolderPath, 0777, true);
			}  //------  if (!file_exists($newFolderPath)) 

		    	if (!file_exists($newFolderPath.$docFileName)) {
		    		$startBrandName = strtoupper(substr($fileName, 0, 1));

				$sql = "select bid from db_brand where brand_start_name='".$startBrandName."'";
				$num = $mQuery->checkNumRows($sql);

				if($num > 0){
					$brandid = (int)$mQuery->getResultOneRecord($sql, "bid");
				}else{
					$brandid = 0;
				}  //-----  if($num > 0)

				$sql = "insert into db_document values(NULL";
				$sql = $sql.", ".$docTypeID."";
				$sql = $sql.", ".$docCategoryID."";
				$sql = $sql.", ".$brandid."";
				$sql = $sql.", '".$docBranchName."'";
				$sql = $sql.", '-'";
				$sql = $sql.", '".$docTitle."'";
				$sql = $sql.", '".$docDescription."'";
				$sql = $sql.", '".$docFileName."'";
				$sql = $sql.", '".$newFolderPath."'";
				$sql = $sql.", '".$documentDate."'";
				$sql = $sql.", '".$documentMonth."'";
				$sql = $sql.", '".$documentYear."'";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", 0";
				$sql = $sql.", '".$dateNow."'";
				$sql = $sql.", '".$timeNow."'";
				$sql = $sql.", 0";
				$sql = $sql.")";
				$mQuery->querySQL($sql);


				$sql = "select did from db_document order by did desc limit 1";
				$did = $mQuery->getResultOneRecord($sql, "did");

				$sql = "select uaid, site_customer, email from db_user_auth where shop_code='".$docBranchName."'";
				$num = $mQuery->checkNumRows($sql);

				if($num > 0){
					$uaid = $mQuery->getResultOneRecord($sql, "uaid");
					$site_customer = $mQuery->getResultOneRecord($sql, "site_customer");
					$email_customer = $mQuery->getResultOneRecord($sql, "email");
				}else{
					$uaid = 0;
					$site_customer = "-";
					$email_customer = "-";
				}  //------  if($num > 0)

				$sqlDocBranch = "insert into db_document_authorize values(NULL";
				$sqlDocBranch = $sqlDocBranch.", ".$did."";
				$sqlDocBranch = $sqlDocBranch.", ".$docTypeID."";
				$sqlDocBranch = $sqlDocBranch.", ".$docCategoryID."";
				$sqlDocBranch = $sqlDocBranch.", ".$brandid."";
				$sqlDocBranch = $sqlDocBranch.", ".$uaid."";
				$sqlDocBranch = $sqlDocBranch.", '".$site_customer."'";
				$sqlDocBranch = $sqlDocBranch.", 0";
				$sqlDocBranch = $sqlDocBranch.")";
				$mQuery->querySQL($sqlDocBranch);


				$sqlDocBranch = "insert into db_document_map_special_type values(".$did.", ".$documentSpecialType.")";
				$mQuery->querySQL($sqlDocBranch);

				$mFunc->sendMailForNewDocument($email_customer, $docTitle, $docDescription, $sender_email);
				
				move_uploaded_file($tempFile,$newFolderPath.$targetFile);
			}else{
				move_uploaded_file($tempFile,$newFolderPath.$targetFile);
			}  //------  if (!file_exists($newFolderPath)) 
		}  //-----  if(strlen($fileName) == 12)
	}  //-----  if (!empty($_FILES))
	
	unset($mFunc, $mQuery, $dFunc);
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>