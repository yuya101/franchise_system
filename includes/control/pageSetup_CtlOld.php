<?php
$mQuery = new MainQuery();
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

if(isset($_REQUEST["f"]) and isset($_SESSION['userAuth']))
{
	$f = $mFunc->chgSpecialCharInputText($_REQUEST["f"]);

	if($_SESSION['userAuth'] == 1)
	{
		switch($f)
		{
			case "addUser" :
				$pageRoute = "includes/view/addUser_view.php";
				$pageTitle = PAGE_ADD_USER_TITLE;
				$pageSubTitle = PAGE_ADD_USER_SUB_TITLE;
				$pageName = "Add User";
				break;
			case "manageUser" :
				$pageRoute = "includes/view/manageUser_view.php";
				$pageTitle = PAGE_MANAGE_USER_TITLE;
				$pageSubTitle = PAGE_MANAGE_USER_SUB_TITLE;
				$pageName = "Manage User";
				break;
			case "showUser" :
				$pageRoute = "includes/view/showUser_view.php";
				$pageTitle = PAGE_SHOW_USER_TITLE;
				$pageSubTitle = PAGE_SHOW_USER_SUB_TITLE;
				$pageName = "Show User";
				break;
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "topupSystem" :
				$pageRoute = "includes/view/topupSystem_view.php";
				$pageTitle = PAGE_TOPUP_SYSTEM_TITLE;
				$pageSubTitle = PAGE_TOPUP_SYSTEM_SUB_TITLE;
				$pageName = "Top Up System";
				break;
			case "topupHistory" :
				$pageRoute = "includes/view/topupHistory_view.php";
				$pageTitle = PAGE_TOPUP_HISTORY_TITLE;
				$pageSubTitle = PAGE_TOPUP_HISTORY_SUB_TITLE;
				$pageName = "Top Up History";
				break;
			case "topupHistorySearchByCustomer" :
				$pageRoute = "includes/view/topupHistorySearchByCustomer_view.php";
				$pageTitle = PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_TITLE;
				$pageSubTitle = PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_SUB_TITLE;
				$pageName = "Search Top Up History By Customer";
				break;
			case "addTopUpData" :
				$pageRoute = "includes/view/addTopUpData_view.php";
				$pageTitle = PAGE_ADD_TOPUP_DATA_TITLE;
				$pageSubTitle = PAGE_ADD_TOPUP_DATA_SUB_TITLE;
				$pageName = "Add Top Up Data";
				break;
			case "addExternalServer" :
				$pageRoute = "includes/view/addExternalServer_view.php";
				$pageTitle = PAGE_ADD_SERVER_TITLE;
				$pageSubTitle = PAGE_ADD_SERVER_SUB_TITLE;
				$pageName = "Add External Server";
				break;
			case "manageExternalServer" :
				$pageRoute = "includes/view/manageExternalServer_view.php";
				$pageTitle = PAGE_MANAGE_SERVER_TITLE;
				$pageSubTitle = PAGE_MANAGE_SERVER_SUB_TITLE;
				$pageName = "Manage External Server";
				break;
			case "withdrawHistory" :
				$pageRoute = "includes/view/withdrawHistory_view.php";
				$pageTitle = PAGE_WITHDRAW_HISTORY_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_HISTORY_SUB_TITLE;
				$pageName = "Withdraw History";
				break;
			case "topupOverAll" :
				$pageRoute = "includes/view/topupOverAll_view.php";
				$pageTitle = PAGE_TOPUP_OVERALL_TITLE;
				$pageSubTitle = PAGE_TOPUP_OVERALL_SUB_TITLE;
				$pageName = "Top Up Analysis Overall";
				break;
			case "topupByPaymentSystem" :
				$pageRoute = "includes/view/topupByPaymentSystem_view.php";
				$pageTitle = PAGE_TOPUP_BY_PAYMENT_SYSTEM_TITLE;
				$pageSubTitle = PAGE_TOPUP_BY_PAYMENT_SYSTEM_SUB_TITLE;
				$pageName = "Top Up Analysis By Payment System";
				break;
			case "topupByServer" :
				$pageRoute = "includes/view/topupByServer_view.php";
				$pageTitle = PAGE_TOPUP_BY_SERVER_TITLE;
				$pageSubTitle = PAGE_TOPUP_BY_SERVER_SUB_TITLE;
				$pageName = "Top Up Analysis By Server";
				break;
			case "withdrawOverAll" :
				$pageRoute = "includes/view/withdrawOverAll_view.php";
				$pageTitle = PAGE_WITHDRAW_OVERALL_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_OVERALL_SUB_TITLE;
				$pageName = "Withdraw Analysis Overall";
				break;
			case "withdrawByPaymentSystem" :
				$pageRoute = "includes/view/withdrawByPaymentSystem_view.php";
				$pageTitle = PAGE_WITHDRAW_BY_PAYMENT_SYSTEM_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_BY_PAYMENT_SYSTEM_SUB_TITLE;
				$pageName = "Withdraw Analysis By Payment System";
				break;
			case "withdrawByServer" :
				$pageRoute = "includes/view/withdrawByServer_view.php";
				$pageTitle = PAGE_WITHDRAW_BY_SERVER_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_BY_SERVER_SUB_TITLE;
				$pageName = "Withdraw Analysis By Server";
				break;
			case "customerOverAll" :
				$pageRoute = "includes/view/customerOverAll_view.php";
				$pageTitle = PAGE_CUSTOMER_OVERALL_TITLE;
				$pageSubTitle = PAGE_CUSTOMER_OVERALL_SUB_TITLE;
				$pageName = "Customer Analysis Overall";
				break;
			case "financial" :
				$pageRoute = "includes/view/financial_view.php";
				$pageTitle = PAGE_FINANCIAL_TITLE;
				$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
				$pageName = "Financial";
				break;
			case "addCategory" :
				$pageRoute = "includes/view/addcategory_view.php";
				$pageTitle = PAGE_ADD_CATEGORY_TITLE;
				$pageSubTitle = PAGE_ADD_CATEGORY_SUB_TITLE;
				$pageName = "Add Category";
				break;
			case "manageCategory" :
				$pageRoute = "includes/view/managecategory_view.php";
				$pageTitle = PAGE_MANAGE_CATEGORY_TITLE;
				$pageSubTitle = PAGE_MANAGE_CATEGORY_SUB_TITLE;
				$pageName = "Manage Category";
				break;
			case "addFileData" :
				$pageRoute = "includes/view/addfiledata_view.php";
				$pageTitle = PAGE_ADD_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_ADD_FILE_DATA_SUB_TITLE;
				$pageName = "Add File Data";
				break;
			case "manageFileData" :
				$pageRoute = "includes/view/manageFileData_view.php";
				$pageTitle = PAGE_MANAGE_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_MANAGE_FILE_DATA_SUB_TITLE;
				$pageName = "Manage File Data";
				break;
			case "addBranch" :
				$pageRoute = "includes/view/addBranch_view.php";
				$pageTitle = PAGE_ADD_BRANCH_TITLE;
				$pageSubTitle = PAGE_ADD_BRANCH_SUB_TITLE;
				$pageName = "Add Branch";
				break;
			case "manageBranch" :
				$pageRoute = "includes/view/manageBranch_view.php";
				$pageTitle = PAGE_MANAGE_BRANCH_TITLE;
				$pageSubTitle = PAGE_MANAGE_BRANCH_SUB_TITLE;
				$pageName = "Manage Branch";
				break;
			case "importBranchFromExcel" :
				$pageRoute = "includes/view/importBranchFromExcel_view.php";
				$pageTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_TITLE;
				$pageSubTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_SUB_TITLE;
				$pageName = "Import Franchise And Branch from excel";
				break;
			default :
				$pageRoute = "includes/view/dashBoard_view.php";
				$pageTitle = PAGE_DASHBOARD_TITLE;
				$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
				$pageName = "Dashboard";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)


	if($_SESSION['userAuth'] == 2)
	{
		switch($f)
		{
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "topupHistory" :
				$pageRoute = "includes/view/topupHistory_view.php";
				$pageTitle = PAGE_TOPUP_HISTORY_TITLE;
				$pageSubTitle = PAGE_TOPUP_HISTORY_SUB_TITLE;
				$pageName = "Top Up History";
				break;
			case "topupHistorySearchByCustomer" :
				$pageRoute = "includes/view/topupHistorySearchByCustomer_view.php";
				$pageTitle = PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_TITLE;
				$pageSubTitle = PAGE_TOPUP_HISTORY_SEARCH_BY_CUSTOMER_SUB_TITLE;
				$pageName = "Search Top Up History By Customer";
				break;
			case "topupOverAll" :
				$pageRoute = "includes/view/topupOverAll_view.php";
				$pageTitle = PAGE_TOPUP_OVERALL_TITLE;
				$pageSubTitle = PAGE_TOPUP_OVERALL_SUB_TITLE;
				$pageName = "Top Up Analysis Overall";
				break;
			case "topupByPaymentSystem" :
				$pageRoute = "includes/view/topupByPaymentSystem_view.php";
				$pageTitle = PAGE_TOPUP_BY_PAYMENT_SYSTEM_TITLE;
				$pageSubTitle = PAGE_TOPUP_BY_PAYMENT_SYSTEM_SUB_TITLE;
				$pageName = "Top Up Analysis By Payment System";
				break;
			case "topupByServer" :
				$pageRoute = "includes/view/topupByServer_view.php";
				$pageTitle = PAGE_TOPUP_BY_SERVER_TITLE;
				$pageSubTitle = PAGE_TOPUP_BY_SERVER_SUB_TITLE;
				$pageName = "Top Up Analysis By Server";
				break;
			case "withdrawOverAll" :
				$pageRoute = "includes/view/withdrawOverAll_view.php";
				$pageTitle = PAGE_WITHDRAW_OVERALL_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_OVERALL_SUB_TITLE;
				$pageName = "Withdraw Analysis Overall";
				break;
			case "withdrawByPaymentSystem" :
				$pageRoute = "includes/view/withdrawByPaymentSystem_view.php";
				$pageTitle = PAGE_WITHDRAW_BY_PAYMENT_SYSTEM_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_BY_PAYMENT_SYSTEM_SUB_TITLE;
				$pageName = "Withdraw Analysis By Payment System";
				break;
			case "withdrawByServer" :
				$pageRoute = "includes/view/withdrawByServer_view.php";
				$pageTitle = PAGE_WITHDRAW_BY_SERVER_TITLE;
				$pageSubTitle = PAGE_WITHDRAW_BY_SERVER_SUB_TITLE;
				$pageName = "Withdraw Analysis By Server";
				break;
			case "customerOverAll" :
				$pageRoute = "includes/view/customerOverAll_view.php";
				$pageTitle = PAGE_CUSTOMER_OVERALL_TITLE;
				$pageSubTitle = PAGE_CUSTOMER_OVERALL_SUB_TITLE;
				$pageName = "Customer Analysis Overall";
				break;
			default :
				$pageRoute = "includes/view/dashBoard_view.php";
				$pageTitle = PAGE_DASHBOARD_TITLE;
				$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
				$pageName = "Dashboard";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)


	if($_SESSION['userAuth'] == 3)
	{
		switch($f)
		{
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "topupSystem" :
				$pageRoute = "includes/view/topupSystem_view.php";
				$pageTitle = PAGE_TOPUP_SYSTEM_TITLE;
				$pageSubTitle = PAGE_TOPUP_SYSTEM_SUB_TITLE;
				$pageName = "Top Up System";
				break;
			case "topupHistory" :
				$pageRoute = "includes/view/topupHistory_view.php";
				$pageTitle = PAGE_TOPUP_HISTORY_TITLE;
				$pageSubTitle = PAGE_TOPUP_HISTORY_SUB_TITLE;
				$pageName = "Top Up History";
				break;
			default :
				$pageRoute = "includes/view/topupSystem_view.php";
				$pageTitle = PAGE_TOPUP_SYSTEM_TITLE;
				$pageSubTitle = PAGE_TOPUP_SYSTEM_SUB_TITLE;
				$pageName = "Top Up System";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)
}
else
{
	if($_SESSION['userAuth'] == 3)
	{
		$pageRoute = "includes/view/topupSystem_view.php";
		$pageTitle = PAGE_TOPUP_SYSTEM_TITLE;
		$pageSubTitle = PAGE_TOPUP_SYSTEM_SUB_TITLE;
		$pageName = "Top Up System";
	}
	else
	{
		$pageRoute = "includes/view/dashBoard_view.php";
		$pageTitle = PAGE_DASHBOARD_TITLE;
		$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
		$pageName = "Dashboard";
	}  //-----  if($_SESSION['userAuth'] == 3)
}  //-----  if(isset($_REQUEST["f"]))
?>