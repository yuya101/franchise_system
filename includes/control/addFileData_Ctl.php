<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['title'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$typeid = $mFunc->chgSpecialCharInputNumber($_REQUEST['typeid']);
	$catid = $mFunc->chgSpecialCharInputNumber($_REQUEST['catid']);
	$docmonth = $mFunc->chgSpecialCharInputText($_REQUEST['docmonth']);
	$title = $mFunc->chgSpecialCharInputText($_REQUEST['title']);
	$description = $mFunc->chgSpecialCharInputText($_REQUEST['description']);

	$docdate = "01";
	list($docmonth, $docyear) = split('[/.-]', $docmonth);

	$brandid = 0;  //--- ไม่ได้ใช้ในการ Add แบบทีละไฟล์ เนื่องจากถูกำหนดโดยร้านสาขาอยู่แล้ว

	$strFileName = $mFunc->uploadOneDocumentFile($uploadAfterSplitFilePath, "docfile", "franchise");


	$sql = "insert into db_document values(NULL";
	$sql = $sql.", ".$typeid."";
	$sql = $sql.", ".$catid."";
	$sql = $sql.", ".$brandid."";
	$sql = $sql.", '-'";
	$sql = $sql.", '-'";
	$sql = $sql.", '".$title."'";
	$sql = $sql.", '".$description."'";
	$sql = $sql.", '".$strFileName."'";
	$sql = $sql.", '".$uploadAfterSplitFilePath."'";
	$sql = $sql.", '".$docdate."'";
	$sql = $sql.", '".$docmonth."'";
	$sql = $sql.", '".$docyear."'";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.", '".$dateNow."'";
	$sql = $sql.", '".$timeNow."'";
	$sql = $sql.", ".$_SESSION['mLoginID']."";
	$sql = $sql.")";
	$mQuery->querySQL($sql);


	$sql = "select did from db_document order by did desc limit 1";
	$did = $mQuery->getResultOneRecord($sql, "did");


	$sqlDocBrand = "select * from db_brand order by brand_name";
	$numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

	if($numDocBrand > 0){
		$resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
		$i = 0;

		foreach ($resultDocBrand as $rb) {
			$docBrandID[$i] = $rb['bid'];

			if((int)$typeid == 1){
				if(isset($_REQUEST['useallbrand'.$docBrandID[$i]])){
					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$typeid."";
					$sqlDocBranch = $sqlDocBranch.", ".$catid."";
					$sqlDocBranch = $sqlDocBranch.", ".$docBrandID[$i]."";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.", '-'";
					$sqlDocBranch = $sqlDocBranch.", 1";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);
				}else{
					$j = 0;
					
					$sqlDocBranch = "select * from db_user_auth where brand_id=".$docBrandID[$i]." group by shop_code order by site_customer";
					$numDocBranch[$i] = $mQuery->checkNumRows($sqlDocBranch);

					if($numDocBranch[$i] > 0){
						$resultDocBranch = $mQuery->getResultAll($sqlDocBranch);

						foreach ($resultDocBranch as $rdb) {
							$docBranchID[$i][$j] = $rdb['uaid'];
							$docBranchSiteName[$i][$j] = $rdb['site_customer'];

							if(isset($_REQUEST[$docBranchID[$i][$j]])){
								$sqlDocBranch = "insert into db_document_authorize values(NULL";
								$sqlDocBranch = $sqlDocBranch.", ".$did."";
								$sqlDocBranch = $sqlDocBranch.", ".$typeid."";
								$sqlDocBranch = $sqlDocBranch.", ".$catid."";
								$sqlDocBranch = $sqlDocBranch.", ".$docBrandID[$i]."";
								$sqlDocBranch = $sqlDocBranch.", ".$docBranchID[$i][$j]."";
								$sqlDocBranch = $sqlDocBranch.", '".$docBranchSiteName[$i][$j]."'";
								$sqlDocBranch = $sqlDocBranch.", 0";
								$sqlDocBranch = $sqlDocBranch.")";
								$mQuery->querySQL($sqlDocBranch);
							}  //--------  if(isset($_REQUEST[$docBranchID[$i][$j]]))
							
							$j++;
						}  //-----  foreach ($resultDocBranch as $rdb)

						unset($resultDocBranch, $rdb);
					}  //------  if($numDocBranch[$i] > 0)
				}  //-----  if(isset($_REQUEST['useallbrand'.$docBrandID[$i]]))
			}else{
				$sqlDocBranch = "insert into db_document_authorize values(NULL";
				$sqlDocBranch = $sqlDocBranch.", ".$did."";
				$sqlDocBranch = $sqlDocBranch.", ".$typeid."";
				$sqlDocBranch = $sqlDocBranch.", ".$catid."";
				$sqlDocBranch = $sqlDocBranch.", ".$docBrandID[$i]."";
				$sqlDocBranch = $sqlDocBranch.", 0";
				$sqlDocBranch = $sqlDocBranch.", '-'";
				$sqlDocBranch = $sqlDocBranch.", 1";
				$sqlDocBranch = $sqlDocBranch.")";
				$mQuery->querySQL($sqlDocBranch);
			}  //------  if((int)$typeid == 1)

			$i++;
		}  //-----  foreach ($resultDocType as $rd)

		unset($resultDocBrand, $rb);
	}  //----  if($numDocType > 0)


	header("location:../../index.php?f=addFileData&confirmOK=".base64_encode($title));
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>