<?php
$mQuery = new MainQuery();
$mFunc = new MainFunction();
$dFunc = new DateFunction();

$dateNow = $dFunc->getDateChris();
$timeNow = $dFunc->getTimeNow();

if(isset($_REQUEST["f"]) and isset($_SESSION['userAuth']))
{
	$f = $mFunc->chgSpecialCharInputText($_REQUEST["f"]);

	if($_SESSION['userAuth'] == 1)
	{
		switch($f)
		{
			case "addUser" :
				$pageRoute = "includes/view/addUser_view.php";
				$pageTitle = PAGE_ADD_USER_TITLE;
				$pageSubTitle = PAGE_ADD_USER_SUB_TITLE;
				$pageName = "Add User";
				break;
			case "manageUser" :
				$pageRoute = "includes/view/manageUser_view.php";
				$pageTitle = PAGE_MANAGE_USER_TITLE;
				$pageSubTitle = PAGE_MANAGE_USER_SUB_TITLE;
				$pageName = "Manage User";
				break;
			case "showUser" :
				$pageRoute = "includes/view/showUser_view.php";
				$pageTitle = PAGE_SHOW_USER_TITLE;
				$pageSubTitle = PAGE_SHOW_USER_SUB_TITLE;
				$pageName = "Show User";
				break;
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "addCategory" :
				$pageRoute = "includes/view/addcategory_view.php";
				$pageTitle = PAGE_ADD_CATEGORY_TITLE;
				$pageSubTitle = PAGE_ADD_CATEGORY_SUB_TITLE;
				$pageName = "Add Category";
				break;
			case "manageCategory" :
				$pageRoute = "includes/view/managecategory_view.php";
				$pageTitle = PAGE_MANAGE_CATEGORY_TITLE;
				$pageSubTitle = PAGE_MANAGE_CATEGORY_SUB_TITLE;
				$pageName = "Manage Category";
				break;
			case "addFileData" :
				$pageRoute = "includes/view/addfiledata_view.php";
				$pageTitle = PAGE_ADD_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_ADD_FILE_DATA_SUB_TITLE;
				$pageName = "Add File Data";
				break;
			case "manageFileData" :
				$pageRoute = "includes/view/manageFileData_view.php";
				$pageTitle = PAGE_MANAGE_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_MANAGE_FILE_DATA_SUB_TITLE;
				$pageName = "Manage File Data";
				break;
			case "importFileData" :
				$pageRoute = "includes/view/importFileData_view.php";
				$pageTitle = PAGE_IMPORT_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_IMPORT_FILE_DATA_SUB_TITLE;
				$pageName = "Import Document File Data";
				break;
			case "addBranch" :
				$pageRoute = "includes/view/addBranch_view.php";
				$pageTitle = PAGE_ADD_BRANCH_TITLE;
				$pageSubTitle = PAGE_ADD_BRANCH_SUB_TITLE;
				$pageName = "Add Branch";
				break;
			case "manageBranch" :
				$pageRoute = "includes/view/manageBranch_view.php";
				$pageTitle = PAGE_MANAGE_BRANCH_TITLE;
				$pageSubTitle = PAGE_MANAGE_BRANCH_SUB_TITLE;
				$pageName = "Manage Branch";
				break;
			case "importBranchFromExcel" :
				$pageRoute = "includes/view/importBranchFromExcel_view.php";
				$pageTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_TITLE;
				$pageSubTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_SUB_TITLE;
				$pageName = "Import Franchise And Branch from excel";
				break;
			case "financial" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FINANCIAL_TITLE;
				$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
				$pageName = "Financial";
				break;
			case "franchiseManual" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FRANCHISE_MANUAL_TITLE;
				$pageSubTitle = PAGE_FRANCHISE_MANUAL_SUB_TITLE;
				$pageName = "Franchise Manual";
				break;
			case "operateAndTraining" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OPERATION_TITLE;
				$pageSubTitle = PAGE_OPERATION_SUB_TITLE;
				$pageName = "Operations and training";
				break;
			case "marketing" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_MARKETING_TITLE;
				$pageSubTitle = PAGE_MARKETING_SUB_TITLE;
				$pageName = "Marketing";
				break;
			case "consultant" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_CONSULTANT_TITLE;
				$pageSubTitle = PAGE_CONSULTANT_SUB_TITLE;
				$pageName = "Franchise (Business Consultant)";
				break;
			case "lastestNews" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LASTEST_NEWS_TITLE;
				$pageSubTitle = PAGE_LASTEST_NEWS_SUB_TITLE;
				$pageName = "Latest News";
				break;
			case "letter" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LETTER_TITLE;
				$pageSubTitle = PAGE_LETTER_SUB_TITLE;
				$pageName = "Letter";
				break;
			case "policy" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_POLICY_TITLE;
				$pageSubTitle = PAGE_POLICY_SUB_TITLE;
				$pageName = "Policy";
				break;
			case "others" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OTHERS_TITLE;
				$pageSubTitle = PAGE_OTHERS_SUB_TITLE;
				$pageName = "Others";
				break;
			case "inquiryData" :
				$pageRoute = "includes/view/centerInquiryData_view.php";
				$pageTitle = PAGE_INQUIRY_DATA_TITLE;
				$pageSubTitle = PAGE_INQUIRY_DATA_SUB_TITLE;
				$pageName = "Inquiry Data";
				break;
			case "importTripExpensesFileData" :
				$pageRoute = "includes/view/importTripExpensesFromExcel_view.php";
				$pageTitle = PAGE_IMPORT_TRIP_EXPENSE_FROM_EXCEL_TITLE;
				$pageSubTitle = PAGE_IMPORT_TRIP_EXPENSE_FROM_EXCEL_SUB_TITLE;
				$pageName = "Import Inquiry Data from Text File";
				break;
			case "searchFinancialForm" :
				$pageRoute = "includes/view/centerSearchFinancialForm_view.php";
				$pageTitle = PAGE_SEARCH_FINANCIAL_FORM_TITLE;
				$pageSubTitle = PAGE_SEARCH_FINANCIAL_FORM_SUB_TITLE;
				$pageName = "Search Financial Document";
				break;
			case "searchInquiryForm" :
				$pageRoute = "includes/view/centerSearchInquiryForm_view.php";
				$pageTitle = PAGE_SEARCH_INQUIRY_FORM_TITLE;
				$pageSubTitle = PAGE_SEARCH_INQUIRY_FORM_SUB_TITLE;
				$pageName = "Search Inquiry Document";
				break;
			case "sendMailToFranchiseAll" :
				$pageRoute = "includes/view/sendMailToFranchiseAll_view.php";
				$pageTitle = PAGE_SEND_MAIL_FRANCHISE_ALL_TITLE;
				$pageSubTitle = PAGE_SEND_MAIL_FRANCHISE_ALL_SUB_TITLE;
				$pageName = "Send Mail To All Franchise";
				break;
			case "sendMailToFranchiseByOne" :
				$pageRoute = "includes/view/sendMailToFranchiseByOne_view.php";
				$pageTitle = PAGE_SEND_MAIL_FRANCHISE_BY_ONE_TITLE;
				$pageSubTitle = PAGE_SEND_MAIL_FRANCHISE_BY_ONE_SUB_TITLE;
				$pageName = "Send Mail By One";
				break;
			default :
				$pageRoute = "includes/view/dashBoard_view.php";
				$pageTitle = PAGE_DASHBOARD_TITLE;
				$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
				$pageName = "Dashboard";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)




	if($_SESSION['userAuth'] == 4)
	{
		switch($f)
		{
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "financial" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FINANCIAL_TITLE;
				$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
				$pageName = "Financial";
				break;
			case "franchiseManual" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FRANCHISE_MANUAL_TITLE;
				$pageSubTitle = PAGE_FRANCHISE_MANUAL_SUB_TITLE;
				$pageName = "Franchise Manual";
				break;
			case "operateAndTraining" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OPERATION_TITLE;
				$pageSubTitle = PAGE_OPERATION_SUB_TITLE;
				$pageName = "Operations and training";
				break;
			case "marketing" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_MARKETING_TITLE;
				$pageSubTitle = PAGE_MARKETING_SUB_TITLE;
				$pageName = "Marketing";
				break;
			case "consultant" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_CONSULTANT_TITLE;
				$pageSubTitle = PAGE_CONSULTANT_SUB_TITLE;
				$pageName = "Franchise (Business Consultant)";
				break;
			case "lastestNews" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LASTEST_NEWS_TITLE;
				$pageSubTitle = PAGE_LASTEST_NEWS_SUB_TITLE;
				$pageName = "Latest News";
				break;
			case "letter" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LETTER_TITLE;
				$pageSubTitle = PAGE_LETTER_SUB_TITLE;
				$pageName = "Letter";
				break;
			case "policy" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_POLICY_TITLE;
				$pageSubTitle = PAGE_POLICY_SUB_TITLE;
				$pageName = "Policy";
				break;
			case "others" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OTHERS_TITLE;
				$pageSubTitle = PAGE_OTHERS_SUB_TITLE;
				$pageName = "Others";
				break;
			case "inquiryData" :
				$pageRoute = "includes/view/centerInquiryData_view.php";
				$pageTitle = PAGE_INQUIRY_DATA_TITLE;
				$pageSubTitle = PAGE_INQUIRY_DATA_SUB_TITLE;
				$pageName = "Inquiry Data";
				break;
			default :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FINANCIAL_TITLE;
				$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
				$pageName = "Financial";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)


	if($_SESSION['userAuth'] == 5)
	{
		switch($f)
		{
			case "addUser" :
				$pageRoute = "includes/view/addUser_view.php";
				$pageTitle = PAGE_ADD_USER_TITLE;
				$pageSubTitle = PAGE_ADD_USER_SUB_TITLE;
				$pageName = "Add User";
				break;
			case "manageUser" :
				$pageRoute = "includes/view/manageUser_view.php";
				$pageTitle = PAGE_MANAGE_USER_TITLE;
				$pageSubTitle = PAGE_MANAGE_USER_SUB_TITLE;
				$pageName = "Manage User";
				break;
			case "showUser" :
				$pageRoute = "includes/view/showUser_view.php";
				$pageTitle = PAGE_SHOW_USER_TITLE;
				$pageSubTitle = PAGE_SHOW_USER_SUB_TITLE;
				$pageName = "Show User";
				break;
			case "chgPass" :
				$pageRoute = "includes/view/chgPassword_view.php";
				$pageTitle = PAGE_CHANGE_USER_PASSWORD_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PASSWORD_SUB_TITLE;
				$pageName = "Change Password";
				break;
			case "chgUserProfile" :
				$pageRoute = "includes/view/chgUserProfile_view.php";
				$pageTitle = PAGE_CHANGE_USER_PROFILE_TITLE;
				$pageSubTitle = PAGE_CHANGE_USER_PROFILE_SUB_TITLE;
				$pageName = "Change Profile";
				break;
			case "addCategory" :
				$pageRoute = "includes/view/addcategory_view.php";
				$pageTitle = PAGE_ADD_CATEGORY_TITLE;
				$pageSubTitle = PAGE_ADD_CATEGORY_SUB_TITLE;
				$pageName = "Add Category";
				break;
			case "manageCategory" :
				$pageRoute = "includes/view/managecategory_view.php";
				$pageTitle = PAGE_MANAGE_CATEGORY_TITLE;
				$pageSubTitle = PAGE_MANAGE_CATEGORY_SUB_TITLE;
				$pageName = "Manage Category";
				break;
			case "addFileData" :
				$pageRoute = "includes/view/addfiledata_view.php";
				$pageTitle = PAGE_ADD_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_ADD_FILE_DATA_SUB_TITLE;
				$pageName = "Add File Data";
				break;
			case "manageFileData" :
				$pageRoute = "includes/view/manageFileData_view.php";
				$pageTitle = PAGE_MANAGE_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_MANAGE_FILE_DATA_SUB_TITLE;
				$pageName = "Manage File Data";
				break;
			case "importFileData" :
				$pageRoute = "includes/view/importFileData_view.php";
				$pageTitle = PAGE_IMPORT_FILE_DATA_TITLE;
				$pageSubTitle = PAGE_IMPORT_FILE_DATA_SUB_TITLE;
				$pageName = "Import Document File Data";
				break;
			case "addBranch" :
				$pageRoute = "includes/view/addBranch_view.php";
				$pageTitle = PAGE_ADD_BRANCH_TITLE;
				$pageSubTitle = PAGE_ADD_BRANCH_SUB_TITLE;
				$pageName = "Add Branch";
				break;
			case "manageBranch" :
				$pageRoute = "includes/view/manageBranch_view.php";
				$pageTitle = PAGE_MANAGE_BRANCH_TITLE;
				$pageSubTitle = PAGE_MANAGE_BRANCH_SUB_TITLE;
				$pageName = "Manage Branch";
				break;
			case "importBranchFromExcel" :
				$pageRoute = "includes/view/importBranchFromExcel_view.php";
				$pageTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_TITLE;
				$pageSubTitle = PAGE_IMPORT_BRANCH_FROM_EXCEL_SUB_TITLE;
				$pageName = "Import Franchise And Branch from excel";
				break;
			case "financial" :
				$pageRoute = "includes/view/centerDocumentFinanceSupportType_view.php";
				$pageTitle = PAGE_FINANCIAL_TITLE;
				$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
				$pageName = "Financial";
				break;
			case "franchiseManual" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_FRANCHISE_MANUAL_TITLE;
				$pageSubTitle = PAGE_FRANCHISE_MANUAL_SUB_TITLE;
				$pageName = "Franchise Manual";
				break;
			case "operateAndTraining" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OPERATION_TITLE;
				$pageSubTitle = PAGE_OPERATION_SUB_TITLE;
				$pageName = "Operations and training";
				break;
			case "marketing" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_MARKETING_TITLE;
				$pageSubTitle = PAGE_MARKETING_SUB_TITLE;
				$pageName = "Marketing";
				break;
			case "consultant" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_CONSULTANT_TITLE;
				$pageSubTitle = PAGE_CONSULTANT_SUB_TITLE;
				$pageName = "Franchise (Business Consultant)";
				break;
			case "lastestNews" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LASTEST_NEWS_TITLE;
				$pageSubTitle = PAGE_LASTEST_NEWS_SUB_TITLE;
				$pageName = "Latest News";
				break;
			case "letter" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_LETTER_TITLE;
				$pageSubTitle = PAGE_LETTER_SUB_TITLE;
				$pageName = "Letter";
				break;
			case "policy" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_POLICY_TITLE;
				$pageSubTitle = PAGE_POLICY_SUB_TITLE;
				$pageName = "Policy";
				break;
			case "others" :
				$pageRoute = "includes/view/centerDocumentType_view.php";
				$pageTitle = PAGE_OTHERS_TITLE;
				$pageSubTitle = PAGE_OTHERS_SUB_TITLE;
				$pageName = "Others";
				break;
			case "inquiryData" :
				$pageRoute = "includes/view/centerInquiryData_view.php";
				$pageTitle = PAGE_INQUIRY_DATA_TITLE;
				$pageSubTitle = PAGE_INQUIRY_DATA_SUB_TITLE;
				$pageName = "Inquiry Data";
				break;
			case "searchFinancialForm" :
				$pageRoute = "includes/view/centerSearchFinancialForm_view.php";
				$pageTitle = PAGE_SEARCH_FINANCIAL_FORM_TITLE;
				$pageSubTitle = PAGE_SEARCH_FINANCIAL_FORM_SUB_TITLE;
				$pageName = "Search Financial Document";
				break;
			case "searchInquiryForm" :
				$pageRoute = "includes/view/centerSearchInquiryForm_view.php";
				$pageTitle = PAGE_SEARCH_INQUIRY_FORM_TITLE;
				$pageSubTitle = PAGE_SEARCH_INQUIRY_FORM_SUB_TITLE;
				$pageName = "Search Inquiry Document";
				break;
			default :
				$pageRoute = "includes/view/dashBoard_view.php";
				$pageTitle = PAGE_DASHBOARD_TITLE;
				$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
				$pageName = "Dashboard";
				break;
		}  //-----  switch($f)
	}  //--------  if($_SESSION['userAuth'] == 1)
}
else
{
	if($_SESSION['userAuth'] == 4)
	{
		$f = "financial";
		$pageRoute = "includes/view/centerDocumentType_view.php";
		$pageTitle = PAGE_FINANCIAL_TITLE;
		$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
		$pageName = "Financial";
	}
	elseif($_SESSION['userAuth'] == 5)
	{
		$f = "financial";
		$pageRoute = "includes/view/centerDocumentFinanceSupportType_view.php";
		$pageTitle = PAGE_FINANCIAL_TITLE;
		$pageSubTitle = PAGE_FINANCIAL_SUB_TITLE;
		$pageName = "Financial";
	}
	else
	{
		$pageRoute = "includes/view/dashBoard_view.php";
		$pageTitle = PAGE_DASHBOARD_TITLE;
		$pageSubTitle = PAGE_DASHBOARD_SUB_TITLE;
		$pageName = "Dashboard";
	}  //-----  if($_SESSION['userAuth'] == 3)
}  //-----  if(isset($_REQUEST["f"]))
?>