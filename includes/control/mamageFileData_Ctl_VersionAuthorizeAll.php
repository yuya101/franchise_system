<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['title'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$title = $mFunc->chgSpecialCharInputText($_REQUEST['title']);
	$docID = $mFunc->chgSpecialCharInputNumber($_REQUEST['docID']);

	if(isset($_REQUEST['manage'])){
		$typeid = $mFunc->chgSpecialCharInputNumber($_REQUEST['typeid']);
		$catid = $mFunc->chgSpecialCharInputNumber($_REQUEST['catid']);
		$docmonth = $mFunc->chgSpecialCharInputText($_REQUEST['docmonth']);
		$description = $mFunc->chgSpecialCharInputText($_REQUEST['description']);

		$docdate = "01";
		list($docmonth, $docyear) = split('[/.-]', $docmonth);

		$brandid = 0;  //--- ไม่ได้ใช้ในการ Add แบบทีละไฟล์ เนื่องจากถูกำหนดโดยร้านสาขาอยู่แล้ว

		if($_FILES["docfile"]["name"][0] != ""){
			$strFileName = $mFunc->uploadOneDocumentFile($uploadAfterSplitFilePath, "docfile", "franchise");

			$sql = "update db_document set file_name='".$strFileName."'";
			$sql = $sql.", file_path='".$uploadAfterSplitFilePath."'";
			$sql = $sql." where did=".$docID;
			$mQuery->querySQL($sql);
		}else{
			$strFileName = "-";
		}  //----  if($_FILES["textFile"]["name"][0] != "")


		$sql = "update db_document set type_id=".$typeid;
		$sql = $sql.", cat_id=".$catid."";
		$sql = $sql.", title='".$title."'";
		$sql = $sql.", description='".$description."'";
		$sql = $sql.", month='".$docmonth."'";
		$sql = $sql.", year='".$docyear."'";
		$sql = $sql.", moddate='".$dateNow."'";
		$sql = $sql.", modtime='".$timeNow."'";
		$sql = $sql.", modid=".$_SESSION['mLoginID']."";
		$sql = $sql." where did=".$docID;
		$mQuery->querySQL($sql);



		$did = $docID;

		$sql = "delete from db_document_authorize where did=".$did;
		$mQuery->querySQL($sql);

		$sqlDocBrand = "select * from db_brand order by brand_name";
		$numDocBrand = $mQuery->checkNumRows($sqlDocBrand);

		if($numDocBrand > 0){
			$resultDocBrand = $mQuery->getResultAll($sqlDocBrand);
			$i = 0;

			foreach ($resultDocBrand as $rb) {
				$docBrandID[$i] = $rb['bid'];

				if(isset($_REQUEST['useallbrand'.$docBrandID[$i]])){
					$sqlDocBranch = "insert into db_document_authorize values(NULL";
					$sqlDocBranch = $sqlDocBranch.", ".$did."";
					$sqlDocBranch = $sqlDocBranch.", ".$typeid."";
					$sqlDocBranch = $sqlDocBranch.", ".$catid."";
					$sqlDocBranch = $sqlDocBranch.", ".$docBrandID[$i]."";
					$sqlDocBranch = $sqlDocBranch.", 0";
					$sqlDocBranch = $sqlDocBranch.", '-'";
					$sqlDocBranch = $sqlDocBranch.", 1";
					$sqlDocBranch = $sqlDocBranch.")";
					$mQuery->querySQL($sqlDocBranch);
				}else{
					$j = 0;
					
					$sqlDocBranch = "select * from db_user_auth where brand_id=".$docBrandID[$i]." group by shop_code order by site_customer";
					$numDocBranch[$i] = $mQuery->checkNumRows($sqlDocBranch);

					if($numDocBranch[$i] > 0){
						$resultDocBranch = $mQuery->getResultAll($sqlDocBranch);

						foreach ($resultDocBranch as $rdb) {
							$docBranchID[$i][$j] = $rdb['uaid'];
							$docBranchSiteName[$i][$j] = $rdb['site_customer'];

							if(isset($_REQUEST[$docBranchID[$i][$j]])){
								$sqlDocBranch = "insert into db_document_authorize values(NULL";
								$sqlDocBranch = $sqlDocBranch.", ".$did."";
								$sqlDocBranch = $sqlDocBranch.", ".$typeid."";
								$sqlDocBranch = $sqlDocBranch.", ".$catid."";
								$sqlDocBranch = $sqlDocBranch.", ".$docBrandID[$i]."";
								$sqlDocBranch = $sqlDocBranch.", ".$docBranchID[$i][$j]."";
								$sqlDocBranch = $sqlDocBranch.", '".$docBranchSiteName[$i][$j]."'";
								$sqlDocBranch = $sqlDocBranch.", 0";
								$sqlDocBranch = $sqlDocBranch.")";
								$mQuery->querySQL($sqlDocBranch);
							}  //--------  if(isset($_REQUEST[$docBranchID[$i][$j]]))
							
							$j++;
						}  //-----  foreach ($resultDocBranch as $rdb)

						unset($resultDocBranch, $rdb);
					}  //------  if($numDocBranch[$i] > 0)
				}  //-----  if(isset($_REQUEST['useallbrand'.$docBrandID[$i]]))

				$i++;
			}  //-----  foreach ($resultDocType as $rd)

			unset($resultDocBrand, $rb);
		}  //----  if($numDocType > 0)

		header("location:../../index.php?f=manageFileData&confirmOK=".base64_encode($title));
	}  //-----  if(isset($_REQUEST['manage']))


	if(isset($_REQUEST['delete'])){
		$sql = "select file_name, file_path from db_document where did=".$docID;
		$num = $mQuery->checkNumRows($sql);

		if($num > 0){
			unlink(($mQuery->getResultOneRecord($sql, "file_path")).($mQuery->getResultOneRecord($sql, "file_name")));
		}  //-----  if($num > 0)

		$sql = "delete from db_document where did=".$docID;
		$mQuery->querySQL($sql);

		$sql = "delete from db_document_authorize where did=".$docID;
		$mQuery->querySQL($sql);


		header("location:../../index.php?f=manageFileData&deleteOK=".base64_encode($title));
	}  //-----  if(isset($_REQUEST['delete']))
	
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>