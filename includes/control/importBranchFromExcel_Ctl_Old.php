<?php
ob_start();
session_start();
set_time_limit(1800);  // ประมาณ 30 นาที

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(isset($_SESSION['mLoginID']))
{
	/** PHPExcel */
	require_once($importClassPath."PHPExcel.php");
	/** PHPExcel_IOFactory - Reader */
	include($importClassPath."PHPExcel/IOFactory.php");
	ini_set("precision", "15");


	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$strFileName = "-";
	$userauth = 4;

	if($_FILES["textFile"]["name"][0] != ""){
		$strFileName = $_FILES["textFile"]["name"][0];
		move_uploaded_file($_FILES["textFile"]["tmp_name"][0], $textFilePath.$_FILES["textFile"]["name"][0]);	
	}  //------  if($_FILES["textFile"]["name"][0] != "")

	if($strFileName != ""){
		$j = 0;

		$inputFileName = $textFilePath.$strFileName; 
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);


		// for No header----------------------------------------
			
		$objWorksheet = $objPHPExcel->setActiveSheetIndex($j);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
			
		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				$namedDataArray[$r] = $dataRow[$row];
			}
		}  //----------  for ($row = 2 $row <= $highestRow; ++$row) 
			
		// End for No header----------------------------------------


		foreach ($namedDataArray as $result){
			$tmpBrand = strtoupper($mFunc->changeToUTFBlankCheck($result['A']));
			$tmpShopCode = $mFunc->changeToUTFBlankCheck($result['B']);
			$tmpBuCode = $mFunc->changeToUTFBlankCheck($result['C']);
			$tmpSiteCustomer = $mFunc->changeToUTFBlankCheck($result['D']);
			$tmpCustomerNo = $mFunc->changeToUTFBlankCheck($result['E']);
			$tmpCustomerName = $mFunc->changeToUTFBlankCheck($result['F']);
			$tmpStoreIDName = $mFunc->changeToUTFBlankCheck($result['G']);
			$tmpEmail = strtoupper($mFunc->changeToUTFBlankCheck($result['H']));

			$tmpBuCodeLen = strlen($tmpBuCode);
			$tmpStoreName = substr($tmpSiteCustomer, $tmpBuCodeLen+1);


			$sql = "select bid from db_brand where brand_short_name='".$tmpBrand."'";
			$num = $mQuery->checkNumRows($sql);

			if($num > 0){
				$brandID = $mQuery->getResultOneRecord($sql, "bid");
			}else{
				$brandID = 1;
			}  //------  if($num > 0)


			$sql = "select aid from db_user where email='".$tmpEmail."'";
			$num = $mQuery->checkNumRows($sql);

			if($num > 0){
				$userID = $mQuery->getResultOneRecord($sql, "aid");
			}else{
				
				$tmpPass = base64_encode($tmpShopCode);

				$sql = "insert into db_user values(NULL, '".$tmpEmail."', '".$tmpPass."', '".$tmpCustomerNo."', '".$tmpCustomerName."', '".$tmpCustomerName."', '-', '-', ".$userauth.", '".$tmpEmail."', '".$dateNow."', '".$timeNow."', '".$dateNow."', '".$timeNow."', '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['REMOTE_ADDR']."', ".$_SESSION['mLoginID'].", ".$_SESSION['mLoginID'].")";
				$mQuery->querySQL($sql);

				$sql = "select aid from db_user where email='".$tmpEmail."'";
				$userID = $mQuery->getResultOneRecord($sql, "aid");
			}  //-----  if($num > 0)



			$sql = "select uaid from db_user_auth where shop_code='".$tmpShopCode."'";
			$num = $mQuery->checkNumRows($sql);

			if($num == 0){
				$sql = "insert into db_user_auth values(NULL, ".$userID.", ".$brandID.", '".$tmpShopCode."', '".$tmpBuCode."', '".$tmpCustomerNo."', '".$tmpCustomerName."', '".$tmpStoreName."', '".$tmpSiteCustomer."', '".$tmpStoreIDName."', '".$tmpEmail."', '".$dateNow."', '-', 0, '".$dateNow."', '".$timeNow."', ".$_SESSION['mLoginID'].", '".$dateNow."', '".$timeNow."', ".$_SESSION['mLoginID'].")";
				$mQuery->querySQL($sql);
			}  //-------  if($num == 0)
		}  //------  foreach ($namedDataArray as $result)

		unset($inputFileType, $objReader, $objPHPExcel, $objWorksheet, $highestRow, $highestColumn, $namedDataArray);
	}  //------  if($strFileName != "")

	header("location:../../index.php?f=importBranchFromExcel&importComplete=".base64_encode($strFileName));
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>