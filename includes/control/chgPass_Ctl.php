<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");


if(($_REQUEST['oldpassword'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$oldpassword = base64_encode($mFunc->chgSpecialCharInputText($_REQUEST['oldpassword']));
	$newpassword = base64_encode($mFunc->chgSpecialCharInputText($_REQUEST['newpassword']));

	$sql = "select aid from db_user where aid=".$_SESSION['mLoginID']." and apass='".$oldpassword."'";
	$num = $mQuery->checkNumRows($sql);

	if($num > 0)
	{
		$sql = "update db_user set apass='".$newpassword."', moddate='".$dateNow."', modtime='".$timeNow."', modip='".$_SERVER['REMOTE_ADDR']."', modaid=".$_SESSION['mLoginID']." where aid=".$_SESSION['mLoginID'];
		$mQuery->querySQL($sql);

		unset($_SESSION['modDatePlus60'], $_SESSION['forceChgPass']);

		$_SESSION['modDatePlus60'] = $dFunc->datePlusDay($dateNow, 60, 0, 0);
		session_write_close();

		header("location:../../index.php?f=chgPass&confirmOK=".base64_encode("OK"));
	}
	else
	{
		header("location:../../index.php?f=chgPass&errPassword=".base64_encode("Error"));
	}  //------  if($num == 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>