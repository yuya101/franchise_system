<?php
ob_start();
session_start();

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(($_REQUEST['username'] != "") and ($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();
	
	$email = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['email']));
	$username = strtoupper($mFunc->chgSpecialCharInputText($_REQUEST['username']));
	$mobile = $mFunc->chgSpecialCharInputText($_REQUEST['mobile']);
	$name = $mFunc->chgSpecialCharInputText($_REQUEST['name']);
	$lastname = $mFunc->chgSpecialCharInputText($_REQUEST['lastname']);
	$room = $mFunc->chgSpecialCharInputNumber($_REQUEST['room']);
	$building = $mFunc->chgSpecialCharInputText($_REQUEST['building']);
	$village = $mFunc->chgSpecialCharInputText($_REQUEST['village']);
	$address = $mFunc->chgSpecialCharInputText($_REQUEST['address']);
	$soi = $mFunc->chgSpecialCharInputText($_REQUEST['soi']);
	$mou = $mFunc->chgSpecialCharInputText($_REQUEST['mou']);
	$street = $mFunc->chgSpecialCharInputText($_REQUEST['street']);
	$district = $mFunc->chgSpecialCharInputText($_REQUEST['district']);
	$amphor = $mFunc->chgSpecialCharInputText($_REQUEST['amphor']);
	$province = $mFunc->chgSpecialCharInputText($_REQUEST['province']);
	$postcode = $mFunc->chgSpecialCharInputText($_REQUEST['postcode']);
	$telephone = $mFunc->chgSpecialCharInputText($_REQUEST['telephone']);

	$sql = "select uid from db_user_detail where user_name='".$username."'";
	$num = $mQuery->checkNumRows($sql);

	if($num == 0)
	{
		$sql = "select uid from db_user_detail where user_email='".$email."'";
		$num = $mQuery->checkNumRows($sql);

		if($num == 0)
		{
			$sql = "insert into db_user_detail values(NULL, '".$username."', '".$email."', '".$mobile."', '".$name."', '".$lastname."', '".$room."', '".$building."', '".$village."', '".$address."', '".$soi."', '".$mou."', '".$street."', '".$district."', '".$amphor."', '".$province."', '".$postcode."', '".$telephone."', 1, '".$dateNow."', '".$timeNow."', '".$_SERVER['REMOTE_ADDR']."', '".$dateNow."', '".$timeNow."', '".$_SERVER['REMOTE_ADDR']."')";
			$mQuery->querySQL($sql);

			header("location:../../index.php?f=addTopUpData&confirmAddCusOK=".base64_encode("OK")."&username=".base64_encode($username));
		}
		else
		{
			header("location:../../index.php?f=addTopUpData&errEmailDup=".base64_encode("Error")."&emailDup=".base64_encode($email));
		}  //------  if($num == 0)
	}
	else
	{
		header("location:../../index.php?f=addTopUpData&errUserDup=".base64_encode("Error")."&userDup=".base64_encode($username));
	}  //------  if($num == 0)
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>