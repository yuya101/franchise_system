<?php
ob_start();
session_start();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
set_time_limit(1800);  // ประมาณ 30 นาที

header("Content-Type: text/plain; charset=UTF-8");

require("../class/autoload.php");

if(isset($_SESSION['mLoginID']))
{
	$dFunc = new DateFunction();
	$mFunc = new MainFunction();
	$mQuery = new MainQuery();
	
	$dateNow = $dFunc->getDateChris();
	$timeNow = $dFunc->getTimeNow();

	$monthChk = substr($dateNow, 0, 6);

	$strFileName = "-";
	$showFileName = "-";

	if($_FILES["textFile"]["name"][0] != ""){
		$extensionArr = array("TXT", "txt");
		$showFileName = $_FILES["textFile"]["name"][0];
		$strFileName = $mFunc->uploadOneFileWithExtension($textFilePath, 'textFile', 'inquiry', $extensionArr);	
	}  //------  if($_FILES["textFile"]["name"][0] != "")

	// $strFileName = "inquiry20180407215949.txt";

	if(($strFileName != "") and ($strFileName != "-")){
		$inquiryType = (int)$mFunc->chgSpecialCharInputNumber($_REQUEST['inq_type']);

		$sql = "select id from db_inquiry_type where id=".$inquiryType;
	         	$num = $mQuery->checkNumRows($sql);

	         	if($num > 0){
			$OpenFile = $textFilePath.$strFileName;
			$numberCount = 0;
			$flagStatus = 0;
			$flagMonthChk = 0;

			foreach (file($OpenFile) as $row) {
				$arr = explode('|', $row);
         				$arr_num = count($arr);

	         			if($arr_num > 1){  //------ Because Row 1 is Topic
	         				if($inquiryType == 1){  //-----  Case Credit Card
	         					if($numberCount == 0){
	         						$sql = "select credit_id from db_inquiry_credit_card where business_date like '".$monthChk."%' limit 1";
	         						$num = $mQuery->checkNumRows($sql);

	         						if($num == 0){
	         							$flagMonthChk = 1;
	         						}  //-----  if($num == 0)
	         					}  //-----  if($numberCount == 0)

	         					$flagMonthChk = 1;

	         					if($flagMonthChk == 1){
	         						if($numberCount > 0){  //-----  ไม่ต้องการบรรทัดที่ 1
	         							//  Date Format Commission MM/DD/YYYY

		         						$site_id = $mFunc->chgSpecialCharInputText($arr[0]);
						         		$site_name = $mFunc->chgSpecialCharInputText($arr[1]);
						         		$channel_type = $mFunc->chgSpecialCharInputText($arr[2]);
						         		$business_date = $mFunc->chgSpecialCharInputText($arr[3]);
						         		$business_date = $dFunc->chgFromMMDDYYYY($business_date);
						         		$statement_date = $mFunc->chgSpecialCharInputText($arr[4]);
						         		$statement_date = $dFunc->chgFromMMDDYYYY($statement_date);
						         		$auto_matched_date = $mFunc->chgSpecialCharInputText($arr[5]);
						         		$auto_matched_date = $dFunc->chgFromMMDDYYYY($auto_matched_date);
						         		$statement_amount = $mFunc->chgSpecialCharInputText($arr[6]);
						         		$statement_amount = $mFunc->clearDataForCurrency($statement_amount);
						         		$journal_amount = $mFunc->chgSpecialCharInputText($arr[7]);
						         		$journal_amount = $mFunc->clearDataForCurrency($journal_amount);
						         		$diff_amount = $mFunc->chgSpecialCharInputText($arr[8]);
						         		$diff_amount = $mFunc->clearDataForCurrency($diff_amount);
						         		$fee_amount = $mFunc->chgSpecialCharInputText($arr[9]);
						         		$fee_amount = $mFunc->clearDataForCurrency($fee_amount);
						         		$tax_amount = $mFunc->chgSpecialCharInputText($arr[10]);
						         		$tax_amount = $mFunc->clearDataForCurrency($tax_amount);
						         		$net_amount = $mFunc->chgSpecialCharInputText($arr[11]);
						         		$net_amount = $mFunc->clearDataForCurrency($net_amount);
						         		$manual_map_site = $mFunc->chgSpecialCharInputText($arr[12]);
						         		$business_unit = $mFunc->chgSpecialCharInputText($arr[13]);
						         		$aid = $mFunc->chgSpecialCharInputText($arr[14]);

						         		$year = substr($statement_date, 0, 4);
						         		$month = substr($statement_date, -4, 2);
						         		$day = substr($statement_date, -2, 2);

						         		$sql = "insert into db_inquiry_credit_card values(NULL";
						         		$sql = $sql.", '".$site_id."'";
						         		$sql = $sql.", '".$site_name."'";
						         		$sql = $sql.", '".$channel_type."'";
						         		$sql = $sql.", '".$business_date."'";
						         		$sql = $sql.", '".$statement_date."'";
						         		$sql = $sql.", '".$auto_matched_date."'";
						         		$sql = $sql.", ".$statement_amount."";
						         		$sql = $sql.", ".$journal_amount."";
						         		$sql = $sql.", ".$diff_amount."";
						         		$sql = $sql.", ".$fee_amount."";
						         		$sql = $sql.", ".$tax_amount."";
						         		$sql = $sql.", ".$net_amount."";
						         		$sql = $sql.", '".$manual_map_site."'";
						         		$sql = $sql.", '".$business_unit."'";
						         		$sql = $sql.", '".$aid."'";
						         		$sql = $sql.", '".$year."'";
						         		$sql = $sql.", '".$month."'";
						         		$sql = $sql.", '".$day."'";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.")";
						         		$mQuery->querySQL($sql);

						         		$flagStatus = 1;
		         					}  //--------  if($numberCount > 0)
	         					}else{
							header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ข้อมูล Credit Card เดือนนี้มีการนำเข้าระบบแล้ว"));	
	         					}  //------  if($num == 0)
	         				}elseif($inquiryType == 2){  //-----  Case Docket
	         					if($numberCount == 0){
	         						$sql = "select docket_id from db_inquiry_docket where dob like '".$monthChk."%' limit 1";
	         						$num = $mQuery->checkNumRows($sql);

	         						if($num == 0){
	         							$flagMonthChk = 1;
	         						}  //-----  if($num == 0)
	         					}  //-----  if($numberCount == 0)

	         					$flagMonthChk = 1;

	         					if($flagMonthChk == 1){
		         					if($numberCount > 0){  //-----  ไม่ต้องการบรรทัดที่ 1
	         							//  Date Format Docket DD-MMM-YY เช่น 01-Sep-17

		         						$concept = $mFunc->chgSpecialCharInputText($arr[0]);
		         						$status = $mFunc->chgSpecialCharInputText($arr[1]);
		         						$store_name = $mFunc->chgSpecialCharInputText($arr[2]);
		         						$dob = $mFunc->chgSpecialCharInputText($arr[3]);
						         		$dob = $dFunc->chgFromDDMMMYYYY($dob);
		         						$count = $mFunc->chgSpecialCharInputText($arr[4]);
						         		$count = $mFunc->clearDataForCurrency($count);
		         						$order_total = $mFunc->chgSpecialCharInputText($arr[5]);
						         		$order_total = $mFunc->clearDataForCurrency($order_total);
		         						$order_mode = $mFunc->chgSpecialCharInputText($arr[6]);
		         						$branch = $mFunc->chgSpecialCharInputText($arr[7]);
		         						$bu = $mFunc->chgSpecialCharInputText($arr[8]);

						         		$year = substr($dob, 0, 4);
						         		$month = substr($dob, -4, 2);
						         		$day = substr($dob, -2, 2);

						         		$sql = "insert into db_inquiry_docket values(NULL";
						         		$sql = $sql.", '".$concept."'";
						         		$sql = $sql.", '".$status."'";
						         		$sql = $sql.", '".$store_name."'";
						         		$sql = $sql.", '".$dob."'";
						         		$sql = $sql.", ".$count."";
						         		$sql = $sql.", ".$order_total."";
						         		$sql = $sql.", '".$order_mode."'";
						         		$sql = $sql.", '".$branch."'";
						         		$sql = $sql.", '".$bu."'";
						         		$sql = $sql.", '".$year."'";
						         		$sql = $sql.", '".$month."'";
						         		$sql = $sql.", '".$day."'";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.")";
						         		$mQuery->querySQL($sql);

						         		$flagStatus = 1;
		         					}  //--------  if($numberCount > 0)
	         					}else{
							header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ข้อมูล Docket เดือนนี้มีการนำเข้าระบบแล้ว"));
	         					}  //-----  if($num == 0)
	         				}elseif($inquiryType == 3){  //-----  Case Trip Expense
	         					if($numberCount == 0){
	         						$sql = "select trip_id from db_inquiry_trip_expense where date like '".$monthChk."%' limit 1";
	         						$num = $mQuery->checkNumRows($sql);

	         						if($num == 0){
	         							$flagMonthChk = 1;
	         						}  //-----  if($num == 0)
	         					}  //-----  if($numberCount == 0)

	         					$flagMonthChk = 1;

	         					if($flagMonthChk == 1){
		         					if($numberCount > 0){  //-----  ไม่ต้องการบรรทัดที่ 1
	         							//  Date Format Trip expense DD-MM-YYYY เช่น 01-09-2017
	         							
		         						$date = $mFunc->chgSpecialCharInputText($arr[0]);
						         		$date = $dFunc->chgFromDDMMYYYY($date);
		         						$code = $mFunc->chgSpecialCharInputText($arr[1]);
		         						$branch = $mFunc->chgSpecialCharInputText($arr[2]);
		         						$stores = $mFunc->chgSpecialCharInputText($arr[3]);
		         						$comment = $mFunc->chgSpecialCharInputText($arr[4]);
		         						$reason = $mFunc->chgSpecialCharInputText($arr[5]);
		         						$f_d = $mFunc->chgSpecialCharInputText($arr[6]);
		         						$number = $mFunc->chgSpecialCharInputText($arr[7]);
		         						$cubics = $mFunc->chgSpecialCharInputText($arr[8]);
						         		$cubics = $mFunc->clearDataForCurrency($cubics);
		         						$weights = $mFunc->chgSpecialCharInputText($arr[9]);
						         		$weights = $mFunc->clearDataForCurrency($weights);
		         						$final = $mFunc->chgSpecialCharInputText($arr[10]);
						         		$final = $mFunc->clearDataForCurrency($final);

						         		$year = substr($date, 0, 4);
						         		$month = substr($date, -4, 2);
						         		$day = substr($date, -2, 2);

						         		$sql = "insert into db_inquiry_trip_expense values(NULL";
						         		$sql = $sql.", '".$date."'";
						         		$sql = $sql.", '".$code."'";
						         		$sql = $sql.", '".$branch."'";
						         		$sql = $sql.", '".$stores."'";
						         		$sql = $sql.", '".$comment."'";
						         		$sql = $sql.", '".$reason."'";
						         		$sql = $sql.", '".$f_d."'";
						         		$sql = $sql.", '".$number."'";
						         		$sql = $sql.", ".$cubics."";
						         		$sql = $sql.", ".$weights."";
						         		$sql = $sql.", ".$final."";
						         		$sql = $sql.", '".$year."'";
						         		$sql = $sql.", '".$month."'";
						         		$sql = $sql.", '".$day."'";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.", '".$dateNow."'";
						         		$sql = $sql.", '".$timeNow."'";
						         		$sql = $sql.", ".$_SESSION['mLoginID']."";
						         		$sql = $sql.")";
						         		$mQuery->querySQL($sql);

						         		$flagStatus = 1;
		         					}  //--------  if($numberCount > 0)
	         					}else{
							header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ข้อมูล Trip Expense เดือนนี้มีการนำเข้าระบบแล้ว"));
	         					}  //-----  if($num == 0)
	         				}else{
						header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ไม่พบ Inquiry Type ที่เลือก"));
	         				}  //--------  if($inquiryType == 1)
	         			}else{
					header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("เอกสารไม่มีข้อมูล"));
	         			}  //---  if($arr_num > 1)

	         			$numberCount++;
         			}  //-----  foreach (file($OpenFile) as $row)

         			if($flagStatus == 1){
         				header("location:../../index.php?f=importTripExpensesFileData&completeImport=".base64_encode($showFileName));
         			}  //------  if($flagStatus == 1)
	         	}else{
	         		header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ไม่พบหัวข้อที่เลือก"));
	         	}  //----  if($num > 0)
	}else{
		header("location:../../index.php?f=importTripExpensesFileData&errImport=".base64_encode("ไม่มีเอกสาร"));
	}  //------  if($strFileName != "")
	
	unset($mFunc, $mQuery, $dFunc);
}
else
{
	header("location:../../index.php");
}  //----  if(($_REQUEST['email'] != "") and isset($_SESSION['mLoginID']))
?>